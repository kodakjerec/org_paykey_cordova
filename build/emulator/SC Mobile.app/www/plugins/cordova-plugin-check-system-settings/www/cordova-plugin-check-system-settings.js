cordova.define("cordova-plugin-check-system-settings.checkSystemSettings", function(require, exports, module) {
var exec = require('cordova/exec');

exports.isADBModeEnabled = function (success, error) {
    exec(success, error, 'CDVCheckSystemSettings', 'isADBModeEnabled', []);
};

});
