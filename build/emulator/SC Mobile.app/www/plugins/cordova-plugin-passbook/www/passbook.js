cordova.define("cordova-plugin-passbook.passbook", function(require, exports, module) {
var exec = require('cordova/exec');

function passbook() {
}

/**
 * 
 * @param resultCallback {function} is called with result
 */
passbook.prototype.avaliable = function (resultCallback) {
    exec(resultCallback, null, "passbook", "avaliable", []);
};

/**
 * 
 * @param passCallback {function} is called with passes result 
 * @param errorCallback
 */
passbook.prototype.getPasses = function (passCallback, errorCallback) {
    exec(passCallback, errorCallback, "passbook", "getPasses", []);
};

/**
 * 
 * @param resultCallback {function} is called with result
 * @param errorCallback
 * @param creditCardNo
 */
passbook.prototype.isCardAdded = function (resultCallback, creditCardNo) {
    exec(resultCallback, null, "passbook", "isCardAdded", [{creditCardNo: creditCardNo}])
}

const reinstall = function () {
    delete window.plugins.passbook; 
    cordova.addConstructor(passbook.install);
}
/**
 * 
 * @param {function} successCallback
 * @param {function} errorCallback
 * @param {string} token
 * @param {string} creditCardNo
 * @param {string} custEngName
 * @param {string} pageCheckId
 * @param {string} FPAN_ID
 */
passbook.prototype.addPass = function (successCallback, errorCallback, token, creditCardNo, custEngName, cardTypeName, pageCheckId, FPAN_ID) {

    const custSuccessCallback = function () {
        successCallback();
        reinstall();
    }

    const custErrorCallback = function (err) {
        errorCallback(err);
        reinstall();
    }

    exec(custSuccessCallback, custErrorCallback, "passbook", "addPass", [{creditCardNo: creditCardNo, custEngName: custEngName, token: token, cardTypeName: cardTypeName, pageCheckId: pageCheckId, FPAN_ID: FPAN_ID}]);
}

passbook.prototype.getEncryptDataApi = function(successCallback) {
    exec(successCallback,null, "passbook", "getEncryptDataApi", null);
}
/**
 * @param {string} callback from JavaScript
 */
passbook.prototype.callback = function (resultString) {
    exec(null, null, "passbook", "callback", [{resultString}]);
}

// 設定語言
passbook.prototype.language = function (languageJson) {
    exec(null, null, 'passbook', 'language', [languageJson]);
};

passbook.install = function () {
    if (!window.plugins) {
      window.plugins = {};
    }
  
    window.plugins.passbook = new passbook();
    return window.plugins.passbook;
};
  
cordova.addConstructor(passbook.install);

});
