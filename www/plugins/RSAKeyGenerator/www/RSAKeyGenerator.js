cordova.define("RSAKeyGenerator.RSAKeyGenerator", function(require, exports, module) {
var exec = require('cordova/exec');

exports.genRsaKey = function (success, error) {
    exec(success, error, 'RSAKeyGenerator', 'genRsaKey', []);
};

});
