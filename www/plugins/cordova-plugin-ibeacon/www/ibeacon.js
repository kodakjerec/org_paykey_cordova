cordova.define("cordova-plugin-ibeacon.ibeacon", function(require, exports, module) {
var exec = require('cordova/exec');

function ibeacon() {
}

/**
 * 
 * @param resultCallback {function} is called with result
 */
ibeacon.prototype.avaliable = function (resultCallback) {
    exec(resultCallback, null, "ibeacon", "avaliable", []);
};

/**
 * 
 * @param {function} successCallback
 * @param {function} errorCallback
 * @param {string} token
 * @param {string} creditCardNo
 * @param {string} custEngName
 * @param {string} pageCheckId
 */
ibeacon.prototype.changeSwitch = function (successCallback, errorCallback, switchStatus, uuid) {
    exec(successCallback, errorCallback, "ibeacon", "changeSwitch", [{switchStatus: switchStatus, uuid: uuid}]);
}

ibeacon.prototype.keepReservationFlag = function (resultCallback, flag) {
    exec(resultCallback, null, "ibeacon", "keepReservationFlag", [{flag: flag}]);
}

ibeacon.prototype.hasCallPullData = function (resultCallback) {
    exec(resultCallback, null, "ibeacon", "hasCallPullData", []);
}

ibeacon.prototype.getIBeaconSignal = function (resultCallback) {
    exec(resultCallback, null, "ibeacon", "getIBeaconSignal", [])
}

ibeacon.prototype.resetIBeaconSignal = function (resultCallback) {
    exec(resultCallback, null, "ibeacon", "resetIBeaconSignal", [])
}

ibeacon.prototype.setIBeaconService = function (resultCallback) {
    exec(resultCallback, null, "ibeacon", "setIBeaconService", [])
}

ibeacon.prototype.pushLocalNotificaion = function (resultCallback, uuid, major, minor, custType) {
    exec(resultCallback, null, "ibeacon", "pushLocalNotificaion", [{uuid: uuid, major: major, minor: minor, custType: custType}])
}
// 設定語言
ibeacon.prototype.language = function (languageJson) {
    exec(null, null, 'ibeacon', 'language', [languageJson]);
};

ibeacon.install = function () {
    if (!window.plugins) {
      window.plugins = {};
    }
    
    window.plugins.ibeacon = new ibeacon();
    return window.plugins.ibeacon;
};
  
cordova.addConstructor(ibeacon.install);

});
