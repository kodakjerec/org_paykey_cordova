cordova.define("org-paykey-cordova.PayKeySDKCordova", function(require, exports, module) {
var exec = require("cordova/exec");

exports.setCallback = function(callback) {
  exec(callback, null, "PayKeySDKCordova", "setCallback");
};

exports.setupCompleted = function(success, error) {
    exec(success, error, "PayKeySDKCordova", "setupCompleted");
  };

exports.fetchContactsCompleted = function(contacts, success, error) {
    exec(success, error, "PayKeySDKCordova", "fetchContactsCompleted", [contacts]);
};

exports.fetchAuthTypeCompleted = function(authType, success, error) {
  exec(success, error, "PayKeySDKCordova", "fetchAuthTypeCompleted", [authType]);
};

exports.authenticateCompleted = function(success, error) {
  exec(success, error, "PayKeySDKCordova", "authenticateCompleted");
};

exports.executeTransactionCompleted = function(message, success, error) {
  exec(success, error, "PayKeySDKCordova", "executeTransactionCompleted", [message]);
};
});
