//
//  KeyboardViewController.h
//  Keyboard
//
//  Created by YuCheng on 2019/6/28.
//  Copyright © 2019 Carslos Chen. All rights reserved.
//

#import <UIKit/UIKit.h>

@import PayKeyUI;

@interface KeyboardViewController : PKKeyboardUIViewController

@end
