//
//  KeyboardViewController.m
//  Keyboard
//
//  Created by YuCheng on 2019/6/28.
//  Copyright © 2019 Carslos Chen. All rights reserved.
//

#import "KeyboardViewController.h"

@interface KeyboardConfigImpl : PKUIKeyboardConfig
@end

@implementation KeyboardConfigImpl

-(instancetype)init {
    if (self = [super init]) {
        self.extensionType = PKExtensionTypeKeyboard;
    }
    return self;
}

- (id<NSObject>)payKeyDelegate {
    return [NSClassFromString(@"PayKeyDelegateImpl") new];
}

@end


@interface KeyboardViewController () <PKKeyboardEventsProtocol>

@end

@implementation KeyboardViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    self.eventsDelegate = self;
}

- (Class)protocolImpl {
    return [KeyboardConfigImpl class];
}

- (void)reportEvent:(PKAnalyticsEvent*)event {
    NSLog(@"PKLOGGER: \n event name:%@ \n params: %@ \n ENDPKLOGGER",event.name,event.params);
}

@end
