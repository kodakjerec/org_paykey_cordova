/********* RSAKeyGenerator.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>
#import <Cordova/CDVPlugin.h>
#include <openssl/rsa.h>
#include <openssl/bn.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#define RSA_KEY_LENGTH 2048

@interface RSAKeyGenerator : CDVPlugin {
  // Member variables go here.
}

- (void)genRsaKey:(CDVInvokedUrlCommand*)command;
@end

@implementation RSAKeyGenerator

- (void)genRsaKey:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    
    NSError* error = nil;
    // NSDictionary* rsaKeys = [self createRSA2048Keys:&error];
    //NSDictionary* rsaKeys = [self createRSAByOpenssl:&error];
    NSString* rsaKeys = [self createRSAByOpenssl:&error];
    if (error) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:[error localizedDescription]];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:rsaKeys];
        //pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:rsaKeys];
    }
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

//- (NSDictionary *) createRSAByOpenssl:(NSError **)err {
- (NSString *) createRSAByOpenssl:(NSError **)err {
    
    RSA *rsa = RSA_new();
    int ret = 0;
    BIGNUM *bne = BN_new(); //  建立一個公開指數（Public Exponent） 用來進行RSA運算
    ret = BN_set_word(bne, RSA_F4); //  設定公開指數的值 使用RSA_F4
    ret = RSA_generate_key_ex(rsa, RSA_KEY_LENGTH, bne, NULL);  //運算產生RSA Key 成功時回傳1 失敗時回傳0
    if (ret == 0) {
        NSLog(@"Error Code: %lu", ERR_get_error());
        return nil;
    } else {
        NSLog(@"Rsa Generate");
        EVP_PKEY* evpkey = EVP_PKEY_new();  //  建立一個EVP_PKEY物件 用來儲存公鑰跟私鑰
        if (!EVP_PKEY_assign_RSA(evpkey, rsa))  //  將RSA Key assign 給EVP_PKEY
            return nil;
        //  建立用來輸出公鑰的BIO BIO是Key輸出輸入的界面 建立BIO物件的時候同時用BIO_s_mem()回傳需要分配給它的記憶體
        BIO *publickeybio = BIO_new(BIO_s_mem());
        //  將公鑰以pkcs8格式寫入BIO
        PEM_write_bio_PUBKEY(publickeybio, evpkey);
        //  建立一個buffer 分配2048bit的記憶體空間
        //  格式設定為char這樣在輸出的時候自動將原本的2進位資料轉成字元
        char *publickeybuffer = malloc(RSA_KEY_LENGTH);
        //  初始化記憶體空間
        memset((void *)publickeybuffer, 0x0, RSA_KEY_LENGTH);
        //  buffer從publickeybio裡面讀取資料
        BIO_read (publickeybio, publickeybuffer, RSA_KEY_LENGTH);
        //  從c語言的字串轉成obj c的字串格式
        NSString *publicPEM = [NSString stringWithUTF8String:publickeybuffer];
        NSLog(@"%@",publicPEM);
        
        //  同上 建立輸出私鑰的BIO
        BIO *privatekeybio = BIO_new(BIO_s_mem());
        //PEM_write_bio_PKCS8PrivateKey(BIO *bp, EVP_PKEY *x, const EVP_CIPHER *enc, char *kstr, int klen, pem_password_cb *cb, void *u);
        //  這個method有7個參數 前兩個不用多作介紹
        //  第三個參數EVP_CIPHER是指定私鑰的加密演算法 目前沒有特別指定
        //  第四個參數是用來加密私鑰的字串passphrase
        //  第五個參數passphrase的長度
        //  第六個是callback method
        //  第七個目前沒看到網路上有人使用它
        PEM_write_bio_PKCS8PrivateKey(privatekeybio, evpkey, NULL, NULL, 0, NULL, NULL);
        
        //  同上 建立私鑰的buffer
        char *privatekeybuffer = malloc(RSA_KEY_LENGTH);
        memset((void *)privatekeybuffer, 0x0, RSA_KEY_LENGTH);
        
        //  buffer從privatekeybio裡面讀取資料
        BIO_read (privatekeybio, privatekeybuffer, RSA_KEY_LENGTH);
        NSString *privatePEM = [NSString stringWithUTF8String:privatekeybuffer];
        NSLog(@"%@",privatePEM);
        
        NSData *privatePlainData = [privatePEM dataUsingEncoding:NSUTF8StringEncoding];
        NSString *privatePEM_b64 = [privatePlainData base64EncodedStringWithOptions:0];
        
        NSData *publicPlainData = [publicPEM dataUsingEncoding:NSUTF8StringEncoding];
        NSString *publicPEM_b64 = [publicPlainData base64EncodedStringWithOptions:0];
        
        return [NSString stringWithFormat:@"{\"prvKeyObj\":\"%@\",\"pubKeyObj\":\"%@\"}", privatePEM_b64, publicPEM_b64];
        //return @{ @"prvKeyObj": privatePEM, @"pubKeyObj": publicPEM };
    }
}

- (NSDictionary *) createRSA2048Keys:(NSError **)err {
    // generate public & private key
    NSDictionary* attributes = @{ (id)kSecAttrKeyType: (id)kSecAttrKeyTypeRSA,
                                  (id)kSecAttrKeySizeInBits: @2048 };
    
    CFErrorRef cfError = NULL;
    SecKeyRef privateKey = SecKeyCreateRandomKey((__bridge CFDictionaryRef)attributes, &cfError);
    if (!privateKey) {
        *err = CFBridgingRelease(cfError);  // ARC takes ownership
        return nil;
    }
    
    SecKeyRef publicKey = SecKeyCopyPublicKey(privateKey);
    
    // covert base64 string
    NSError* error = nil;
    NSString* prvivateString = [self convertBase64String:privateKey error:&error];
    if (!prvivateString) {
        *err = error;
        return nil;
    }
    NSString* publicString = [self convertBase64String:publicKey error:&error];
    if (!publicString) {
        *err = error;
        return nil;
    }
    
    // relese cf resource
    if (publicKey) CFRelease(publicKey);
    if (privateKey) CFRelease(privateKey);
    
    // base64 to pem
    NSMutableString *privatePEM = [NSMutableString string];
    [privatePEM appendString:@"-----BEGIN PRIVATE KEY-----\n"];
    [privatePEM appendString:prvivateString];
    [privatePEM appendString:@"\n-----END PRIVATE KEY-----"];
    
    NSMutableString *publicPEM = [NSMutableString string];
    [publicPEM appendString:@"-----BEGIN PUBLIC KEY-----\n"];
    [publicPEM appendString:publicString];
    [publicPEM appendString:@"\n-----END PUBLIC KEY-----"];
    
    return @{ @"prvKeyObj": privatePEM, @"pubKeyObj": publicPEM };
}

- (NSString *) convertBase64String:(SecKeyRef)key error:(NSError **)err {
    CFErrorRef error = NULL;
    NSData* keyData = (NSData*)CFBridgingRelease(SecKeyCopyExternalRepresentation(key, &error)); // ARC takes ownership
    if (!keyData) {
        *err = CFBridgingRelease(error);  // ARC takes ownership
        return nil;
    }
    return [keyData base64EncodedStringWithOptions:0];
}

@end
