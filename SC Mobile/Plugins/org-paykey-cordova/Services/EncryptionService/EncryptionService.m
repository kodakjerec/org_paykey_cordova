//
//  EncryptionService.m
//  javaTest
//
//  Created by YuCheng on 2019/5/14.
//  Copyright © 2019 Carslos Chen. All rights reserved.
//

#import "EncryptionService.h"

@implementation EncryptionService

+ (GMEllipticCurveCrypto *)cryptob{
    GMEllipticCurveCrypto *cryptob = [GMEllipticCurveCrypto generateKeyPairForCurve:GMEllipticCurveSecp256r1];
    return cryptob;
}

//Rsa 使用 privateKey 簽章
+ (NSString *)RsaSignature:(NSString *)pemString Message:(NSString *)message{

    //    NSString * publicKeyString = @"MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAJma7iWHjLCpeiHHhkwf/IGVtJvrHVW/"
    //    "F+te83hB85YiCuAOqvaG7Isy4dYbwBZKkuIq9x+FVZ+6SrsaRQuaFSkCAwEAAQ==";
    //    NSData * pub = [NSData MIH_dataByBase64DecodingString:publicKeyString];
    //    MIHRSAPublicKey * publicKey = [[MIHRSAPublicKey alloc] initWithData:pub];

    //NSData *pem = [pemString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSData * pem = [NSData MIH_dataByBase64DecodingString:pemString];
    MIHRSAPrivateKey * privateKey = [[MIHRSAPrivateKey alloc] initWithData:pem];
    NSData * messageData = [message dataUsingEncoding:NSUTF8StringEncoding];
    NSError *signingError = nil;
    NSData *signatureData = [privateKey signWithSHA256:messageData error:&signingError];

    /*驗證
     //BOOL isVerified = [publicKey verifySignatureWithSHA256:signatureData message:messageData];
     //NSLog(@"%d",isVerified);
     */

    return [EncryptionService byteToHex:signatureData];
}

//計算出AES key
+ (NSString *)getAesKey:(NSData *)serverpublicKey GMElliptic:(GMEllipticCurveCrypto *)cryptob{
    //使用client private key和server public key透過KeyAgreement來計算出shared secret(crypto.publicKey 要換 server public key)
    NSData *aliceSharedSecret = [cryptob sharedSecretForPublicKey:serverpublicKey];
    //未壓縮的clientpublicKey
    NSData * decompressClientpublicKey = [cryptob decompressPublicKey:cryptob.publicKey];
    NSString *base64String = [aliceSharedSecret base64EncodedStringWithOptions:0];
    NSString *base64StringB = [decompressClientpublicKey base64EncodedStringWithOptions:0];
    //跑java kdf
    KdfjavaConcatKeyHelper * test = [[KdfjavaConcatKeyHelper alloc]init];
    //計算出的AES key in hex
    //NSLog(@"計算出的AES key in hex:%@",[test concatKDFWithNSString:base64String withNSString:base64StringB]);
    return [test concatKDFWithNSString:base64String withNSString:base64StringB];
}

// then AesGcm 解密
+ (NSString *)aesDenCode:(NSString *)denString Aeskey:(NSString *)aeskey{

    NSData *key = [NSData iag_dataWithHexString:aeskey];
    NSData *iv = [NSData iag_dataWithHexString:@"000000000000000000000000"];
    NSData *aad = [NSData iag_dataWithHexString:[EncryptionService hexFromStr:@"SCBSIT"]];

    NSData *strData = [NSData iag_dataWithHexString:denString];
    NSMutableData *data = [NSMutableData data];
    [data appendData:strData];
    // delete
    [data replaceBytesInRange:NSMakeRange(0, data.length-16) withBytes:NULL length:0];
    NSData * authenticationTag = [data copy];
    // delete
    [data replaceBytesInRange:NSMakeRange(0, data.length) withBytes:NULL length:0];
    [data appendData:strData];
    // delete
    [data replaceBytesInRange:NSMakeRange(data.length-16, 16) withBytes:NULL length:0];
    NSData * cipheredData = [data copy];

    // when
    IAGCipheredData *result = [[IAGCipheredData alloc] initWithCipheredData:cipheredData
                                                          authenticationTag:authenticationTag];
    // when
    NSData *plainData = [IAGAesGcm plainDataByAuthenticatedDecryptingCipheredData:result
                                                  withAdditionalAuthenticatedData:aad
                                                             initializationVector:iv
                                                                              key:key
                                                                            error:nil];
    NSString * convertedStr =[[NSString alloc] initWithData:plainData encoding:NSUTF8StringEncoding];
    //NSLog(@"Converted String 解密 = %@",convertedStr);
    return convertedStr;
}

+ (NSString*)byteToHex:(NSData *)resource {
    NSMutableString* buffer = [NSMutableString stringWithCapacity:([resource length]*2)];
    const unsigned char* data = [resource bytes];
    for (int idx=0; idx<[resource length]; ++idx) {
        [buffer appendFormat:@"%02lX", (unsigned long)data[idx]];
    }
    return [buffer copy];
}

//string to hexString
+(NSString*)hexFromStr:(NSString*)str
{
    NSData* nsData = [str dataUsingEncoding:NSUTF8StringEncoding];
    const char* data = [nsData bytes];
    NSUInteger len = nsData.length;
    NSMutableString* hex = [NSMutableString string];
    for(int i = 0; i < len; ++i)[hex appendFormat:@"%02X", data[i]];
    return hex;
}
@end
