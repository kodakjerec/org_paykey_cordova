//
//  ApiConfig.h
//  ObjcApi
//
//  Created by YuCheng on 2019/5/13.
//  Copyright © 2019 SCTWBank. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ApiConfig : NSObject

+(NSDictionary *)deviceInfo;

@end

NS_ASSUME_NONNULL_END
