//
//  ApiConfig.m
//  ObjcApi
//
//  Created by YuCheng on 2019/5/13.
//  Copyright © 2019 SCTWBank. All rights reserved.
//

#import "ApiConfig.h"
#import <UIKit/UIKit.h>

@implementation ApiConfig

+(NSDictionary *)deviceInfo {
    return @{
             @"osVersion": [UIDevice currentDevice].systemVersion,
             @"os" : @"iOS",
             @"deviceModel" : [UIDevice currentDevice].model,
             @"version" : @"tw.com.SCBreeze;1.0.0",
             @"udid" : [[UIDevice currentDevice] identifierForVendor].UUIDString
             };
}

@end
