//
//  KBApiService.h
//  ObjcApi
//
//  Created by YuCheng on 2019/5/13.
//  Copyright © 2019 SCTWBank. All rights reserved.
//

#import "YTKBaseRequest.h"

typedef NS_ENUM( NSInteger, KBApiName) {
    None, // 初始為None
    RefreshJWTToken, // 重新refresh 使用者的JWT Token
    InquireSwiftCode, // 銀行代碼查詢
    CommonAccountList, // 取得常用帳號清單
    GetNotes, // 取得畫面下方的說明文字
    GetShowMenuFlag, // 取得是否顯示特定Menu的Flag
    GetTCUrl, // 取得 T&C PDF 條款 URL
    PageUserTrack, // 紀錄使用者軌跡
    GenJwtToken, // 使用平台帳號登入 JWT Token API
    GetServerPublicKey, // 取得Server Public Key API
    CheckFunctionAvailable, // 取得KB功能狀態
    ChgOutAcctNo, // 已啟用KB更換綁定轉出帳號
    ConfirmRedEnvDetail, // 1對1或群組確認紅包資訊及驗證方式
    DesendRedEnv, // 取消發紅包確認
    DesendRedEnvResendSmsOtp, // 取消發紅包重發簡訊OTP
    DesendValidateIdentity, // 取消發紅包驗證身分
    DisableKB, // 停用KB
    DoReceiveRedEnvelope, // 1對1或群組完成領紅包
    EnableKB, // 啟用KB_確認
    EnableKBResendSmsOtp, // 啟用KB_重發SMS OTP
    EnableKBValidateIdentity, // 啟用KB_驗證身分
    GetDefaultCover, // 取得紅包預設圖片
    GetForeignExchange, // 取得外幣買賣匯率資訊
    GetKBChallenge, // 取得KB登入時的Challenge
    GetKBOutAcctBalance, // 取得綁定的轉出帳號可用餘額
    GetWhatsNew, // 取得最新消息
    LoginKeyboardBanking, // KeyboardBanking登入(不可與其他API一起呼叫)
    LogoutKeyboardBanking, // KeyboardBanking登出(不可與其他API一起呼叫)
    PreEnableOrDisableKB, // 啟用/停用KB_頁面
    PreReceiveRedEnvelope, // 1對1或群組領紅包確認
    PreSendGroupRedEnv, // 群組發送紅包確認
    PreSendSingleRedEnv, // 1對1發送紅包確認
    RedEnvelopeRecInq, // 查詢發紅包紀錄(轉帳紀錄)
    ResendPwdSms, // 重新發送提領密碼簡訊(只能重新發送一次)
    SendRedEnvResendSmsOtp, // 1對1或群組發送紅包重發簡訊OTP
    SetReceiveAcctNo, // 1對1或群組領紅包收款人輸入帳號
    ValidateIdentity, // 1對1或群組發送紅包驗證身分
    ValidateReceiveCode // 1對1或群組領紅包驗證提領密碼
};
NS_ASSUME_NONNULL_BEGIN

@interface KBApiService : YTKBaseRequest

-(instancetype)initWithApi:(KBApiName)name Params:(NSDictionary *)params;

@end

NS_ASSUME_NONNULL_END
