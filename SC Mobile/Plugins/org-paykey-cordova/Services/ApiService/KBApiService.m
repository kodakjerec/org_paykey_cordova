//
//  KBApiService.m
//  ObjcApi
//
//  Created by YuCheng on 2019/5/13.
//  Copyright © 2019 SCTWBank. All rights reserved.
//

#import "KBApiService.h"
#import <YTKNetworkConfig.h>
#import "ApiConfig.h"

// 新竹渣打(中台)api
//#define API_BASE_URL @"https://10.229.202.92:9444/mobilebank"
// 文檔上的domain
//@"https://linebc.webcomm.com.tw/mobilebank"
// 台北mock api
#define API_BASE_URL @"https://thinkpower.azurewebsites.net"

@implementation KBApiService {
    NSDictionary *requestParams;
    KBApiName apiName;
}

-(instancetype)initWithApi:(KBApiName)name Params:(NSDictionary *)params {
    if (self = [super init]) {
        YTKNetworkConfig *config = [YTKNetworkConfig sharedConfig];
        config.baseUrl = API_BASE_URL;
        requestParams = params;
        apiName = name;
    }
    return self;
}
-(NSString *)requestUrl {
    switch (apiName) {
        case RefreshJWTToken:
            return @"/rest/common/common0002";
            break;
        case InquireSwiftCode:
            return @"/rest/common/common0007";
            break;
        case CommonAccountList:
            return @"/rest/common/common0008";
            break;
        case GetNotes:
            return @"/rest/common/getNotes";
            break;
        case GetShowMenuFlag:
            return @"/rest/common/getShowMenuFlag";
            break;
        case GetTCUrl:
            return @"/rest/common/getTCUrl";
            break;
        case PageUserTrack:
            return @"/rest/common/pageUserTrack";
            break;
        case GenJwtToken:
            return @"/rest/jwt/genJwtToken";
            break;
        case GetServerPublicKey:
            return @"/rest/jwt/getServerPublicKey";
            break;
        case CheckFunctionAvailable:
            return @"/rest/keyboardBanking/checkFunctionAvailable";
            break;
        case ChgOutAcctNo:
            return @"/rest/keyboardBanking/chgOutAcctNo";
            break;
        case ConfirmRedEnvDetail:
            return @"/rest/keyboardBanking/confirmRedEnvDetail";
            break;
        case DesendRedEnv:
            return @"/rest/keyboardBanking/desendRedEnv";
            break;
        case DesendRedEnvResendSmsOtp:
            return @"/rest/keyboardBanking/desendRedEnvResendSmsOtp";
            break;
        case DesendValidateIdentity:
            return @"/rest/keyboardBanking/desendValidateIdentity";
            break;
        case DisableKB:
            return @"/rest/keyboardBanking/disableKB";
            break;
        case DoReceiveRedEnvelope:
            return @"/rest/keyboardBanking/doReceiveRedEnvelope";
            break;
        case EnableKB:
            return @"/rest/keyboardBanking/enableKB";
            break;
        case EnableKBResendSmsOtp:
            return @"/rest/keyboardBanking/enableKBResendSmsOtp";
            break;
        case EnableKBValidateIdentity:
            return @"/rest/keyboardBanking/enableKBValidateIdentity";
            break;
        case GetDefaultCover:
            return @"/rest/keyboardBanking/getDefaultCover";
            break;
        case GetForeignExchange:
            return @"/rest/keyboardBanking/getForeignExchange";
            break;
        case GetKBChallenge:
            return @"/rest/keyboardBanking/getKBChallenge";
            break;
        case GetKBOutAcctBalance:
            return @"/rest/keyboardBanking/getKBTxChallenge";
            break;
        case GetWhatsNew:
            return @"/rest/keyboardBanking/getWhatsNew";
            break;
        case LoginKeyboardBanking:
            return @"/rest/keyboardBanking/loginKeyboardBanking";
            break;
        case LogoutKeyboardBanking:
            return @"/rest/keyboardBanking/logoutKeyboardBanking";
            break;
        case PreEnableOrDisableKB:
            return @"/rest/keyboardBanking/preEnableOrDisableKB";
            break;
        case PreReceiveRedEnvelope:
            return @"/rest/keyboardBanking/preReceiveRedEnvelope";
            break;
        case PreSendGroupRedEnv:
            return @"/rest/keyboardBanking/preSendGroupRedEnv";
            break;
        case PreSendSingleRedEnv:
            return @"/rest/keyboardBanking/preSendSingleRedEnv";
            break;
        case RedEnvelopeRecInq:
            return @"/rest/keyboardBanking/redEnvelopeRecInq";
            break;
        case ResendPwdSms:
            return @"/rest/keyboardBanking/resendPwdSms";
            break;
        case SendRedEnvResendSmsOtp:
            return @"/rest/keyboardBanking/sendRedEnvResendSmsOtp";
            break;
        case SetReceiveAcctNo:
            return @"/rest/keyboardBanking/setReceiveAcctNo";
            break;
        case ValidateIdentity:
            return @"/rest/keyboardBanking/validateIdentity";
            break;
        case ValidateReceiveCode:
            return @"/rest/keyboardBanking/validateReceiveCode";
            break;
        default:
            break;
    }
    return @"";
}
-(YTKRequestSerializerType)requestSerializerType {
    return YTKRequestSerializerTypeJSON;
}
-(YTKResponseSerializerType)responseSerializerType {
    return YTKResponseSerializerTypeJSON;
}
-(NSTimeInterval)requestTimeoutInterval {
    return 30.0;
}
-(NSDictionary<NSString *,NSString *> *)requestHeaderFieldValueDictionary {
    return @{@"Content-type": @"application/json",
             @"Accept" : @"application/json",
             @"jwt" : @"",
             @"deviceInfo" : [NSString stringWithFormat:@"%@",[ApiConfig deviceInfo]],
             @"locale": @"zh_TW"
             };
}
-(YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}
-(id)requestArgument {
    return requestParams;
}
@end
