//
//  LoginKBFlow.m
//  javaTest
//
//  Created by YuCheng on 2019/7/5.
//  Copyright © 2019 Carslos Chen. All rights reserved.
//

#import "LoginKBFlow.h"



@interface LoginKBFlow()

@end

@implementation LoginKBFlow

#pragma mark - public

-(instancetype)init {
    if (self = [super init]) {
        [self addObserver:self forKeyPath:@"kbApiName" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
    }
    return self;
}
-(void)executeLoginAction{
    [Request getInstance];
    self.kbApiName = GetServerPublicKey;
}

-(void)callLoginApis{
    NSDictionary *requestParams = nil;
    switch (self.kbApiName) {
        case GetServerPublicKey:
            requestParams = @{};
            break;
        case GenJwtToken:
        {
            /* need set j2objc in keyboard
             GMEllipticCurveCrypto *cryptob = [GMEllipticCurveCrypto generateKeyPairForCurve:GMEllipticCurveSecp256r1];
             NSData * decompressClientpublicKey = [cryptob decompressPublicKey:cryptob.publicKey];
             NSArray * bodyKeys = @[@"clientEccPublicKey",@"sercurityPassword",@"sercurityUserName"];
             NSArray * bodyValue = @[[Request byteToHex:decompressClientpublicKey],@"test3",@"test3"];
             */
            NSData * decompressClientpublicKey =[[Request cryptob] decompressPublicKey:[Request cryptob].publicKey];
            
            NSArray * bodyKeys = @[@"clientEccPublicKey",@"sercurityPassword",@"sercurityUserName"];
            NSArray * bodyValue = @[[Request byteToHex:decompressClientpublicKey],@"test3",@"test3"];
            
            requestParams = [Request getSignatureDic:bodyKeys BodyValue:bodyValue JwtToken:[Request getPliststring:@"jwtToken"] AesKey:[Request getAeskeyString] GMElliptic:[Request cryptob]];
            
            
        }break;
        case GetKBChallenge:
        {
            
            NSData * rsapulicKey = [[Request getRsapublicKey] dataUsingEncoding:NSUTF8StringEncoding];

            NSArray * bodyKeys = @[@"token"];
            NSArray * bodyValue = @[[Request byteToHex:rsapulicKey]];
            
            requestParams = [Request getSignatureDic:bodyKeys BodyValue:bodyValue JwtToken:[Request getPliststring:@"jwtToken"] AesKey:[Request getAeskeyString] GMElliptic:[Request cryptob]];
        }break;
        case LoginKeyboardBanking:
        {
            NSString * challengeCode = [NSString stringWithFormat:@"%@%@",[Request getPliststring:@"challengeCode"],@"SCBKBLogin"];
           
            NSString * signCode = [Request RsaSignature:[Request getRsaprivateKey] Message:challengeCode];
            
            NSArray * bodyKeys = @[@"signCode"];
            NSArray * bodyValue = @[signCode];
            
            requestParams = [Request getSignatureDic:bodyKeys BodyValue:bodyValue JwtToken:[Request getPliststring:@"jwtToken"] AesKey:[Request getAeskeyString] GMElliptic:[Request cryptob]];
        }
            break;
        default:
            break;
    }
    KBApiService *apiService = [[KBApiService alloc] initWithApi:_kbApiName Params:requestParams];
    [apiService startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        // Handle api result here
        
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:request.responseData options:NSJSONReadingMutableContainers error:nil];
        [Request setJwt:jsonDict];
        
        switch (self.kbApiName) {
            case GetServerPublicKey:
            {
                NSDictionary *bodyDict =  [jsonDict dictionaryForKey:@"body"];
                NSData * serverpublicKey = [[Request cryptob] compressPublicKey:[Request hexToBytes:[bodyDict stringForKey:@"serverPublickey"]]];
                //計算出的AES key in hex
                NSString * aesKey = [Request getAesKey:serverpublicKey GMElliptic:[Request cryptob]];
                [Request writePlist:aesKey key:@"Aeskey"];
                // Status changed notify observer call next api
                NSLog(@"Will call GenJwtToken");
                self.kbApiName = GenJwtToken;
                
            }break;
            case GenJwtToken:
            {
                NSLog(@"Will call GetKBChallenge");
                self.kbApiName = GetKBChallenge;
                
            }break;
            case GetKBChallenge:
            {
                NSDictionary * body = [jsonDict dictionaryForKey:@"body"];
                NSString * challengeCode = [body stringForKey:@"challengeCode"];
                [Request writePlist:challengeCode key:@"challengeCode"];
                NSLog(@"Will call LoginKeyboardBanking");
                self.kbApiName = LoginKeyboardBanking;
            }break;
            case LoginKeyboardBanking:
            {
                self.loginResultBlock(YES,jsonDict);
            }break;
            default:
                break;
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSLog(@"第%ld支Api失敗",(long)self->_kbApiName);
        //self.loginResultBlock(NO);
    }];
}
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if ([@"kbApiName" isEqualToString:keyPath]) {
        [self callLoginApis];
    }
}

-(void)dealloc{
    [self removeObserver:self forKeyPath:@"kbApiName"];
}
@end
