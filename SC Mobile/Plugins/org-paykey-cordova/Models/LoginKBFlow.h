//
//  LoginKBFlow.h
//  javaTest
//
//  Created by YuCheng on 2019/7/5.
//  Copyright © 2019 Carslos Chen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KBApiService.h"
#import "Request.h"

NS_ASSUME_NONNULL_BEGIN

@interface LoginKBFlow : NSObject

@property (assign, nonatomic) KBApiName kbApiName;
-(void)executeLoginAction;

@property (nonatomic, copy) void (^loginResultBlock)(BOOL isSuccess,NSDictionary *jsonDict);

@end

NS_ASSUME_NONNULL_END
