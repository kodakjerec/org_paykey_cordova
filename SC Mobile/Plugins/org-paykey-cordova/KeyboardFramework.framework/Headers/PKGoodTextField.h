//
//  PKTextField.h
//  PayKeyUI
//
//  Created by Eden Landau on 21/02/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import "PKViewWithCursor.h"
#import "KeyboardController.h"
#import "KeyModelUI.h"
#import "UIReturnKeyProxy.h"

@class PKGoodTextField;
@protocol PKKeyboardLocalizationProvider;

@protocol PKGoodTextFieldDelegate <NSObject>

@optional
- (BOOL)textFieldShouldBeginEditing:(PKGoodTextField *)textField;
- (void)textFieldDidBeginEditing:(PKGoodTextField *)textField;
- (BOOL)textFieldShouldEndEditing:(PKGoodTextField *)textField;
- (void)textFieldDidEndEditing:(PKGoodTextField *)textField;
- (BOOL)textField:(PKGoodTextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
- (BOOL)textFieldShouldClear:(PKGoodTextField *)textField;
- (BOOL)textFieldShouldReturn:(PKGoodTextField *)textField;
- (void)textDidChange:(PKGoodTextField *)textField;
- (KeyModelUI*)returnKeyModel;

@end

@interface PKGoodTextField : PKViewWithCursor <PKTextDocumentProxy, UIReturnKeyProxy>

//////// XIB
@property (nonatomic) IBInspectable UIColor *cursorColor;
@property (nonatomic) IBInspectable UIColor *textColor;
@property (nonatomic) IBInspectable UIColor *placeholderTextColor;
@property (nonatomic) IBInspectable NSString *placeholderText;
@property (nonatomic) IBInspectable UIImage *icon;
@property (nonatomic) IBInspectable UIImage *clearIcon;
@property (nonatomic) IBInspectable CGFloat letterSpacing;
@property (nonatomic) IBInspectable CGFloat textHorizontalMargin;
@property (nonatomic) IBInspectable NSString *initialValue;
@property (nonatomic) IBInspectable BOOL hidePlaceHolderWhenEditing;
@property (nonatomic) IBInspectable BOOL hideMagnifier;
@property (nonatomic) IBInspectable CGFloat iconWidth;
@property (nonatomic, getter=isSecureTextEntry) IBInspectable BOOL secureTextEntry;

////////////////////////////////////////////////////////////
@property (nonatomic) UIEdgeInsets clearButtonEdgeInest;
@property (nonatomic) CGFloat clearButtonRightPadding;
@property (nonatomic) UITextFieldViewMode clearButtonMode;
@property (nonatomic) NSTextAlignment textAlignment;
@property (nonatomic) UIFont *font;
@property (nonatomic) UIFont *placeHolderFont;

@property (readonly) BOOL inputActive;
@property (readonly) NSString *text;
@property (nonatomic, weak) id<PKGoodTextFieldDelegate>delegate;
@property (nonatomic, weak) id <PKInputTextStateObserver> inputTextStateObserver;
@property (nonatomic, weak) UIViewController *rootVC;
@property (nonatomic, weak) id<PKKeyboardLocalizationProvider> localizationProvider;

- (void)beginEditing;
- (BOOL)endEditing;

- (void)clear;

@end
