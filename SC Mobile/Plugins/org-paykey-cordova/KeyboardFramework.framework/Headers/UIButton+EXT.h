//
//  UIButton+EXT.h
//  PayKeyboard
//
//  Created by Alex Kogan on 13/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (EXT)

- (UIImage*)imageWithColor:(UIColor*)color;
- (void)setBackgroundColor:(UIColor*)color  forUIControlState:(UIControlState)state;
- (void)switchIconAndTitleWithPadding:(CGFloat)padding;
- (void)centerButtonAndImageWithSpacing:(CGFloat)spacing;



@end
