//
//  enums.h
//  PayKeyboard
//
//  Created by Alex Kogan on 09/01/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#ifndef enums_h
#define enums_h
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, EngineShiftMode) {
    EngineShiftModeNone,
    EngineShiftModeShift,
    EngineShiftModeShiftLocked
};

typedef NS_ENUM(NSInteger, PKCustomReturnKey) {
    PKCustomReturnKeyEnabled = 1000,
    PKCustomReturnKeyDisabled
};

typedef NS_ENUM(NSInteger, PKArtViewType) {
    PKArtViewTypeEmoji,
    PKArtViewTypeGif
};

typedef NS_ENUM(NSInteger, PKKeyboardId) {
    PKKeyboardIdAlphabet,
    PKKeyboardIdAlphabetShifted,
    PKKeyboardIdAlphabetShiftedLocked,
    PKKeyboardIdSymbols,
    PKKeyboardIdExtendedSymbols,
    PKKeyboardIdNumeric,
    PKKeyboardIdNumericPayKey,
    PKKeyboardIdArt,
    PKKeyboardIdNone
};

typedef NS_ENUM(NSInteger,PKLanguageType) {
    PKLanguageTypeEnglish_US = 0,
    PKLanguageTypeHebrew = 1,
    PKLanguageTypeSpanish = 2,
    PKLanguageTypeItalian = 3,
    PKLanguageTypeDutch = 4,
    PKLanguageTypeNorwegian = 5,//Currently not working
    PKLanguageTypeEnglish_UK = 6,
    PKLanguageTypeEnglish_AU = 7,
    PKLanguageTypeSpanish_CO = 8,
    PKLanguageTypeTurkish = 9,//Currently not working
    PKLanguageTypeHangul = 10,
    PKLanguageTypePolish = 11,
    PKLanguageTypeRussian = 12,
    PKLanguageTypeBahasa = 13,
    PKLanguageTypeZhuyin = 14,
    PKLanguageTypeThai = 15,
    PKLanguageTypeSlovak = 16,
    PKLanguageTypeRomanian = 17,
    PKLanguageTypeSpanish_AR = 18,
    PKLanguageTypeFrench = 19,
    PKLanguageTypeSpanish_MX = 20,
    PKLanguageTypeSpanish_CR = 21,
};

typedef NS_ENUM(NSInteger,PKDictionaryType){
    PKDictionaryTypeMain = 0,
    PKDictionaryTypeEmoji = 1,
};

typedef NS_ENUM(NSInteger,PKNextKeyboardType){
    PKNextKeyboardTypeCurrentLanguage = 0,
    PKNextKeyboardTypeNextLanguage = 1
};

typedef NS_ENUM(NSInteger,PKLanguageLayout){
    PKLanguageLayoutQwerty,
    PKLanguageLayoutQwertz,
    PKLanguageLayoutAzerty,
    PKLanguageLayoutAzertyAccented,
    PKLanguageLayoutSpanish,
    PKLanguageLayoutRussain,
    PKLanguageLayoutZhuyin,
    PKLanguageLayoutThai,
    PKLanguageLayoutSlovak,
    PKLanguageLayoutHebrew,
    PKLanguageLayoutHangul,
    PKLanguageLayoutColemak,
};

typedef NS_ENUM(NSUInteger, KeyType)  {
    KeyTypeLetterOrNumberKey,
    KeyTypeCapsLockKey,
    KeyTypeBackspaceKey,
    KeyTypeToNumericKey,
    KeyTypeToLettersKey,
    KeyTypeToMoreSymbolsKey,
    KeyTypeChangeKeyboardKey,
    KeyTypeSpaceKey,
    KeyTypePaymentModeKey,
    KeyTypeReturnKey,
    KeyTypeToArtKey,
    KeyTypeToArtFromNextLanguageKey,
    KeyTypeToEmojiKey,
    KeyTypeToGIFKey,
    KeyTypeNextLanguageKey,
    KeyTypeExtraActionKey,
    KeyTypeSpacer,
    KeyTypeEmojiCategoryKey,
    KeyTypeHideEmojiKey,
    KeyTypeNumericDecimalSeparator
};

typedef NS_ENUM(NSUInteger, PKCapslockMode)  {
    PKCapslockModeLowerCase,
    PKCapslockModeUpperCase,
    PKCapslockModeDoNothing,
};

typedef NS_ENUM(NSInteger, KeyboardMode) {
    KeyboardModeNormal,
    KeyboardModeSelectContacts,
    KeyboardModePayment,
    KeyboardModeAuthentication,
};

typedef  NS_ENUM(NSInteger,PKNextInputType) {
    PKNextInputTypeEmoji,
    PKNextInputTypeGlobe
};

typedef  NS_ENUM(NSInteger,PKKeyboardViewType) {
    PKKeyboardViewTypeNormal,
    PKKeyboardViewTypeEmoji
};

typedef NS_ENUM(NSInteger, PKWordTypedSource) {
    PKWordTypedSourceManual             = 0,
    PKWordTypedSourceAutoCorrection     = 1,
    PKWordTypedSourceSuggestion         = 2,
    PKWordTypedSourcePrediction         = 3
};

typedef NS_ENUM(NSInteger, PKPayButtonState) {
    PKPayButtonStateClosed              = 0,
    PKPayButtonStateOpened              = 1
};

typedef NS_ENUM(NSInteger, PKPayButtonLocation) {
    PKPayButtonLocationNone,
    PKPayButtonLocationBar,
    PKPayButtonLocationKey
};

typedef NS_ENUM(NSInteger, PKKeySize) {
    PKKeySizeRegular,
    PKKeySizeRussian,
    PKKeySizeZhuyin,
    PKKeySizeThai,
    PKKeySizeSlovak,
    PKKeySizeNumeric,
};

typedef struct KeyboardLayoutParams {
    PKLanguageLayout layout;
    PKKeyboardId identifier;
    UIKeyboardType type;
    CGFloat width;
    CGFloat height;
    BOOL portrait;
    BOOL isExtraKeyVisible;
    BOOL enableWideLanguageSwitch;
    BOOL showExtraActionButtonInPortrait;
} KeyboardLayoutParams;

typedef NS_ENUM(NSInteger, LanguageDirection) {
    LanguageDirectionLTR,
    LanguageDirectionRTL
};

typedef NS_ENUM(NSUInteger, TopBarViewType) {
    TopBarViewTypeNone,
    TopBarViewTypeOriginal,
    TopBarViewTypeMessage,
    TopBarViewTypeSuggestion,
    TopBarViewTypeQuickActions,
    TopBarViewTypeDownloadDictionary,
    TopBarViewTypeCustom,
};

typedef NS_ENUM(NSInteger,PKQuickActionType) {
    PKQuickActionTypeNone,
    PKQuickActionTypeSettings,
    PKQuickActionTypeEmoji,
    PKQuickActionTypeGif,
};

typedef NS_ENUM(NSInteger,PKDictionariesOTAAutoDownloadType) {
    PKDictionariesOTAAutoDownloadTypeNone,
    PKDictionariesOTAAutoDownloadTypeData,
    PKDictionariesOTAAutoDownloadTypeWIFI
};

typedef NS_ENUM(NSInteger,PKReachabilityType) {
    PKReachabilityTypeNone,
    PKReachabilityTypeData,
    PKReachabilityTypeWifi
};

#endif /* enums_h */
