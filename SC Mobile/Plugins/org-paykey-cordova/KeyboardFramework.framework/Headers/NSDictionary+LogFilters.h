//
//  NSDictionary+LogFilters.h
//  KeyboardFramework
//
//  Created by Eran Israel on 26/06/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (LogFilters)

- (NSDictionary *)filters;
- (NSDictionary *)defaultFilters;
- (NSDictionary *)tagFilters;
- (NSDictionary *)logLevelFilters;

@end

