//
//  PKSettingsNavigationController.h
//  PayKeyboard
//
//  Created by Eden Landau on 05/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const PKSwitchCellIdentifier;
extern NSString * const PKDisclosureCellIdentifier;

@protocol PKKeyboardEventsProtocol;

typedef NS_ENUM(NSInteger, PKSettingsTableViewControllerType) {
    kMainTableViewController,
    kLanguagesTableViewController
};

@interface PKSettingsNavigationController : UINavigationController
@property (nonatomic, strong) UIBarButtonItem *rightItem;
@property (nonatomic, assign) BOOL shouldShowItemForFirstScreen;
@property (nonatomic, assign) BOOL shouldShowItemForAllScreens;
@property (nonatomic, weak) id<PKKeyboardEventsProtocol>eventsDelegate;
@end
