//
//  PKUserDefaultsSettingsInteractor.h
//  PayKeyboard
//
//  Created by Jonathan Beyrak-Lev on 17/07/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKSettingsInteractor.h"

@interface PKUserDefaultsSettingsInteractor : NSObject <PKSettingsInteractor>

- (void)suggestionSwitchChanged:(BOOL)isOn;
- (void)autocorrectSwitchChanged:(BOOL)isOn;
- (void)keyboardSoundSwitchChanged:(BOOL)isOn;
- (void)dotShortcutSwitchChanged:(BOOL)isOn;
- (void)emojiRowSwitchChanged:(BOOL)isOn;
- (void)quickActionBarSwitchChanged:(BOOL)isOn;
- (void)emojiSuggestionSwitchChanged:(BOOL)isOn;
- (void)enableEmojiButtonRowSwitchChanged:(BOOL)isOn;

@end
