//
//  PKDelayAggregatorMessageQueue.h
//  PayKeyboard
//
//  Created by Alex Kogan on 16/07/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "PKMessagesQueue.h"
@class PKMessageDispatcher;

@interface PKDelayAggregatorMessageQueue : PKMessagesQueue

-(instancetype)initWithMessageDispatcher:(PKMessageDispatcher*)dispatcher timeDelay:(double)delta;

@end
