//
//  NumericKeyboardModel.h
//  PayKeyboard
//
//  Created by Alex Kogan on 11/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KeyboardModel.h"

@interface NumericKeyboardModel : KeyboardModel


@property(nonatomic, assign) BOOL showSeperationLines;

@end
