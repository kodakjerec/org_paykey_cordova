//
//  PKLayoutFactory.h
//  KeyboardFramework
//
//  Created by Jonathan Beyrak-Lev on 28/09/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "PKLayout.h"
#import "PKEnums.h"

@interface PKLayoutFactory : NSObject

//Layout layers
@property (nonatomic) PKLanguageLayout layout;
@property (nonatomic) NSString *os;
@property (nonatomic) NSString *language;
@property (nonatomic) UIKeyboardType type;
@property (nonatomic) CGFloat width;
@property (nonatomic) PKKeyboardId identifier;
@property (nonatomic) BOOL isExtraKeyVisible;
@property (nonatomic) BOOL enableWideLanguageSwitch;
@property (nonatomic) BOOL showExtraActionButtonInPortrait;

+ (instancetype)factoryWithLanguage:(NSString *)language layoutParams:(KeyboardLayoutParams)params;

- (PKLayout *)numericLayout;
- (PKLayout *)fullLayout;
- (PKLayout *)characterLayout;
- (PKLayout *)utilityLayout;

- (PKLayout *)landscapeUtils;
- (PKLayout *)layoutFor:(NSString *)keys center:(BOOL)center;
- (PKLayout *)offsetUtilityLayout;
- (CGFloat)dimension:(NSString *)dimension;
- (BOOL)isPortrait;

@end
