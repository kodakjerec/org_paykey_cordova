//
//  UsageLogger.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 28/05/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UsageLogger : NSObject

+ (void)logMemory:(NSString*)calledFrom;

@end
