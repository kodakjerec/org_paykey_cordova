//
//  SuggestionsBarViewModel.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 20/03/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKEnums.h"

@class SuggestionEngineResult;
@class SuggestionViewModel;

@interface SuggestionsBarViewModel : NSObject

- (instancetype)initSuggestionWithResult:(SuggestionEngineResult*)results word:(NSString*)word autoCorrectEnabled:(BOOL)autoCorrectEnabled suggestionEnabled:(BOOL)suggestionEnabled emojiSuggestionEnabled:(BOOL)emojiSuggestionEnabled;

- (instancetype)initPredictionsWithResult:(SuggestionEngineResult*)results emojiSuggestionEnabled:(BOOL)emojiSuggestionEnabled;

@property (nonatomic, strong, readonly) NSArray *wordSuggestions;
@property (nonatomic, strong, readonly) NSArray *emojiSuggestions;
@property (nonatomic, readonly) BOOL showEmoji;
@property (nonatomic, readonly) PKWordTypedSource wordSource;
@property (nonatomic, readonly) LanguageDirection languageDirection;


// protected
- (NSArray *)innerSetupSuggesion:(BOOL)autoCorrectEnabled emojiSuggestionEnabled:(BOOL)emojiSuggestionEnabled suggestionEnabled:(BOOL)suggestionEnabled results:(SuggestionEngineResult *)results word:(NSString *)word;
- (SuggestionViewModel*)suggestionViewModelWithWord:(NSString*)word isAutoCorrection:(BOOL)isAutoCorrection showHideButton:(BOOL)showHideButton shouldLearnWord:(BOOL)shouldLearnWord;

@end
