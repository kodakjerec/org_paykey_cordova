//
//  NSBundle+DisplayName.h
//  PayKeyboard
//
//  Created by Marat  on 1/4/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (DisplayName)
-(NSString*)keyboardDisplayName;

@end
