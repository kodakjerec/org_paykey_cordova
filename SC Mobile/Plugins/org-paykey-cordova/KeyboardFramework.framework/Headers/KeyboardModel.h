//
//  KeyboardModel.h
//  PayKeyboard
//
//  Created by Alex Kogan on 11/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKEnums.h"

@class PKLanguage;

@interface KeyboardModel : NSObject

@property (nonatomic, readonly) LanguageDirection languageDirection;
@property (nonatomic, strong) PKLanguage *language;
@property (nonatomic, strong) NSArray *keys;
@property (nonatomic, strong) NSArray *rowParams;

- (void)layoutKeys:(KeyboardLayoutParams)params;

@end
