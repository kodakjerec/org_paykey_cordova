//
//  SuggestionContainer.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 18/03/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuggestionViewModel.h"
#import "PKEnums.h"

@class SuggestionsBarConfig;

@protocol SuggestionContainerDelegate <NSObject>

- (void)onSuggestionSelected:(SuggestionViewModel*)suggestionModel index:(NSInteger)index;
- (void)cancelAutoCorrection;

@end

@interface SuggestionContainer : UIView

- (instancetype)initWithConfig:(SuggestionsBarConfig *)config;
- (void)setButtonsData:(NSArray*)buttonsData languageDirection:(LanguageDirection)languageDirection;
- (void)clear;

@property (nonatomic, weak) id<SuggestionContainerDelegate> delegate;
@property (nonatomic, strong) SuggestionsBarConfig *config;

//protected
- (NSInteger)defaultSuggestionsCount;

@end
