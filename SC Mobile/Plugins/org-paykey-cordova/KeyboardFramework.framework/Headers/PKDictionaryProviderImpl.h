//
//  PKDictionaryProviderImpl.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 18/03/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKDictionaryProvider.h"

@interface PKDictionaryProviderImpl : NSObject <PKDictionaryProvider>

- (instancetype)initWithBundle:(NSBundle*)bundle;

@end

