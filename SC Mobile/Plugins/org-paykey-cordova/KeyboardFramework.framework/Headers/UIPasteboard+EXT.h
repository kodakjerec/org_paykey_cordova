//
//  UIPasteboard+EXT.h
//  PayKeyboard
//
//  Created by Alex Kogan on 13/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIPasteboard (EXT)

+(BOOL)isOpenAccessGranted;

@end
