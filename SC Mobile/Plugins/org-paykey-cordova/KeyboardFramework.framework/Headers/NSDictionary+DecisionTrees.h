//
//  NSDictionary+DecisionTrees.h
//  PayKeyboard
//
//  Created by Jonathan Beyrak-Lev on 13/09/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSArray+Fallback.h"

@interface NSDictionary(DecisionTrees)

- (id)choose:(NSArray *)keys;
- (CGFloat)chooseFloat:(NSArray *)keys;

@end

