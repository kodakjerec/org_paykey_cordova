//
//  KeyboardController+TopBar.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 23/01/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

#import "KeyboardController.h"
#import "QuickActionsBar.h"
#import "PKEnums.h"

NS_ASSUME_NONNULL_BEGIN

@interface KeyboardController (TopBar)

- (void)updateTopBarWithType:(TopBarViewType)type;
- (void)resetTopBarView;
- (void)updateTopBar;
- (TopBarViewType)suggestionBarType;

@end

NS_ASSUME_NONNULL_END
