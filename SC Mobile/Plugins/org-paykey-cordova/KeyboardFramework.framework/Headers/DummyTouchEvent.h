//
//  DummyTouchEvent.h
//  PayKeyboard
//
//  Created by Alex Kogan on 06/07/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "PKTouchEvent.h"

@interface PKDummyTouchEvent : PKTouchEvent

@property (nonatomic, strong) NSString* title;
-(instancetype)initWithTitle:(NSString*)string;

@end
