//
//  NSTextAttachment+Resizing.h
//  PayKeyboard
//
//  Created by Marat  on 1/3/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSTextAttachment (Resizing)
-(void)setImageHeight:(CGFloat)height;
@end
