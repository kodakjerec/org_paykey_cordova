//
//  PKSizeClass.h
//  PayKeyboard
//
//  Created by Alex Kogan on 21/05/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PKSizeClass : NSObject

+(PKSizeClass*)small;
+(PKSizeClass*)medium;
+(PKSizeClass*)normal;
+(PKSizeClass*)mediumLarge;
+(PKSizeClass*)large;
+(PKSizeClass*)full;
+(PKSizeClass*)custom:(CGFloat)ratio;

-(CGRect)resizeRect:(CGRect)rect;


@end
