//
//  PKSwitchCell.h
//  PayKeyboard
//
//  Created by Eden Landau on 06/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKLocalizableTableViewCell.h"

@interface PKSwitchCell : PKLocalizableTableViewCell
@property (nonatomic, copy) void(^onSwitchBlock)(BOOL switchIsOn);
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (nonatomic,assign) BOOL enableSwitch;
@property (nonatomic,strong) NSString* additionalDescription;
- (void)setSwitchValue:(BOOL)initialValue animated:(BOOL)animated;
@end
