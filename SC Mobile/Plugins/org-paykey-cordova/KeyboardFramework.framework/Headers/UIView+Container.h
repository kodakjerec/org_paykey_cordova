//
//  UIView+container.h
//  PayKeyUI
//
//  Created by Eran Israel on 30/09/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (Container)
- (void)fillContinerWithView:(UIView*)view;
- (void)fillVerticallyInView:(UIView*)view;
@end

NS_ASSUME_NONNULL_END
