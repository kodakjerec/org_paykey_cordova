//
//  PKEngineLoggerTouchEvent.h
//  PayKeyboard
//
//  Created by Alex Kogan on 06/07/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "PKTouchEvent.h"

@interface PKEngineLoggerTouchEvent : PKTouchEvent


+(PKTouchEvent*)deleteEvent:(NSString*)title originEvent:(PKTouchEvent*)event;
+(PKTouchEvent*)insertEvent:(NSString*)title originEvent:(PKTouchEvent*)event insertText:(NSString*)insertText;


@end
