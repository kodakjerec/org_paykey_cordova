//
//  UIColorExtension.h
//  KeyboardRefactor
//
//  Created by GRMINI-NEW-01 on 02/02/17.
//  Copyright © 2017 Alex Kogan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreText/CoreText.h>
#import <UIKit/UIKit.h>


@interface UIColor (HEX)

+(UIColor *)rgba :(NSString *)rgba;
+(NSString *)hexStringFromColor:(UIColor *)color;


@end
