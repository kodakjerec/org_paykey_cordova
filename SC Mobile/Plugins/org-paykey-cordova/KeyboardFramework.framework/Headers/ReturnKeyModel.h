//
//  RetrunKeyModel.h
//  PayKeyboard
//
//  Created by Alex Kogan on 09/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KeyModel.h"
@class PKLanguage;
@class KeyModelUI;

@interface ReturnKeyModel : KeyModel

@property (nonatomic, strong, setter=setCustomKeyModelUI:) KeyModelUI* customKeyModelUI;
@property (nonatomic,assign)UIReturnKeyType retunrKeyType;

-(instancetype)initWith:(PKLanguage*)language returnType:(UIReturnKeyType)type;
- (void)maybeEnabled:(BOOL)enabled;

@end
