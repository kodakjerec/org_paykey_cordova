//
//  NSObject+BundleFinding.h
//  Pods
//
//  Created by Jonathan Beyrak-Lev on 25/04/2017.
//
//

#import <Foundation/Foundation.h>

@interface NSObject(BundleFinding)

+ (NSBundle *)containingBundle;

@end
