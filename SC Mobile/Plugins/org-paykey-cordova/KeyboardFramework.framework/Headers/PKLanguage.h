//
//  PKLanguage.h
//  PayKeyboard
//
//  Created by alon muroch on 13/09/2016.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKEnums.h"
#import "PKLocalization.h"

@interface PKLanguage : NSObject<PKLocalization,NSCoding>

@property (nonatomic) PKLanguageType language;
@property (nonatomic) PKLanguageLayout layout;
@property (nonatomic) NSArray *availableLayouts;
@property (nonatomic, readonly) LanguageDirection languageDirection;

- (instancetype)initWithLanguage:(PKLanguageType)lang andLayout:(PKLanguageLayout)layout;
- (instancetype)initWithLanguage:(PKLanguageType)lang andAvailableLayouts:(NSArray*)availableLayouts;

- (instancetype)initWithLayout:(PKLanguageLayout)layout;
-(NSString*)ISO;
-(NSString*)nativeKeyboardISO;
-(NSString*)BCP47;
-(NSString*)localizationBCP47;
-(NSString*)description;
-(NSString*)dictionaryLocale;
-(NSString*)layoutName;
-(NSString*)localizedLayoutName;
-(BOOL)supportsCapitalLetters;
-(BOOL)supportsShiftedLockedLayout;
-(BOOL)isCaseSensetive;
-(BOOL)isSuggestionMandatory;
-(BOOL)isSuggestionSupported;
+ (NSString*)localizedLayoutNameForLayout:(PKLanguageLayout)layout;
+ (NSArray*)availableLayoutsValueForLayouts:(NSArray*)layouts;
+ (PKLanguageType)deviceLocaleLanguageCodeToPKLanguageType:(NSString*)deviceLocale;
+ (NSArray*)languageTypes;
+ (PKLanguageType)languageTypeValue:(NSString*)str;
+ (NSLocaleLanguageDirection)languageDirectionFromBCP47:(NSString*)BCP47;
- (NSString *)localizedKeyForLangDescritption;
- (NSInteger)maxSuggesionResults;

@end
