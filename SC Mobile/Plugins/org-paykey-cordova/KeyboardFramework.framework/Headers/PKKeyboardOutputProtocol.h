//
//  PKKeyboardOutputProtocol.h
//  PayKeyboard
//
//  Created by Alex Kogan on 05/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PKKeyboardOutputProtocol <NSObject>

- (void)didSelectExtraButton;
- (void)didPressExtraActionKey;
- (BOOL)extraButtonIsToggledOn;
- (void)assignContainerView:(UIView *)container;

@end
