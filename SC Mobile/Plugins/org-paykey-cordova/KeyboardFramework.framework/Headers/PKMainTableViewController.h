//
//  PKMainTableViewController.h
//  PayKeyboard
//
//  Created by Eden Landau on 05/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKSettingsTableView.h"


@protocol PKSettingsInteractor;
@interface PKMainTableViewController : UIViewController<PKSettingsTableViewDelegate>
@property id<PKSettingsInteractor> interactor;
@end
