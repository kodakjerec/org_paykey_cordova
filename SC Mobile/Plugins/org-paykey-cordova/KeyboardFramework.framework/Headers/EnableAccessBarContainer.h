//
//  EnableAccessBarView.h
//  PayKeyboard
//
//  Created by Alex Kogan on 15/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TopBarView.h"
#import "EnableAccessModel.h"
@class PKKeyboardConfig;
@class PKLanguage;

@protocol EnableAccessBarDelegate <TopBarViewDelegate>

- (void) enableAccessBarViewClicked;
                                                                              
@end

@interface EnableAccessBarContainer : UIView

@property (nonatomic) EnableAccessModel *enableAccessModel;
@property (nonatomic, weak) id<EnableAccessBarDelegate> delegate;
@property (nonatomic, weak) IBOutlet UILabel *lbl;

+(EnableAccessBarContainer*)loadFromNibWithModel:(EnableAccessModel*)model;
-(void)shake;

@end
