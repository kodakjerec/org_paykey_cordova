//
//  ZhuyinInputPlanner.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 03/12/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

#import "DefaultInputPlanner.h"
#import "InputCommandFactory.h"
#import "InputLogic.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZhuyinInputPlanner : DefaultInputPlanner <PKLocalization>

- (void)enterComposition;//only for tests
@end

NS_ASSUME_NONNULL_END
