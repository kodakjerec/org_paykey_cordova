//
//  PKEngineLogger.h
//  PayKeyboard
//
//  Created by Alex Kogan on 24/04/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
@class KeyboardId;
@class PKTouchEvent;
@class SuggestionEngineResult;


@interface PKEngineLogger : NSObject

+ (PKEngineLogger*)sharedInstance;

-(void)addEvent:(PKTouchEvent*)event;
-(void)report;
-(void)reset;
-(NSString*)readLogFile;
-(NSString*)readLogFileHeader;
-(void)addSuggestionEngineEvent:(NSString*)word andSuggestionResult:(SuggestionEngineResult*)result forEvent:(PKTouchEvent*)event;
-(void)addEventWithTitle:(NSString*)title;

@property(nonatomic,assign) BOOL enable;
@property(nonatomic,strong) KeyboardId* keyboardId;
@property(nonatomic,strong) NSString* layoutDump;
@property(nonatomic,strong) NSString* dictionaryInfo;
@property(nonatomic,strong) NSString* engineVersion;
@property(nonatomic, weak) id <UITextDocumentProxy> textDocumentProxy;

@end




