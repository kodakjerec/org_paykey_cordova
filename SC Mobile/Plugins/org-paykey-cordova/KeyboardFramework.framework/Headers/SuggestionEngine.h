//
//  SuggestionEngine.h
//  PayKeyboard
//
//  Created by Alex Kogan on 14/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuggestionEngineResult.h"
#import "SuggestionEngineSetupData.h"
#import "PKEnums.h"
@class PKLanguage;
@class PKTouchEvent;
@protocol PKDictionaryProvider;

@protocol CheckerProtocl <NSObject>

@optional
- (void)keyboardDidLoad;
- (void)didLoadMainDictionary;

@end

@interface SuggestionEngine : NSObject

@property (nonatomic, weak) id<CheckerProtocl> delegate;
@property (nonatomic,assign) PKKeyboardId keyboardId;

- (instancetype)initWithDictionaryProviders:(NSArray*)dictionaryProviders;
- (void)close;
- (void)autoLearnWord:(NSString*)word sentence:(NSString*)sentence wasAutoCapitalized:(BOOL)wasAutoCapitalized;
- (void)autoUnlearnWord:(NSString*)word sentence:(NSString*)sentence;
    
- (void)loadForKeyboard:(SuggestionEngineSetupData*)keyboardData language:(PKLanguage*)language;
- (void)learnWord:(NSString*)word;

- (NSString*)correct:(NSString*)wordToCorrect res:(SuggestionEngineResult*)res;
- (SuggestionEngineResult*)predict:(NSString*)sentence;
- (SuggestionEngineResult*)suggestions:(NSString*)word
                              sentence:(NSString*)sentence
                           touchEvents:(NSArray<PKTouchEvent*>*)touchEvents;

- (void)setLexiconEntries:(NSArray *)lexiconEntries;

- (NSString*)dumpDictionary:(int)index;

- (void)clearUserHistoryDictionary;

@end
