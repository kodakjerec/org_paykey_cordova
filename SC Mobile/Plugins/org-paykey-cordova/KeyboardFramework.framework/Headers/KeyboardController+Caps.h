//
//  KeyboardController+Caps.h
//  PayKeyboard
//
//  Created by Alex Kogan on 11/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KeyboardController.h"

@interface KeyboardController (Caps)

-(void)updateToCorrectKbCapsLock;
@end
