//
//  PKTouchEvent+MakeWithKey.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 02/01/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

#import "PKTouchEvent.h"

NS_ASSUME_NONNULL_BEGIN

@interface PKTouchEvent (MakeWithKey)

+ (instancetype)eventWithKeyCode:(int32_t)key;
+ (PKTouchEvent *)eventWithKeyText:(NSString*)text;

@end

NS_ASSUME_NONNULL_END
