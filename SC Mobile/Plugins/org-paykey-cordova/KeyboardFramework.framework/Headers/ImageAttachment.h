//
//  ImageAttachment.h
//  PayKeyboard
//
//  Created by Marat  on 1/3/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageAttachment : NSTextAttachment
@property(nonatomic,assign)CGFloat fontDescender;
@end
