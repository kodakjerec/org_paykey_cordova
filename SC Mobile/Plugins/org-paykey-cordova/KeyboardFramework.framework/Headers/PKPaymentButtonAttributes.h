//
//  PKPaymentButtonAttribParser.h
//  PayKeyboard
//
//  Created by Alex Kogan on 29/05/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PKSizeClass;

@interface PKPaymentButtonAttributes : NSObject

-(instancetype _Nullable)initWithChain:(NSArray *_Nonnull)chain;
-(instancetype _Nullable)initWithChain:(NSArray *_Nonnull)chain andAttributesKey:(NSString*)key;


@property (nonatomic, strong) UIImage* _Nonnull paymentButtonImage;
@property (nonatomic, strong) UIImage* _Nonnull paymentButtonSelectedImage;
@property (nonatomic, strong) UIColor* _Nonnull paymentButtonBackgroundColor;
@property (nonatomic, strong) UIColor* _Nonnull paymentButtonSelectedBackgroundColor;
@property (nonatomic, assign) CGFloat paymentButtonLeadingIndent;
@property (nonatomic, strong) PKSizeClass* _Nonnull paymentIconSize;

@end
