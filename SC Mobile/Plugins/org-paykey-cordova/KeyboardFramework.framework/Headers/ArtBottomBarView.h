//
//  ArtBottomBar.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 28/08/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeyboardContainerProtocol.h"
@class ArtKeyboardModel;
@class KeyView;
@class KeyModel;
@class PKLanguage;

@protocol ArtBottomBarViewDelegate <NSObject>

- (void)setKeyView:(KeyView*)keyView forKeyModel:(KeyModel*)keyModel;

@end

@interface ArtBottomBarView : UIStackView

- (void)loadKeys:(NSArray*)loadKeys;
- (KeyModel *)getKeyModelWithPoint:(CGPoint)point;
- (void)insertMiddleView:(UIView*)middleView;
- (void)artItemWasSelected;

@property (nonatomic, strong) PKLanguage* nextLanguage;
@property (nonatomic,weak) id<ArtBottomBarViewDelegate> delegate;
@property (nonatomic,weak) id<KeyboardContainerProtocol> kbContainerDelegate;


@end
