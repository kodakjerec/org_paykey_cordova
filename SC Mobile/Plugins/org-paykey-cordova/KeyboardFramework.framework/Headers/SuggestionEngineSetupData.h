//
//  CheckerData.h
//  PayKeyboard
//
//  Created by Mac on 30/04/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
@class KeyModel;

@interface SuggestionEngineKey : NSObject

@property(nonatomic, assign, readonly) NSInteger code;
@property(nonatomic, strong, readonly) NSString *displayText;
@property(nonatomic, assign, readonly) int x;
@property(nonatomic, assign, readonly) int y;
@property(nonatomic, assign, readonly) int width;
@property(nonatomic, assign, readonly) int height;
@property(nonatomic, assign, readonly) float sweetSpotX;
@property(nonatomic, assign, readonly) float sweetSpotY;
@property(nonatomic, assign, readonly) float sweetSpotRadii;
@property(nonatomic, strong, readonly) NSArray *auxKeyCodes;

- (int)squaredDistanceToEdge:(int)x y:(int)y;
- (CGRect)hitBox;
- (id)initWithX:(int)x y:(int)y width:(int)width height:(int)height keyCode:(int)kCode displayText:(NSString *)displayText auxArray:(NSArray*)aux;

@end

@interface SuggestionEngineSetupData : NSObject

+(SuggestionEngineSetupData*) createWithKeyboardWidth:(CGFloat)keyboardWidthPoint
                                  keyboardHeightPoint:(CGFloat)keyboardHeightPoint
                                            modelKeys:(NSArray*)modelKeys;

@property(nonatomic, strong) NSArray<SuggestionEngineKey*>* keys;
@property(nonatomic, assign) int keyboardWidth;
@property(nonatomic, assign) int keyboardHeight;
@property(nonatomic, assign) int gridWidth;
@property(nonatomic, assign) int gridHeight;
@property(nonatomic, assign) int cellWidth;
@property(nonatomic, assign) int cellHeight;
@property(nonatomic, assign) int mostCommonKeyWidth;
@property(nonatomic, assign) int mostCommonKeyHeight;
@property(nonatomic, assign) int keyCount;

-(NSString*)dump;

@end



