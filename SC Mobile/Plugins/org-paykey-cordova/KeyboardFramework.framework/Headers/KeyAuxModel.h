//
//  KeyAuxModel.h
//  PayKeyboard
//
//  Created by Alex Kogan on 09/01/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#ifndef KeyAuxModel_h
#define KeyAuxModel_h

@interface KeyAuxModel : NSObject
    
@property (nonatomic, strong) NSArray * lettersOnLeft;
@property (nonatomic, strong) NSArray * lettersOnRight;

- (id)initWithlettersOnLeft:(NSArray *)lettersOnLeft lettersOnRight:(NSArray *)lettersOnRight;


@end
#endif /* KeyAuxModel_h */
