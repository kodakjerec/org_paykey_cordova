//
//  PKAnalyticsEvent.h
//  PayKeyboard
//
//  Created by Marat  on 8/30/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PKAnalyticsEvent : NSObject
- (instancetype)initWithName:(NSString*)name andParams:(NSDictionary*)params;
@property(nonatomic,readonly) NSString *name;
@property(nonatomic,readonly) NSDictionary *params;
@end
