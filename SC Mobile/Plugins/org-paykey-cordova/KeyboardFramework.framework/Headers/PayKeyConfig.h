//
//  NSDictionary+PKConfig.h
//  PayKeyboard
//
//  Created by Alex Kogan on 04/05/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKEnums.h"

#define PK_DEFAULT_ENABLE_AUTO_CORRETION_KEY @"PKDefaultAutoCorrectionValue"
#define PK_DEFAULT_ENABLE_KEY_SOUND_KEY @"PKDefaultKeySoundValue"
#define PK_DEFAULT_ENABLE_SUGGESTIONS_KEY @"PKDefaultSuggestionsValue"
#define PK_DEFAULT_ENABLE_GIF_KEY @"PKDefaultGifEnableValue"
#define PK_DEFAULT_ENABLE_EMOJI_BUTTON @"PKDefaultEnableEmojiButtonKeyValue"
#define PK_DEFAULT_ENABLE_EMOJI_KEY @"PKDefaultEmojiEnableValue"
#define PK_DEFAULT_ENABLE_EMOJI_SUGGESTION_KEY @"PKDefaultEmojiSuggestionEnableValue"
#define PK_DEFAULT_ENABLE_DEFAULT_AUTO_CAPITALIZATION_KEY @"PKDefaultAutoCapitalizationValue"
#define PK_DEFAULT_ENABLE_DEFAULT_CAPS_LOCK_KEY @"PKDefaultCapsLockValue"
#define PK_DEFAULT_ENABLE_QUICK_ACTION_BAR @"PKDefaultQuickActionBarValue"


#define PK_DEFAULT_ENABLE_PERIOD_DOUBLE_TAP_SACE_KEY @"PKDefaultPeriodOnDoubeTapSpaceValue"
#define PK_SHOW_SETTINGS_AT_SELECTION_MENU @"PKShowSettingsAtSelectionMenuValue"
#define PK_AVAILABLE_SETTINGS_KEY @"PKAvailableSettings"

#define PK_AVAILABLE_LANGUAGES_NAME_KEY @"PKLanguages"
#define PK_LANGUAGE_VALUE_ON @"ON"
#define PK_LANGUAGE_VALUE_OFF @"OFF"
#define PK_LANGUAGE_VALUE_AUTO @"AUTO_DETECT"
#define PK_LANGUAGE_VALUE_DISABLE @"NEVER"
#define PK_LANGUAGE_AVAILABILITY_KEY @"Avilablity"
#define PK_LANGUAGE_LAYOUTS_KEY @"Layouts"

#define PK_PAY_BUTTON_LOCATION_NONE @"NONE"
#define PK_PAY_BUTTON_LOCATION_BAR @"BAR"
#define PK_PAY_BUTTON_LOCATION_KEY @"KEY"

#define PK_SETTINGS_URI_KEY @"PKSettingsUri"
#define PK_SETTINGS_APP_GROUP_NAME_KEY @"PKSettingsAppGroupName"
#define PK_PAY_BUTTON_LOCATION @"PKPayButtonLocation"
#define PK_LONG_NEXT_INPUT_BUTTON_VISIBLE @"PKWideNextInputButton"
#define PK_SPACE_CHANGE_LANGUAGE_BY_SWIPE_ENABLED @"PKSpaceLanguageChangeEnabled"
#define PK_NEXT_INPUT_LONG_PRESS_ENABLED @"PKNextInputLongPressEnabled"
#define PK_NEXT_INPUT_SHOW_TEXT_ENABLED @"PKNextInputShowTextEnabled"
#define PK_NEXT_INPUT_AFTER_LAST_LANGUAGE_ENABLED_ENABLED @"PKNextInputAfterLastLanguageEnabled"

#define PK_DICTIONARIES_OTA_PARAMS @"PKDictionariesOTA"
#define PK_DICTIONARIES_OTA_INTERVAL_TO_UPDATE @"PKDictionariesOTAIntervalToUpdate"
#define PK_DICTIONARIES_OTA_AUTO_UPDATE_TYPE @"PKDictionariesOTAAutoDownloadType"
#define PK_DICTIONARIES_OTA_AUTO_UPDATE_TYPE_NONE @"NONE"
#define PK_DICTIONARIES_OTA_AUTO_UPDATE_TYPE_DATA @"DATA"
#define PK_DICTIONARIES_OTA_AUTO_UPDATE_TYPE_WIFI @"WIFI"


@class PKLanguage;
@class UIColor;

@interface PayKeyConfig : NSObject

+(NSDictionary*)pkConfigDictionary:(Class)cls;

+(id)objectForKey:(NSString*)key fromBundleChain:(NSArray*)chain;

+(BOOL)boolValue:(NSString*)key fromBundleChain:(NSArray*)chain;

+(UIColor*)colorValue:(NSString*)key fromBundleChain:(NSArray*)chain;

+(NSDictionary*)availableLanguageTypesFromBundleChain:(NSArray*)chain;

+(NSArray<PKLanguage*>*)firstLaunchEnabledLanguagesFromBundleChain:(NSArray*)chain;

+(NSArray*)availableLanguagesFromBundleChain:(NSArray*)chain;

+(NSString*)settingsUri;

+(NSString*)appGroupName;

+(BOOL)hasAppGroupName;

+(NSArray*)configClassChain;

+(NSBundle*)containerAppBundle;

+(PKPayButtonLocation)paykeyButtonLocationFromBundleChain:(NSArray*)chain;

+(BOOL)longNextInputButtonFromBundleChain:(NSArray*)chain;

+(BOOL)spaceLanguageChangeBySwipeEnabledFromBundleChain:(NSArray*)chain;

+(BOOL)nextInputLongPressEnabledFromBundleChain:(NSArray*)chain;

+(BOOL)nextInputShowTextEnabledFromBundleChain:(NSArray*)chain;

+(BOOL)nextInputAfterLastLanguageEnabledFromBundleChain:(NSArray*)chain;

+ (NSNumber*)dictionariesOTAIntervalToUpdateFromBundleChain:(NSArray*)chain;

+ (PKDictionariesOTAAutoDownloadType)dictionariesOTAAutoUpdateTypeFromBundleChain:(NSArray*)chain;

@end
