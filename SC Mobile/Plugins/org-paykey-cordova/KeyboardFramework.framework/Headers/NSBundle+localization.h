//
//  NSBundle+localization.h
//  PayKeyboard
//
//  Created by Marat  on 1/2/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSBundle (localization)
+(instancetype)loadLocalizedBundle:(NSString*)lang withClass:(Class)type NS_SWIFT_NAME(init(localization:withClass:));
+(instancetype)loadLocalizedBundle:(NSString*)lang withBundle:(NSBundle*) bundle;
@end
