//
//  Constants.h
//  PayKeyboard
//
//  Created by Alex Kogan on 09/01/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#import "PKDefines.h"
#import "PKEnums.h"

extern int  const SPACE_KEY_CODE;
extern int  const EMOJI_KEY_CODE;

extern int  const SHIFT_KEY_CODE ;
extern int  const BACKSPACE_KEY_CODE ;
extern int  const NEXT_INPUT_KEY_CODE ;
extern int  const RETURN_KEY_CODE ;
extern int  const HIDE_KB_KEY_CODE ;
extern int  const TO_LETTERS_KEY_CODE ;
extern int  const TO_SYMBOLS_KEY_CODE ;
extern int  const TO_EXTRA_SYMBOLS_KEY_CODE ;
extern int  const RETURN_FROM_EMOJI_KEY_CODE ;
extern int  const TO_EMOJI_KEY_CODE;
extern int  const TO_GIF_KEY_CODE;
extern int  const TO_ART_KEY_CODE;
extern int  const SPACER_KEY_CODE ;
extern int  const PAY_KEY_CODE ;
extern int  const MOVE_LEFT_CODE ;
extern int  const MOVE_RIGHT_CODE ;
extern int  const EXTRA_ACTION_KB_KEY_CODE;

extern int const LANDSCAPE_EXTENDED_L1;
extern int const LANDSCAPE_EXTENDED_L2;
extern int const LANDSCAPE_EXTENDED_L3;
extern int const LANDSCAPE_EXTENDED_R1;

extern NSString * const keyboardLocalizationTableName;

extern NSString const * PKLogTagAllocations;
extern NSString const * PKLogTagMessageQueue;
extern NSString const * PKLogTagEngineLogger;
extern NSString const * PKLogTagLayout;
extern NSString const * PKLogTagInput;
extern NSString const * PKLogTagSuggestions;
extern NSString const * PKLogTagSuggestLogic;
extern NSString const * PKLogTagNLP;


#define IS_IOS8_2_AND_UP ([[UIDevice currentDevice].systemVersion floatValue] >= 8.2)

#endif /* Constants_h */
