//
//  EmojiSuggestionProvider.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 18/03/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EmojiSuggestionProvider : NSObject

+ (instancetype)sharedManager;
- (NSArray*)emojiSuggestionForWord:(NSString*)word;
- (NSArray*)emojiPredictionForContext:(NSString*)context;

@property (nonatomic, strong, readonly) NSMutableSet* emojisInVersion;

@end
