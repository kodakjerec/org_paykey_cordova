//
//  NSCharacterSet+EXT.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 31/07/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSCharacterSet (EXT)

+ (NSCharacterSet*)separatorsCharacterSet;
+ (NSCharacterSet*)isBegningOfSentenceCharacterSet;
+ (NSCharacterSet*)emojiCharacterSet;

@end
