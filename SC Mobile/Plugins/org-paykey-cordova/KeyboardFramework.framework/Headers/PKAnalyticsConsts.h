//
//  PKAnalyticsConsts.h
//  PayKeyboard
//
//  Created by Marat  on 8/30/17.
//  Copyright © 2017 Apple. All rights reserved.
//


#define PK_ANALYTICS_LOCALE_KEY                      @"locale"
#define PK_ANALYTICS_ACTIVE_KEY                      @"active"
#define PK_ANALYTICS_LAYOUT_KEY                      @"change_layout"
#define PK_ANALYTICS_SUGGESTION_KEY                  @"prediction"
#define PK_ANALYTICS_AUTO_CORRECTION_KEY             @"auto_correct"
#define PK_ANALYTICS_KEYBOARD_SOUND_KEY              @"keyboard_sounds"
#define PK_ANALYTICS_DOT_SHORTCUT_KEY                @"dot_shortcut"
#define PK_ANALYTICS_EMOJI_KEY                       @"show_emoji"
#define PK_ANALYTICS_SHOW_EMOJI_BUTTON_KEY           @"show_emoji_enabled"
#define PK_ANALYTICS_QUICK_ACTION_KEY                @"show_quick_action_bar"
#define PK_ANALYTICS_EMOJI_SUGGESTION_KEY            @"emoji_suggestion"
#define PK_ANALYTICS_KEYBOARD_EVENT                  @"keyboard"
#define PK_ANALYTICS_SETTINGS_EVENT                  @"settings"
#define PK_ANALYTICS_SWITCHED_FROM_PAYKEY_KEY        @"switched_from_paykey"
#define PK_ANALYTICS_SHOW_KEYBOARD                   @"open_keyboard_view"
#define PK_ANALYTICS_EXTRA_BUTTON_CLICKED            @"banking_button_clicked"
#define PK_ANALYTICS_KEY_PRESSED                     @"key_pressed"
#define PK_ANALYTICS_KEY_NAME                        @"key_name"
#define PK_ANALYTICS_TOUCH_POSITION                  @"touch_position"
#define PK_ANALYTICS_SUGGESION_PRESSED               @"manual_correction"
#define PK_ANALYTICS_SUGGESION_CONTENT               @"suggestion_content"
#define PK_ANALYTICS_AUTO_CORRECTION                 @"auto_correction"
#define PK_ANALYTICS_REVERT_AUTO_CORRECTION          @"revert_auto_correction"
#define PK_ANALYTICS_SUGGESTION_INDEX                @"index"

