//
//  UIScreen.h
//  PayKeyboard
//
//  Created by Alex Kogan on 11/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScreen (EXT)

+(BOOL)inPortrait;
+(BOOL)inLandscape;

@end
