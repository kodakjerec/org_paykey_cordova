//
//  NSString+PKLayout.h
//  PayKeyboard
//
//  Created by Jonathan Beyrak-Lev on 04/09/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKKeyShape.h"

@interface NSString(PKLayout)

- (NSArray *)unwound;
- (PKKeyShape *)shapeWithNormWidth:(CGFloat)norm bigWidth:(CGFloat)big;

@end
