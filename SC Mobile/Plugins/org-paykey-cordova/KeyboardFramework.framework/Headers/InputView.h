//
//  InputView.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 11/02/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

#import "KeyboardViewBase.h"
#import "PKInputTextState.h"

@class SuggestionsBar;
@class ChooseLangView;
@class PKLanguage;
@class SuggestionEngineResult;
@class KeyboardId;
@class SuggestionsBarViewModel;
@class PKPaymentButtonAttributes;
@class TopBarView;
@class KeyModel;

@interface InputView : KeyboardViewBase <PKInputTextStateObserver>

- (void)invalidateKey:(KeyModel*)model;
- (void)setKeyboard:(Keyboard*)keyboard;
- (void)updateTopBarHeightConstraints;
- (KeyView*)createKeyViewForKey:(KeyModel*)keyModel;

@property (nonatomic, strong, readonly) KeyboardContainerView* container;
@property (nonatomic, weak) id<PKCurrentTextDocumentProxyProvider> currentTextDocumentProxyProvider;
@property (nonatomic, assign) BOOL isExtraKeyEnabled;
@property (nonatomic, assign) BOOL isExtraKeySelected;
@property (nonatomic ,assign) BOOL isReturnKeyEnabled;
@property (nonatomic, strong) Keyboard* keyboard;
@property (nonatomic, strong) TopBarView* topBarView;
@property (nonatomic, assign) PKPayButtonLocation paykeyButtonLocation;

@end
