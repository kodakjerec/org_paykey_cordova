//
//  Suggest.h
//  PayKeyboard
//
//  Created by Alex Kogan on 06/07/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "SuggestionEngineResult.h"

@class PKTouchEvent;
@class PKInputTextState;
@class SuggestionEngine;
@class WordComposer;


struct SuggestLogicConfig {
    BOOL suggestionsEnabled;
    BOOL emojiSuggestionsEnabled;
    BOOL autoCorrectionEnabled;
    BOOL keyboardSupportSuggestions;
    BOOL keyboardSupportAutoCorrection;
    BOOL revertingLastAutoCorrectedWord;
    BOOL isContinousBackspce;
};

typedef struct SuggestLogicConfig SuggestLogicConfig;

@protocol SuggestDelegate <NSObject>

-(void)newSuggestionCallForWord:(NSString*)word;
-(void)newSuggestionResult:(SuggestionEngineResult*)result forWord:(NSString*)word autoCorrectionEnabled:(BOOL)autoCorrectionEnabled;
-(void)clearSuggestionBar;
-(void)newPredictionCall;
-(void)newPredictionsResult:(SuggestionEngineResult *)result;

@end


@interface SuggestLogic : NSObject

-(instancetype)initWithSuggestionEngine:(SuggestionEngine*)suggestionEngine inputTextState:(PKInputTextState*)inputTextState;

@property (nonatomic, weak) id<SuggestDelegate> delegate;

-(void)maybeUpdateSuggestionBarWithWordComposer:(WordComposer*)wordComposer
                                         config:(SuggestLogicConfig)config;

-(void)maybeUpdatePrediction:(SuggestLogicConfig)config;

@end
