//
//  KoreanInputPlanner.h
//  PayKeyboard
//
//  Created by Jonathan Beyrak-Lev on 27/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InputLogic.h"
#import "InputCommandFactory.h"
#import "DefaultInputPlanner.h"

@interface KoreanInputPlanner : DefaultInputPlanner 


- (void)startComposing;
- (void)stopComposing;

@end
