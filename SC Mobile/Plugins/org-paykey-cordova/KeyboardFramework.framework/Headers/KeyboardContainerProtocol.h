//
//  KeyboardContainerProtocol.h
//  PayKeyboard
//
//  Created by ishay weinstock on 19/09/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#ifndef KeyboardContainerProtocol_h
#define KeyboardContainerProtocol_h


@protocol KeyboardContainerProtocol <NSObject>

- (void)kbContainerTouchesBegan:(NSSet<UITouch *> *)touches;
- (void)kbContainerTouchesMoved:(NSSet<UITouch *> *)touches;
- (void)kbContainerTouchesEnded:(NSSet<UITouch *> *)touches;
- (void)kbContainerTouchesCancelled:(NSSet<UITouch *> *)touches;

@end

#endif /* KeyboardContainerProtocol_h */
