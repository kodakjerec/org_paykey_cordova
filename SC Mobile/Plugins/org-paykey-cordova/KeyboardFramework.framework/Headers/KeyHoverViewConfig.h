//
//  KeyHoverViewConfig.h
//  test
//
//  Created by ishay weinstock on 02/10/2017.
//  Copyright © 2017 ishay weinstock. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface KeyHoverViewConfig : NSObject

@property (nonatomic, readonly) BOOL canShowSeparator;
@property (nonatomic, readonly) BOOL ignoreSides;
@property (nonatomic, readonly) BOOL ignoreDirection;
@property (nonatomic, readonly) CGFloat letterPadding;

- (instancetype)initWithCanShowSeparator:(BOOL)canShowSeparator
                             ignoreSides:(BOOL)ignoreSides
                         ignoreDirection:(BOOL)ignoreDirection
                           letterPadding:(CGFloat)letterPadding;

@end
