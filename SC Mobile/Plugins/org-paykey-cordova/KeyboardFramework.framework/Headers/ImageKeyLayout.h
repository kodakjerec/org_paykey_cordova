//
//  ImageKeyLayout.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 23/10/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ImageKeyLayout : NSObject

+ (ImageKeyLayout*)layoutWithDictionary:(NSDictionary*)dictionary;

@property (nonatomic,readonly) CGSize imageSize;

@end
