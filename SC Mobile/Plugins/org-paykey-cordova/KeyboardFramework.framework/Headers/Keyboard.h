//
//  Keyboard.h
//  PayKeyboard
//
//  Created by Alex Kogan on 11/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKKeyboardLayoutBuilderProtocol.h"
#import "KeyboardId.h"
#import "PaymentKeyModel.h"
#import "KeyboardModel.h"
#import "KeyboardViewModel.h"

@class PKLanguage;
@class KeyModel;
@class PaymentKeyModel;
@class CapsKeyModel;
@class SuggestionEngineSetupData;
@class ReturnKeyModel;

@interface Keyboard : NSObject

@property(nonatomic,readonly) SuggestionEngineSetupData* suggestionEngineSetupData;
@property(nonatomic,readonly) KeyboardId* keyboardId;
@property(nonatomic,readonly) PKLanguage* language;
@property(nonatomic,readonly) PKNextInputType nextInputType;
@property(nonatomic,readonly) LanguageDirection LanguageDirection;
@property(nonatomic,readonly) BOOL shouldDisplayTopBar;
@property(nonatomic,readonly) BOOL shouldPerformSuggestionRequest;
@property(nonatomic,readonly) BOOL shouldPerfromAutoCorrection;
@property(nonatomic,readonly) UIKeyboardAppearance keyboardAppearance;
@property (nonatomic) BOOL showExtraActionButtonInPortrait;

- (instancetype)initWith:(KeyboardId*)keyboardId
            keyboardType:(UIKeyboardType)keyboardType
      keyboardAppearance:(UIKeyboardAppearance)keyboardAppearance
              returnType:(UIReturnKeyType)returnType
       keyboardViewModel:(KeyboardViewModel*)keyboardViewModel
keyboardLayoutBuilderProtocol:(id<PKKeyboardLayoutBuilderProtocol>)keyboardLayoutBuilderProtocol
    paykeyButtonLocation:(PKPayButtonLocation)paykeyButtonLocation
showExtraActionButtonInPortrait:(BOOL)showExtraActionButtonInPortrait;

- (NSArray<KeyModel*>*)keys;
- (void)layoutKeys:(KeyboardLayoutParams)params;
- (PaymentKeyModel*)getPaymentKeyModel;
- (ReturnKeyModel*)getReturnKeyModel;
- (PaymentKeyModel*)getSpaceModel; 

- (KeyModel*)enableKey:(KeyType)type enabled:(BOOL)enabled;
- (KeyModel*)hideDecimalSeperator;
- (KeyModel*)replaceDecimalSeperatorWithPlus;
- (KeyModel*)showDecimalSeperator;
- (KeyModel*)showDecimalSeperator:(NSNumberFormatter*)formatter;
- (KeyModel*)getKeyModelByKeyType:(KeyType)type;
- (BOOL)isAlphabeticKeyboard;

@end
