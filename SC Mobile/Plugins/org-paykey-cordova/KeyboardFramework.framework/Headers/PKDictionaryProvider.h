//
//  PKDictionaryProvider.h
//  PayKeyboard
//
//  Created by ishay weinstock on 17/03/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

#ifndef PKDictionaryProvider_h
#define PKDictionaryProvider_h

@class PKLanguage;
#import "PKEnums.h"

typedef void (^DictionaryProviderCompletionBlock)(NSString* path);

@protocol PKDictionaryProvider <NSObject>

- (BOOL)dictionaryForLanguage:(PKLanguage*)language type:(PKDictionaryType)type completion:(DictionaryProviderCompletionBlock)completion;

@end

#endif /* PKDictionaryProvider_h */
