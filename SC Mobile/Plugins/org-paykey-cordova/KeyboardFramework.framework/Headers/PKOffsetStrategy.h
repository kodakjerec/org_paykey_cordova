//
//  PKOffsetStrategy.h
//  PayKeyboard
//
//  Created by Jonathan Beyrak-Lev on 18/09/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "PKLayout.h"

@interface PKOffsetStrategy : NSObject

@property (readonly, nonatomic) CGFloat dx;
@property (readonly, nonatomic) CGFloat dy;

+ (instancetype)offsetWithVector:(CGVector)v;
+ (instancetype)offsetY:(CGFloat)y;
+ (instancetype)offsetX:(CGFloat)x;

@end

@interface PKVectorOffsetStrategy : PKOffsetStrategy


@end

@interface PKRelativeOffsetStrategy : PKOffsetStrategy

@property (nonatomic) PKLayout *reference;

+ (instancetype)rightward;
+ (instancetype)downward;
- (instancetype)withReference:(PKLayout *)reference;

@end
