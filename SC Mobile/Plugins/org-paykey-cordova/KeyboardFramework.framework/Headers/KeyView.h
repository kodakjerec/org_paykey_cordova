//
//  KeyView.h
//  KeyboardRefactor
//
//  Created by Sumit on 23/01/17.
//  Copyright © 2017 Alex Kogan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "PKUtility.h"

@class KeyHoverBoxView;
@class KeyModel;

@interface KeyView : UIControl {
}
@property (nonatomic, strong) UILabel *lblKey;
@property (nonatomic, strong) UIImageView *iconIV;
/**
 * the visable key itself
 */
@property (nonatomic, strong) UIView *key;
/**
 * container is effected by the insets
 */
@property (nonatomic, strong) UIView *container;
@property (nonatomic, strong) UIView *shadow;
@property (nonatomic, strong) KeyModel *model;
@property (nonatomic) CGFloat cornerRadius;
@property (nonatomic) CGFloat shadowHeight;
@property (nonatomic ,readonly) BOOL isDefault;


- (void)refresh;
- (void)setAllCornerRadius:(CGFloat)rad;
- (void)commonInit;
- (void)layoutLabel;
- (void)prepareForReuse;

@end
