//
//  PKTheme.h
//  PayKeyboard
//
//  Created by Alex Kogan on 02/04/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKEnums.h"
@class PKLanguage;
@class NumericKeyModel;
@class KeyModel;
@class KeyboardId;
@class KeyLabelLayout;
@class ImageKeyLayout;

@interface PKTheme : NSObject

@property (nonatomic, strong) KeyboardId* keyboardId;
@property (nonatomic, strong) UIColor* numericKeyBackground;
@property (nonatomic, strong) UIColor* numericFunctionalKeyBackground;

@property (nonatomic, strong) UIFont* numericKeyTitleFont;
@property (nonatomic, strong) UIFont* numericKeySubtitleFont;

@property (nonatomic, strong) UIFont* numericTextKeyLowercaseFont;
@property (nonatomic, strong) UIFont* numericTextKeyUppercaseFont;

@property (nonatomic, strong) UIFont* functionalKeyLabelFont;

@property (nonatomic, strong) KeyLabelLayout *keyLabelLayout;
@property (nonatomic, strong) KeyLabelLayout *toSymbolKeyLabelLayout;
@property (nonatomic, strong) KeyLabelLayout *toLettersKeyLabelLayout;
@property (nonatomic, strong) KeyLabelLayout *returnKeyLabelLayout;
@property (nonatomic, strong) KeyLabelLayout *returnSmallKeyLabelLayout;
@property (nonatomic, strong) KeyLabelLayout *spaceKeyLabelLayout;
@property (nonatomic, strong) KeyLabelLayout *comKeyLabelLayout;
@property (nonatomic, strong) KeyLabelLayout *comHoverKeyLabelLayout;
@property (nonatomic, strong) KeyLabelLayout *nonCharacterKeyLabelLayout;

@property (nonatomic, strong) ImageKeyLayout *backspaceKeyImageLayout;

@property (nonatomic, strong) UIColor* functionalKeyColor;
@property (nonatomic, strong) UIColor* functionalKeyPressedColor;

@property (nonatomic, strong) UIColor* textKeyColor;
@property (nonatomic, strong) UIColor* textKeyPressedColor;

@property (nonatomic, assign) CGFloat keyLabelLowercaseHorizontalCenterCoeficient;
@property (nonatomic, assign) CGFloat keyLabelUppercaseHorizontalCenterCoeficient;

@property (nonatomic, strong) PKLanguage* language;
@property (nonatomic, assign) UIKeyboardAppearance keyboardAppearance;

-(UIFont*)keyLabelLowercaseFont:(PKKeyboardId)keyboardId;
-(UIFont*)keyLabelUppercaseFont:(PKKeyboardId)keyboardId;


-(void)numericKey:(NumericKeyModel*)key;
-(void)numericTextKey:(KeyModel*)key;
-(void)numericBackspace:(KeyModel*)key;
-(void)numericExtraActionKey:(KeyModel*)key;
@end
