//
//  PKOnboardingManager.h
//  PayKeyOnboarding
//
//  Created by alon muroch on 07/06/2016.
//  Copyright © 2016 alon muroch. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PKOnboardingDelegate <NSObject>

- (void)openURL:(NSURL*)url;
@optional
- (void)keyboardNotInstalled;
- (void)keyboardWasInstalled;
- (void)keyboardWasActivated;
@end

@interface PKOnboardingManager : NSObject

- (id)initWithKeyboardBundle:(NSString*)kbBunlde appGroup:(NSString*)appGrounp;
- (void)goToKeyboardSettings;
- (BOOL)paykeyInstalled;

@property (nonatomic, weak) id<PKOnboardingDelegate> delegate;

@end
