//
//  NSString+textDirection.h
//  PayKeyboard
//
//  Created by Marat  on 1/2/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface NSString (TextDirection)

-(NSTextAlignment)textAlignment;
@end
