//
//  OpenSettingsView.h
//  PayKeyboard
//
//  Created by Marat  on 1/1/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "PKLocalization.h"
@class OpenSettingsModel;
@class PKKeyboardConfig;
@protocol KeyboardViewLanguageProtocol;

@protocol OpenSettingsViewDelegate <NSObject>
@end

@interface OpenSettingsView : UIView

+(OpenSettingsView *)loadFromNibWithSettingsModel:(OpenSettingsModel*)openSettingsModel;
+(OpenSettingsView *)loadFromNibWithSettingsModel:(OpenSettingsModel*)openSettingsModel nibName:(NSString*)nibName;
+(OpenSettingsView *)loadFromNibWithSettingsModel:(OpenSettingsModel*)openSettingsModel nibName:(NSString*)nibName bundleClass:(Class)type;

@property (nonatomic, weak) id <OpenSettingsViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UIView *toplineView;
@property (weak, nonatomic) IBOutlet UILabel *instructionsLabel;
@property (nonatomic,strong)OpenSettingsModel *openSettingsModel;

-(void)addWithConstraintsToView:(UIView*)view;
-(void)setupDoneButton;

@property (nonatomic,assign)IBInspectable CGFloat lineSpacing;
@property (nonatomic,assign)IBInspectable CGFloat bodyFontSize;



@end
