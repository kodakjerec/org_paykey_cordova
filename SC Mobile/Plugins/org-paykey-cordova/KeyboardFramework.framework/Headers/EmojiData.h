//
//  EmojiData.h
//  EmojiUI
//
//  Created by Sumit on 15/02/17.
//  Copyright © 2017 Alex Kogan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EmojiData : NSObject

@property (nonatomic, strong) NSString *mainEmoji;
@property (nonatomic, strong) NSArray *diversity;
@property (nonatomic) int count;

- (instancetype)init:(EmojiData *)data;
- (instancetype)initWithEmoji:(NSString *)emoji;
- (instancetype)initWithEmojiAndDiversity:(NSString *)emoji systemVersion:(double)systemVersion andDiversity:(NSArray *)diversity ;
- (instancetype)initWithEmojiAndCounter:(NSString *)emoji andCounter:(int)counter;
- (instancetype)initWithEmojiAndDiversity:(NSString *)emoji withDiversity:(NSArray *)diversity andCount:(int)count;
- (NSString *)description;

@end
