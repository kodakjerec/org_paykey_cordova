//
//  NSObject+VariableArity.h
//  KeyboardFramework
//
//  Created by Jonathan Beyrak-Lev on 07/01/2018.
//

#import <Foundation/Foundation.h>

@interface NSObject (VariableArity)

+ (NSArray *)arrayFrom:(id)x;
- (NSArray *)plus:(id)addend;
- (NSArray *)asArray;

@end
