//
//  NextInputInteractor.h
//  PayKeyboard
//
//  Created by ishay weinstock on 18/09/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "KeyInteractor.h"

@class NextInputInteractor;

@protocol NextInputInteractorDelegate <NSObject>

- (void)nextInputInteractorOnLongPress:(NextInputInteractor*)interactor;
- (void)nextInputInteractorOnLongPressHover:(NextInputInteractor*)interactor;
- (void)nextInputInteractorOnLongPressTouchEnded:(NextInputInteractor*)interactor;

@end

@interface NextInputInteractor : KeyInteractor

@property (nonatomic,weak) id<NextInputInteractorDelegate> delegate;

@end
