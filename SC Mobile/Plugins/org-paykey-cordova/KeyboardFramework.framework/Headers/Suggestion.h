//
//  Suggestion.h
//  PayKeyboard
//
//  Created by Mac on 30/04/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger,SuggestionType){
    SuggestionTypeWord = 0,
    SuggestionTypeEmoji = 1
};

@interface Suggestion : NSObject

@property(nonatomic, strong) NSString *word;
@property(nonatomic, assign) NSInteger score;
@property(nonatomic, assign) float normScore;
@property(nonatomic, assign) NSInteger edits;
@property(nonatomic, assign) SuggestionType type;

@end
