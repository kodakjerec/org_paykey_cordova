//
//  KeyboardViewModel.h
//  PayKeyboard
//
//  Created by Alex Kogan on 10/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PKEnums.h"

@class Keyboard;
@class KeyModelUI;


@protocol KeyboardViewModelDelegate <NSObject>

- (void) topBarViewTypeHasChanged:(TopBarViewType)topBarViewType;
- (void) setupHeightConstraint;
- (Keyboard*)getKeyoard;

@end

@interface KeyboardViewModel : NSObject

@property(nonatomic, weak)   id<KeyboardViewModelDelegate>delegate;
@property(nonatomic, assign) TopBarViewType topBarViewType;

@property(nonatomic, assign) BOOL showKeyboard;
@property(nonatomic, assign, getter=isTopBarShown) BOOL showTopBar;

@property(nonatomic, assign) BOOL isPortrait;
@property(nonatomic, assign) CGFloat keyboardWidth;
@property(nonatomic, assign) CGFloat keyboardHeight;
@property(nonatomic, assign) CGFloat paymentFlowHeight;
@property(nonatomic, assign) CGFloat topPadding;
@property(nonatomic, assign) CGFloat bottomPortraitPadding;

@property(nonatomic, readonly) CGFloat keyboardPlusBarHeight;
@property(nonatomic, readonly) CGFloat totalHeight;
@property(nonatomic, readonly) CGFloat topBarHeight;

-(BOOL)isUsingExtraHeight;
-(UIColor*)backgroundColor:(PKKeyboardId)keyboardId;
-(BOOL)willChangeKeyboardSize:(CGSize)size;
-(BOOL)changeKeyboardSize:(CGSize)size;
-(CGFloat)selectionMenuWidth;

-(void)updateFromKeyboard:(Keyboard*)keyboard;
@end
