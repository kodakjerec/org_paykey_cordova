//
//  KeyboardSelectionMenuProtocol.h
//  PayKeyboard
//
//  Created by Alex Kogan on 14/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#ifndef KeyboardSelectionMenuProtocol_h
#define KeyboardSelectionMenuProtocol_h

@class KeyboardSelectionMenuView;
@class KBMenuSelectionItem;

@protocol KeyboardSelectionMenuProtocol <NSObject>

-(void)didSelectItemAtIndex:(KeyboardSelectionMenuView*)keyboardMenu item:(KBMenuSelectionItem*)item;

@end

#endif /* KeyboardSelectionMenuProtocol_h */
