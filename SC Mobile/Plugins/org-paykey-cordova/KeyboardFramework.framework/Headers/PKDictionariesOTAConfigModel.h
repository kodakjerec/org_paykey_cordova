//
//  DictionariesOTAConfigData.h
//  KeyboardExampleExtension
//
//  Created by ishay weinstock on 04/04/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class PKDictionaryOTAModel;

@interface PKDictionariesOTAConfigModel : NSObject <NSCoding>

+ (instancetype)modelFromDictionary:(NSDictionary*)dict;

- (void)selectDictionary:(PKDictionaryOTAModel*)model;
- (NSDictionary*)dictionaryValue;

@property (readonly,strong) NSMutableArray *dictionariesModel;
@property (readonly,strong) NSNumber *version;

@end

NS_ASSUME_NONNULL_END
