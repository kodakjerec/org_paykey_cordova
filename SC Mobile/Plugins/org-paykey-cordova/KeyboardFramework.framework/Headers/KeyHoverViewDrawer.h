//
//  HoverDrawer.h
//  test
//
//  Created by ishay weinstock on 02/10/2017.
//  Copyright © 2017 ishay weinstock. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface KeyHoverViewDrawer : NSObject

+ (CGPathRef)hoverPathWithFrame:(CGRect)frame
                       keyFrame:(CGRect)keyFrame
                       leftMost:(BOOL)leftMost
                      rightMost:(BOOL)rightMost
              drawBottomCorners:(BOOL)drawBottomCorners
               numberOfKeysLeft:(NSInteger)numberOfKeysLeft
              numberOfKeysRight:(NSInteger)numberOfKeysRight
                  conrnerRadius:(CGFloat)conrnerRadius
                curveHeightCoff:(CGFloat)curveHeightCoff;

@end
