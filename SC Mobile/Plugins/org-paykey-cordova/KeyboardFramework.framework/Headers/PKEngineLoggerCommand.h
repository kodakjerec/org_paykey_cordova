//
//  PKEngineLoggerCommand.h
//  PayKeyboard
//
//  Created by Alex Kogan on 06/07/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "InputCommand.h"
#import "PKTouchEvent.h"

@interface PKEngineLoggerCommand : InputCommand

-(instancetype)initWithTitle:(NSString*)title;
-(instancetype)initWithEvent:(PKTouchEvent*)event;

@end
