//
//  KBMenuSelectionItem.h
//  PayKeyboard
//
//  Created by Alex Kogan on 14/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PKLocalization.h"
typedef NS_ENUM(NSInteger, KBMenuSelectionItemType) {
    KBMenuSelectionItemTypeSettings,
    KBMenuSelectionItemTypeEmoji,
    KBMenuSelectionItemTypeSwitchKeyboard,
    KBMenuSelectionItemTypeChangeLang
};

@class PKLanguage;

@interface KBMenuSelectionItem : NSObject<PKLocalization>
@property(nonatomic,strong) NSString *txt;
@property(nonatomic, assign) KBMenuSelectionItemType type;
@property(nonatomic, strong) PKLanguage* language;

-(instancetype)initWithType:(KBMenuSelectionItemType)type;
-(instancetype)initWithType:(KBMenuSelectionItemType)type language:(PKLanguage*)language;


-(UIImage*)icon;

@end
