//
//  ArtKeyboardModel.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 28/08/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

#import "KeyboardModel.h"
#import "PKLocalization.h"

@class PKKeyboardConfig;
@class KeyModel;
@protocol PKKeyboardLocalizationProvider;

typedef NS_ENUM(NSInteger, ArtKeyboardType) {
    ArtKeyboardTypeEmoji,
    ArtKeyboardTypeGif
};


@interface ArtKeyboardModel : KeyboardModel<PKLocalization>

@property (nonatomic, strong) NSArray <KeyModel*>* bottomRowButtons;
@property (nonatomic, strong) NSArray <KeyModel*>* artTypeButtons;
@property (nonatomic, weak, readonly) id<PKKeyboardLocalizationProvider> localizationProvider;

- (instancetype)initWithKeyboardConfig:(PKKeyboardConfig*)keyboardConfig isFlowOpen:(BOOL)isFlowOpen;
- (void)commonInitWithKeyboardConfig:(PKKeyboardConfig*)keyboardConfig;
- (void)updateByLocalization:(id<PKKeyboardLocalizationProvider>)provider;
- (KeyModel *)baseCategoryKey:(NSString *)txt iconName:(NSString*)iconName  highlightedIconName:(NSString*)highlightedIconName;

@end
