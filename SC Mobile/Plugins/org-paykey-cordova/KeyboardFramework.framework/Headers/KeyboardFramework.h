//
//  KeyboardFramework.h
//  KeyboardFramework
//
//  Created by alon muroch on 08/11/2015.
//  Copyright © 2015 alon muroch. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for KeyboardFramework.
FOUNDATION_EXPORT double KeyboardFrameworkVersionNumber;

//! Project version string for KeyboardFramework.
FOUNDATION_EXPORT const unsigned char KeyboardFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <KeyboardFramework/PublicHeader.h>

#import <LocalAuthentication/LocalAuthentication.h>


// blocks
#import "PKKeyboardEventsProtocol.h"
#import "UIScreen+EXT.h"
#import "PKAnalyticsEvent.h"
#import "PKSettingsNavigationController.h"
#import "PayKeyConfig.h"
#import "PKKeyboardConfig.h"
#import "PKOnboardingManager.h"
#import "KeyboardController.h"
#import "PKLanguage.h"
#import "PKUtility.h"
#import "PKPrettyUtils.h"
#import "PKEngineLogger.h"
#import "UIDevice+EXT.h"
#import "OpenSettingsConfig.h"
#import "PKSettingsTableView.h"
#import "NSException+PKExceptions.h"
#import "OpenSettingsView.h"
#import "KeyModelUI.h"

#import "PKLocalization.h"
#import "PKDictionaryProvider.h"
#import "PKKeyboardLocalizationProvider.h"
#import "PKKeyboardViewProtocol.h"
#import "PKKeyboardLayoutSwitchingProtocol.h"
#import "PKCommand.h"

#import "NSBundle+localization.h"
#import "NSString+Character.h"
#import "NSUserDefault+EXT.h"
#import "UILabel+TextApproximatedFont.h"
#import "UIColor+HEX.h"
#import "NSString+EXT.h"
#import "PKLog.h"
#import "TopBarView.h"
#import "UIReturnKeyProxy.h"
#import "PKInputTextState.h"
#import "NSArray+SequenceOperations.h"
#import "UIButton+EXT.h"
#import "PKKeyboardViewFactory.h"
#import "NSString+textDirection.h"
#import "PKPaymentButtonAttributes.h"
#import "ArtContentViewProtocol.h"
#import "ArtKeyboardModel.h"

#import "PKDictionaryProviderImpl.h"
#import "PKGoodTextField.h"
#import "PKComponentsEnums.h"

#import "NSString+Language.h"
#import "PKAnalyticsConsts.h"
#import "PKEnums.h"

#import "PKDictionariesOTAProvider.h"

