//
//  PKKeyboardLifeCycleDelegate.h
//  PayKeyboard
//
//  Created by Alex Kogan on 05/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class UITraitCollection;


@protocol PKKeyboardLifeCycleDelegate <NSObject>
-(void)didLoad:(UIView*)view;
-(void)willAppear;
-(void)didAppear;
-(void)willDisappear;
-(void)didDisappear;
-(void)textWillChange:(id<UITextDocumentProxy>) textDocumentProxy;
-(void)textDidChange:(id<UITextDocumentProxy>) textDocumentProxy;
-(void)viewWillTransitionToSize:(CGSize)size;
-(void)traitCollectionDidChange:(UITraitCollection *)previousTraitCollection;
-(void)updateViewConstraints;
-(void)viewWillLayoutSubviews:(UIView*)view;
@end
