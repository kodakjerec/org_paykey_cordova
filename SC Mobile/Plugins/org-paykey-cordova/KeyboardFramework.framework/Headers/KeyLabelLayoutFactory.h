//
//  KeyLabelLayoutFactory.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 23/10/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KeyboardId;

@interface KeyLabelLayoutFactory : NSObject

+ (id)keyLabelLayoutWithKeyboardId:(KeyboardId*)keyboardId;
+ (id)keyLabelLayoutWithKeyboardId:(KeyboardId*)keyboardId andKeyName:(NSString*)keyName;
+ (id)keyImageLayoutForKeyboardId:(KeyboardId*)keyboardId keyName:(NSString*)keyName;

@end
