//
//  PKDictionaryOTADB.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 08/04/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKDictionaryOTAModel.h"

@class PKLanguage;

@interface PKDictionaryOTADB : NSObject <NSCoding>

+ (instancetype)modelWithInstalledLanguages:(NSArray*)installedLanguages;
- (void)addModel:(PKDictionaryOTAModel*)model forLanguage:(PKLanguage*)language;
- (PKDictionaryOTAModel*)modelForLanguage:(PKLanguage*)language type:(PKDictionaryType)type;
- (BOOL)didAddNewLanguage:(NSArray*)language;
- (void)addLanguage:(PKLanguage*)language;

@property (nonatomic,strong,readonly) NSDate *lastUpdateDate;

@end

