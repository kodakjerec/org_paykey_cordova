//
//  KeyboardSelectionMenuLayout.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 08/10/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface KeyboardSelectionMenuLayout : NSObject

- (instancetype)initWithKeyFrame:(CGRect)keyFrame
                  contianerFrame:(CGRect)contianerFrame
                           width:(CGFloat)width
                    cornerRadius:(CGFloat)cornerRadius
                      itemsCount:(NSInteger)itemsCount;

- (CGRect)frameForKeyWithKeyView;
- (CGRect)tableViewFrame;
- (BOOL)leftMost;
- (CGFloat)cellHeight;
- (BOOL)drawBottomCorners;

@property (nonatomic,assign,readonly) CGFloat cornerRadius;
@property (nonatomic,assign,readonly) CGRect keyFrame;

@end
