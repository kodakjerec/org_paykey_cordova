//
//  NSUserDefaults+KB.h
//  Integrator
//
//  Created by alon muroch on 13/04/2016.
//  Copyright © 2016 alon muroch. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSUserDefaults (KB)

- (BOOL)didSwitchToPayKeyForTheFirstTime;
- (void)setDidSwitchToPayKeyForTheFirstTime:(BOOL)didSwitchToPayKeyForTheFirstTime;

@end
