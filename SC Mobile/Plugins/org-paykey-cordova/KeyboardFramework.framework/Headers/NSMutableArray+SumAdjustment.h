//
//  NSArray+RemainderDistribution.h
//  KeyboardFramework
//
//  Created by German Velibekov on 06/12/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

//TODO: Create immutable implementation
@interface NSMutableArray (SumAdjustment)

- (void)adjustSumToNumber:(NSNumber *)sum
distributeRemainderOn:(NSNumber *)numberToDistribute;

@end

NS_ASSUME_NONNULL_END
