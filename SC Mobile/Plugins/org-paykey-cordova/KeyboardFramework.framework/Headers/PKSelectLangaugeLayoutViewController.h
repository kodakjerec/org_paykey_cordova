//
//  PKSelectLangaugeLayoutViewController.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 16/04/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PKLanguage;

@interface PKSelectLangaugeLayoutViewController : UITableViewController

- (instancetype)initWithLangauge:(PKLanguage*)langauge;

@end

