//
//  NLPController.h
//  PayKeyboard
//
//  Created by Mac on 30/04/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "PKEnums.h"
@class NLPControllerResult;
@class SuggestionEngineSetupData;

@interface NLPController : NSObject

-(void)loadChecker:(SuggestionEngineSetupData*)keyboardData
dictionaryIsoLocale:(NSString*)dictionaryIsoLocale
        maxResults:(NSInteger)maxResults;
- (void)loadMainDictionaryWithPath:(NSString *)mainDictionaryPath;
- (void)loadEmojiDictionaryWithPath:(NSString*)path;

-(NLPControllerResult*)getSuggestions:(NSArray*)data withContext:(NSString*)context shiftMode:(EngineShiftMode)shiftMode;

-(void)autoLearnWord:(NSString*)word sentence:(NSString*)sentence wasAutoCapitalized:(BOOL)wasAutoCapitalized;
-(void)autoUnlearnWord:(NSString*)word sentence:(NSString*)sentence;
    
-(NSString*)dictionaryInfo;
-(NSString*)engineVersion;
- (void)setLexiconEntries:(NSArray*)lexiconEntries;
- (void)close;
- (NSString*)dumpDictionary:(int)index;
- (void)clearUserHistoryDictionary;


@end
