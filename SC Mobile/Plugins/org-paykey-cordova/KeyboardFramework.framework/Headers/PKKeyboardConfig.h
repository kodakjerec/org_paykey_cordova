//
//  KeyboardProtocol.h
//  PayKeyboard
//
//  Created by alon muroch on 12/04/2016.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKKeyboardLocalizationProvider.h"
#import "PKKeyboardLayoutBuilderProtocol.h"
#import "PKDictionaryProvider.h"
#import "PKEnums.h"

@protocol PKDictionariesOTAProvider;
@class KeyboardController;
@class PKLanguage;
@class PKSizeClass;
@class PKPaymentButtonAttributes;


@interface PKKeyboardConfig : NSObject <PKKeyboardLocalizationProvider, PKKeyboardLayoutBuilderProtocol, PKDictionaryProvider>
@property (nonatomic, strong) PKLanguage * __nullable currentLanguage;
@property (nonatomic, strong) NSArray * __nullable languages;

- (id<PKDictionaryProvider> _Nullable)createDictionaryProvider;

- (id<PKDictionariesOTAProvider> _Nullable)dictionariesOTAProvider;

/**
 * Will be set the first time the keyboard is loaded.
 * Very usefull for onboarding processes the show the user for the first time how to switch to the keyboard
 */
- (void)setDidSwitchToPayKeyForTheFirstTime;

/**
 * Order the payment flow
 */
- (void)paymentFlow:(KeyboardController*_Nonnull)controller;

- (BOOL)enableAutoCorrection;

- (BOOL)enableEmojiSuggestion;

- (BOOL)enableKeyPressSound;

- (BOOL)paykeyButtonEnabled;

- (PKPayButtonLocation)paykeyButtonLocation;

- (BOOL)enableSuggestions;

- (BOOL)enableEmojiLayout;

- (BOOL)enableGif;

- (BOOL)globeToNextKeyboard;

- (BOOL)periodDoubleSpace;

- (BOOL)showSettingsAtSelectionMenu;

- (BOOL)nextInputWideButton;

- (BOOL)nextInputShowTextEnabled;

- (BOOL)nextInputAfterLastLanguageEnabled;

- (BOOL)emojiButtonEnabled;

- (BOOL)autoCapitaliztionEnabled;

- (BOOL)capsLockEnabled;

- (BOOL)quickActionBarEnabled;

- (BOOL)logUsage;

- (NSString*_Nonnull)keyboardName;

- (PKDictionariesOTAAutoDownloadType)dictionariesOTAAutoUpdateTypeFromPayKeyConfig;

- (NSNumber*)dictionariesOTAIntervalToUpdateFromPayKeyConfig;

+ (NSString *)localizedStringWithlanguage:(NSString *)language key:(NSString *)key table:(NSString *)table localizations:(NSDictionary*)localizations;

+ (NSString*_Nonnull)deviceLocalizedStringForKey:(NSString *_Nonnull)key;

+ (NSDictionary *)loadLocalizationsFromChain;

+ (NSArray *)bundlesChain;

@end
