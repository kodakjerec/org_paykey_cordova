//
//  NSString+EXT.h
//  PayKeyboard
//
//  Created by Alex Kogan on 09/05/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKEnums.h"

@interface NSString (EXT)

- (BOOL)isEmailWord;

- (BOOL)isQuatationMark;

- (BOOL)isAutoCorrectionChar:(PKLanguageType)type;

- (BOOL)isSeparatorChar:(PKLanguageType)type;

- (BOOL)isLetterChar;

- (BOOL)isWhiteSpaceAndNewLineChar;

- (BOOL)containsDecimalDigitChar;

- (BOOL)containsOnlyDecimalDigitChar;

- (BOOL)containsOnlyLetters;

- (BOOL)containsOnlyWhiteSpaces;

- (BOOL)isCaseSensitive;

- (NSRange)rangeOfEmojiAtIndex:(NSInteger)index;

- (BOOL)containsEmoji;

- (NSInteger)characterSequenceLength;

- (NSString*)concatWith:(NSString*)str;

- (NSArray*)rangesOccurencesOfCharacter:(NSString*)character;

- (NSArray*)rangesOccurencesOfCharacterSet:(NSCharacterSet*)set;

@end
