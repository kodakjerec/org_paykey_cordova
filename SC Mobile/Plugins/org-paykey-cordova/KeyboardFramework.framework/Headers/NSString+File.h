//
//  NSString+File.h
//  PayKeyboard
//
//  Created by Alex Kogan on 25/04/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (File)

-(NSString*)newLine;
-(NSString*)appendLine:(NSString*)line;

@end
