//
//  EnterInputCommand.h
//  Pods
//
//  Created by Jonathan Beyrak-Lev on 19/03/2017.
//
//

#import "InputCommand.h"

@interface InsertInputCommand : InputCommand

@property (nonatomic, strong) NSString *textToInput;

@end
