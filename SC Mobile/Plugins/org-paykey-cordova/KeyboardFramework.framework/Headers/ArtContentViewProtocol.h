//
//  ArtContentViewProtocol.h
//  PayKeyboard
//
//  Created by ishay weinstock on 11/10/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

#ifndef ArtContentViewProtocol_h
#define ArtContentViewProtocol_h

@class PKGoodTextField;
@class ArtKeyboardModel;

@protocol PKKeyboardLocalizationProvider;

@protocol ArtContentViewDelegate

- (void)artContnetViewWasSelected;
- (void)showKeyboardWithTextField:(PKGoodTextField*)textField;
- (void)hideKeyboard;


@end

@protocol ArtContentViewProtocol <NSObject>

@property (nonatomic, weak) id<ArtContentViewDelegate> artContentViewDelegate;
@property (nonatomic, assign) CGFloat topBarHeight;
@property (nonatomic, strong) ArtKeyboardModel*  keyboardModel;


- (UIView *)bottomBar;

@optional


@property (nonatomic, assign) BOOL isSmallVersion;
- (void)viewWillAppear;
- (void)preTraitCollectionDidChange;
- (void)postTraitCollectionDidChange;

@end

#endif /* ArtContentViewProtocol_h */
