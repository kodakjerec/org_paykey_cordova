//
//  SpaceInteractor.h
//  PayKeyboard
//
//  Created by ishay weinstock on 17/09/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "KeyInteractor.h"

@class SpaceInteractor;

@protocol SpaceInteractorDelegate <NSObject>

- (void)spaceInteractorShowChooseLanguageView:(SpaceInteractor*)interactor;
- (void)spaceInteractorHideLanguageView:(SpaceInteractor*)interactor;
- (void)spaceInteractorSwitchLanguage:(SpaceInteractor*)interactor;
- (void)spaceInteractorHandlePan:(SpaceInteractor*)interactor;

@end

@interface SpaceInteractor : KeyInteractor

@property (nonatomic,weak) id<SpaceInteractorDelegate> delegate;

@end
