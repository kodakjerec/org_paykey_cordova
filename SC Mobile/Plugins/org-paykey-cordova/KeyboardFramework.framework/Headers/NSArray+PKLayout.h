//
//  NSArray+PKLayout.h
//  PayKeyboard
//
//  Created by Jonathan Beyrak-Lev on 04/09/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSArray+SequenceOperations.h"

@interface NSArray(PKLayout)

- (BOOL)shouldBiasRightward;
- (NSArray *)firstLineForWidth:(CGFloat)width;
- (NSArray *)restLinesForWidth:(CGFloat)width;
- (CGFloat)summedWidth;
- (CGFloat)centeringWidth;
- (BOOL)hasKeyWiderThan:(CGFloat)max;
- (NSArray *)trimmed;
- (NSArray *)normals;
- (NSArray *)arbitraries;

@end
