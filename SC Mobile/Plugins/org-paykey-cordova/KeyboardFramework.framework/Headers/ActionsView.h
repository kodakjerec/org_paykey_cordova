//
//  ActionsViewController.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 05/03/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^PKActionCompletionBlock)(void);


@interface PKAction : NSObject

@property (nonatomic,strong,readonly) NSString *name;
@property (nonatomic,strong,readonly) PKActionCompletionBlock completionBlock;

+ (instancetype)actionWithName:(NSString*)name completionBlock:(PKActionCompletionBlock)completionBlock;

@end

@interface ActionsView : UIView

- (instancetype)initWithActionRect:(CGRect)actionRect actions:(NSArray*)actions;

@end

NS_ASSUME_NONNULL_END
