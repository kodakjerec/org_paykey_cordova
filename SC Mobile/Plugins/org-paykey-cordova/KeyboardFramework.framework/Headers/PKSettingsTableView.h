//
//  PKSettingsTableView.h
//  PayKeyboard
//
//  Created by Alex Kogan on 29/05/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKLocalizableTableViewCell.h"

@protocol PKSettingsTableViewDelegate <NSObject>

- (NSString *)localizedString:(NSString *)key;

@end

@interface PKSettingsTableView : UITableView<UITableViewDelegate, UITableViewDataSource, PKLocalizableTableViewCellDelegate>

@property (nonatomic, weak) id<PKSettingsTableViewDelegate> settingsTableViewDelegate;

- (void)displayPresenters:(NSArray *)presenters;

@end
