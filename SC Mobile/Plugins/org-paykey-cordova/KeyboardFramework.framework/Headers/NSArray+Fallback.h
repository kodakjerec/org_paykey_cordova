//
//  NSArray+Fallback.h
//  KeyboardFramework
//
//  Created by Jonathan Beyrak-Lev on 21/12/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKFallbacks.h"

@interface NSArray (Fallback) <PKFallbacks>

- (NSArray *)fallbacks;

@end
