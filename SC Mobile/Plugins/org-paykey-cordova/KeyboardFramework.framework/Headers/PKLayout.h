//
//  PKLayout.h
//  PayKeyboard
//
//  Created by Jonathan Beyrak-Lev on 06/09/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKFrame.h"

@interface PKLayout : NSObject

- (NSArray *)calculateFrames;

@end
