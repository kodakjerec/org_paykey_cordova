//
//  PKDisclosureCell.h
//  PayKeyboard
//
//  Created by Alex Kogan on 12/04/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKLocalizableTableViewCell.h"

@interface PKDisclosureCell : PKLocalizableTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;

@property (weak, nonatomic) IBOutlet UILabel *detail;

@end
