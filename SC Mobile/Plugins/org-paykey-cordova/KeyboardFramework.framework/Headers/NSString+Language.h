//
//  NSString+Language.h
//  PayKeyUI
//
//  Created by Marat  on 7/19/17.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Language)
- (NSLocaleLanguageDirection)textDirection;
@end
