//
//  PKLog.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 02/01/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSObject+VariableArity.h"

extern NSString const * PKLogLevelNone;
extern NSString const * PKLogLevelError;
extern NSString const * PKLogLevelWarn;
extern NSString const * PKLogLevelInfo;
extern NSString const * PKLogLevelDebug;
extern NSString const * PKLogLevelVerbose;

#define selfBundleID [[NSBundle bundleForClass:[self class]] bundleIdentifier]

#define PKTaggedLog(tags, userTags, message, ...)\
    [[PKLog sharedInstance] logWithInternalTags:[selfBundleID plus:tags]\
                                 userFacingTags:userTags\
                                  messageFormat:message, ##__VA_ARGS__]

#define PKLog(message, ...) PKTaggedLog(nil, nil, message, ##__VA_ARGS__)
#define PKUserFacingTagLog(flulben, message, ...) PKTaggedLog(nil, flulben, message, ##__VA_ARGS__)
#define PKInternalTagLog(flulben, message, ...) PKTaggedLog(flulben, nil, message, ##__VA_ARGS__)
#define PK_DUMP_METHOD_CALL PKInternalTagLog(PKLogLevelDebug, [NSString stringWithUTF8String:__PRETTY_FUNCTION__])

@interface PKLog : NSObject

@property (strong, nonatomic) NSDictionary *filters;

+ (instancetype)sharedInstance;
+ (NSDictionary *)filtersForLogLevel:(NSString *)level;
- (void)logWithInternalTags:(id)internal userFacingTags:(id)userFacing messageFormat:(NSString *)message, ...;

@end
