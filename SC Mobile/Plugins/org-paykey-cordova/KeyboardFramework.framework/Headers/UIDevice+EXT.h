//
//  UIDevice.h
//  PayKeyboard
//
//  Created by Alex Kogan on 11/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, DeviceModel) {
    DeviceModeliPodTouch5,
    DeviceModeliPodTouch6,
    DeviceModeliPhone4,
    DeviceModeliPhone4s,
    DeviceModeliPhone5,
    DeviceModeliPhone5c,
    DeviceModeliPhone5s,
    DeviceModeliPhone6,
    DeviceModeliPhone6Plus,
    DeviceModeliPhone6s,
    DeviceModeliPhone6sPlus,
    DeviceModeliPhone7,
    DeviceModeliPhone7Plus,
    DeviceModeliPhoneX,
    DeviceModeliPhoneXR,
    DeviceModeliPhoneXMax,
    DeviceModeliPad2,
    DeviceModeliPad3,
    DeviceModeliPad4,
    DeviceModeliPadAir,
    DeviceModeliPadAir2,
    DeviceModeliPadMini,
    DeviceModeliPadMini2,
    DeviceModeliPadMini3,
    DeviceModeliPadMini4,
    DeviceModeliPadPro,
    DeviceModelSimulator,
    DeviceModelUnknown,
};

@interface UIDevice (EXT)
-(BOOL) isLandscapeSizeSmallerOrEqualTo:(DeviceModel)model;
-(BOOL) is4InchScreenSize;
-(BOOL) is58InchScreenSize;
-(BOOL) is47InchScreenSize;

-(NSString*)deviceName;
-(DeviceModel)modelBySize;
@end
