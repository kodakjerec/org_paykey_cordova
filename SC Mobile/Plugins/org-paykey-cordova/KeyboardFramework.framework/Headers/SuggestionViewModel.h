//
//  SuggestionButtonData.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 18/03/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SuggestionViewModel : NSObject

@property BOOL isAutoCorrection;
@property BOOL shouldLearnWord;
@property BOOL showHideButton;
@property NSString *value;
@property NSString *displayText;

@end
