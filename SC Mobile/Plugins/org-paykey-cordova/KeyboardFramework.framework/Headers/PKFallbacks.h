//
//  PKFallbacks.h
//  PayKeyboard
//
//  Created by Jonathan Beyrak-Lev on 24/12/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#ifndef PKFallbacks_h
#define PKFallbacks_h

@protocol PKFallbacks

- (NSArray *)fallbacks;

@end

#endif /* PKFallbacks_h */
