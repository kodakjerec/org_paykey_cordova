//
//  WordComposer.h
//  PayKeyboard
//
//  Created by Alex Kogan on 13/07/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKTouchEvent.h"

@interface WordComposer : NSObject
    
@property (nonatomic, strong, readonly) PKTouchEvent*  latestTouchEvent;
@property (nonatomic, strong, readonly) NSMutableArray* tapHistory;
@property (nonatomic, assign, readonly) BOOL composing;
@property (nonatomic, readonly ,strong) NSString* composedWord;
@property (nonatomic,assign) BOOL wasAutoCapitalized;

- (void)setComposedWord:(NSString*)word;
-(void)addTouchEvent:(PKTouchEvent*)event;
-(void)removeLastTouchEvent;
-(void)reset:(BOOL)resetWasAutoCapitalized;
-(BOOL)hasTypedWord;

@end
