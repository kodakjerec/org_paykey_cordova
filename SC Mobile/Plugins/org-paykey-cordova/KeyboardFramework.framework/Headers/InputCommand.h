//
//  InputCommand.h
//  Pods
//
//  Created by Jonathan Beyrak-Lev on 19/03/2017.
//
//

#import <Foundation/Foundation.h>

@protocol Inputtable <NSObject>

- (void)addStringToInput:(NSString *)text;
- (void)deleteBackward;

@end

@interface InputCommand : NSObject

@property (nonatomic, strong) id<Inputtable> inputTarget;

- (void)execute;

@end
