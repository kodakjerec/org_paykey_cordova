//
//  CompositeInputCommand.h
//  PayKeyboard
//
//  Created by Alex Kogan on 04/07/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InputCommand.h"

@interface CompositeInputCommand : InputCommand

-(instancetype)initWithCommands:(NSArray<InputCommand*>*)commands;

@end
