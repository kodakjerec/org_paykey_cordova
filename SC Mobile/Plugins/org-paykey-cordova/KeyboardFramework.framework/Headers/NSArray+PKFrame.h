//
//  NSArray+PKFrame.h
//  PayKeyboard
//
//  Created by Jonathan Beyrak-Lev on 07/09/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSArray(PKFrame)

- (CGFloat)maxX;
- (CGFloat)maxY;
- (NSArray *)offsettingX:(CGFloat)offset;
- (NSArray *)offsettingY:(CGFloat)offset;
- (BOOL)isHorizontallyEqual:(NSArray *)a;
- (BOOL)isVerticallyEqual:(NSArray *)a;

@end
