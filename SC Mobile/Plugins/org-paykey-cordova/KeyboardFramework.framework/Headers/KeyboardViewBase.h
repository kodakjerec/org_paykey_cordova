//
//  KeyboardViewBase.h
//  EmojiUI
//
//  Created by Alex Kogan on 09/02/2017.
//  Copyright © 2017 Alex Kogan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKKeyboardViewProtocol.h"
#import "KeyboardContainerView.h"
#import "NextInputInteractor.h"
#import "PKInputTextState.h"

@class Keyboard;
@class KeyboardViewModel;
@class PKTouchEvent;
@class KeyHoverBoxView;
@class KeyInteractor;
@class KeyModel;

@interface KeyboardViewBase : UIView <KeyboardContainerProtocol,NextInputInteractorDelegate>

@property (nonatomic, weak) id<KeyboardViewProtocol> delegate;
@property (nonatomic, strong) KeyboardViewModel*  keyboardViewModel;
@property (nonatomic, strong) PKTouchEvent *highlightedEvent;

- (void)setKeyView:(KeyView*)keyView forKeyModel:(KeyModel*)keyModel;
- (KeyView*)keyViewForModel:(KeyModel*)keyModel;
- (void)clearKeyViews;
- (NSArray*)allKeyViews;
- (void)pressed:(PKTouchEvent *)key;
- (void)touchStarted:(PKTouchEvent *)key;

- (void)onTouchBegin:(UITouch *)touch withModel:(KeyModel*)model;
- (void)onTouchMoved:(UITouch *)touch withModel:(KeyModel*)model;
- (void)onTouchEnded:(UITouch *)touch withModel:(KeyModel*)model;

- (CGFloat)getNextRowCenterYOfKeyView:(KeyView*)keyView;
- (void)addInteractor:(KeyInteractor*)interactor forKeyCode:(int32_t)keyCode;
- (void)hideHover;
- (BOOL)hoverViewShouldPan;
- (void)handleHoverPanWithTouch:(UITouch *)touch andModel:(KeyModel *)model;
- (PKTouchEvent*)createPKTouchEvent:(UITouch*)touch withModel:(KeyModel*)model;

@end
