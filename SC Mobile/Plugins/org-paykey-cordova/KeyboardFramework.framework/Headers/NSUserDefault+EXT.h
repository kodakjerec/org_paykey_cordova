//
//  NSUserDefault+EXT.h
//  PayKeyboard
//
//  Created by Alex Kogan on 13/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PKLanguage;
@interface NSUserDefaults (EXT)

@property (nonatomic, strong) PKLanguage* lastSelectedLanguage;
@property (nonatomic, assign) BOOL didMoveToDifferentKeyboardLastTime;
@property (nonatomic, strong)NSArray* enabledLanguages;


- (void)enableLanguage:(PKLanguage*)langauge;
- (void)disableLanguage:(PKLanguage*)language;
- (void)updateLanguage:(PKLanguage*)language;

+ (NSUserDefaults*)appGroupUserDefaults;

@end
