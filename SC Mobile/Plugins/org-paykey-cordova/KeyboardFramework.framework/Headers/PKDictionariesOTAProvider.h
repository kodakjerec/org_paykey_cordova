//
//  PKDictionariesOTAProvider.h
//  PayKeyboard
//
//  Created by ishay weinstock on 11/04/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

#ifndef PKDictionariesOTAProvider_h
#define PKDictionariesOTAProvider_h

typedef void (^DictionariesOTAConfigurationCompletionBlock)(NSString *onfiguration,NSError *error);
typedef void (^DictionariesOTAFileCompletionBlock)(NSError *error);
typedef void (^ReachabilityCompletionBlock)(PKReachabilityType type);

@protocol PKDictionariesOTAProvider <NSObject>

- (void)reciveConfigurationWithCompletion:(DictionariesOTAConfigurationCompletionBlock)completion;
- (void)downloadFileForURL:(NSString*)url writeToURL:(NSURL*)writeToURL configurationWithCompletion:(DictionariesOTAFileCompletionBlock)completion;
- (void)registerForReachabilityWithCompletion:(ReachabilityCompletionBlock)completion;

@end

#endif /* PKDictionariesOTAProvider_h */
