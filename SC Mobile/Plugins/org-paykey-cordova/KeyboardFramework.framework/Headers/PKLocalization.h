//
//  PKLocalization.h
//  PayKeyboard
//
//  Created by Marat  on 3/15/17.
//  Copyright © 2017 Apple. All rights reserved.
//
/* PKLocalization_h */
#import  "PKKeyboardLocalizationProvider.h"

@protocol PKLocalization <NSObject>
-(void)updateByLocalization:(id<PKKeyboardLocalizationProvider>)provider;

@end
