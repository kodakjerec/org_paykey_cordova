//
//  NSDictionary+SequenceManipulation.h
//  Sample
//
//  Created by Jonathan Beyrak-Lev on 07/06/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (SequenceManipulation)

+ (NSDictionary *)dictionaryWithDictionaries:(NSArray *)dictionaries;
- (instancetype)removing:(NSArray *)keysToRemove;
- (instancetype)adding:(NSDictionary *)addend;

@end
