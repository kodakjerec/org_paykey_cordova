//
//  KeyHoverLayout.h
//  test
//
//  Created by ishay weinstock on 02/10/2017.
//  Copyright © 2017 ishay weinstock. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, HoverDirection) {
    HoverDirectionNone,
    HoverDirectionRight,
    HoverDirectionLeft
};

@class KeyHoverViewConfig;

@interface KeyHoverViewLayout : NSObject

- (instancetype)initWithKeyFrame:(CGRect)keyFrame
                  contianerFrame:(CGRect)contianerFrame
                 contianerBounds:(CGRect)contianerBounds
                            keys:(NSArray*)keys
                    cornerRadius:(CGFloat)cornerRadius
                          config:(KeyHoverViewConfig*)config
                calculateKeySize:(BOOL)calculateKeySize;

- (CGRect)hoverFrame;
- (CGRect)frameForKeyAtIndex:(NSInteger)index;
- (CGRect)separatorFrame;
- (BOOL)leftMost;
- (BOOL)rightMost;
- (CGFloat)curveHeightCoff;
- (HoverDirection)direction;

@property (nonatomic,assign,readonly) CGRect keyFrame;
@property (nonatomic,assign,readonly) CGFloat cornerRadius;
@property (nonatomic,assign) NSInteger numberOfKeysLeft;
@property (nonatomic,assign) NSInteger numberOfKeysRight;

@end
