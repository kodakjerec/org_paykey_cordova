//
//  EmojiCell.h
//  EmojiUI
//
//  Created by Sumit on 14/02/17.
//  Copyright © 2017 Alex Kogan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EmojiKeyboardView.h"
#import "PKUtility.h"
#import "EmojiData.h"

static NSString * EmojiCellIdentifier = @"EmojiCell";

@class KeyHoverView;

@interface EmojiCell : UICollectionViewCell

@property (nonatomic, strong) UILabel *container;
@property (nonatomic, strong) DelayedBlockHandle pressClosure;
@property (nonatomic, assign) int section;
@property (nonatomic, weak) EmojiKeyboardView * kbView;
@property (nonatomic, strong) KeyHoverView *hoverView;
@property (nonatomic, strong) EmojiData * emojiData;

- (void)bindView:(EmojiData *)emojiData;

@end
