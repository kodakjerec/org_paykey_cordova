//
//  DictionariesATOManager.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 02/04/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKEnums.h"
#import "PKDictionaryProvider.h"
#import "PKDictionariesOTAProvider.h"

@class PKKeyboardConfig;

@protocol PKDictionariesOTAManagerDelegate <NSObject>

- (void)updateDownloadDictionariesTopBarVisibility:(BOOL)visible;

@end

@interface PKDictionariesOTAManager : NSObject <PKDictionaryProvider>

@property (nonatomic,weak) id<PKDictionariesOTAManagerDelegate> delegate;

- (instancetype)initWithProvider:(id<PKDictionariesOTAProvider>)provider
                  keyboardConfig:(PKKeyboardConfig*)keyboardConfig
                        delegate:(id<PKDictionariesOTAManagerDelegate>)delegate;

- (void)downloadDictionariesForLangauge:(PKLanguage *)language;

@end
