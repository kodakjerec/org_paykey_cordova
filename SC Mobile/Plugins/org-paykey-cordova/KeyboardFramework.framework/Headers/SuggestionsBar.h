//
//  SuggestionsBar.h
//  PayKeyboard
//
//  Created by Alex Kogan on 13/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TopBarView.h"
#import "SuggestionsBarConfig.h"
#import "PKInputTextState.h"
#import "SuggestionsBarBase.h"


@class SuggestionsBarViewModel;

@interface SuggestionsBar : SuggestionsBarBase <PKInputTextStateObserver>

@end
