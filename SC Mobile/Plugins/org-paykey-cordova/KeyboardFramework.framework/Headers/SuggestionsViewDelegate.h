//
//  SuggestionsViewDelegate.h
//  PayKeyboard
//
//  Created by ishay weinstock on 03/10/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

#ifndef SuggestionsViewDelegate_h
#define SuggestionsViewDelegate_h

@import Foundation;
@class PKSuggestionBarTouchEvent;

@protocol SuggestionsViewDelegate <NSObject>

-(void)onSuggestionSelected:(PKSuggestionBarTouchEvent*)event;
-(void)cancelAutoCorrection;
-(void)learnWord:(NSString *)word;

@end

#endif /* SuggestionsViewDelegate_h */
