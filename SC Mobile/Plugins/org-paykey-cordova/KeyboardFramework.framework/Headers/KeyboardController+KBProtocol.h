//
//  KeyboardController+KBProtocol.h
//  PayKeyboard
//
//  Created by Alex Kogan on 13/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KeyboardController.h"
#import "PKKeyboardViewProtocol.h"

@class PKSuggestionBarTouchEvent;

@interface KeyboardController (KBProtocol) <KeyboardViewProtocol, KeyboardViewProtocol>

-(void)handleTouchEvent:(PKTouchEvent*)event;

@end
