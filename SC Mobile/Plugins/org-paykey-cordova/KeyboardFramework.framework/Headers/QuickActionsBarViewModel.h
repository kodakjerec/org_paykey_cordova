//
//  QuickActionsBarViewModel.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 22/01/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

#import "PKEnums.h"

@class PKKeyboardConfig;
@class TopBarConfig;

NS_ASSUME_NONNULL_BEGIN

@interface QuickActionsBarItemModel : NSObject

@property (nonatomic,readonly) PKQuickActionType actionType;
@property (nonatomic,readonly) UIImage *actionImage;

- (instancetype)initWithActionType:(PKQuickActionType)type;

@end

@interface QuickActionsBarViewModel : NSObject

- (instancetype)initWithKeyboardConfig:(PKKeyboardConfig*)keyboardConfig topBarConfig:(TopBarConfig*)topBarConfig;

@property (nonatomic,strong,readonly) NSArray *actions;

@end

NS_ASSUME_NONNULL_END
