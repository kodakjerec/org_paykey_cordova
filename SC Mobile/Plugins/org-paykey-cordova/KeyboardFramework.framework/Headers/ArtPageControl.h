//
//  ArtPageControl.h
//  PayKeyboard
//
//  Created by ishay weinstock on 26/09/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EmojiCategoryExtension.h"

@class ArtPageControl;

@protocol ArtPageControlDelegate <NSObject>

- (void)artPageControl:(ArtPageControl*)artControl didScrollToIndex:(NSInteger)index withOffset:(CGFloat)offset;

@end

@interface ArtPageControl : UIControl

- (instancetype)initWithItemViewClass:(Class)itemViewClass;
- (void)selectIndex:(NSInteger)index;
- (void)loadKeys:(NSArray*)keyModels;

@property (nonatomic,weak) id<ArtPageControlDelegate> delegate;

@end
