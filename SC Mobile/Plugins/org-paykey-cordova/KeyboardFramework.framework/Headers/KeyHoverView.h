//
//  EmojiCellHoverBoxView.h
//  EmojiUI
//
//  Created by Sumit on 15/02/17.
//  Copyright © 2017 Alex Kogan. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "KeyHoverViewLayout.h"

@class EmojiCell;
@class KeyModel;
@class KeyHoverView;
@class KeyHoverViewConfig;
@class KeyHoverViewLayout;

@protocol KeyHoverViewDelegate <NSObject>

- (void)keyHoverView:(KeyHoverView*)view didSelectNewKey:(KeyModel*)model;

@end

@interface KeyHoverView : UIView


@property (nonatomic, strong, readonly) KeyModel *model;
@property (nonatomic, weak) id<KeyHoverViewDelegate> delegate;
@property (nonatomic, assign) BOOL showingDiversity;
@property (nonatomic, strong) NSArray *diversity;

- (instancetype)initWithKeyModel:(KeyModel*)model
                       diversity:(NSArray*)diversity
                   containerView:(UIView*)containerView
                          layout:(KeyHoverViewLayout*)layout
                andConfiguration:(KeyHoverViewConfig*)configuration;
- (void)show;
- (void)hide;
- (void)pan:(CGPoint)point;
- (void)updateLayout:(KeyHoverViewLayout*)layout;

@end
