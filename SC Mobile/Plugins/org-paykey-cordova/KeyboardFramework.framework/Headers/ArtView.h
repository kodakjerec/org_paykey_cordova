//
//  ArtView.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 28/08/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeyboardViewBase.h"
#import "PKKeyboardDelegate.h"

@class ArtKeyboardModel;
@protocol ArtContentViewProtocol;

typedef NS_ENUM(NSInteger, ArtViewBottomBarLocation) {
    ArtViewBottomBarLocationBottom,
    ArtViewBottomBarLocationMiddle,
};

@protocol ArtViewDelegate <KeyboardViewProtocol>

- (void)onArtViewType:(PKArtViewType)type;
- (KeyboardViewBase*)keyboardView;

@end

@interface ArtView :  KeyboardViewBase


- (void)loadKeyboardModel:(ArtKeyboardModel*)artKeyboardModel;
- (void)setBottomBar:(UIView *)view atLocation:(ArtViewBottomBarLocation)location;
- (void)setMainView:(id<ArtContentViewProtocol>)view;
- (void)setSelectedIndex:(NSInteger)index;

@property(nonatomic, assign) id <PKKeyboardDelegate> keyboardDelegate;
@property (nonatomic, strong) PKLanguage * nextLanguage;
@property (nonatomic, weak) id<ArtViewDelegate> delegate;


@end
