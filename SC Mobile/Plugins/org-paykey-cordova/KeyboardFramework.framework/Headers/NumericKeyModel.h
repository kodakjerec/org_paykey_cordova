//
//  NumericKeyModel.h
//  PayKeyboard
//
//  Created by Alex Kogan on 09/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//


#import "KeyModel.h"

@interface NumericKeyModel : KeyModel

@property (nonatomic, strong) NSString* subtitle;
@property (nonatomic, strong) UIColor* subtitleTextColor;
@property (nonatomic, strong) UIFont* keySubtitleFont;
@property (nonatomic, assign) CGFloat subtitleCharSpace;
@property (nonatomic, assign) BOOL isProtrait;

@end
