//
//  KeyboardViewProtocol.h
//  PayKeyboard
//
//  Created by Alex Kogan on 02/01/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#ifndef KeyboardViewProtocol_h
#define KeyboardViewProtocol_h


@class PKLanguage;
@class PKTouchEvent;
@class KeyView;
@protocol PKLocalization;
@protocol KeyboardSelectionMenuProtocol;

@protocol KeyboardViewProtocol <NSObject>

@optional
-(void)keyPressed:(PKTouchEvent*)event;
-(void)keydDoublePressed:(PKTouchEvent*)event;
-(void)touchStarted:(PKTouchEvent*)event;
-(void)continuousCharDelete:(PKTouchEvent*)event;
-(void)continuousWordDelete:(PKTouchEvent*)event;

-(PKLanguage*)getNextLanguage;
-(BOOL)hasMultipleLanguagesEnabled;
-(void)switchLanguage:(PKLanguage*)newLanguage;
-(NSArray<PKLanguage*>*)switchableLanguages;
-(PKLanguage*)currentLanguage;
-(void)loadLocalizationFor:(id<PKLocalization>)localization;

-(NSArray*)getMenuItems;
- (id<KeyboardSelectionMenuProtocol>)getSelectionMenuDelegate;

@end

#endif /* KeyboardViewProtocol_h */



