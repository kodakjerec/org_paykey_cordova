//
//  NSLocale+EXt.h
//  PayKeyboard
//
//  Created by Alex Kogan on 10/04/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSLocale (EXT)

+(NSString*)deviceLocaleLanguageCode;
@end
