//
//  EnableAccessBar.h
//  KeyboardFramework
//
//  Created by German Velibekov on 21/06/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

#import "TopBarView.h"

@class EnableAccessModel;

@protocol EnableAccessBarDelegate;

@interface EnableAccessBar : TopBarView

- (instancetype)initWithConfig:(TopBarConfig *)config
                         model:(EnableAccessModel *)model
                      delegate:(id<EnableAccessBarDelegate>)delegate;

@property (readonly) EnableAccessModel *model;

- (void)shake;

@end
