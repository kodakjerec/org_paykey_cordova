//
//  PKButton.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 23/01/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PKButton : UIButton
@property (nonatomic,assign) CGSize imageSize;
@end

NS_ASSUME_NONNULL_END
