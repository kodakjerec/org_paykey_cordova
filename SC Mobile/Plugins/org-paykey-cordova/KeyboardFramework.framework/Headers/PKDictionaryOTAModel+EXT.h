//
//  PKDictionaryOTAModelEXT.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 08/04/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

#import "PKDictionaryOTAModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PKDictionaryOTAModel (EXT)

- (NSString*)fileName;

@end

NS_ASSUME_NONNULL_END
