//
//  InputLogicResult.h
//  PayKeyboard
//
//  Created by Alex Kogan on 13/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKEnums.h"
#import "InputCommand.h"

@interface InputLogicResult : NSObject

@property (nonatomic,strong) NSString* currentInput;
@property (nonatomic,assign) BOOL shouldSuggest;
@property (nonatomic,assign) BOOL shouldPredict;
@property (nonatomic,assign) BOOL shouldClearSuggetions;
@property (nonatomic,assign) BOOL shouldReturnToLetters;
@property (nonatomic,assign) BOOL shouldAddTapEvent;
@property (nonatomic,assign) BOOL shouldRemoveLastTapEvent;
@property (nonatomic,assign) BOOL shouldResetTapEvents;
@property (nonatomic,assign) BOOL shouldAutoLearnWord;
@property (nonatomic,assign) BOOL shouldUnlearnAutoWord;
@property (nonatomic,strong) NSString* wordToLearn;
@property (nonatomic,strong) NSString* prevWords;
@property (nonatomic,strong) NSString* spaceTitle;
@property (nonatomic,assign) BOOL wasAutoCapitalized;
@property (nonatomic,assign) BOOL dontResetWasAutoCapitalized;

-(instancetype)init:(NSString*)currentInput;

-(void)appendCommand:(InputCommand*)command;
-(void)executeCommands;
    


@end
