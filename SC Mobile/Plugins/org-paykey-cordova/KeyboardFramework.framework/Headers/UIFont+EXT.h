//
//  UIFont+EXT.h
//  PayKeyboard
//
//  Created by Alex Kogan on 28/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIFont (EXT)

+ (UIFont *)font:(CGFloat)size weight:(CGFloat)weight;
@end
