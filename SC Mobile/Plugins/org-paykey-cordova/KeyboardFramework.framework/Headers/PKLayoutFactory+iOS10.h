//
//  PKLayoutFactory+iOS10.h
//  KeyboardFramework
//
//  Created by German Velibekov on 19/07/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

#import "PKLayoutFactory.h"

@interface PKLayoutFactory (iOS10)

- (PKLayout *)allKeys_iOS10;
- (NSDictionary *)characterKeysString_iOS10;

- (NSDictionary *)utilityLeftKeysSizesiOS10Landscape;
- (NSDictionary *)utilityCenterKeysSizesiOS10Landscape;
- (NSDictionary *)utilityRightKeysSizesiOS10Landscape;

@end
