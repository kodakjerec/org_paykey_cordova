//
//  TextFieldLabel.h
//  PayKeyUI
//
//  Created by ishay weinstock on 15/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextFrame : NSObject

@property (nonatomic,assign,readonly) NSRange range;
@property (nonatomic,assign,readonly) CGRect frame;
@property (nonatomic,readonly) NSInteger startIndex;
@property (nonatomic,readonly) NSInteger endIndex;
@property (nonatomic,readonly) NSInteger cursorIndex;

+ (instancetype)TextFrame:(CGRect)frame forRange:(NSRange)range cursorIndex:(NSInteger)cursorIndex;

@end

@interface PKTextFieldLabel : UILabel

- (CGRect)textRect;
- (CGRect)lastCharRect;

@property (nonatomic,strong,readonly) NSMutableArray *textRectsArray;
@property (nonatomic,assign) NSUInteger textRectIndex;

@end
