//
//  UIImage+EXT.h
//  PayKeyboard
//
//  Created by Alex Kogan on 30/05/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (EXT)

+(id)imageNamed:(NSString*)name fromBundleChain:(NSArray*)chain;

@end
