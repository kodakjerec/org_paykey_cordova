//
//  PKSwitchSettingPresenter.h
//  PayKeyboard
//
//  Created by Jonathan Beyrak-Lev on 13/07/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "PKSettingPresenter.h"

@interface PKSwitchSettingPresenter : PKSettingPresenter

@property BOOL isOn;
@property BOOL allowsSwitchChange;

- (instancetype)initWithSubtitle:(NSString *)subtitle label:(NSString *)label description:(NSString *)description isOn:(BOOL)isOn switchHandler:(void (^)(BOOL isOn))switchHandler;

@end
