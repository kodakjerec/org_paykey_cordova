//
//  NSUserDefault+Settings.h
//  PayKeyboard
//
//  Created by Alex Kogan on 05/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSUserDefaults (Settings)

@property(nonatomic, assign)BOOL firstIntial;
@property(nonatomic, assign)BOOL enableAutoCorrection;
@property(nonatomic, assign)BOOL enableKeyPressSound;
@property(nonatomic, assign)BOOL enableEmojiSuggestion;
@property(nonatomic, assign)BOOL enableSuggestions;
@property(nonatomic, assign)BOOL enableEmojiLayout;
@property(nonatomic, assign)BOOL enableGif;
@property(nonatomic, assign)BOOL emojiButtonEnabled;
@property(nonatomic, assign)BOOL periodDoubleSpace;
@property(nonatomic, assign)BOOL showSettingsAtSelectionMenu;
@property(nonatomic, assign)BOOL autoCapitaliztionEnabled;
@property(nonatomic, assign)BOOL capsLockEnabled;
@property(nonatomic, assign)BOOL quickActionBarEnabled;

@end
