//
//  KeyModelText.h
//  PayKeyboard
//
//  Created by Alex Kogan on 09/01/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#ifndef KeyModelText_h
#define KeyModelText_h

@interface KeyModelText : NSObject <NSCopying>

/**
 * The actual text that will end up inserted into the text field
 */
@property (nonatomic, strong) NSString * inputText;

/**
 * the text to be displayed on the key itself
 */
@property (nonatomic, strong) NSString * displayText;


- (id)initWithinputText:(NSString *)inputText displayText:(NSString *)displayText;

-(void)inputText:(NSString*)inputText;
-(void)displayText:(NSString*)displayText;

@end

#endif /* KeyModelText_h */
