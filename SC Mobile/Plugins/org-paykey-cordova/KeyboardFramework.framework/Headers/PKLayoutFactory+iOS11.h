//
//  PKLayoutFactory+iOS11.h
//  KeyboardFramework
//
//  Created by German Velibekov on 19/07/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

#import "PKLayoutFactory.h"

@interface PKLayoutFactory (iOS11)

- (PKLayout *)utilityLayoutiOS11Landscape;
- (PKLayout *)allKeys_iOS11;
- (NSDictionary *)characterKeysString_iOS11;

@end
