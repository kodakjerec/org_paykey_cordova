//
//  KeyboardEvents.h
//  PayKeyboard
//
//  Created by alon muroch on 08/05/2016.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PKAnalyticsEvent;


@protocol PKKeyboardEventsProtocol <NSObject>
- (void)reportEvent:(PKAnalyticsEvent*)event;
@end
