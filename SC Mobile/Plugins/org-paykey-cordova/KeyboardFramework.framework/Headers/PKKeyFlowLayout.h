//
//  PKLayout.h
//  PKLayout
//
//  Created by Jonathan Beyrak-Lev on 22/08/2017.
//  Copyright © 2017 Jonathan Beyrak-Lev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKLayout.h"


@interface PKKeyFlowLayout : PKLayout

@property (nonatomic) CGFloat boardWidth;
@property (nonatomic) CGFloat normWidth;
@property (nonatomic) CGFloat bigWidth;
@property (nonatomic) CGFloat lineHeight;
@property (nonatomic) CGFloat lineSpacing;
@property (nonatomic) NSString *keysString;
@property (nonatomic) BOOL center;
@property (nonatomic) BOOL ignoresBaselineMargin;

@end
