//
//  MoreSuggestionCollectionViewCell.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 02/10/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreSuggestionCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
