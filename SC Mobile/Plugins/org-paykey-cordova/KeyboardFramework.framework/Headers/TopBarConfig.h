//
//  TopBarConfig.h
//  KeyboardFramework
//
//  Created by German Velibekov on 18/06/2018.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIColor.h>
#import "PKEnums.h"
#import "PKLocalization.h"

@class PKPaymentButtonAttributes;

@interface TopBarConfig : NSObject <PKLocalization>

- (UIColor *)bgColor;

@property (nonatomic, strong) PKPaymentButtonAttributes* paymentButtonAttributes;
@property (nonatomic, assign) LanguageDirection languageDirection;
@property (nonatomic, assign, readonly, getter=hasPaykeyKey) BOOL showPaykeyKey;

@end
