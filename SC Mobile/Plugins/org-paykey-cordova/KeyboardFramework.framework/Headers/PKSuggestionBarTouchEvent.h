//
//  PKSuggestionBarTouchEvent.h
//  PayKeyboard
//
//  Created by Alex Kogan on 13/09/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKTouchEvent.h"

@interface PKSuggestionBarTouchEvent : PKTouchEvent

@property (nonatomic, assign) PKWordTypedSource wordSource;
@property (nonatomic, strong) NSString* word;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, assign) BOOL isAutoCorrectionAvailable;

-(instancetype)initWithWordSource:(PKWordTypedSource)wordSource word:(NSString*)word index:(NSInteger)index isAutoCorrectionAvailable:(BOOL)isAutoCorrectionAvailable;

@end
