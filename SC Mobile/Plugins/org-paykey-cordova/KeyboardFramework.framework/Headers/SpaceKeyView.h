//
//  SpaceKeyView.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 20/02/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

#import "KeyView.h"
#import "KeyModel.h"

@interface SpaceKeyView : KeyView

@property (nonatomic) BOOL showSpaceArrows;

@end
