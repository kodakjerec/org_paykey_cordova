//
//  KeyboardId.h
//  PayKeyboard
//
//  Created by Alex Kogan on 11/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKEnums.h"
@class PKLanguage;

@interface KeyboardId : NSObject<NSCopying>

@property (nonatomic, readonly) PKKeyboardId keyboardId;
@property (nonatomic, readonly) PKLanguage* language;
@property (nonatomic, readonly) CGFloat keyboardHeight;
@property (nonatomic, readonly) CGFloat keyboardWidth;
@property (nonatomic, readonly) BOOL isPortrait;
@property (nonatomic, readonly) NSString* layoutName;
@property(nonatomic,  readonly) UIKeyboardAppearance keyboardAppearance;

-(instancetype)initWithKeyboardId:(PKKeyboardId)keyboardId
                         language:(PKLanguage*)language
                   keyboardHeight:(CGFloat)keyboardHeight
                    keyboardWidth:(CGFloat)keyboardWidth
                       isPortrait:(BOOL)isPortrait
                       layoutName:(NSString*)layoutName
               keyboardAppearance:(UIKeyboardAppearance)keyboardAppearance;
@end

