//
//  ChooseLangView.h
//  PayKeyboard
//
//  Created by Alex Kogan on 14/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PKLanguage;
@interface ChooseLangView : UIView

-(instancetype)initWithFrame:(CGRect)frame currentLanguage:(PKLanguage*)currentLanguage availableLanguages:(NSArray*)availableLanguages initialTapPoint:(CGPoint)initialTapPoint;

-(PKLanguage*)getSelectedLang;
- (void)panWithPoint:(CGPoint)point;

@end
