//
//  PKOffsetLayout.h
//  PayKeyboard
//
//  Created by Jonathan Beyrak-Lev on 18/09/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "PKLayout.h"
#import "PKOffsetStrategy.h"

@interface PKOffsetLayout : PKLayout

@property (nonatomic) PKLayout *offsetee;
@property (nonatomic) PKOffsetStrategy *offset;

+ (instancetype)offset:(PKLayout *)offsetee using:(PKOffsetStrategy *)offset;

@end
