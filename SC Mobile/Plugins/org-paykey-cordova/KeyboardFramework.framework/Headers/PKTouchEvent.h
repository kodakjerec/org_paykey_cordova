//
//  PKTouchEvent.h
//  PayKeyboard
//
//  Created by Alex Kogan on 24/04/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKConstants.h"

typedef NS_OPTIONS(NSInteger, LongTouchStatus) {
    LongTouchStatusNone,
    LongTouchStatusNormal,
    LongTouchStatusLong,
};

@class KeyModel;

@interface PKTouchEvent : NSObject<NSCopying>

@property (nonatomic, assign, readonly) CGPoint startPoint;
@property (nonatomic, assign, readonly) CGPoint touchPoint;
@property (nonatomic, assign, readonly) NSTimeInterval timestamp;
@property (nonatomic, strong, readonly) NSDate* startDate;
@property (nonatomic, strong, readonly) KeyModel* keyModel;
@property (nonatomic, weak, readonly) UITouch* touch;
@property (nonatomic, readonly) BOOL isContinuous;
@property (nonatomic, readonly) BOOL isContinuousCharDelete;
@property (nonatomic, readonly) BOOL isContinuousWordDelete;
@property (nonatomic, assign) LongTouchStatus longTouchStatus;
@property (nonatomic, readonly) BOOL isSuggestionSelection;
@property (nonatomic, readonly) int32_t code;
@property (nonatomic, readonly) NSInteger tapCount;
@property (nonatomic, readonly) NSString* inputText;
@property (nonatomic, readonly) NSString* name;
@property (nonatomic, readonly) NSInteger xTouchPointInPixel;
@property (nonatomic, readonly) NSInteger yTouchPointInPixel;
@property (nonatomic, readonly) NSInteger xStartPointInPixel;
@property (nonatomic, readonly) NSInteger yStartPointInPixel;
@property (nonatomic, assign) PKKeyboardId keyboardId;


-(instancetype)initWithKey:(KeyModel*)model andPoint:(CGPoint)point touch:(UITouch*)touch;
- (void)updatePoint:(CGPoint)point;
- (void)updateModel:(KeyModel*)model;
- (EngineShiftMode)shiftMode;

@end
