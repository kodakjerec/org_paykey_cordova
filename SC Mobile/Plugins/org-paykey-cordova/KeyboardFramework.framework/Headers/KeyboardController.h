//
//  KeyboardController.h
//  PayKeyboard
//
//  Created by alon muroch on 12/04/2016.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKConstants.h"

#import "PKInputTextState.h"
#import "PKKeyboardDelegate.h"
#import "PKKeyboardEventsProtocol.h"
#import "PKKeyboardLayoutSwitchingProtocol.h"
#import "PKKeyboardOutputProtocol.h"


@protocol PKTextDocumentProxy <UITextDocumentProxy>

@optional
- (void)setInputTextStateObserver:(id <PKInputTextStateObserver> _Nonnull)observer;
- (NSLocale* _Nullable)locale;
@end


@class PKKeyboardConfig;
@class PKCurrentTextDocumentProxyProvider;
@class EnableAccessBarContainer;
@class SuggestionsBar;
@class EnableAccessBar;
@protocol PKKeyboardOutputProtocol;
@protocol OpenSettingsViewDelegate;

@interface KeyboardController : UIInputViewController <PKInputTextStateObserver,PKKeyboardDelegate,PKCurrentTextDocumentProxyProvider>

- (Class _Nonnull)protocolImpl;
- (void)openUrl:(NSURL * _Nonnull)url;
- (void)didSelectExtraButton;
- (NSString* _Nullable)dumpDictionary:(int)index;
- (void)clearUserHistoryDictionary;

@property (nonatomic, assign, readonly) BOOL isOpenAccessGranted;
@property (nonatomic, assign, readonly) id<OpenSettingsViewDelegate> __nullable openSettingsViewDelegate;
@property (nonatomic, weak)  id<PKKeyboardEventsProtocol> __nullable eventsDelegate;
@property (nonatomic, weak)  id<PKKeyboardOutputProtocol> __nullable keyboardOutputProtocol;

@property (nonatomic, weak, readonly) id <PKKeyboardLayoutSwitchingProtocol> __nullable keyboardSwitching;
@property (nonatomic, strong) UIView *__nullable keyboardOverlay;
@property (nonatomic, assign) KeyboardMode mode;
@property (nonatomic, weak, readonly) id <PKTextDocumentProxy> __nullable currentTextDocumentProxy;


@end
