//
//  AlphabetInputView.h
//  PayKeyboard
//
//  Created by Alex Kogan on 16/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "InputView.h"

@interface AlphabetInputView : InputView

- (void)addMoreSuggestionView:(UIView*)view;
- (void)setSpaceTitle:(NSString*)title;

@end
