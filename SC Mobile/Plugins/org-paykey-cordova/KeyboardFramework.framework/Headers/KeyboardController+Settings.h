//
//  KeyboardController+Settings.h
//  PayKeyboard
//
//  Created by Alex Kogan on 13/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "KeyboardController.h"

@class PKPaymentButtonAttributes;
@class PKSizeClass;

@interface KeyboardController (Settings)

-(KeyboardController*)enablePaymentIcon:(BOOL)enabled;
-(void)setPaymentButtonAttributes:(PKPaymentButtonAttributes*)attributes;
-(KeyboardController*)setReturnButtonEnabled:(BOOL)enabled;

@end
