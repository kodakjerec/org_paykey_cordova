//
//  KeyboardSelectionMenu.h
//  PayKeyboard
//
//  Created by Alex Kogan on 14/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeyboardSelectionMenuProtocol.h"


@class PKTouchEvent;
@class KeyboardSelectionMenuLayout;

@interface KeyboardSelectionMenuView : UIView

+(instancetype)create:(NSArray*)menuItems
        contanierView:(UIView*)containerView
               layout:(KeyboardSelectionMenuLayout*)layout;

@property(nonatomic, weak) id<KeyboardSelectionMenuProtocol> delegate;
-(void)showMenu;
-(void)dissmisMenu;

- (void)onTouchMoved:(PKTouchEvent*)event;
- (void)onTouchEnded:(PKTouchEvent*)event;

@end
