//
//  KeyboardController_Internal.h
//  PayKeyboard
//
//  Created by Alex Kogan on 06/07/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "KeyboardController.h"
#import "InputLogic.h"
#import "SuggestLogic.h"
#import "InputEventProcessor.h"
#import "InputCommandFactory.h"
#import "KeyboardViewModel.h"
#import "Keyboard.h"
#import "AlphabetInputView.h"
#import "KeyboardSwitcher.h"

#import "EnableAccessBarContainer.h"
#import "PKKeyboardDelegate.h"
#import "PKKeyboardViewFactory.h"
#import "PKKeyboardEventsProtocol.h"
#import "PKKeyboardOutputProtocol.h"
#import "PKKeyboardConfig.h"
#import "KeyboardSelectionMenuView.h"
#import "PKLanguage.h"
#import "PKAnalyticsEvent.h"
#import "PKAnalyticsConsts.h"
#import "NSString+Character.h"
#import "NSString+EXT.h"
#import "NSUserDefault+EXT.h"
#import "PKSuggestionBarTouchEvent.h"
#import "ArtView.h"
#import "SuggestionsBarBase.h"
#import "QuickActionsBar.h"
#import "DownloadDictionariesTopBar.h"

@class ArtReturnKeyModel;
@class ArtView;

@interface KeyboardController()<KeyboardViewModelDelegate,
PKKeyboardDelegate,
EnableAccessBarDelegate,
ArtViewDelegate,
SuggestionsBarDelegate,
QuickActionBarDelegate,
DownloadDictionariesTopBarDelegate>

@property (nonatomic, strong, readonly) InputLogic * inputLogic;
@property (nonatomic, strong, readonly) InputEventProcessor* inputEventProcessor;
@property (nonatomic, strong, readonly) KeyboardViewModel * keyboardViewModel;
@property (nonatomic, strong, readonly) KeyboardSwitcher * keyboardSwitcher;
@property (nonatomic, strong, readonly) AlphabetInputView * kb;
@property (nonatomic, strong, readonly) InputView * numericKB;
@property (nonatomic, strong, readonly) ArtView * artView;
@property (nonatomic, strong, readonly) PKInputTextState* inputTextState;
@property (nonatomic, strong, readonly) SuggestLogic* suggestLogic;
@property (nonatomic, strong, readonly) InputCommandFactory *commandFactory;
@property (nonatomic, strong) PKKeyboardViewFactory* keyboardViewFactory;
@property (nonatomic, strong) id<InputLogicPlanner> inputPlanner;
@property (nonatomic, strong) NSUserDefaults * localDefaults;
@property (nonatomic, assign) BOOL viewWillDisppearCalled;
@property (nonatomic, assign) BOOL didLoadKeyboard;
@property (nonatomic, strong) PKKeyboardConfig * keyboardConfig;
@property (nonatomic, strong) KeyboardSelectionMenuView* menu;
@property (nonatomic, assign) BOOL downloadDictionariesTopBarVisible;
@property (nonatomic, strong) SuggestionsBar* suggestionsBar;
@property (nonatomic, strong) EnableAccessBar* messageBar;


- (void)resetInputPlanner;
+ (SuggestionEngine*)suggestionEngine;

- (void)insertText:(NSString*) text;
- (void)deleteBackward;
- (struct InputProcessorConfig)inputProcessorConfig;
- (void)openSettings;
- (void)didSelectExtraButton;
- (void)didPressExtraActionKey;
- (void)setCurrentLanguage:(PKLanguage*)currentLanguage;
- (void)globeButtonTapped;
- (void)returnFromEmojiTapped:(ArtReturnKeyModel*)model;
- (void)updateSuggestionEngine;

@end
