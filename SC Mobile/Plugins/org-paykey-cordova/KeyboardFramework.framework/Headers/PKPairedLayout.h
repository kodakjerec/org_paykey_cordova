//
//  PKAboveLayout.h
//  PayKeyboard
//
//  Created by Jonathan Beyrak-Lev on 06/09/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKLayout.h"
#import "PKOffsetStrategy.h"

@interface PKPairedLayout : PKLayout

+ (instancetype)pair:(PKLayout *)first with:(PKLayout *)second;
+ (instancetype)pair:(PKLayout *)first with:(PKLayout *)second using:(PKOffsetStrategy *)offset;

@end
