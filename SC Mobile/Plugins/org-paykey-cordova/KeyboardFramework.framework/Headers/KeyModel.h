//
//  KeyModel.h
//  KeyboardRefactor
//
//  Created by Sumit on 23/01/17.
//  Copyright © 2017 Alex Kogan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PKConstants.h"
#import "PKLocalization.h"
#import "NSString+Key.h"
#import "KeyModelText.h"

@class PKSizeClass;
@class KeyAuxModel, KeyView;

@interface KeyModel : NSObject<PKLocalization,NSCopying>

@property (nonatomic, strong) UIImage *icon;
@property (nonatomic, strong) UIImage *iconHighlighted;
@property (nonatomic, strong) UIImage *iconSelected;


@property (nonatomic, assign) UIEdgeInsets keyInsets;

@property (nonatomic,assign) CGAffineTransform labelTransformTranslation;
@property (nonatomic,assign) CGAffineTransform labelTransformScale;
@property (nonatomic,assign) CGSize labelShadowOffset;
@property (nonatomic,strong) UIColor* labelShadowColor;
@property (nonatomic, readonly) BOOL useSpecificLabelLayout;

@property (nonatomic, strong) NSString *accessability;
@property (nonatomic, strong) NSString *displayText;


@property (nonatomic, assign) BOOL disablePressEffect;
@property (nonatomic, assign) BOOL showPressEffect;
@property (nonatomic, assign) BOOL forceLowecaseAlways;

@property (nonatomic, assign) BOOL pressOnTouchBegin;
@property (nonatomic, assign) BOOL canFloatToNewKey;
@property (nonatomic, assign) BOOL canFloatFromOtherKey;

@property (nonatomic, assign) NSInteger tapCount;
@property (nonatomic, assign) BOOL hidden;
@property (nonatomic, assign) BOOL enabled;
@property (nonatomic, assign) BOOL selected;
@property (nonatomic, assign) BOOL isAux;
@property (nonatomic, assign) CGFloat alpha;
@property (nonatomic, assign) float ratio;
@property (nonatomic, assign) int32_t code;

@property (nonatomic, assign) CGFloat x; // in pixels
@property (nonatomic, assign) CGFloat y; // in pixels
@property (nonatomic, assign) CGFloat width; // in pixels
@property (nonatomic, assign) CGFloat height; // in pixels
@property (nonatomic, assign) CGFloat cornerRadius;
@property (nonatomic, strong) UIFont * keyTextFont;
@property (nonatomic, strong) UIFont * keyLowercaseFont;
@property (nonatomic, strong) UIFont * letterOrNumberKeyFont;
@property (nonatomic, strong) UIFont * hoverFont;
@property (nonatomic, assign) CGFloat shadowHeight;

@property (nonatomic, strong) UIColor *keyColor;
@property (nonatomic, strong) UIColor *keyHighlightedColor;
@property (nonatomic, strong) UIColor *keySelectedColor;

@property (nonatomic, strong) UIColor *keyTextColor;
@property (nonatomic, strong) UIColor *keyTextHighlightedColor;
@property (nonatomic, strong) UIColor *shadowColor;

@property (nonatomic, strong) KeyModelText *txt;
@property (nonatomic, strong) NSArray *aux;

@property (nonatomic, strong) PKSizeClass* iconSize;
@property (nonatomic, assign) CGSize imageSize;
@property (nonatomic, assign) PKCapslockMode capslockMode;
@property (nonatomic, assign) KeyType keyType;
@property (nonatomic, strong) NSLocale *locale;

@property (nonatomic, assign) CGFloat keyLabelLowercaseHorizontalCenterCoeficient;
@property (nonatomic, assign) CGFloat keyLabelUppercaseHorizontalCenterCoeficient;

@property (nonatomic, assign) PKKeyboardId keyboardId;
@property (nonatomic, assign) BOOL longPressEnabled;
@property (nonatomic, assign) NSTimeInterval longPressInterval;
@property (nonatomic, assign) NSTimeInterval longLongPressInterval;
@property (nonatomic, assign) NSTimeInterval doubleTapInterval;
@property (nonatomic, assign) BOOL doubleTapEnabled;
@property (nonatomic, readonly) NSArray *diversity;
@property (nonatomic, readonly) NSString *hashKey;
@property (nonatomic, readonly) Class keyViewClass;

- (instancetype)initWithKeyType:(KeyType)keyType txt:(NSString *)txt icon:(UIImage *)icon;
- (instancetype)initWithKeyType:(KeyType)keyType txt:(NSString *)txt;
- (instancetype)initWithKeyType:(KeyType)keyType;
- (NSString *)displayText;
- (NSString*)name;
- (UIFont*)font;
- (void)addAuxModel:(KeyModel *)model;

@end
