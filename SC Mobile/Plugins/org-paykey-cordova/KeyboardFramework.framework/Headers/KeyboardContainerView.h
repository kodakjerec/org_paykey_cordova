//
//  KeyboardViewContainer.h
//  PayKeyboard
//
//  Created by ishay weinstock on 19/09/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeyboardContainerProtocol.h"

@class KeyboardContainerView;

@interface KeyboardContainerView : UIView

@property (nonatomic,weak) id<KeyboardContainerProtocol> delegate;

@end
