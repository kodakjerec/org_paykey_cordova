//
//  KeyboardLayoutRowParams.h
//  PayKeyboard
//
//  Created by Alex Kogan on 11/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KeyboardLayoutRowParams : NSObject

-(_Nullable instancetype)initWithKeys:(NSArray *__nonnull )keys offsetX:(CGFloat)offset;

@property(nonatomic, strong)NSArray* __nullable keys;

-(void)startLayout:(CGFloat)keyboardWidth keyboardHeight:(CGFloat)keyboardHeight isPortrait:(BOOL)isPortrait;

-(CGFloat)getKeyX;
-(CGFloat)getKeyY;
-(CGFloat)getKeyWidth:(int)position;
-(CGFloat)getKeyHeight;
-(void)advanceXPos:(CGFloat)width;
-(void)setRowIndex:(int)row;
@end
