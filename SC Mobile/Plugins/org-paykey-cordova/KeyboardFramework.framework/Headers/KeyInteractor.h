//
//  KeyInteractor.h
//  PayKeyboard
//
//  Created by ishay weinstock on 14/09/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKTouchEvent.h"

@interface KeyInteractor : NSObject

//All perform methods return YES if event should cancel the rest of the handling
- (BOOL)preformLongTouch:(LongTouchStatus)longTouchStatus;
- (BOOL)preformTouchMoved:(PKTouchEvent*)touchEvent;
- (BOOL)preformTouchBegin:(PKTouchEvent*)touchEvent;
- (BOOL)preformTouchEnded:(PKTouchEvent*)touchEvent;

@end
