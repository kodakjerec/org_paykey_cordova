//
//  KoreanInput.h
//  PayKeyboard
//
//  Created by Jonathan Beyrak-Lev on 21/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(KoreanComposition)

- (NSString *)composed;
- (NSString *)decomposed;
- (NSString *)lastCharacter;
- (BOOL)isKoreanCharacter;
- (BOOL)canBeComposed;
- (BOOL)canBeDecomposed;
@end

@interface KoreanInput : NSObject

- (NSString *)syllableWithOpeningConsonant:(NSString *)opening vowel:(NSString *)vowel;
- (NSString *)syllableWithOpeningConsonant:(NSString *)opening vowel:(NSString *)vowel closingConsonant:(NSString *)closing;
- (NSString *)syllableWithSyllable:(NSString *)syllable closingConsonant:(NSString *)closing;
- (NSString *)syllableComponentsFromSyllable:(NSString *)syllable;

@end
