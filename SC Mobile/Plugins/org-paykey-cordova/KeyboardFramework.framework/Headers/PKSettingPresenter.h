//
//  PKSettingPresenter.h
//  PayKeyboard
//
//  Created by Jonathan Beyrak-Lev on 13/07/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKLocalizableTableViewCell.h"

@interface PKSettingPresenter : NSObject

@property (readonly) NSString *subtitle;

- (instancetype)initWithSubtitle:(NSString *)subtitle;
- (PKLocalizableTableViewCell *)cellForTableView:(UITableView *)tableView;
- (void)select;

@end
