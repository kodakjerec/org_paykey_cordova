//
//  NSString+Key.h
//  PayKeyboard
//
//  Created by Alex Kogan on 26/04/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
@class KeyModel;

@interface NSString (Key)

+(NSString*)keyName:(int32_t)code displayName:(NSString*)displayName;

@end
