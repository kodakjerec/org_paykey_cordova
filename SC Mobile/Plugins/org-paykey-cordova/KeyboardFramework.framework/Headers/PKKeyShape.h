//
//  PKKeyShape.h
//  PayKeyboard
//
//  Created by Jonathan Beyrak-Lev on 04/09/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKFrame.h"

@interface PKKeyShape : NSObject

@property (nonatomic) BOOL flowsInline;
@property (nonatomic) CGFloat centeringSize;
@property (nonatomic, weak) NSArray *line;
@property (nonatomic) BOOL center;
@property (nonatomic) CGFloat boardWidth;
@property (nonatomic) CGFloat baselineMargin;
@property (nonatomic) CGFloat yPosition;
@property (nonatomic) CGFloat height;
@property (nonatomic) CGFloat width;

- (BOOL)canFollowInLine:(NSArray *)line;

- (PKFrame *)frame;

@end
