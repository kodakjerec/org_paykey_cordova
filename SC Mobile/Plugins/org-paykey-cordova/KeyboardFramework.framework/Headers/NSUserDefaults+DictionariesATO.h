//
//  NSUserDefaults+DictionariesATO.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 14/04/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKEnums.h"
NS_ASSUME_NONNULL_BEGIN

@interface NSUserDefaults (DictionariesATO)

@property (nonatomic,assign) PKDictionariesOTAAutoDownloadType dictionariesOTAAutoDownloadType;
@property (nonatomic,assign) NSNumber *dictionariesOTAIntervalToUpdate;

@end

NS_ASSUME_NONNULL_END
