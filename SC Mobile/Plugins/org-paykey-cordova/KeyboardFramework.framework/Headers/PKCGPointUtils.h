//
//  PKCGPointUtils.h
//  PayKeyboard
//
//  Created by Alex Kogan on 10/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PKCGPointUtils : NSObject

+(CGFloat)squaredDistanceToEdge:(CGRect)rect toPoint:(CGPoint)point;
+(CGFloat)distanceToRect:(CGRect)rect fromPoint:(CGPoint)point;
+(CGFloat)horizontalDistanceTo:(CGPoint)to from:(CGPoint)from;
+(BOOL)isParallelInVertical:(CGRect)rect fromPoint:(CGPoint)point;
+(BOOL)isParallelInHorizontal:(CGRect)rect fromPoint:(CGPoint)point;
+(CGFloat)distanceToPoint:(CGPoint)pointB fromPoint:(CGPoint)pointA;
@end
