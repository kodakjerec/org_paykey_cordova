//
//  PKMessageDispatcher.h
//  PayKeyboard
//
//  Created by Alex Kogan on 13/07/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKMessage.h"

@protocol PKMessageDispatcherProtocol <NSObject>

-(void)handleMessage:(PKMessage*)message;

@end

@interface PKMessageDispatcher : NSObject

-(void)dipatchMessge:(PKMessage*)message;
@property (nonatomic, weak) id<PKMessageDispatcherProtocol> delegte;

@end
