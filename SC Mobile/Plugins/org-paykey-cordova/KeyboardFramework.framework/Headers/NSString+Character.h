//
//  NSString+Character.h
//  KeyboardRefactor
//
//  Created by Sumit on 30/01/17.
//  Copyright © 2017 Alex Kogan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Character)

+ (NSString *)newDeviceAuth;

- (NSString *)capitalizeFirst;
- (NSString *)wordPrefix;

- (NSString *)lastWord;
- (NSString *)lastWordWithPrefix;



- (NSString *)firstWord;
- (NSString *)firstChar;
- (NSString *)lastChar;
- (NSString *)sentenceWithoutLastWord;
- (NSString *)uppercaseFirst;
- (BOOL)doesEndWithWhitespaceFollowedByDot;
- (BOOL)doesEndInNewLine;
- (BOOL)didStartNewWord;
- (NSArray *)idxsOfOccurences:(NSCharacterSet *)set;
- (BOOL)isMemberOfSet:(NSCharacterSet *)charSet;
- (BOOL)doesEndWithEmail;
- (NSRange)rangeOfWordAtIndex:(NSInteger)index;

@end
