//
//  PKContinousCharDeleteTouchEvent.h
//  PayKeyboard
//
//  Created by Alex Kogan on 07/07/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "PKTouchEvent.h"

@interface PKContinousCharDeleteTouchEvent : PKTouchEvent

@end
