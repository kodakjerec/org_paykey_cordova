//
//  EnableAccessModel.h
//  PayKeyboard
//
//  Created by Marat  on 3/15/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKLocalization.h"

@interface EnableAccessModel : NSObject <PKLocalization>

-(instancetype)initWithKeyboardName:(NSString*)keyboardName;

@property(nonatomic) NSString *keyboardName;
@property(nonatomic) NSString *descriptionText;

@end
