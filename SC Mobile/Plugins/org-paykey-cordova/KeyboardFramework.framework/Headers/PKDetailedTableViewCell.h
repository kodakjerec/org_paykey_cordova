//
//  PKDetailedTableViewCell.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 15/04/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

#import "PKLocalizableTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface PKDetailedTableViewCell : PKLocalizableTableViewCell

@end

NS_ASSUME_NONNULL_END
