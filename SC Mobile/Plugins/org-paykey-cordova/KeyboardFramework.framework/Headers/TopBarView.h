//
//  TopBarView.h
//  PayKeyboard
//
//  Created by alon muroch on 12/04/2016.
//  Copyright © 2016 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKEnums.h"

@protocol TopBarViewDelegate
- (void)onPaymentButton;
- (BOOL)isPaymentButtonEnabled;
@end

@class TopBarConfig;
@class PKPaymentButtonAttributes;

@interface TopBarView : UIView

/**
 UI configuration of TopBarView. This property shouldn't carry any content data inside.
 Use ViewModel pattern in order to pass content data. (See example how SuggestionBarViewModel implements this)
 */
@property (nonatomic, strong) TopBarConfig *config;

@property (nonatomic, weak) id<TopBarViewDelegate> delegate;
/**
 Used as a container for all subviews added to TopBarView
 @note All subviews need to be added through this property only
 */
@property (nonatomic, strong) UIView *contentView;

/**
 For proper configuration of TopBar all subclasses should call
 through this initializaer

 @param config Configuration class for top bar which acts like a ViewModel.
 @return An initialized TopBarView object.
 */
- (instancetype)initWithConfig:(TopBarConfig *)config;

@end
