//
//  NSException+PKExceptions.h
//  PayKeyboard
//
//  Created by Alex Kogan on 05/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, PKCustomException) {
    // Thrown when placeholder or "abstract" methods are called directly
    PKUnimplementedMethodException,
    
    // Thrown when forbidden methods are called
    PKForbiddenMethodException,
    
    // Thrown for invalid controllers
    PKInvalidControllerException,
    
    
    PKMissingParametersException,
};

@interface NSException (PKExceptions)

+ (nonnull NSException *)exceptionWithType:(PKCustomException)exceptionType reason:(nullable NSString *)reason userInfo:(nullable NSDictionary *)userInfo;
+ (nonnull NSException *)exceptionWithType:(PKCustomException)exceptionType reason:(nullable NSString *)reason;
+ (nonnull NSException *)exceptionWithType:(PKCustomException)exceptionType;

@end
