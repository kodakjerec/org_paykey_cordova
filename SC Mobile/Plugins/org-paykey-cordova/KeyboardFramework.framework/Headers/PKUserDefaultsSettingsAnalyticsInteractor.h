//
//  PKUserDefaultsSettingsAnalyticsInteractor.h
//  PayKeyboard
//
//  Created by Marat  on 9/3/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "PKUserDefaultsSettingsInteractor.h"

@protocol PKKeyboardEventsProtocol;

@interface PKUserDefaultsSettingsAnalyticsInteractor : PKUserDefaultsSettingsInteractor

@property (nonatomic,weak)id<PKKeyboardEventsProtocol>eventDelegate;

@end
