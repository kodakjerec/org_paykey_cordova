//
//  KBMenuItemCell.h
//  PayKeyboard
//
//  Created by Eden Landau on 07/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KBMenuItemCell : UITableViewCell

@property (nonatomic, strong) NSString *labelText;
@property (nonatomic, strong) UIImage *iconImage;
@property (nonatomic, assign) BOOL showSeparator;

@end
