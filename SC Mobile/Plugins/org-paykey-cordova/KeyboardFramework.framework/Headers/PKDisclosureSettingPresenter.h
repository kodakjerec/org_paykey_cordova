//
//  PKDisclosureSettingPresenter.h
//  PayKeyboard
//
//  Created by Jonathan Beyrak-Lev on 13/07/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "PKSettingPresenter.h"

@interface PKDisclosureSettingPresenter : PKSettingPresenter

- (instancetype)initWithSubtitle:(NSString *)subtitle title:(NSString *)title detail:(NSString *)detail selectionHandler:(void (^)(void))selectionHandler;

@end
