//
//  EmojiHeader.h
//  EmojiUI
//
//  Created by Sumit on 14/02/17.
//  Copyright © 2017 Alex Kogan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmojiHeader : UICollectionViewCell

@property (strong, nonatomic) UILabel *lbl;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) UIFont *labelFont;

- (void)commonInit;

@end
