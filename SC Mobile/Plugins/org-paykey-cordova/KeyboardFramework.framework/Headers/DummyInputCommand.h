//
//  DummyInputCommand.h
//  PayKeyboard
//
//  Created by Alex Kogan on 05/07/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "InputCommand.h"

@interface DummyInputCommand : InputCommand

@end
