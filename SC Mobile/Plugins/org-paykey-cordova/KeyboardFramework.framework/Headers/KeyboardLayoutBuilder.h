//
//  KeyboardLayoutParser.h
//  PayKeyboard
//
//  Created by Alex Kogan on 10/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PKEnums.h"
#import "PKKeyboardLayoutBuilderProtocol.h"

@class KeyboardModel;
@class PKLanguage;
@class KeyboardId;
@class KeyModel;

@interface KeyboardLayoutBuilder : NSObject

@property (nonatomic, weak) id<PKKeyboardLayoutBuilderProtocol> delegate;

-(KeyboardModel*)buildNumeric:(UIKeyboardAppearance)keyboardAppearance
 showExtraActionButtonInPortrait:(BOOL)showExtraActionButtonInPortrait
  isPortrait:(BOOL)isPortrait;

-(KeyboardModel*)build:(PKKeyboardId)keyboardId
                layout:(NSString*)layoutName
              language:(PKLanguage*)language
            keyboardId:(KeyboardId*)keyboardId
          keyboardType:(UIKeyboardType)keyboardType
    keyboardAppearance:(UIKeyboardAppearance)keyboardAppearance
            returnType:(UIReturnKeyType)returnType
            isPortrait:(BOOL)isPortrait
            isFlowOpen:(BOOL)isFlowOpen
showExtraActionButtonInPortrait:(BOOL)showExtraActionButtonInPortrait;

//Convenience initialize
- (KeyboardModel*)build:(KeyboardId*)keyboardId
           keyboardType:(UIKeyboardType)keyboardType
             returnType:(UIReturnKeyType)returnType;

//Tests Public
-(KeyModel*)backspace;





@end
