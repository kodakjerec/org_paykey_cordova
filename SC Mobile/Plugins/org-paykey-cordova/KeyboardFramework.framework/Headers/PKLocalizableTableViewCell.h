//
//  PKLocalizableTableViewCell.h
//  PayKeyboard
//
//  Created by Alex Kogan on 01/06/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PKLocalizableTableViewCellDelegate <NSObject>

-(NSString *)localizedString:(NSString *)key;

@end

@interface PKLocalizableTableViewCell : UITableViewCell

@property (nonatomic, weak) id<PKLocalizableTableViewCellDelegate> localizagionDelegate;

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier;
- (NSString *)localizedString:(NSString *)key;

@end
