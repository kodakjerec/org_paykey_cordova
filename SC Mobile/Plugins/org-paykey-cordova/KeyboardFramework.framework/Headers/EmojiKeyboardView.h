//
//  EmojiKeyboardView.h
//  EmojiUI
//
//  Created by Sumit on 13/02/17.
//  Copyright © 2017 Alex Kogan. All rights reserved.
//

#import "KeyboardViewBase.h"
#import "ArtContentViewProtocol.h"
@class KeyboardModel;
@class ArtReturnKeyModel;
@class EmojiKeyboardModel, EmojiFlowLayout;

@interface EmojiKeyboardView : KeyboardViewBase <ArtContentViewProtocol>

@end
