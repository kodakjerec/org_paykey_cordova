//
//  UILabel+TextApproximatedFont.h
//  PayKeyboard
//
//  Created by Marat  on 1/4/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (TextApproximatedFont)

-(CGFloat)getApproximateAdjustedFontSizeWithLabelWithAttributes:(NSDictionary *)attributes;
-(UIFont*)getApproximatedFontForLabel;
@end
