//
//  PKOpenUrlUtil.h
//  PayKeyboard
//
//  Created by Alex Kogan on 08/08/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PKOpenUrlUtil : NSObject

+ (BOOL)openUrl:(NSURL *)url withController:(UIInputViewController*)controller;

@end
