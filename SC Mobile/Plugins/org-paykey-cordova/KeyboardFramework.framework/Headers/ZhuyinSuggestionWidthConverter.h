//
//  SuggestionWordSizeConverter.h
//  KeyboardFramework
//
//  Created by German Velibekov on 06/12/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

NS_ASSUME_NONNULL_BEGIN

@interface ZhuyinSuggestionWidthConverter : NSObject

- (instancetype)initWithSuggestions:(NSArray *)suggestions
                              width:(CGFloat)totalWidth;

-(CGFloat)suggestionWidthAtIndex:(int)index;

- (NSArray *)suggestionWidths;

@end

NS_ASSUME_NONNULL_END
