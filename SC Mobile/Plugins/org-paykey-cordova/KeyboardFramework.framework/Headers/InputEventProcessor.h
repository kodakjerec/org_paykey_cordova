//
//  InputLogicResultProcessor.h
//  PayKeyboard
//
//  Created by Alex Kogan on 06/07/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SuggestLogic.h"
@class InputLogicResult;
@class PKTouchEvent;
@class PKLanguage;
@class InputLogic;


struct InputProcessorConfig {
    BOOL suggestionsEnabled;
    BOOL emojiSuggestionsEnabled;
    BOOL autoCorrectionEnabled;
    BOOL keyboardSupportSuggestion;
    BOOL keyboardSupportAutoCorrection;
    BOOL periodOnDoubleSpace;
    BOOL isContinousBackspce;
};

typedef struct InputProcessorConfig InputProcessorConfig;

@protocol InputLogicResultProcessorProtocol <SuggestDelegate>

- (void)autoLearnWord:(NSString*)word sentence:(NSString*)sentence wasAutoCapitalized:(BOOL)wasAutoCapitalized;
- (void)autoUnlearnWord:(NSString*)word sentence:(NSString*)sentence;
- (void)setAlphabetKeyboard;
- (void)setSpaceTitle:(NSString*)string;
@end

@interface InputEventProcessor : NSObject<SuggestDelegate>

-(instancetype)initWithSuggestLogic:(SuggestLogic*)suggestLogic
                         inputLogic:(InputLogic*)inputLogic;

-(void)processEvent:(PKTouchEvent*)event
           language:(PKLanguage*)language
             config:(struct InputProcessorConfig)config;

-(void)processCursorMoved:(struct InputProcessorConfig)config;

- (void)callSuggestion:(struct InputProcessorConfig)config;

@property (nonatomic, weak) id<InputLogicResultProcessorProtocol> delegate;

@end

