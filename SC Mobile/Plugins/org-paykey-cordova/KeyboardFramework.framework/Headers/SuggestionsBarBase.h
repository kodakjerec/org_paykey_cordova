//
//  SuggestionsBarBase.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 26/09/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

#import "TopBarView.h"
#import "SuggestionsViewDelegate.h"
#import "PKInputTextState.h"
#import "SuggestionsBarConfig.h"
@class PKSuggestionBarTouchEvent;

@protocol SuggestionsBarDelegate <SuggestionsViewDelegate, TopBarViewDelegate>

@end


@class SuggestionsBarViewModel;
@class SuggestionContainer;

@interface SuggestionsBarBase : TopBarView <PKInputTextStateObserver>

- (instancetype)initWithConfig:(SuggestionsBarConfig*)config;

- (void)clearSuggestions;
- (void)setViewModel:(SuggestionsBarViewModel*)viewModel;
- (void)update;


@property (nonatomic, weak) id<SuggestionsBarDelegate> delegate;
@property (nonatomic, assign) PKWordTypedSource wordSource;
@property (nonatomic, strong) SuggestionContainer *suggestionContainer;

//protected

- (Class)suggestionContainerClass;
@end

