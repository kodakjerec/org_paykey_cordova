//
//  KeyboardLayoutSet.h
//  PayKeyboard
//
//  Created by Alex Kogan on 11/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKEnums.h"
#import "PKKeyboardLayoutBuilderProtocol.h"

@class PKLanguage;
@class Keyboard;
@class KeyboardViewModel;

@interface KeyboardLayoutSet : NSObject

-(instancetype)initWithLanguage:(PKLanguage*)language
             keyboardAppearance:(UIKeyboardAppearance)keyboardAppearance
                  returnKeyType:(UIReturnKeyType)returnKeyType
                   keyboardType:(UIKeyboardType)keyboardType
              keyboardViewModel:(KeyboardViewModel*)keyboardViewModel
  keyboardLayoutBuilderProtocol:(id<PKKeyboardLayoutBuilderProtocol>)keyboardLayoutBuilderProtocol
           paykeyButtonLocation:(PKPayButtonLocation)paykeyButtonLocation;

-(Keyboard*)getKeyboard:(PKKeyboardId)keyboardId;

@property(nonatomic,assign)UIKeyboardAppearance keyboardAppearance;
@property(nonatomic, strong)NSMutableDictionary* keyboards;
@property (nonatomic) BOOL showExtraActionButtonInPortrait;

@end
