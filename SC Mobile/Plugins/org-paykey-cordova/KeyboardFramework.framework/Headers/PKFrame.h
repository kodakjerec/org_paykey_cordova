//
//  PKFrame.h
//  PayKeyboard
//
//  Created by Jonathan Beyrak-Lev on 04/09/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PKFrame : NSObject

@property (nonatomic) CGFloat xPosition;
@property (nonatomic) CGFloat yPosition;
@property (nonatomic) CGFloat width;
@property (nonatomic) CGFloat height;

+ (instancetype)frameWithXPosition:(CGFloat)xPosition width:(CGFloat)width;
+ (instancetype)frameWithXPosition:(CGFloat)xPosition yPosition:(CGFloat)yPosiiton width:(CGFloat)width height:(CGFloat)height;

- (instancetype)addingYPosition:(CGFloat)yPosition height:(CGFloat)height;
- (BOOL)isHorizontallyEqual:(PKFrame *)f;
- (BOOL)isVerticallyEqual:(PKFrame *)f;
- (NSString *)horizontalDescription;
- (NSString *)verticalDescription;
- (CGFloat)maxX;
- (CGFloat)maxY;

@end
