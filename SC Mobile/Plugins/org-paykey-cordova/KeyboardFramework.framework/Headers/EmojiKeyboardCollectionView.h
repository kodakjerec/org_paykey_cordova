//
//  EmojiKeyboardCollectionView.h
//  PayKeyboard
//
//  Created by ishay weinstock on 19/09/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeyboardContainerProtocol.h"

@interface EmojiKeyboardCollectionView : UICollectionView

@property (nonatomic,weak) id<KeyboardContainerProtocol> kbContainerDelegate;

@end
