//
//  NLPControllerResult.h
//  PayKeyboard
//
//  Created by Mac on 30/04/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NLPControllerResult : NSObject

@property (nonatomic, strong) NSArray* suggestions;
@property (nonatomic, assign) BOOL isAutoCorrection;
@property (nonatomic, assign) BOOL isInDictionary;

@end
