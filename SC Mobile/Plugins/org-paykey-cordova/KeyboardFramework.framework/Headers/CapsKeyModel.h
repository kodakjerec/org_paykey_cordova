//
//  CapsKeyModel.h
//  KeyboardRefactor
//
//  Created by Sumit on 23/01/17.
//  Copyright © 2017 Alex Kogan. All rights reserved.
//

#import "KeyModel.h"

@interface CapsKeyModel : KeyModel

@property (nonatomic, strong) UIImage *iconLocked;
@property (nonatomic, strong) UIImage *iconUppercase;
@property (nonatomic, strong) UIImage *iconLowercase;

@property (nonatomic, strong) UIColor *colorLocked;
@property (nonatomic, strong) UIColor *colorUppercase;
@property (nonatomic, strong) UIColor *colorLowercase;
@property (nonatomic, assign) BOOL isLocked;

- (instancetype)initWithKeyType:(KeyType)keyType txt:(NSString *)txt languageType:(PKLanguageType)languageType;

@end
