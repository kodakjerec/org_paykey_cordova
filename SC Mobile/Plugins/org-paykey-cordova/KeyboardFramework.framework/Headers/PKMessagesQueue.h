//
//  PKMessagesQueue.h
//  PayKeyboard
//
//  Created by Alex Kogan on 13/07/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKMainQueueMessageDispatcher.h"

@interface PKMessagesQueue : NSObject

-(instancetype)initWithMessageDispatcher:(PKMessageDispatcher*)dispatcher;
    
-(void)addMessage:(PKMessage*)messge;
-(void)addMessageWithDelay:(double)delta message:(PKMessage*)message;
-(void)removeAllMessagesFromType:(NSUInteger)type;
@end
