//
//  MoreSuggestionView.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 30/09/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

#import "KeyboardViewBase.h"
#import "SuggestionsViewDelegate.h"

@class KeyboardModel;

@interface MoreSuggestionView : KeyboardViewBase

@property (nonatomic,strong) NSArray *moreSuggestions;
@property (nonatomic,weak) id<SuggestionsViewDelegate> suggestionsDelegate;

- (void)loadKeyboardModel:(KeyboardModel*)keyboardModel;

@end
