//
//  PKMessageQueue_Internal.h
//  PayKeyboard
//
//  Created by Alex Kogan on 16/07/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKMessageDispatcher.h"
#import "PKMessage.h"


@interface PKMessagesQueue()

@property (nonatomic, strong, readonly) dispatch_queue_t operationQueue;
@property (nonatomic, strong, readonly) NSMutableArray<PKMessage*>* messagesQueue;
@property (nonatomic, strong, readonly) PKMessageDispatcher* dispatcher;
@property (nonatomic, assign) BOOL isRunning;

@end
