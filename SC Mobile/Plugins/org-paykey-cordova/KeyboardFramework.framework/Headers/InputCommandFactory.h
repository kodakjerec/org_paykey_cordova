//
//  InputCommandFactory.h
//  Pods
//
//  Created by Jonathan Beyrak-Lev on 19/03/2017.
//
//

#import <Foundation/Foundation.h>
#import "InputCommand.h"

@protocol Inputtable;
@class PKTouchEvent;

@interface InputCommandFactory : NSObject

@property (weak, nonatomic) id<Inputtable> inputTarget;

- (instancetype)initWithTarget:(id<Inputtable>)inputTarget;

- (InputCommand *)commandToDelete;
- (InputCommand *)commandToInsert:(NSString *)toEnter;
- (InputCommand *)commandWithCommands:(NSArray<InputCommand*>*)commnands;
- (InputCommand *)dummyCommand;
- (InputCommand *)engineLoggerCommand:(NSString*)title;
- (InputCommand *)engineLoggerCommandWithEvent:(PKTouchEvent*)event;
@end
