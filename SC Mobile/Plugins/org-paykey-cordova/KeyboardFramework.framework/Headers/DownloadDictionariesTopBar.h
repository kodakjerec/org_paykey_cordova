//
//  DownloadDictionariesTopBar.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 10/04/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

#import "TopBarView.h"

NS_ASSUME_NONNULL_BEGIN

@protocol DownloadDictionariesTopBarDelegate <TopBarViewDelegate>

- (void)onDownloadDictionariesTopBarTapped;

@end

@interface DownloadDictionariesTopBar : TopBarView

- (instancetype)initWithConfig:(TopBarConfig *)config andTitle:(NSString*)title;

@property (nonatomic,weak) id<DownloadDictionariesTopBarDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
