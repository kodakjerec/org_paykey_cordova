//
//  KeyboardRootView.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 02/08/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KeyboardViewModel;

@interface KeyboardRootView : UIView

@property (nonatomic, strong) NSLayoutConstraint* heightConstraint;
@property (nonatomic, strong) KeyboardViewModel*  keyboardViewModel;
@property (strong,nonatomic) UIView *view;

@end
