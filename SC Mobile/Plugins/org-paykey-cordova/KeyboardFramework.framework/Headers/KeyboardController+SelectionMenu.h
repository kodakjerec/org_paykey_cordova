//
//  KeyboardController+SelectionMenu.h
//  PayKeyboard
//
//  Created by Alex Kogan on 14/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KeyboardController.h"
#import "PKKeyboardViewProtocol.h"
#import "KeyboardSelectionMenuProtocol.h"

@class KeyView;
@interface KeyboardController (SelectionMenu) <KeyboardSelectionMenuProtocol>

@end
