//
//  PKInputTextState.h
//  PayKeyboard
//
//  Created by Alex Kogan on 04/07/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PKInputTextStateObserver <NSObject>

@optional
-(void)cursorMoved:(id<UITextDocumentProxy>)textDocumentProxy;
-(void)textChanged:(id<UITextDocumentProxy>)textDocumentProxy;
-(void)textFieldTypeChanged:(id<UITextDocumentProxy>)textDocumentProxy;
-(void)textCleard:(id<UITextDocumentProxy>)textDocumentProxy;
-(void)returnTypeChanged:(id<UITextDocumentProxy>)textDocumentProxy;

@end

@protocol PKCurrentTextDocumentProxyProvider <NSObject>

- (id<UITextDocumentProxy>) currentTextDocumentProxy;

@end

@interface PKInputTextState : NSObject

@property( nonatomic, weak) id<PKCurrentTextDocumentProxyProvider> currentTextDocumentProxyProvider;
@property (nonatomic, readonly, strong) NSString* currentDocumentContextBeforeInput;
@property (nonatomic, readonly, strong) NSString* currentDocumentContextAfterInput;


-(instancetype)initWithCurrentTextDocumentProxyDelegate:(id<PKCurrentTextDocumentProxyProvider>)currentTextDocumentProxyProvider;
-(void)updateShouldNotifyObservers:(BOOL)shouldNotifyObservers;
-(NSString*)fullText;

- (void)addObserver:(id<PKInputTextStateObserver>)observer;
- (void)removeObserver:(id<PKInputTextStateObserver>)observer;

@end
