//
//  PKMainQueueMessageDispatcher.h
//  PayKeyboard
//
//  Created by Alex Kogan on 13/07/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKMessageDispatcher.h"

@interface PKMainQueueMessageDispatcher : PKMessageDispatcher

@end
