//
//  EmojiCategoryObjC.h
//  EmojiUI
//
//  Created by Sumit on 15/02/17.
//  Copyright © 2017 Alex Kogan. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger,EmojiCategory) {
    EmojiCategoryFrequentlyUsed = 0,
    EmojiCategorySmiliesAndPeople = 1,
    EmojiCategoryAnimalsAndNature = 2,
    EmojiCategoryFoodAndDrinks = 3,
    EmojiCategoryActivityAndSport = 4,
    EmojiCategoryTravelAndPlaces = 5,
    EmojiCategoryObjects = 6,
    EmojiCategorySymbols = 7,
    EmojiCategoryFlags = 8,
    EmojiCategoryNone = 9
};

@interface EmojiCategoryExtension : NSObject

+ (EmojiCategory)fromString:(NSString *)string;
+ (NSString *)rawValue:(EmojiCategory)category;
+ (EmojiCategory)categoryForSection:(NSInteger)section;

@end
