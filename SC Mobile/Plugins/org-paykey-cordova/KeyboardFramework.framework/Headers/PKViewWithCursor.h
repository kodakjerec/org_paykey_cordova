//
//  PKViewWithCursor.h
//  PayKeyUI
//
//  Created by ishay weinstock on 15/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

#define CURSOR_WIDTH 2

typedef NS_ENUM(NSInteger, PKViewCursorMode) {
    PKViewCursorModeNever,
    PKViewCursorModeWhileEditing,
    PKViewCursorModeAlways
};

typedef NS_ENUM(NSInteger, PKCursorCalculationType) {
    PKCursorCalculationTypeRect, 
    PKCursorCalculationTypeChar
};


@interface PKViewWithCursor : UIView <UIKeyInput>

@property (nonatomic, strong) UIColor *cursorColor;
@property (nonatomic) PKViewCursorMode cursorMode;
@property (nonatomic) PKCursorCalculationType cursorCalculationType;
@property (nonatomic, assign) BOOL disabled;
- (void)commonInit;
- (CGRect)labelsFrame;
- (CGFloat)calcCursorXPosition;
- (CGRect)calcCursorRect;
- (BOOL)isCursorHidden;
- (CGFloat)cursorHeight;
- (void)updateCursorPosition;
- (void)setupCursor;
- (void)beginEditing;
- (BOOL)endEditing;
- (CGRect)cursorRect;


@end
