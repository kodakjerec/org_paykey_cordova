//
//  QuickActionsBar.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 22/01/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

#import "TopBarView.h"
#import "PKEnums.h"

@class TopBarConfig;
@class QuickActionsBarViewModel;

@protocol QuickActionBarDelegate <TopBarViewDelegate>

- (void)onQuickActionAction:(PKQuickActionType)action;

@end

NS_ASSUME_NONNULL_BEGIN

@interface QuickActionsBar : TopBarView

- (instancetype)initWithConfig:(TopBarConfig *)config
                     viewModel:(QuickActionsBarViewModel *)viewModel
                      delegate:(id<QuickActionBarDelegate>)delegate;

@property (nonatomic,weak) id<QuickActionBarDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
