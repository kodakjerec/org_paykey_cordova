//
//  ZhuyinSuggestionBar.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 22/09/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

#import "SuggestionsBarBase.h"
#import "SuggestionsBarConfig.h"
#import "SuggestionsBarBase.h"

@protocol ZhuyinSuggestionsBarDelegate <SuggestionsBarDelegate>

@property (nonatomic, assign) BOOL showMoreSuggestionButtonSelectedState;

- (void)showMoreSuggestion:(NSArray*)moreSuggestion;
- (void)hideMoreSuggestion;

@end

@interface ZhuyinSuggestionBar : SuggestionsBarBase

@property (nonatomic, weak) id<ZhuyinSuggestionsBarDelegate> delegate;
@property (nonatomic, strong) SuggestionsBarConfig *config;

@end
