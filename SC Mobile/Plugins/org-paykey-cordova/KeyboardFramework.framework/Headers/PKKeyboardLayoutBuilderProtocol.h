//
//  PKKeyboardLayoutBuilderProtocol.h
//  PayKeyboard
//
//  Created by Alex Kogan on 03/08/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKEnums.h"

@class PKPaymentButtonAttributes;

@protocol PKKeyboardLayoutBuilderProtocol <NSObject>

- (PKPayButtonLocation)paykeyButtonLocation;
- (BOOL)spaceLanguageChangeBySwipeEnabled;
- (BOOL)nextInputWideButton;
- (BOOL)emojiButtonEnabled;
- (BOOL)nextInputLongPressEnabled;
- (BOOL)nextInputShowTextEnabled;
- (BOOL)enableGif;
- (BOOL)enableEmojiLayout;
- (PKPaymentButtonAttributes*)paymentButtonAttributes;


@end
