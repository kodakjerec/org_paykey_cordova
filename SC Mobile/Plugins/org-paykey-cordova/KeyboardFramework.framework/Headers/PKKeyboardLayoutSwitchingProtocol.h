//
//  PKKeyboardLayoutSwitchingProtocol.h
//  PayKeyboard
//
//  Created by Marat  on 6/1/17.
//  Copyright © 2017 Apple. All rights reserved.
//

@class PKLanguage;

@protocol PKKeyboardLayoutSwitchingProtocol <NSObject>

@optional
-(void)setAlphaKeyboardWithLang:(PKLanguage *__nonnull)lang;


-(void)setEmojiKeyboard:(PKNextKeyboardType)nextKeyboardType;
-(void)setNumericKeyboard;
-(void)setNumericKeyboard:(NSLocale *__nonnull)locale
       keyboardAppearance:(UIKeyboardAppearance)keyboardAppearance;
-(void)setNumericKeyboardNoDecimalSeperator:(UIKeyboardAppearance)keyboardAppearance;
-(void)setSymbolsKeyboard;
-(void)setExtendedSymbolsKeyboard;
-(void)setAlphabetKeyboard;
-(void)setAlphabetKeyboardShifted;
-(void)setAlphabetKeyboardShiftedLocked;

@end
