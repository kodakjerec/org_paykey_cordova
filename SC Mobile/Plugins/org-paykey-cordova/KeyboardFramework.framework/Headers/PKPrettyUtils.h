//
//  PKPrettyUtils.h
//  KeyboardRefactor
//
//  Created by TechRacers on 08/02/17.
//  Copyright © 2017 Alex Kogan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PKEnums.h"

@interface PKPrettyUtils : NSObject

+ (NSString *)NSStringPrettyUIKeyboardType:(UIKeyboardType)type;
+ (NSString *)NSStringPrettyState:(UIGestureRecognizerState)state;
+ (NSString *)NSStringPrettyKeyboardId:(PKKeyboardId)state;

@end
