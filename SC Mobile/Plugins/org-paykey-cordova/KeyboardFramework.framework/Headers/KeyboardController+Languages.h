//
//  KeyboardController+Languages.h
//  PayKeyboard
//
//  Created by Alex Kogan on 13/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "KeyboardController.h"
#import "PKKeyboardViewProtocol.h"

@interface KeyboardController (Languages) <KeyboardViewProtocol>

- (BOOL)lastLanguageSelected;
- (void)changeLanguage:(PKLanguage*)newLang;
- (NSString*)locale;
- (void)setKeyboardControllerPrimaryLang:(PKLanguage*)newLang;
- (void)sendTextInputCurrentInputModeDidChangeNotification;
@end
