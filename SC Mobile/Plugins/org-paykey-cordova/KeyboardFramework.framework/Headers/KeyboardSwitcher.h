//
//  KeyboardSwitcher.h
//  PayKeyboard
//
//  Created by Alex Kogan on 11/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKKeyboardLayoutSwitchingProtocol.h"
#import "PKEnums.h"

@class InputView;
@class KeyboardViewModel;
@class PKLanguage;
@class Keyboard;
@class SuggestionEngine;
@class KeyModel;
@class PKKeyboardConfig;
@class KeyboardViewBase;
@class KeyboardSwitcher;

@protocol KeyboardSwitcherDelegate <NSObject>

- (void)keyboardSwitcher:(KeyboardSwitcher*)switcher showKeyboardView:(KeyboardViewBase*)kbView;
- (KeyboardViewBase*)keyboardSwitcher:(KeyboardSwitcher*)switcher keyboardViewForId:(PKKeyboardId)keyboardId;
- (PKLanguage*)keyboardSwitcherGetCurrentLanguage:(KeyboardSwitcher*)switcher;
- (PKLanguage*)keyboardSwitcherGetNextLanguage:(KeyboardSwitcher*)switcher;
- (void)keyboardSwitcher:(KeyboardSwitcher*)switcher keyboardCapsModeDidChange:(PKKeyboardId)keyboardId;

@end

@interface KeyboardSwitcher : NSObject<PKKeyboardLayoutSwitchingProtocol>

@property (nonatomic, weak) KeyboardViewModel * keyboardViewModel;
@property (nonatomic, weak) SuggestionEngine * suggestionEngine;
@property (nonatomic) BOOL showExtraActionButtonInPortrait;

- (void)loadFirstKeyboard;
- (instancetype)initWithDelegate:(id<KeyboardSwitcherDelegate>)delegate;
- (void)textCleared:(id<UITextDocumentProxy>)proxy;
- (void)reset;
- (void)loadKeyboard:(PKKeyboardConfig*)config
   textDocumentProxy:(id<UITextDocumentProxy>)proxy;

- (Keyboard*)getKeyboard;
- (void)invalidateKey:(KeyModel*)key;
- (PKKeyboardId)currentKeyboardId;

//-(void)hideEmojiKeyboard:(id<UITextDocumentProxy>)proxy;

- (void)toggleAlphaKeyboardCaps;
- (void)setAlphaberKeyboardByCapsMode:(PKCapslockMode)mode;
- (void)updateAlphaKeyboardCaps:(NSString*)lastKnownInputText proxy:(id<UITextDocumentProxy>)proxy;

- (PKCapslockMode)capsModeForInput:(NSString*)lastKnownInputText autocapitalizationType:(UITextAutocapitalizationType)autocapitalizationType;


@end
