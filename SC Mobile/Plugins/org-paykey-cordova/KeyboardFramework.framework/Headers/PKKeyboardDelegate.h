//
//  PKKeyboardDelegate.h
//  PayKeyboard
//
//  Created by Alex Kogan on 05/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PKEnums.h"

@protocol PKTextDocumentProxy;

@protocol PKKeyboardDelegate <NSObject>

- (void)setOutputText:(NSString *)text;
- (void)clearOutputText;
- (void)hideKeyboard;
- (void)showKeyboard;
- (void)setExtraHeight:(NSNumber *)extraHeight;
- (void)openUrl:(NSURL *)url;
- (void)advanceToNextInputMode;
- (void)selectPaymentButton:(BOOL)selected;
- (void)setDocumentTextProxy:(id<PKTextDocumentProxy>)textProxy;
- (void)updateReturnKey;
- (BOOL)isOpenAccess;
- (void)enableAccessBarViewClicked;
- (void)enableExtraButton:(BOOL)enabled;
- (void)updateTopBarType;
- (NSNumber *)getKeyboardHeight;
- (void)setBottomPortraitPadding:(CGFloat)padding;

@property (nonatomic) BOOL showExtraActionButtonInPortrait;

@end
