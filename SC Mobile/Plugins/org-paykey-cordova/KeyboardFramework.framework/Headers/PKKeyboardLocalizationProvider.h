//
//  PKKeyboardLocalizationProvider.h
//  PayKeyboard
//
//  Created by Marat  on 3/15/17.
//  Copyright © 2017 Apple. All rights reserved.
//
#import <Foundation/Foundation.h>
@protocol PKKeyboardLocalizationProvider <NSObject>
- (NSString*_Nullable)deviceLocalizedStringForKey:(NSString*_Nonnull)key;
- (NSString*_Nullable)keyboardLocalizedStringForKey:(NSString*_Nonnull)key;
@end
