//
//  NSString+Zhuyin.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 04/12/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Zhuyin)

- (BOOL)isZhuyinCharacter;
- (BOOL)isZhuyinTone;
+ (NSString*)firstZhuyinTone;

@end

