//
//  SuggestionEngineResult.h
//  PayKeyboard
//
//  Created by Alex Kogan on 13/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Suggestion.h"
@class PKLanguage;

@interface SuggestionEngineResult : NSObject

@property(nonatomic, strong)NSArray<Suggestion*>* suggestions;
@property(nonatomic, assign)BOOL isAutoCorrection;
@property(nonatomic, assign)BOOL isInDictionary;
@property(nonatomic, assign) PKLanguage *language;


-(instancetype)init;
-(instancetype)init:(NSArray<Suggestion*>*)suggestions;
-(instancetype)init:(NSArray<Suggestion*>*)suggestions
     isInDictionary:(BOOL)isInDictionary
   isAutoCorrection:(BOOL)isAutoCorrection
           language:(PKLanguage*)language;


@end
