//
//  DictionaryOTAModel.h
//  KeyboardExampleApp
//
//  Created by ishay weinstock on 03/04/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKEnums.h"

NS_ASSUME_NONNULL_BEGIN

@interface PKDictionaryOTAModel : NSObject <NSCoding>

+ (instancetype)modelFromDictionary:(NSDictionary*)dict;

@property (nonatomic,strong,readonly) NSString* checksum;
@property (nonatomic,strong,readonly) NSString* locale;
@property (nonatomic,strong,readonly) NSString* url;
@property (nonatomic,strong,readonly) NSNumber* version;
@property (nonatomic,assign,readonly) PKDictionaryType type;
@property (nonatomic,readonly) NSUInteger localeAndTypeHash;

- (NSComparisonResult)compareVersion:(PKDictionaryOTAModel *)other;
- (NSDictionary*)dictionaryValue;
- (NSString*)typeDescription;

@end

NS_ASSUME_NONNULL_END
