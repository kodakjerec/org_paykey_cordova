//
//  PKKeyboardViewFactory.h
//  PayKeyboard
//
//  Created by Alex Kogan on 08/08/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKKeyboardConfig.h"

@class AlphabetInputView;
@class InputView;
@class OpenSettingsView;
@class EmojiKeyboardView;
@class TopBarConfig;
@class TopBarView;
@class SuggestionsBarConfig;
@class SuggestionsBarBase;
@class EnableAccessBarContainer;
@class EnableAccessBar;
@class QuickActionsBar; 

@protocol EnableAccessBarDelegate;
@protocol SuggestionsBarDelegate;
@protocol OpenSettingsViewDelegate;
@protocol TopBarViewDelegate;
@protocol PKLocalization;
@protocol QuickActionBarDelegate;
@protocol DownloadDictionariesTopBarDelegate;

@interface PKKeyboardViewFactory : NSObject

@property (nonatomic, strong) PKKeyboardConfig * keyboardConfig;

// ------------ Factory ------------
- (InputView *)numericInputView;
- (AlphabetInputView *)alphabetInputView;
- (EmojiKeyboardView *)emojiKeyboardView;
- (SuggestionsBarBase *)suggestionsBar:(id<SuggestionsBarDelegate>) delegate;
- (TopBarView *)topBar:(id<TopBarViewDelegate>) delegate;
- (EnableAccessBar *)enableFullAccessBar:(id<EnableAccessBarDelegate>) delegate;
- (QuickActionsBar *)quickActionBar:(id<QuickActionBarDelegate>) delegate;
- (TopBarView *)downloadDictionariesTopBar:(id<DownloadDictionariesTopBarDelegate>)delegate;

// ------------ Abstract ------------
- (SuggestionsBarBase *)createSuggestionsBar:(SuggestionsBarConfig*)config;
//- (__kindof SuggestionsBarConfig *)createSuggestionsBarConfig:(id<SuggestionsBarDelegate>)delegate;
- (__kindof TopBarView *)createTopBar:(TopBarConfig *)config;
- (__kindof TopBarConfig *)createTopBarConfig;
- (__kindof OpenSettingsView*)createOpenSettingsViewWithDelegate:(id<OpenSettingsViewDelegate>)delegate;

@end
