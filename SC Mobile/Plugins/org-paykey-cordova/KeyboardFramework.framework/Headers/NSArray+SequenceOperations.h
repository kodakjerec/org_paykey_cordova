//
//  NSArray+SequenceOperations.h
//  PayKeyboard
//
//  Created by Jonathan Beyrak-Lev on 12/09/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef BOOL (^SequenceComparer)(id x1, id x2);
typedef id (^SequenceMapper)(NSArray *xs);

@interface NSArray(SequenceOperations)

- (BOOL)isEqualTo:(NSArray *)a using:(SequenceComparer)comparator;
- (NSArray *)map:(SequenceMapper)mapper;
- (NSArray *)map:(SequenceMapper)mapper over:(NSArray <NSArray *> *)additionalArrays;
- (NSArray *)taking:(NSInteger)ns;
- (NSArray *)dropping:(NSInteger)ns;
- (NSArray *)arrayByAddingIfNonnull:(id)object;

@end
