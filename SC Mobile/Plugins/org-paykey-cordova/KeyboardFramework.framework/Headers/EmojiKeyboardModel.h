//
//  EmojiKeyboardModel.h
//  EmojiUI
//
//  Created by Sumit on 15/02/17.
//  Copyright © 2017 Alex Kogan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EmojiData.h"
#import "ArtKeyboardModel.h"

@class KeyModel;

@interface EmojiKeyboardModel : ArtKeyboardModel

@property (nonatomic, strong) NSArray <KeyModel*>* categoriesButtons;
@property (nonatomic, strong) NSMutableDictionary *emojiDictionary;
@property (nonatomic, strong) NSMutableDictionary *emojisData;
@property (nonatomic, strong) NSMutableArray *frequentlyUsedEmojis;
@property (nonatomic, strong) NSMutableDictionary *frequentlyUsedEmojisCount;
@property (nonatomic, strong) NSDictionary *emojiTitles;

- (void)storeFrequentlyUsedEmoji:(NSString *)emoji;
- (void)storeDiversityUsageIfNeeded:(NSString *)emoji;
- (void)updateRecents;
+ (void)initialize;

@end
