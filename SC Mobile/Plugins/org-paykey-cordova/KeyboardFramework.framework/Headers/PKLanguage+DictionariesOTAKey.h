//
//  PKLanguage+DictionariesOTAKey.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 08/04/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

#import "PKEnums.h"
#import "PKLanguage.h"

NS_ASSUME_NONNULL_BEGIN

@interface PKLanguage (DictionariesOTAKey)

- (NSString*)dictionariesOTAKeyWithType:(PKDictionaryType)type;

@end

NS_ASSUME_NONNULL_END
