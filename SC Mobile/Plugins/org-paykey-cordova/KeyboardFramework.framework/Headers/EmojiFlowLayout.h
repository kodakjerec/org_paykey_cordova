//
//  EmojiFlowLayout.h
//  EmojiUI
//
//  Created by Sumit on 15/02/17.
//  Copyright © 2017 Alex Kogan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmojiFlowLayout : UICollectionViewFlowLayout

@property (nonatomic, strong) NSDictionary *headerTitles;
@property (nonatomic, strong, readonly) UIFont *headerTitleFont;
@property (nonatomic, assign) BOOL isSmallVersion;

- (CGRect)scrollRectForSection:(NSInteger)section;

@end
