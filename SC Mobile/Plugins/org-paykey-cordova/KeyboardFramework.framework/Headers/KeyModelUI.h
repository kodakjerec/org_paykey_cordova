//
//  KeyModelUI.h
//  PayKeyboard
//
//  Created by Alex Kogan on 09/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UIColor;


@interface KeyModelUI : NSObject

@property (nonatomic, strong) UIColor* keyColor;
@property (nonatomic, strong) UIColor* keyPressedColor;
@property (nonatomic, strong) UIColor* keyDisabledColor;
@property (nonatomic, strong) UIColor* keyTextColor;
@property (nonatomic, strong) UIColor* keyPressedTextColor;
@property (nonatomic, strong) UIColor* keyDisabledTextColor;
@property (nonatomic, assign) UIFont * keyTextFont;
@property (nonatomic, strong) NSString* text;
@property (nonatomic, strong) NSString* accessibility;


-(instancetype)initWithText:(NSString*)text textColor:(UIColor*)textColor keyColor:(UIColor*)keyColor;
-(instancetype)initWithText:(NSString*)text
                  textColor:(UIColor*)textColor
          disabledTextColor:(UIColor*)disabledTextColor
                   keyColor:(UIColor*)keyColor
           keyDisabledColor:(UIColor*)keyDisabledColor;

@end
