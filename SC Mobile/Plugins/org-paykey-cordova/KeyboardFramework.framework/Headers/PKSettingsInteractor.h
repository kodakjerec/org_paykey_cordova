//
//  PKSettingsInteractor.h
//  PayKeyboard
//
//  Created by Jonathan Beyrak-Lev on 17/07/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#ifndef PKSettingsInteractor_h
#define PKSettingsInteractor_h

@protocol PKSettingsInteractorDelegate <NSObject>

- (void)refresh;
- (void)showViewControllerOfClass:(Class)viewControllerClass;

@end

@protocol PKSettingsInteractor <NSObject>

@property (weak) id<PKSettingsInteractorDelegate> delegate;

- (NSArray *)settingPresenters;

@end

#endif /* Header_h */
