//
//  OpenSettingsConfig.h
//  PayKeyboard
//
//  Created by Marat  on 7/20/17.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "OpenSettingsModel.h"

@interface OpenSettingsConfig : NSObject

- (instancetype)initWithKeyboardName:(NSString*)keyboardName;
- (Class)openSettingsViewBundleClass;
- (NSString*)openSettingsViewNibName;
- (OpenSettingsModel *)openSettingsModel;

@end
