//
//  DefaultInputPlanner.h
//  PayKeyboard
//
//  Created by Jonathan Beyrak-Lev on 27/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InputCommandFactory.h"
#import "InputLogic.h"

@interface DefaultInputPlanner : NSObject <InputLogicPlanner>

@property (strong, nonatomic) InputCommandFactory *commandFactory;

- (instancetype)initWithCommandFactory:(InputCommandFactory *)commandFactory;
- (InputCommand*)commandToInsert:(NSString*)insert event:(PKTouchEvent*)event;

@end
