//
//  UIReturnKeyProxy.h
//  PayKeyUI
//
//  Created by Eran Israel on 26/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UIReturnKeyProxy <NSObject>

@property (readonly, weak) KeyModelUI* returnKeyUIModel;

@end
