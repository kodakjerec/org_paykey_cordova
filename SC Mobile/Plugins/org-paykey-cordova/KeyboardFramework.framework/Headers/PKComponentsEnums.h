//
//  PKComponentsEnums.h
//  PayKeyUI
//
//  Created by Alex Kogan on 01/08/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

typedef NS_ENUM(NSInteger, PKCursorLabelAlignment) {
    PKCursorLabelAlignmentLeft,
    PKCursorLabelAlignmentRight,
};

