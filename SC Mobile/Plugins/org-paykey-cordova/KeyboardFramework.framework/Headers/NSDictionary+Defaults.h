//
//  NSDictionary+Defaults.h
//  PayKeyboard
//
//  Created by Alex Kogan on 05/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSDictionary (Defaults)

-(BOOL)boolValue:(NSString*)key defaultValue:(BOOL)value;

@end
