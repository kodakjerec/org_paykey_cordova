//
//  NSData+MD5.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 08/04/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSData (MD5)

- (NSString*)MD5:(NSData*)data;

@end

NS_ASSUME_NONNULL_END
