//
//  PKCommand.h
//  PayKeyUICore
//
//  Created by Eden Landau on 02/04/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

@protocol PKCommand <NSObject>
- (void)execute;
@end
