//
//  BackspaceInteractor.h
//  PayKeyboard
//
//  Created by ishay weinstock on 14/09/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import "KeyInteractor.h"

@class BackspaceInteractor;

@protocol BackspaceInteractorDelegate <NSObject>

- (void)backspaceInteractorContinuousCharDeletion:(BackspaceInteractor*)interactor;
- (void)backspaceInteractorContinuousWordDeletion:(BackspaceInteractor*)interactor;

@end

@interface BackspaceInteractor : KeyInteractor


@property (nonatomic,weak) id<BackspaceInteractorDelegate> delegate;

@end
