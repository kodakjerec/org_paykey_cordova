//
//  KeyLabelLayout.h
//  KeyboardFramework
//
//  Created by ishay weinstock on 23/10/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface KeyLabelLayout : NSObject

+ (KeyLabelLayout*)layout;
+ (KeyLabelLayout*)comHoverLayout;
+ (KeyLabelLayout*)layoutWithDictionary:(NSDictionary*)dictionary;

- (KeyLabelLayout*)layoutForKey:(NSString*)text;

@property (nonatomic,readonly) CGAffineTransform transformTranslation;
@property (nonatomic,readonly) CGAffineTransform transformScale;
@property (nonatomic,readonly) UIFont *font;
@property (nonatomic,readonly) CGSize shadowOffset;
@property (nonatomic,readonly) UIColor* shadowColor;


@end
