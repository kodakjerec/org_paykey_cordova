//
//  InputLogic.h
//  PayKeyboard
//
//  Created by Alex Kogan on 13/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PKInputTextState.h"
#import "WordComposer.h"

#import "PKLanguage.h"
#import "PKTouchEvent.h"

#import "InputLogicResult.h"
#import "SuggestionEngineResult.h"

@class PKSuggestionBarTouchEvent;
@class PKAnalyticsEvent;

@protocol InputLogicProtocol <NSObject>

- (void)insertText:(NSString*) text;
- (void)deleteBackward;
- (void)reportEvent:(PKAnalyticsEvent*)analyticsEvent;

@end

@protocol InputLogicPlannerDelegate <NSObject>

- (void)manuallyInsertBestCorrection;
- (void)askForSuggestions;
- (void)setSpaceTitle:(NSString*)title;
- (void)askToAddEvent:(PKTouchEvent*)touchEvent;

@end

@protocol InputLogicPlanner <NSObject>

@optional

- (InputCommand *)commandsWithPreviousInput:(NSString * )previous
                               currentInput:(NSString * )current
                                      event:(PKTouchEvent * )event;

- (InputCommand *)commandReplaceLastInputWord:(NSString*)newPhrase
                                    oldPhrase:(NSString*)oldPhrase
                                 currentInput:(NSString *)input
                                        event:(PKTouchEvent *)event;

- (InputCommand *)commandDeleteWords:(int)wordsCount
                           fromInput:(NSString *)input
                               event:(PKTouchEvent*)event;

- (InputCommand*)commandDoubleSpaceTap:(BOOL)periodOnDoubleSpace
                                 input:(NSString*)input
                                 event:(PKTouchEvent*)event;

- (InputCommand*)commandAutoCorrect:(NSString*)suggestion
        addSpaceAfterAutoCorrection:(BOOL)addSpaceAfterAutoCorrection
                              input:(NSString*)input
                              event:(PKTouchEvent*)event;

- (BOOL)shouldAutoCorrectOnSpaceWithCurrentInput:(NSString*)input;
- (BOOL)shouldAddSpaceAfterSuggestion;
- (BOOL)enableDoubleTapOnSpace;
- (void)didPickSuggestion;

@property id<InputLogicPlannerDelegate> delegate;

@end

@interface InputLogic : NSObject<Inputtable, PKInputTextStateObserver, InputLogicPlannerDelegate>

@property (nonatomic, assign, readonly) BOOL revertingLastAutoCorrectedWord;

@property (nonatomic, strong) SuggestionEngineResult*  checkerResult;
@property (nonatomic, strong, readonly) WordComposer* wordComposer;

@property (nonatomic, weak) id<InputLogicProtocol> delegate;
@property (nonatomic, strong) id<InputLogicPlanner> planner;

- (instancetype)initWithInputTextState:(PKInputTextState*)inputTextState;

- (InputLogicResult*)handleTouchEvent:(PKTouchEvent* )key
                             language:(PKLanguage* )language
              isAutoCorrectionEnabled:(BOOL)isAutoCorrectionEnabled
                  periodOnDoubleSpace:(BOOL)periodOnDoubleSpace;

- (BOOL)shouldSuggestAccordingToTheLastKnownInput;
- (void)updateWordComposerWord;
- (void)setWasAutoCapitalized:(BOOL)wasAutoCapitalized;

@end
