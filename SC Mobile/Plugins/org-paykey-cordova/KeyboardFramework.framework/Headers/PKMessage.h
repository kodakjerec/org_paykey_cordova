//
//  PKMessage.h
//  PayKeyboard
//
//  Created by Alex Kogan on 13/07/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, PKMessageType)  {
    PKMessageTypeInvokeSuggestionCall,
    PKMessageTypeInvokeLog,
};

@interface PKMessage : NSObject

+(NSString*)prettyType:(PKMessageType)type;
    
-(instancetype)initWithType:(PKMessageType)type;
    
@property (nonatomic, assign) PKMessageType type;
@property (nonatomic, assign) dispatch_time_t minDispatchTime;
    
@end
