//
//  BaseKeyboardModel.h
//  PayKeyboard
//
//  Created by Alex Kogan on 11/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeyboardModel.h"

@interface BaseKeyboardModel : KeyboardModel

@end
