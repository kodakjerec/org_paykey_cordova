//
//  EmojiReturnKeyModel.h
//  PayKeyboard
//
//  Created by Alex Kogan on 05/04/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KeyModel.h"

@class PKLanguage;

@interface ArtReturnKeyModel : KeyModel

@property (nonatomic, strong) PKLanguage* language;
@property (nonatomic) BOOL emojiWasSelected;

@end
