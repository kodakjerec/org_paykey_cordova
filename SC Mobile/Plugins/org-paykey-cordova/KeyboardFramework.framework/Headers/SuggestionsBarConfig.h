//
//  SuggestionsBarConfig.h
//  PayKeyboard
//
//  Created by Alex Kogan on 08/08/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKPaymentButtonAttributes.h"
#import "TopBarConfig.h"

@class PKKeyboardConfig;

@interface SuggestionsBarConfig : TopBarConfig

- (instancetype)initWithKeyboardConfig:(PKKeyboardConfig*)keyboardConfig;
- (UIFont*)font;
- (UIColor*)textColor;
- (UIColor*)bgHeighlightedColor;
- (UIColor*)separatorColor;
- (UIColor*)autoCorrectionBgColor;
- (UIColor*)autoCorrectionTextColor;
- (BOOL)separatorHidden;
- (CGFloat)separatorHeightMultiplier;
- (CGFloat)buttonHeightMultiplier;
- (CGFloat)space;
- (BOOL)hideSeparatorOnHighlighted;
- (CGFloat)suggestionCornerRadius;

@end
