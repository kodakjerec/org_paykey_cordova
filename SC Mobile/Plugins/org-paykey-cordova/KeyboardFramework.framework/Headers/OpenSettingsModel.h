//
//  OpenSettingsModel.h
//  PayKeyboard
//
//  Created by Alex Kogan on 08/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#ifndef OpenSettingsModel_h
#define OpenSettingsModel_h
#import "PKLocalization.h"
@interface OpenSettingsModel : NSObject<PKLocalization>

- (instancetype)initWithKeyboardName:(NSString*)keyboardName;
- (void)commonInit;

@property (nonatomic, strong) NSNumber *viewHeight;
@property (nonatomic, strong) UIColor* backgroundColor;
@property (nonatomic, strong) UIColor* topLineBackgroundColor;
@property (nonatomic, strong) UIColor* textColor;
@property (nonatomic, strong) UIColor* buttonColor;
@property (nonatomic, strong) NSString *fontName;
@property (nonatomic, strong) NSString* buttonText;
@property (nonatomic, strong) NSArray* boldWords;
@property (nonatomic, strong) NSString *dynamBoldWord;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *donButtonTitle;
@property (nonatomic, strong) NSString *firstRow;
@property (nonatomic, strong) NSString *secondRow;
@property (nonatomic, strong) NSString *thirdRow;
@property (nonatomic, strong) NSString *keyboardName;
@property (nonatomic, strong) NSNumber *firstLineHeadIndent;
@end

#endif /* OpenSettingsModel_h */
