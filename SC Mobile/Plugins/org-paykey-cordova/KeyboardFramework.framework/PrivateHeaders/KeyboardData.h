//
//  KeyboardData.cpp
//
//  Created by paul kalnitz on 05/03/2017.
//  Copyright © 2017 paul kalnitz. All rights reserved.
//

#ifndef KEYBOARD_DATA_H
#define KEYBOARD_DATA_H

#include <vector>
#include <math.h>
#include "proximity_info.h"
#include "nlpEngineKey.h"
#include "KeyboardKey.h"

#define SUGGESTION_GRID_NEIGHBORS_DEBUG false

#define SEARCH_DISTANCE 1.2f
#define NOT_A_CODE -1


namespace latinime {

    typedef std::vector<KeyboardKey *> KeyboardKeys;

    class KeyboardData {
    public:
        KeyboardData();

        ~KeyboardData();

        ProximityInfo *init(int keyboardId, int gridWidth, int gridHeight, int kbdWidth, int kbdHeight,
             nlpEngineKeys *keys, int mostCommonKeyWidth, int mostCommonKeyHeight);

    private:
        int mKeyboardWidth;
        int mKeyboardHeight;
        int mGridWidth;
        int mGridHeight;
        int mCellWidth;
        int mCellHeight;
        int mMostCommonKeyWidth;
        int mMostCommonKeyHeight;
        int mKeyCount;

        KeyboardKeys *mSortedKeys;
        std::map<int, ProximityInfo *> mCache;


        KeyboardKeys getNearestKeys(float x, float y);

        std::vector<KeyboardKeys> mGridNeighbors;

        std::vector<KeyboardKeys> gridNeighbors();

        bool needsProximityInfo(KeyboardKey *key);

        int getProximityInfoKeysCount(const KeyboardKeys *sortedKeys);

        int *makeProximityCharsArray();

        //std::unique_ptr<ProximityInfo> mProximity;
        ProximityInfo *proximity();
    };
} // namespace latinime

#endif /* KEYBOARD_DATA_H */
