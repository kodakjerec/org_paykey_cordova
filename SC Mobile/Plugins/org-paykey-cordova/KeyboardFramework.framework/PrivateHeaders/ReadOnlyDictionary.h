//
// Created by YakirPayKey on 3/22/18.
//

#ifndef NLP_ENGINE_READONLYDICTIONARY_H
#define NLP_ENGINE_READONLYDICTIONARY_H

#include "DictionaryDelegate.h"
#include <file_utils.h>


namespace latinime {

    class ReadOnlyDictionary : public DictionaryDelegate {

    public:
        ReadOnlyDictionary(const char *name, const char *pathToDict, long dictionarySize, long offset);

        bool exists() override {
            return getFilePath() != nullptr && FileUtils::existsFile(getFilePath());
        }

        bool isUpdatable() override;

    private:

        bool create() override {
            return false;
        }

    };

}


#endif //NLP_ENGINE_READONLYDICTIONARY_H
