/*
 *  pt_node_pos_utils.h
 *
 *  Created by paul on 19/12/2018.
 *  Copyright © 2017, 2018 PayKey. All rights reserved.
 *
 */

#ifndef LATINIME_PT_NODE_POS_UTILS_H
#define LATINIME_PT_NODE_POS_UTILS_H

#include <sstream>
#include <cstring>
#include <map>
#include <math.h>
#include "dictionary_structure_with_buffer_policy.h"
#include <scoring_params.h>

namespace latinime {

class PtNodePOSUtils {

/**
 * POS (Part-Of-Speech) data
 *
 * encodes DUALITY, POS, AND NEXTPOS into a ByteBuffer
 * structure is:
 *   POS - 2 bytes - top 6 bits: posIndex (0-15), bottom 10 bits: probability*1000
 *
 *   duality - 1 byte
 *   gender - 1 byte
 *   POSlistLength - 1 byte (up to 16)
 *   POSlist - 2-byte POS values
 *   nextPOSlistLength - 1 byte  (up to 16)
 *   nextPOSlist - 2-byte POS values
 *   verbSubtypePOSlistLength - 1 byte  (up to 16)
 *   verbSubtypePOSlist - 2-byte POS values
 *
 *   MAX_POS_BUFFER_SIZE = 1+1+16*2*3 = 98
 *
 * NOTE: DUALITY, GENDER, POS, NEXTPOS, VERBSUBTYPES must come in that order in each wordlist entry
 */


 public:
    static AK_FORCE_INLINE float getPOSImprobability(
            const DictionaryStructureWithBufferPolicy *const policy,
            int contextWordId,
            int candidateWordId,
            const unsigned char *contextPOSbuf,
            const unsigned char *candidatePOSbuf) {

        /*
         * extract duality and gender
         */
        int posBufIndex = 0;
        const int contextDuality = getByte(contextPOSbuf, posBufIndex++);
        const int contextGender = getByte(contextPOSbuf, posBufIndex++);

        /*
         * extract context POS lists
         */
        const int contextPOSListLen = getByte(contextPOSbuf, posBufIndex++);
        float contextPOSprobabilities[MAX_POS_COUNT];
        posBufIndex += getPOSList(contextPOSbuf, posBufIndex, contextPOSListLen, contextPOSprobabilities);

        const int contextNextPOSListLen = getByte(contextPOSbuf, posBufIndex++);
        float contextNextPOSprobabilities[MAX_POS_COUNT];
        posBufIndex += getPOSList(contextPOSbuf, posBufIndex, contextNextPOSListLen, contextNextPOSprobabilities);

        const int contextVerbSubtypesPOSListLen = getByte(contextPOSbuf, posBufIndex++);
        float contextVerbSubtypesPOSprobabilities[MAX_POS_COUNT];
        getPOSList(contextPOSbuf, posBufIndex, contextVerbSubtypesPOSListLen, contextVerbSubtypesPOSprobabilities);

        /*
         * extract candidate POS lists
         */
        posBufIndex = 0;
        const int candidateDuality = getByte(candidatePOSbuf, posBufIndex++);
        const int candidateGender = getByte(candidatePOSbuf, posBufIndex++);

        const int candidatePOSListLen = getByte(candidatePOSbuf, posBufIndex++);
        float candidatePOSprobabilities[MAX_POS_COUNT];
        posBufIndex += getPOSList(candidatePOSbuf, posBufIndex, candidatePOSListLen, candidatePOSprobabilities);

        const int candidateNextPOSListLen = getByte(candidatePOSbuf, posBufIndex++);
        float candidateNextPOSprobabilities[MAX_POS_COUNT];
        posBufIndex += getPOSList(candidatePOSbuf, posBufIndex, candidateNextPOSListLen, candidateNextPOSprobabilities);

        const int candidateVerbSubtypesPOSListLen = getByte(candidatePOSbuf, posBufIndex++);
        float candidateVerbSubtypesPOSprobabilities[MAX_POS_COUNT];
        getPOSList(candidatePOSbuf, posBufIndex, candidateVerbSubtypesPOSListLen, candidateVerbSubtypesPOSprobabilities);

        /*
         * calculate POSImprobability
         */
        float POSImprobability = 0.0;
        float verbSubtypeImprobability = 0.0;
        for (int i = 0; i < MAX_POS_COUNT; i++) {
            POSImprobability += (1.0 - contextNextPOSprobabilities[i]) * candidatePOSprobabilities[i];
            //TODO verbSubtypeImprobability += (1.0 - contextVerbSubtypesPOSprobabilities[i]) * candidateVerbSubtypesPOSprobabilities[i];
            //TODO there may be a weighting factor to verbSubtypeImprobability which can be developed by testing & tweaking SketchEngine English
        }

        if(verbSubtypeImprobability > 0.0)
            POSImprobability = (POSImprobability + verbSubtypeImprobability) / 2.0;

#if DEBUG_POS_IMPROBABIITY
        int codePoints[MAX_WORD_LENGTH];
        int len1 = policy->getCodePointsAndReturnCodePointCount(contextWordId, MAX_WORD_LENGTH, codePoints);
        char contextWord[MAX_WORD_LENGTH];
        INTS_TO_CHARS(codePoints, len1, contextWord, NELEMS(contextWord));

        int len2 = policy->getCodePointsAndReturnCodePointCount(candidateWordId, MAX_WORD_LENGTH, codePoints);
        char candidateWord[MAX_WORD_LENGTH];
        INTS_TO_CHARS(codePoints, len2, candidateWord, NELEMS(candidateWord));

        AKLOGI("CONTEXT NEXTPOS %20s %s\n", contextWord, POSArrayToString(contextNextPOSprobabilities).c_str());
        AKLOGI("CANDIDATE POS %22s %s\n", candidateWord, POSArrayToString(candidatePOSprobabilities).c_str());
        AKLOGI("POS_IMPROBABILITY:%0.3f\n\n", POSImprobability);
#endif

        return POSImprobability;
    }

    static AK_FORCE_INLINE int getPOSList(const unsigned char *POSbuf, int POSlistIndex,
                                           int POSlistlen, float *POSarray) {

        //this is the case where all POS or nextPOS are UNKNOWN
        if(POSlistlen == 0) {
            for(int i=0; i<MAX_POS_COUNT; i++)
                POSarray[i] = static_cast<float>(1.0 / (float)MAX_POS_COUNT);
            return 0;
        }

        //initialize the POSarray to all zero's
        for(int i=0; i<MAX_POS_COUNT; i++)
            POSarray[i] = 0.0;

        //get the POSmap values from the POSbuffer
        float probabilitySum=0.0;
        for(int i=POSlistIndex; i<POSlistIndex+POSlistlen*2; i+=2) {
            unsigned short POS = getShort(POSbuf, i);
            int posIndex = (POS>>10) & 0x3F;
            auto probability = static_cast<float>((float)((POS & 0x3FF)) / 1000.0);
            POSarray[posIndex] = probability;
            probabilitySum += probability;
        }

        //if the probabilitySum < 1.0, spread the unused probability over the 0.0 POStypes
        if(probabilitySum < 1.0) {
            int zeroPOScount=0;
            for(int i=0; i<MAX_POS_COUNT; i++)
                if(POSarray[i] == 0.0)
                    zeroPOScount++;
            auto probabilitySpread = static_cast<float>((1.0 - probabilitySum) / zeroPOScount);
            if(probabilitySpread > 0.001)
                for(int i=0; i<MAX_POS_COUNT; i++)
                    if(POSarray[i] == 0.0)
                        POSarray[i] = probabilitySpread;
        }

        return POSlistlen * 2;
    }

    static AK_FORCE_INLINE unsigned short getShort(const unsigned char* POSbuf, const int index) {
        return (POSbuf[index] << 8) | (POSbuf[index+1]);
    }

    static AK_FORCE_INLINE unsigned short getByte(const unsigned char* POSbuf, const int index) {
        return POSbuf[index];
    }

    static AK_FORCE_INLINE std::string POSArrayToString(const float *POSarray) {
        std::string result;
        for (int i=0; i<MAX_POS_COUNT; i++) {
            double probability = POSarray[i];
            char str[20];
            sprintf(str, "%d:%.3f,", i, probability);
            result += str;
        }
        return result.substr(0, result.size()-1);
    }
};
} // namespace latinime
#endif /* LATINIME_PT_NODE_POS_UTILS_H */
