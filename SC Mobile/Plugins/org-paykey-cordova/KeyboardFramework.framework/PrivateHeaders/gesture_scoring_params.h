/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_GESTURE_PARAMS_H
#define LATINIME_GESTURE_PARAMS_H

#include "defines.h"

namespace latinime {

    class GestureScoringParams {
    public:
        // Fixed model parameters
        static const float MAX_SPATIAL_DISTANCE;
        static const float AUTOCORRECT_OUTPUT_THRESHOLD;
        static const int MAX_CACHE_DIC_NODE_SIZE;
        static const int MAX_CACHE_DIC_NODE_SIZE_FOR_SINGLE_POINT;
        static const int MAX_CACHE_DIC_NODE_SIZE_FOR_LOW_PROBABILITY_LOCALE;

        static const float EXACT_MATCH_PROMOTION;
        static const float PERFECT_MATCH_PROMOTION;

        static const float DISTANCE_WEIGHT_LENGTH_NO_PROB;
        static const float DISTANCE_WEIGHT_LENGTH_FIRST_CHAR;
        static const float DISTANCE_WEIGHT_LENGTH;

        static const float INTENTIONAL_OMISSION_COST;
        static const float OMISSION_COST;
        static const float OMISSION_COST_SAME_CHAR;
        static const float OMISSION_BEST_PROB_LIMIT;
        static const float OMISSION_CURRENT_PROB_LIMIT;

        static const float TERMINAL_INSERTION_COST;

        static const float INSERTION_COST;

        static const float SPACE_SUBSTITUTION_COST;
        static const float SPACE_OMISSION_COST;
        static const float ADDITIONAL_PROXIMITY_COST;
        static const float SUBSTITUTION_COST;
        static const float DISTANCE_WEIGHT_LANGUAGE;
        static const float DISTANCE_WEIGHT_LANGUAGE_POS;
        static const float DISTANCE_WEIGHT_LANGUAGE_POS_SPLIT;
        static const float COST_FIRST_COMPLETION;
        static const float COST_COMPLETION;
        static const float GESTURE_BASE_OUTPUT_SCORE;
        static const float GESTURE_MAX_OUTPUT_SCORE_PER_INPUT;
        static const float NORMALIZED_SPATIAL_DISTANCE_THRESHOLD_FOR_EDIT;
        static const float LOCALE_WEIGHT_THRESHOLD_FOR_SPACE_SUBSTITUTION;
        static const float LOCALE_WEIGHT_THRESHOLD_FOR_SPACE_OMISSION;
        static const float LOCALE_WEIGHT_THRESHOLD_FOR_SMALL_CACHE_SIZE;

        static const float TRAVERSAL_SUBSTITUTION_PROB_THRESH;
        static const float TRAVERSAL_INSERTION_PROB_RATIO_THRESH;
        static const int TRAVERSAL_INSERTION_END_DELTA;
        static const float TRAVERSAL_INSERTION_MAX_SKIP_LOG;


    private:
        DISALLOW_IMPLICIT_CONSTRUCTORS(GestureScoringParams);
    };
} // namespace latinime
#endif // LATINIME_SCORING_PARAMS_H
