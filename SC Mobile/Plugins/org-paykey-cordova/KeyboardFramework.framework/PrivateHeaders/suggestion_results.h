/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_SUGGESTION_RESULTS_H
#define LATINIME_SUGGESTION_RESULTS_H

#include <queue>
#include <vector>

#include "defines.h"
#include "suggested_word.h"
#include "double_priority_queue.h"

namespace latinime {

    class SuggestionResults {
    public:
        explicit SuggestionResults(const int maxSuggestionCount)
                : mMaxSuggestionCount(maxSuggestionCount),
                  mWeightOfLangModelVsSpatialModel(NOT_A_WEIGHT_OF_LANG_MODEL_VS_SPATIAL_MODEL),
                  mSuggestedWords() {}


        void addPrediction(const int *const codePoints, const int codePointCount, const int score);

        void addPredictionAsSuggestion(int codePoints[MAX_WORD_LENGTH], const int codePointCount,
                                       int score, int prefixCount);

        bool willBeAdded(const int score) const;

        void addSuggestion(const int *const codePoints, const int codePointCount, const int *const parentCodePoints,
                           const int parentCodePointCount,
                           const int score, const int type, const int indexToPartialCommit,
                           const int autocommitFirstWordConfindence, const float languageDistance,
                           const bool isValidMultipleWordSuggestion, const int lastInputConsumed);

        void dumpSuggestions() const;

        void dumpSuggestions(std::string header) const;

        void popInOther(SuggestionResults *other, const int maxToPop);

        void setWeightOfLangModelVsSpatialModel(const float weightOfLangModelVsSpatialModel) {
            mWeightOfLangModelVsSpatialModel = weightOfLangModelVsSpatialModel;
        }

        int getMaxSuggestionCount() const {
            return mMaxSuggestionCount;
        }

        SuggestedWord pop() {
            return mSuggestedWords.popWorst();
        }

        SuggestedWord top() const {
            return mSuggestedWords.peekWorst();
        }

        bool empty() const {
            return mSuggestedWords.empty();
        }

        int size() const {
            return mSuggestedWords.size();
        }

    private:

        void push(SuggestedWord suggestedWord) {
            mSuggestedWords.push(suggestedWord);
        }

        DISALLOW_IMPLICIT_CONSTRUCTORS(SuggestionResults);

        const int mMaxSuggestionCount;
        float mWeightOfLangModelVsSpatialModel;
        DoublePriorityQueue mSuggestedWords;
    };
} // namespace latinime
#endif // LATINIME_SUGGESTION_RESULTS_H
