#include <utility>

//
//  NLPEngineHelper.h
//
//  Created by paul kalnitz on 28/03/20178
//  Copyright © 2017,2018 PayKey. All rights reserved.
//

#ifndef NLP_ENGINE_HELPER_H
#define NLP_ENGINE_HELPER_H

#include <iostream>
#include "DictionaryFacilitator.h"
#include "defines.h"
#include "nlpDefines.h"

namespace latinime {

    class NlpEngineHelper {

    public:
        static DictionaryFacilitator *createNative() {
            AKLOGI("Creating new DictionaryFacilitator\n");
            return new DictionaryFacilitator();
        }

        static void closeNative(DictionaryFacilitator *dictionaryFacilitator) {
            AKLOGI("Deleting DictionaryFacilitator\n");
            if (isNotValid(dictionaryFacilitator, "Close")) {
                return;
            }
            delete dictionaryFacilitator;
        }

        static void loadKeyboardDataNative(DictionaryFacilitator *dictionaryFacilitator, int keyboardId,
                                           int gridWidth, int gridHeight, int kbdWidth, int kbdHeight,
                                           nlpEngineKeys *keys,
                                           int mostCommonKeyWidth, int mostCommonKeyHeight) {
            if (isNotValid(dictionaryFacilitator, "load keyboard data")) {
                return;
            }
            dictionaryFacilitator->loadKeyboardData(keyboardId, gridWidth, gridHeight, kbdWidth, kbdHeight, keys,
                                                    mostCommonKeyWidth,
                                                    mostCommonKeyHeight);
        }

        static bool
        loadMainDictionaryNative(DictionaryFacilitator *dictionaryFacilitator, const char *dictionaryPath,
                                 long dictionarySize, long fileOffset) {
            if (isNotValid(dictionaryFacilitator, "load main dictionary")) {
                return false;
            }
            return dictionaryFacilitator->loadMainDictionary(dictionaryPath, dictionarySize, fileOffset);
        }

        static bool
        loadEmojiDictionary(DictionaryFacilitator *dictionaryFacilitator, const char *dictionaryPath,
                            long dictionarySize,
                            long fileOffset, bool alsoUseForPrediction) {
            if (isNotValid(dictionaryFacilitator, "load emoji dictionary")) {
                return false;
            }
            return dictionaryFacilitator->loadEmojiDictionary(dictionaryPath, dictionarySize, fileOffset,
                                                              alsoUseForPrediction);
        }

        static bool
        loadContactsDictionary(DictionaryFacilitator *dictionaryFacilitator) {
            if (isNotValid(dictionaryFacilitator, "load contacts dictionary")) {
                return false;
            }
            return dictionaryFacilitator->loadContactsDictionary();
        }

        static bool
        loadUserHistoryDictionary(DictionaryFacilitator *dictionaryFacilitator, const char *dictionaryPath,
                                  std::vector<int> localeCodePoints) {
            if (isNotValid(dictionaryFacilitator, "load user history dictionary")) {
                return false;
            }
            return dictionaryFacilitator->loadUserHistoryDictionary(dictionaryPath, std::move(localeCodePoints));
        }

        static bool
        loadUserWordsDictionary(DictionaryFacilitator *dictionaryFacilitator, std::vector<int> localeCodePoints) {
            if (isNotValid(dictionaryFacilitator, "load shortcuts dictionary")) {
                return false;
            }
            return dictionaryFacilitator->loadUserWordsDictionary(std::move(localeCodePoints));
        }

        static void unloadMainDictionary(DictionaryFacilitator *dictionaryFacilitator) {
            if (isNotValid(dictionaryFacilitator, "unload main dictionary")) {
                return;
            }
            dictionaryFacilitator->unloadMainDictionary();
        }

        static void unloadEmojiDictionary(DictionaryFacilitator *dictionaryFacilitator) {
            if (isNotValid(dictionaryFacilitator, "unload emoji dictionary")) {
                return;
            }
            dictionaryFacilitator->unloadEmojiDictionary();
        }

        static void unloadUserWordsDictionary(DictionaryFacilitator *dictionaryFacilitator) {
            if (isNotValid(dictionaryFacilitator, "unload user words dictionary")) {
                return;
            }
            dictionaryFacilitator->unloadUserWordsDictionary();
        }

        static void unloadUserHistoryDictionary(DictionaryFacilitator *dictionaryFacilitator) {
            if (isNotValid(dictionaryFacilitator, "unload user history dictionary")) {
                return;
            }
            dictionaryFacilitator->unloadUserHistoryDictionary();
        }

        static void unloadContactsDictionary(DictionaryFacilitator *dictionaryFacilitator) {
            if (isNotValid(dictionaryFacilitator, "unload contacts dictionary")) {
                return;
            }
            dictionaryFacilitator->unloadContactsDictionary();
        }

        static bool
        addUserWord(DictionaryFacilitator *dictionaryFacilitator, int *word, int wordSize, int *shortcut,
                    int shortcutSize) {
            if (isNotValid(dictionaryFacilitator, "add shortcut")) {
                return false;
            }
            return dictionaryFacilitator->addUserWord(CodePointArrayView(word, static_cast<const size_t>(wordSize)),
                                                      CodePointArrayView(shortcut,
                                                                         static_cast<const size_t>(shortcutSize)));
        }

        static bool addContactNative(DictionaryFacilitator *dictionaryFacilitator, CodePointsList fullName) {
            if (isNotValid(dictionaryFacilitator, "add contact")) {
                return false;
            }
            return dictionaryFacilitator->addContact(std::move(fullName));
        }

        static bool addToUserHistoryNative(DictionaryFacilitator *dictionaryFacilitator,
                                           const int prevWordCodePoints[][MAX_WORD_LENGTH],
                                           const int prevWordCodePointCount[],
                                           const bool isBeginningOfSentence[],
                                           const int prevWordCount,
                                           CodePointsList words,
                                           bool wasAutoCapitalized,
                                           bool blockPotentiallyOffensive,
                                           long timeStampInSeconds) {

            if (isNotValid(dictionaryFacilitator, "add to user history")) {
                return false;
            }

            const NgramContext ngramContext = NgramContext(prevWordCodePoints, prevWordCodePointCount,
                                                           isBeginningOfSentence,
                                                           static_cast<const size_t>(prevWordCount));
            bool success = true;
            const NgramContext *advancedNgram = &ngramContext;
            std::vector<const NgramContext *> ngramToDelete;
            for (int i = 0; i < words.size(); i++) {

                bool wasAutoCapitalizedWord = wasAutoCapitalized && i == 0;
                CodePointArraysView emptyShortcuts;
                CodePoints emptyFrequencies;
                std::vector<int> wordLearned;
                CodePointArrayView wordAsPointArrayView(words.at(i).data(), words.at(i).size());
                if (!dictionaryFacilitator->addToUserHistory(advancedNgram,
                                                             wordAsPointArrayView,
                                                             wasAutoCapitalizedWord,
                                                             static_cast<const int>(timeStampInSeconds),
                                                             &emptyShortcuts, &emptyFrequencies,
                                                             blockPotentiallyOffensive,
                                                             wordLearned)) {
                    success = false;
                }

                CodePointArrayView wordToLearnView(wordLearned.data(), wordLearned.size());
                advancedNgram = new NgramContext(*advancedNgram, wordToLearnView);
                ngramToDelete.push_back(advancedNgram);
            }

            for (int j = 0; j < ngramToDelete.size(); ++j) {
                delete ngramToDelete.at(static_cast<unsigned long>(j));
            }

            return success;
        }

        static bool addToUserHistoryNative(DictionaryFacilitator *dictionaryFacilitator,
                                           const int prevWordCodePoints[][MAX_WORD_LENGTH],
                                           const int prevWordCodePointCount[],
                                           const bool isBeginningOfSentence[],
                                           const int prevWordCount,
                                           CodePoints word,
                                           bool wasAutoCapitalized,
                                           const CodePointsList *shortcuts,
                                           const std::vector<int> *frequencies,
                                           bool blockPotentiallyOffensive,
                                           long timeStampInSeconds) {
            if (isNotValid(dictionaryFacilitator, "add to user history")) {
                return false;
            }

            const NgramContext ngramContext = NgramContext(prevWordCodePoints, prevWordCodePointCount,
                                                           isBeginningOfSentence,
                                                           static_cast<const size_t>(prevWordCount));

            CodePointArrayView suggestionsAsView(&word[0], word.size());
            CodePoints wordLearned;

            CodePointArraysView shortcutsAsView;
            for (int i = 0; i < shortcuts->size(); i++) {
                shortcutsAsView.push_back(CodePointArrayView(&shortcuts->at(i)[0], shortcuts->at(i).size()));
            }

            return dictionaryFacilitator->addToUserHistory(&ngramContext, suggestionsAsView, wasAutoCapitalized,
                                                           static_cast<const int>(timeStampInSeconds), &shortcutsAsView,
                                                           frequencies,
                                                           blockPotentiallyOffensive,
                                                           wordLearned);
        }

        static bool
        unlearnFromUserHistoryNative(DictionaryFacilitator *dictionaryFacilitator, CodePoints wordCodePoints) {
            if (isNotValid(dictionaryFacilitator, "unlearn from user history")) {
                return false;
            }
            return dictionaryFacilitator->unlearnFromUserHistory(
                    CodePointArrayView(&wordCodePoints[0], wordCodePoints.size()));
        }

        static bool
        unlearnFromUserHistoryNative(DictionaryFacilitator *dictionaryFacilitator,
                                     const int prevWordCodePoints[][MAX_WORD_LENGTH],
                                     const int prevWordCodePointCount[],
                                     const bool isBeginningOfSentence[],
                                     const int prevWordCount, CodePoints wordCodePoints) {
            if (isNotValid(dictionaryFacilitator, "unlearn from user history")) {
                return false;
            }

            const NgramContext ngramContext = NgramContext(prevWordCodePoints, prevWordCodePointCount,
                                                           isBeginningOfSentence,
                                                           static_cast<const size_t>(prevWordCount));
            return dictionaryFacilitator->unlearnFromUserHistory(&ngramContext,
                                                                 CodePointArrayView(&wordCodePoints[0],
                                                                                    wordCodePoints.size()));
        }

        static bool saveUserHistoryDictionaryNative(DictionaryFacilitator *dictionaryFacilitator) {
            if (isNotValid(dictionaryFacilitator, "save user history dictionary")) {
                return false;
            }
            return dictionaryFacilitator->saveUserHistoryDictionary();
        }

        static void clearUserHistoryDictionaryNative(DictionaryFacilitator *dictionaryFacilitator) {
            if (isNotValid(dictionaryFacilitator, "clear user history dictionary")) {
                return;
            }
            dictionaryFacilitator->clearUserHistoryDictionary();
        }

        static void clearUserWordsDictionaryNative(DictionaryFacilitator *dictionaryFacilitator) {
            if (isNotValid(dictionaryFacilitator, "clear user words dictionary")) {
                return;
            }
            dictionaryFacilitator->clearUserWordsDictionaryNative();
        }

        static void clearContactsDictionaryNative(DictionaryFacilitator *dictionaryFacilitator) {
            if (isNotValid(dictionaryFacilitator, "clear contacts words dictionary")) {
                return;
            }
            dictionaryFacilitator->clearContactsDictionary();
        }

        static bool isWordExistsNative(DictionaryFacilitator *dictionaryFacilitator, CodePoints wordCodePoints) {
            if (isNotValid(dictionaryFacilitator, "word exists")) {
                return false;
            }
            return dictionaryFacilitator->isWordExists(CodePointArrayView(&wordCodePoints[0], wordCodePoints.size()));
        }

        static std::string getVersionNative(DictionaryFacilitator *dictionaryFacilitator) {
            if (isNotValid(dictionaryFacilitator, "getVersion")) {
                return "-1";
            }
            const std::string version = dictionaryFacilitator->getVersion();
            AKLOGI("DictionaryFacilitator: Version %s\n", version.c_str());
            return version;
        }

        static NlpResults getSuggestionResultsNative(DictionaryFacilitator *dictionaryFacilitator,
                                                     const int prevWordCodePoints[MAX_WORD_LENGTH][MAX_WORD_LENGTH],
                                                     const int prevWordCodePointCount[],
                                                     const bool isBeginningOfSentence[],
                                                     const int prevWordCount,
                                                     CodePoints wordCodePoints,
                                                     int x[], int y[], int times[], int pointers[],
                                                     int options[MAX_OPTIONS_LENGTH]) {
            if (isNotValid(dictionaryFacilitator, "get suggestions")) {
                return NlpResults();
            }

            NgramContext ngramContext =
                    NgramContext(prevWordCodePoints, prevWordCodePointCount, isBeginningOfSentence, prevWordCount);
            SuggestOptions givenSuggestOptions(options, MAX_OPTIONS_LENGTH);

            return dictionaryFacilitator->getSuggestionResults(&ngramContext, std::move(wordCodePoints), x, y, times, pointers,
                                                               &givenSuggestOptions);
        }

        static NlpResults getEmojiSuggestionResultsNative(DictionaryFacilitator *dictionaryFacilitator,
                                                          const int prevWordCodePoints[MAX_WORD_LENGTH][MAX_WORD_LENGTH],
                                                          const int prevWordCodePointCount[],
                                                          const bool isBeginningOfSentence[],
                                                          const int prevWordCount,
                                                          CodePoints wordCodePoints,
                                                          int x[], int y[], int times[], int pointers[],
                                                          int options[MAX_OPTIONS_LENGTH]) {
            if (isNotValid(dictionaryFacilitator, "get emoji suggestions")) {
                return NlpResults();
            }

            NgramContext ngramContext =
                    NgramContext(prevWordCodePoints, prevWordCodePointCount, isBeginningOfSentence, prevWordCount);
            SuggestOptions givenSuggestOptions(options, MAX_OPTIONS_LENGTH);
            
            CodePointArrayView codePoints(&wordCodePoints[0], static_cast<const size_t>(wordCodePoints.size()));
            return dictionaryFacilitator->getEmojiSuggestions(&ngramContext, codePoints, x, y, times, pointers,
                                                              &givenSuggestOptions);
        }

        static int getProbability(DictionaryFacilitator *dictionaryFacilitator, CodePoints word, CodePoints shortcut) {
            if (isNotValid(dictionaryFacilitator, "Get probability")) {
                return NOT_A_PROBABILITY;
            }
            CodePointArrayView wordAsView(&word[0], word.size());
            CodePointArrayView shortcutAsView(&shortcut[0], shortcut.size());
            return dictionaryFacilitator->getProbabilityFromMainDictionary(wordAsView, shortcutAsView);
        }

        static std::string getDictionaryInfo(DictionaryFacilitator *dictionaryFacilitator, int dicIndex) {
            if (isNotValid(dictionaryFacilitator, "get dictionary info")) {
                return "-1";
            }
            return dictionaryFacilitator->getDictionaryInfo(dicIndex);
        }

        static std::string dumpDictionary(DictionaryFacilitator *dictionaryFacilitator, int index) {
            if (isNotValid(dictionaryFacilitator, "dump dictionary"))
                return "";
            return dictionaryFacilitator->getDictionaryDump(index);
        }


    private:

        /**
         * Check if the facilitator pointer is valid, prints error if not
         * @param dictionaryFacilitator
         * @param tryingTo
         * @return true if point is NOT valid
         */
        static bool isNotValid(DictionaryFacilitator *dictionaryFacilitator, std::string tryingTo) {
            if (dictionaryFacilitator)
                return false;

            AKLOGEC("Dictionary Facilitator was not initialized! trying to %s", tryingTo.c_str());
            return true;
        }
    }; // namespace latinime



}
#endif /* NLP_ENGINE_HELPER_H */
