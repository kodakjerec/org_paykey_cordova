/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_DIC_NODES_CACHE_H
#define LATINIME_DIC_NODES_CACHE_H

#include <algorithm>

#include "defines.h"
#include "dic_node_priority_queue.h"
#include "dic_node_index_priority_queue.h"

namespace latinime {

    class DicNode;

/**
 * Class for controlling dicNode search priority queue and lexicon trie traversal.
 */
    class DicNodesCache {
    public:
        AK_FORCE_INLINE explicit DicNodesCache(const bool usesLargeCapacityCache, const int terminalQueueSize)
                : mUsesLargeCapacityCache(usesLargeCapacityCache),
                  mDicNodePriorityQueue0(getCacheCapacity()),
                  mDicNodePriorityQueue1(getCacheCapacity()),
                  mDicNodePriorityQueue2(getCacheCapacity()),
                  mDicNodePriorityQueueForTerminal(terminalQueueSize),
                  mActiveDicNodes(&mDicNodePriorityQueue0),
                  mNextActiveDicNodes(&mDicNodePriorityQueue1),
                  mCachedDicNodesForContinuousSuggestion(&mDicNodePriorityQueue2),
                  mTerminalDicNodes(&mDicNodePriorityQueueForTerminal),
                  mInputIndex(0), mLastCachedInputIndex(0) {

        }

        AK_FORCE_INLINE virtual ~DicNodesCache() {}

        AK_FORCE_INLINE void reset(const int nextActiveSize, const int terminalSize) {
            mInputIndex = 0;
            mLastCachedInputIndex = 0;
            // The size of current active DicNode queue doesn't have to be changed.
            mActiveDicNodes->clear();
            // nextActiveSize is used to limit the next iteration's active DicNode size.
            const int nextActiveSizeFittingToTheCapacity = std::min(nextActiveSize, getCacheCapacity());
            mNextActiveDicNodes->clearAndResize(nextActiveSizeFittingToTheCapacity);
            mTerminalDicNodes->clearAndResize(terminalSize);
            // The size of cached DicNode queue doesn't have to be changed.
            mCachedDicNodesForContinuousSuggestion->clear();
        }

        AK_FORCE_INLINE void continueSearch() {
            resetTemporaryCaches();
            restoreActiveDicNodesFromCache();
        }

        AK_FORCE_INLINE void advanceActiveDicNodes() {
            if (DEBUG_DICT) {
                AKLOGI("Advance active %d nodes.\n", mNextActiveDicNodes->getSize());
            }
            if (DEBUG_DICT_FULL) {
                mNextActiveDicNodes->dump();
            }
            mNextActiveDicNodes =
                    moveNodesAndReturnReusableEmptyQueue(mNextActiveDicNodes, &mActiveDicNodes);
        }

        int activeSize() const { return mActiveDicNodes->getSize(); }

        int totalTerminalsSize() const { return mTerminalDicNodes->getTotalTerminalSize(); }

        int terminalsSize(const int index) const { return mTerminalDicNodes->getSize(index); }

        int terminalsBucketsCount() const { return mTerminalDicNodes->getBucketCount(); }

        bool isLookAheadCorrectionInputIndex(const int inputIndex) const {
            return inputIndex == mInputIndex - 1;
        }

        void advanceInputIndex(const int inputSize) {
            if (mInputIndex < inputSize) {
                mInputIndex++;
            }
        }

        AK_FORCE_INLINE void copyPushTerminal(const int queueIndex, DicNode *dicNode) {
            mTerminalDicNodes->copyPush(queueIndex, dicNode);
        }

        AK_FORCE_INLINE void copyPushActive(DicNode *dicNode) {
            mActiveDicNodes->copyPush(dicNode);
        }

        AK_FORCE_INLINE void copyPushContinue(DicNode *dicNode) {
            mCachedDicNodesForContinuousSuggestion->copyPush(dicNode);
        }

        AK_FORCE_INLINE void copyPushNextActive(DicNode *dicNode) {
            mNextActiveDicNodes->copyPush(dicNode);
        }

        void popTerminal(const int index, DicNode *dest) {
            mTerminalDicNodes->copyPop(index, dest);
        }

        void popAll(std::vector<DicNode> &terminals) {
            mTerminalDicNodes->copyPopAll(terminals);
        }
        
        void dumpAllTerminals(){
            mTerminalDicNodes->dump();
        }

        void popActive(DicNode *dest) {
            mActiveDicNodes->copyPop(dest);
        }

        bool hasCachedDicNodesForContinuousSuggestion() const {
            bool a = mCachedDicNodesForContinuousSuggestion != NULL;
            int a2 = mCachedDicNodesForContinuousSuggestion->getSize();
            return a && a2 > 0;
        }

        AK_FORCE_INLINE bool isCacheBorderForTyping(const int inputSize, int cacheBackLength) const {
            const int cacheInputIndex = inputSize - cacheBackLength;
            const bool shouldCache = (cacheInputIndex == mInputIndex)
                                     && (cacheInputIndex != mLastCachedInputIndex);
            return shouldCache;
        }

        AK_FORCE_INLINE void updateLastCachedInputIndex() {
            mLastCachedInputIndex = mInputIndex;
        }
        
        AK_FORCE_INLINE int getTerminalBucketMaxSize(){
            return mDicNodePriorityQueueForTerminal.computeDividedBucketSize();
        }

        AK_FORCE_INLINE int getTerminalQueueMaxSize() {
            return mDicNodePriorityQueueForTerminal.getMaxSize();
        }

        AK_FORCE_INLINE void reduceTerminalQueueMaxSize(int size) {
            mDicNodePriorityQueueForTerminal.resetAndReduceMaxSize(size);
        }
        
        const std::vector<int> getTerminalBucketIndex() const {
            return mDicNodePriorityQueueForTerminal.getBucketsKeys();
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(DicNodesCache);

        AK_FORCE_INLINE void restoreActiveDicNodesFromCache() {
            if (DEBUG_DICT) {
                AKLOGI("Restore %d nodes. inputIndex = %d.",
                       mCachedDicNodesForContinuousSuggestion->getSize(), mLastCachedInputIndex);
            }
            if (DEBUG_DICT_FULL || DEBUG_CACHE) {
                mCachedDicNodesForContinuousSuggestion->dump();
            }
            mInputIndex = mLastCachedInputIndex;
            mCachedDicNodesForContinuousSuggestion = moveNodesAndReturnReusableEmptyQueue(
                    mCachedDicNodesForContinuousSuggestion, &mActiveDicNodes);
        }

        AK_FORCE_INLINE static DicNodePriorityQueue *moveNodesAndReturnReusableEmptyQueue(
                DicNodePriorityQueue *src, DicNodePriorityQueue **dest) {
            const int srcMaxSize = src->getMaxSize();
            const int destMaxSize = (*dest)->getMaxSize();
            DicNodePriorityQueue *tmp = *dest;
            *dest = src;
            (*dest)->setMaxSize(destMaxSize);
            tmp->clearAndResize(srcMaxSize);
            return tmp;
        }

        AK_FORCE_INLINE int getCacheCapacity() const {
            return mUsesLargeCapacityCache ?
                   LARGE_PRIORITY_QUEUE_CAPACITY : SMALL_PRIORITY_QUEUE_CAPACITY;
        }

        AK_FORCE_INLINE void resetTemporaryCaches() {
            mActiveDicNodes->clear();
            mNextActiveDicNodes->clear();
            mTerminalDicNodes->clear();
        }

        static const int LARGE_PRIORITY_QUEUE_CAPACITY;
        static const int SMALL_PRIORITY_QUEUE_CAPACITY;

        const bool mUsesLargeCapacityCache;
        // Instances
        DicNodePriorityQueue mDicNodePriorityQueue0;
        DicNodePriorityQueue mDicNodePriorityQueue1;
        DicNodePriorityQueue mDicNodePriorityQueue2;
        DicNodeIndexPriorityQueue mDicNodePriorityQueueForTerminal;

        // Active dicNodes currently being expanded.
        DicNodePriorityQueue *mActiveDicNodes;
        // Next dicNodes to be expanded.
        DicNodePriorityQueue *mNextActiveDicNodes;
        // Cached dicNodes used for continuous suggestion.
        DicNodePriorityQueue *mCachedDicNodesForContinuousSuggestion;
        // Current top terminal dicNodes.
        DicNodeIndexPriorityQueue *mTerminalDicNodes;
        int mInputIndex;
        int mLastCachedInputIndex;
    };
} // namespace latinime
#endif // LATINIME_DIC_NODES_CACHE_H
