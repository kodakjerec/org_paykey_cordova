#include <NlpEngineHelper.h>

#ifndef NLP_FACILITATOR_DEL_H
#define NLP_FACILITATOR_DEL_H

namespace latinime {

    class DictionaryFacilitatorDelegate {

    public:

        DictionaryFacilitator *mFacilitator;

        void create();

        void close();

        void loadKeyboardData(int keyboardId, int gridWidth, int gridHeight, int kbdWidth, int kbdHeight,
                              nlpEngineKeys *keyboardKeys,
                              int mostCommonKeyWidth, int mostCommonKeyHeight);

        bool loadMainDictionary(const char *dictionaryPath, long dictionarySize, long fileOffset);

        bool loadUserWordsDictionary(std::vector<int> localeCodePoints);

        bool loadContactsDictionary();

        bool loadUserHistoryDictionary(const char *dictionaryPath, std::vector<int> localeCodePoints);

        bool loadEmojiDictionary(const char *dictionaryPath, long dictionarySize, long fileOffset,
                                 bool alsoUseForPrediction);

        void unloadMainDictionary();

        void unloadUserWordsDictionary();

        void unloadEmojiDictionary();

        void unloadContactsDictionary();

        void unloadUserHistoryDictionary();

        std::string getVersion();

        std::string getDictionaryInfo(int index);

        bool addContact(CodePointsList fullName);


        bool addToUserHistory(const int (*prevWordCodePoints)[MAX_WORD_LENGTH],
                              const int *prevWordCodePointCount,
                              const bool *isBeginningOfSentence,
                              int prevWordCount,
                              CodePointsList words,
                              bool wasAutoCapitalized,
                              bool blockPotentiallyOffensive);

        bool addToUserHistory(const int (*prevWordCodePoints)[MAX_WORD_LENGTH],
                              const int *prevWordCodePointCount,
                              const bool *isBeginningOfSentence,
                              int prevWordCount,
                              CodePoints word,
                              bool wasAutoCapitalized,
                              const CodePointsList *shortcuts,
                              const std::vector<int> *frequencies,
                              bool blockPotentiallyOffensive);

        bool addUserWord(CodePoints word, CodePoints shortcut);

        bool unlearnFromUserHistory(CodePoints wordCodePoints);

        bool unlearnFromUserHistory(const int prevWordCodePoints[MAX_PREV_WORD_COUNT_FOR_N_GRAM][MAX_WORD_LENGTH],
                                    const int prevWordCodePointCount[MAX_PREV_WORD_COUNT_FOR_N_GRAM],
                                    const bool isBeginningOfSentence[MAX_PREV_WORD_COUNT_FOR_N_GRAM],
                                    const int prevWordCount, CodePoints wordCodePoints);

        bool saveUserHistoryDictionary();

        void clearUserHistoryDictionary();

        void clearContactsDictionary();

        void clearUserWordsDictionaryNative();

        std::string dumpDictionary(int index);

        int isWordExists(CodePoints wordCodePoints);

        int getSuggestions(const int prevWordCodePoints[MAX_PREV_WORD_COUNT_FOR_N_GRAM][MAX_WORD_LENGTH],
                           const int prevWordCodePointCount[MAX_PREV_WORD_COUNT_FOR_N_GRAM],
                           const bool isBeginningOfSentence[MAX_PREV_WORD_COUNT_FOR_N_GRAM],
                           int prevWordCount,
                           int inputSize, CodePoints inputCodePoints,
                           int *x, int *y, int *t,
                           int options[MAX_OPTIONS_LENGTH],
                           std::string *outSuggestions,
                           int *outScores,
                           int *outTypes,
                           int *outInputData,
                           std::string *outSuggestionsParentss,
                           int *status);

        int getEmojiSuggestions(const int prevWordCodePoints[MAX_PREV_WORD_COUNT_FOR_N_GRAM][MAX_WORD_LENGTH],
                                const int prevWordCodePointCount[MAX_PREV_WORD_COUNT_FOR_N_GRAM],
                                const bool isBeginningOfSentence[MAX_PREV_WORD_COUNT_FOR_N_GRAM],
                                int prevWordCount,
                                int inputSize, CodePoints inputCodePoints,
                                int *x, int *y, int *t,
                                int options[MAX_OPTIONS_LENGTH],
                                std::string *outSuggestions,
                                int *outScores,
                                int *outTypes,
                                int *outInputData,
                                std::string *outSuggestionsParentss,
                                int *status);

        int getProbability(CodePoints word, CodePoints shortcut) const;

    private:
        int formatFacilitatorResults(int inputSize, std::string *outSuggestions,
                                     int *outScores, int *outTypes,
                                     int *outInputData,
                                     std::string *outSuggestionsParents, int *status,
                                     const NlpResults &nlpResults) const;
    };

} // namespace latinime
#endif /* NLP_FACILITATOR_H */
