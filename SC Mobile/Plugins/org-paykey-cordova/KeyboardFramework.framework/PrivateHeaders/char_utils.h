/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_CHAR_UTILS_H
#define LATINIME_CHAR_UTILS_H

#include <cctype>
#include <cstring>
#include <vector>

#include "defines.h"

namespace latinime {

    class CharUtils {
    public:
        static const std::vector<int> EMPTY_STRING;

        static bool isEqual(const int *aArray, int alen, const int *bArray, int blen) {
            if (alen != blen)
                return false;

            for (int i = 0; i < alen; ++i) {
                int a = aArray[i];
                int b = bArray[i];
                if ((a != b) || !a)
                    return false;
            }

            return true;
        }

        static bool compareIgnoreCase(int const a, int const b) {
            int d = toBaseLowerCase(a) - toBaseLowerCase(b);
            if (d != 0 || !a)
                return false;

            return true;
        }

        static bool compareIgnoreCase(const int *word, char *word2) {
            char newWord[MAX_WORD_LENGTH];
            int newLen = intArrayToCharArray(word, MAX_WORD_LENGTH, newWord, MAX_WORD_LENGTH);
            int word2Len = getArraySize(word2, true);

            if (word2Len != newLen)
                return false;

            for (int i = 0; i < word2Len; ++i) {
                int a = newWord[i];
                int b = word2[i];
                int d = toBaseLowerCase(a) - toBaseLowerCase(b);
                if (d != 0 || !a)
                    return false;
            }


            return true;
        }

        static AK_FORCE_INLINE bool isAsciiUpper(int c) {
            // Note: isupper(...) reports false positives for some Cyrillic characters, causing them to
            // be incorrectly lower-cased using toAsciiLower(...) rather than latin_tolower(...).
            return (c >= 'A' && c <= 'Z');
        }

        static AK_FORCE_INLINE bool isUpper(int c) {
            // Note: isupper(...) reports false positives for some Cyrillic characters, causing them to
            // be incorrectly lower-cased using toAsciiLower(...) rather than latin_tolower(...).
            if (isascii(c))
                return isAsciiUpper(c);

            return latin_isupper(c);
        }

        static AK_FORCE_INLINE bool isAsciiLower(int c) {
            // Note: isupper(...) reports false positives for some Cyrillic characters, causing them to
            // be incorrectly lower-cased using toAsciiLower(...) rather than latin_tolower(...).
            return (c >= 'a' && c <= 'z');
        }

        static AK_FORCE_INLINE int getArraySize(char array[], bool countOmission) {
            int index = 0;
            int count = 0;
            while (array[index] != 0) {
                if (countOmission || !CharUtils::isIntentionalOmissionCodePoint(array[index])) {
                    count++;
                }

                index++;
            }
            return count;
        }


        static AK_FORCE_INLINE int toLowerCase(const int c) {
            if (isAsciiUpper(c)) {
                return toAsciiLower(c);
            }
            if (isAscii(c)) {
                return c;
            }
            return latin_tolower(c);
        }

        static AK_FORCE_INLINE int toUpperCase(const int c) {
            if (isAsciiLower(c)) {
                return toAsciiUpper(c);
            }
            if (isAscii(c)) {
                return c;
            }
            return latin_toupper(c);
        }

        static AK_FORCE_INLINE void toLowerCase(int *arr, int len) {
            for (int i = 0; i < len; ++i) {
                arr[i] = tolower(arr[i]);
            }
        }

        static AK_FORCE_INLINE int toBaseUpperCase(const int c) {
            return toUpperCase(toBaseCodePoint(c));
        }

        static AK_FORCE_INLINE int toBaseLowerCase(const int c) {
            return toLowerCase(toBaseCodePoint(c));
        }

        static AK_FORCE_INLINE bool isIntentionalOmissionCodePoint(const int codePoint) {
            // TODO: Do not hardcode here
            return codePoint == KEYCODE_SINGLE_QUOTE || codePoint == KEYCODE_HYPHEN_MINUS
                   /*Zhuyin TW*/
                   || codePoint == KEYCODE_COMBINING_MACRON
                   || codePoint == KEYCODE_COMBINING_ACUTE_ACCENT
                   || codePoint == KEYCODE_COMBINING_CARON
                   || codePoint == KEYCODE_COMBINING_GRAVE_ACCENT;
        }

        static AK_FORCE_INLINE int getCodePointCount(const int arraySize, const int *const codePoints) {
            int size = 0;
            for (; size < arraySize; ++size) {
                if (codePoints[size] == '\0') {
                    break;
                }
            }
            return size;
        }

        static AK_FORCE_INLINE int toBaseCodePoint(int c) {
            if (c < BASE_CHARS_SIZE) {
                return static_cast<int>(BASE_CHARS[c]);
            }
            return c;
        }

        static AK_FORCE_INLINE int getSpaceCount(const int *const codePointBuffer, const int length) {
            int spaceCount = 0;
            for (int i = 0; i < length; ++i) {
                if (codePointBuffer[i] == KEYCODE_SPACE) {
                    ++spaceCount;
                }
            }
            return spaceCount;
        }

        static AK_FORCE_INLINE int isInUnicodeSpace(const int codePoint) {
            return codePoint >= MIN_UNICODE_CODE_POINT && codePoint <= MAX_UNICODE_CODE_POINT;
        }

        // Returns updated code point count. Returns 0 when the code points cannot be marked as a
        // Beginning-of-Sentence.
        static AK_FORCE_INLINE int attachBeginningOfSentenceMarker(int *const codePoints,
                                                                   const int codePointCount, const int maxCodePoint) {
            if (codePointCount > 0 && codePoints[0] == CODE_POINT_BEGINNING_OF_SENTENCE) {
                // Marker has already been attached.
                return codePointCount;
            }
            if (codePointCount >= maxCodePoint) {
                // the code points cannot be marked as a Beginning-of-Sentence.
                return 0;
            }
            memmove(codePoints + 1, codePoints, sizeof(int) * codePointCount);
            codePoints[0] = CODE_POINT_BEGINNING_OF_SENTENCE;
            return codePointCount + 1;
        }

        // Returns updated code point count.
        static AK_FORCE_INLINE int removeBeginningOfSentenceMarker(int *const codePoints,
                                                                   const int codePointCount) {
            if (codePointCount <= 0 || codePoints[0] != CODE_POINT_BEGINNING_OF_SENTENCE) {
                return codePointCount;
            }
            const int newCodePointCount = codePointCount - 1;
            memmove(codePoints, codePoints + 1, sizeof(int) * newCodePointCount);
            return newCodePointCount;
        }


        static AK_FORCE_INLINE void outputCodePoints(int outCodePoints[],
                                                     const int codePoints[],
                                                     unsigned long codePointCount) {
            int outIndex = 0;
            for (int i = 0;  static_cast<unsigned long> (i) < codePointCount; ++i) {
                int codePoint = codePoints[i];
                if (!CharUtils::isInUnicodeSpace(codePoint)) {
                    if (codePoint == CODE_POINT_BEGINNING_OF_SENTENCE) {
                        // Just skip Beginning-of-Sentence marker.
                        continue;
                    }
                    codePoint = CODE_POINT_REPLACEMENT_CHARACTER;
                } else if (codePoint >= 0x01 && codePoint <= 0x1F) {
                    // Control code.
                    codePoint = CODE_POINT_REPLACEMENT_CHARACTER;
                }
                outCodePoints[outIndex++] = codePoint;
            }

            for (int i = outIndex; static_cast<unsigned long> (i) < codePointCount; ++i) {
                outCodePoints[i] = 0 /*null*/;
            }
        }

        static AK_FORCE_INLINE void transformAllToBaseUpper(const int *c, int size, int *out) {
            for (int i = 0; i < size; ++i) {
                out[i] = toBaseUpperCase(c[i]);
            }
        }

        static AK_FORCE_INLINE void transformAllToUpper(const int *c, int size, int *out) {
            for (int i = 0; i < size; ++i) {
                out[i] = toUpperCase(c[i]);
            }
        }

    private:
        DISALLOW_IMPLICIT_CONSTRUCTORS(CharUtils);

        static const int MIN_UNICODE_CODE_POINT;
        static const int MAX_UNICODE_CODE_POINT;
        static const unsigned int CODE_POINT_REPLACEMENT_CHARACTER;

        /**
         * Table mapping most combined Latin, Greek, and Cyrillic characters
         * to their base characters.  If c is in range, BASE_CHARS[c] == c
         * if c is not a combined character, or the base character if it
         * is combined.
         */
        static const int BASE_CHARS_SIZE = 0x0500;
        static const unsigned short BASE_CHARS[BASE_CHARS_SIZE];

        static AK_FORCE_INLINE bool isAscii(int c) {
            return isascii(c) != 0;
        }

        static AK_FORCE_INLINE int toAsciiLower(int c) {
            return c - 'A' + 'a';
        }

        static AK_FORCE_INLINE int toAsciiUpper(int c) {
            return c - 'a' + 'A';
        }

        static int latin_tolower(int c);

        static int latin_toupper(int c);

        static int latin_isupper(int c);
    };
} // namespace latinime
#endif // LATINIME_CHAR_UTILS_H
