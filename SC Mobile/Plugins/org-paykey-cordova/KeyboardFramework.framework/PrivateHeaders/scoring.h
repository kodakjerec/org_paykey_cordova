/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_SCORING_H
#define LATINIME_SCORING_H

#include "defines.h"

namespace latinime {

class DicNode;
class DicTraverseSession;
class SuggestionResults;

// This class basically tweaks suggestions and distances apart from CompoundDistance
class Scoring {
 public:
    virtual int calculateFinalScore(float compoundDistance, int inputSize,
                                    ErrorTypeUtils::ErrorType containedErrorTypes, bool forceCommit,
                                    bool boostExactMatches, bool hasProbabilityZero,int spaces,int edits) const = 0;
    virtual void getMostProbableString(const DicTraverseSession *traverseSession,
                                       float weightOfLangModelVsSpatialModel,
            SuggestionResults *outSuggestionResults) const = 0;
    virtual float getAdjustedWeightOfLangModelVsSpatialModel(
            DicTraverseSession *traverseSession, DicNode *terminals,
            int size) const = 0;
    virtual float getDoubleLetterDemotionDistanceCost(
            const DicNode *terminalDicNode) const = 0;
    virtual bool autoCorrectsToMultiWordSuggestionIfTop() const = 0;
    virtual bool sameAsTyped(const DicTraverseSession *traverseSession,
            const DicNode *dicNode) const = 0;

 protected:
    Scoring() {}
    virtual ~Scoring() {}

 private:
    DISALLOW_COPY_AND_ASSIGN(Scoring);
};
} // namespace latinime
#endif // LATINIME_SCORING_H
