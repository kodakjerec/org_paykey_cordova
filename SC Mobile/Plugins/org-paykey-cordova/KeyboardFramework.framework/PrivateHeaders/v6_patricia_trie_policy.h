/*
 * Copyright (C) 2013, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_PATRICIA_TRIE_POLICY_V6_H
#define LATINIME_PATRICIA_TRIE_POLICY_V6_H

#include <cstdint>
#include <vector>

#include "defines.h"
#include "header_policy.h"
#include "dictionary_structure_with_buffer_policy.h"
#include "bigram_list_policy.h"
#include "shortcut_list_policy.h"
#include "ver6_patricia_trie_node_reader.h"
#include "ver6_pt_node_array_reader.h"
#include "format_utils.h"
#include "mmapped_buffer.h"
#include "byte_array_view.h"
#include "int_array_view.h"

namespace latinime {

    class DicNode;

    class DicNodeVector;

// Word id = Position of a PtNode that represents the word.
// Max supported n-gram is bigram.
    class V6PatriciaTriePolicy : public DictionaryStructureWithBufferPolicy {
    public:
        explicit V6PatriciaTriePolicy(MmappedBuffer::MmappedBufferPtr mmappedBuffer)
                : mMmappedBuffer(std::move(mmappedBuffer)),
                  mHeaderPolicy(mMmappedBuffer->getReadOnlyByteArrayView().data(),
                                FormatUtils::detectFormatVersion(mMmappedBuffer->getReadOnlyByteArrayView())),
                  mBuffer(mMmappedBuffer->getReadOnlyByteArrayView().skip(
                          static_cast<const size_t>(mHeaderPolicy.getSize()))),
                  mBigramListPolicy(mBuffer), mShortcutListPolicy(mBuffer),
                  mPtNodeReader(mBuffer, &mBigramListPolicy, &mShortcutListPolicy,
                                mHeaderPolicy.getCodePointTable()),
                  mPtNodeArrayReader(mBuffer), mTerminalPtNodePositionsForIteratingWords(),
                  mIsCorrupted(false) {}

        AK_FORCE_INLINE int getRootPosition() const override {
            return 0;
        }

        void createAndGetAllChildDicNodes(const DicNode *dicNode, DicNodeVector *childDicNodes) const override;

        int getCodePointsAndReturnCodePointCount(int wordId, int maxCodePointCount,
                                                 int *outCodePoints) const override;

        int getWordId(CodePointArrayView wordCodePoints, bool forceLowerCaseSearch) const override;

        const WordAttributes getWordAttributesInContext(WordIdArrayView prevWordIds,
                                                        int wordId, MultiBigramMap *multiBigramMap) const override;

        int getProbability(int unigramProbability, int bigramProbability) const override;

        int getProbabilityOfWord(WordIdArrayView prevWordIds, int wordId, bool absolute) const override;


        void iterateNgramEntries(WordIdArrayView prevWordIds, NgramListener *listener) const override;

        BinaryDictionaryShortcutIterator getShortcutIterator(int wordId) const override;

        const DictionaryHeaderStructurePolicy *getHeaderStructurePolicy() const override {
            return &mHeaderPolicy;
        }

        bool addUnigramEntry(const CodePointArrayView wordCodePoints,
                             const UnigramProperty *const unigramProperty) override {
            // This method should not be called for non-updatable dictionary.
            AKLOGI("Warning: addUnigramEntry() is called for non-updatable dictionary.");
            return false;
        }

        bool removeUnigramEntry(const CodePointArrayView wordCodePoints) override {
            // This method should not be called for non-updatable dictionary.
            AKLOGI("Warning: removeUnigramEntry() is called for non-updatable dictionary.");
            return false;
        }

        bool addNgramEntry(const NgramProperty *const ngramProperty) override {
            // This method should not be called for non-updatable dictionary.
            AKLOGI("Warning: addNgramEntry() is called for non-updatable dictionary.");
            return false;
        }

        bool removeNgramEntry(const NgramContext *const ngramContext,
                              const CodePointArrayView wordCodePoints) override {
            // This method should not be called for non-updatable dictionary.
            AKLOGI("Warning: removeNgramEntry() is called for non-updatable dictionary.");
            return false;
        }

        bool updateEntriesForWordWithNgramContext(const NgramContext *const ngramContext,
                                                  const CodePointArrayView wordCodePoints, const bool isValidWord,
                                                  const bool isNotAWord,
                                                  const HistoricalInfo historicalInfo) override {
            // This method should not be called for non-updatable dictionary.
            AKLOGI("Warning: updateEntriesForWordWithNgramContext() is called for non-updatable "
                   "dictionary.");
            return false;
        }


        bool updateShortcutsEntriesForWord(const CodePointArrayView wordCodePoints,
                                           std::vector<UnigramProperty::ShortcutProperty> shortcuts) override {
            // This method should not be called for non-updatable dictionary.
            AKLOGI("Warning: updateShortcutsEntriesForWord() is called for non-updatable "
                   "dictionary.");
            return false;
        }

        bool flush(const char *const filePath) override {
            // This method should not be called for non-updatable dictionary.
            AKLOGI("Warning: flush() is called for non-updatable dictionary.");
            return false;
        }

        bool supportsShortcutsIndividualFrequency() const override;

        bool flushWithGC(const char *const filePath) override {
            // This method should not be called for non-updatable dictionary.
            AKLOGI("Warning: flushWithGC() is called for non-updatable dictionary.");
            return false;
        }

        bool needsToRunGC(const bool mindsBlockByGC) const override {
            // This method should not be called for non-updatable dictionary.
            AKLOGI("Warning: needsToRunGC() is called for non-updatable dictionary.");
            return false;
        }

        void getProperty(const char *const query, const int queryLength, char *const outResult,
                         const int maxResultLength) override {
            // getProperty is not supported for this class.
            if (maxResultLength > 0) {
                outResult[0] = '\0';
            }
        }

        const WordProperty getWordProperty(CodePointArrayView wordCodePoints) const override;

        bool isOffensive(CodePointArrayView wordCodePoints) const override;

        int getNextWordAndNextToken(int token, int *outCodePoints,
                                    int *outCodePointCount) override;

        bool isCorrupted() const override {
            return mIsCorrupted;
        }

    private:
        DISALLOW_IMPLICIT_CONSTRUCTORS(V6PatriciaTriePolicy);

        const MmappedBuffer::MmappedBufferPtr mMmappedBuffer;
        const HeaderPolicy mHeaderPolicy;
        const ReadOnlyByteArrayView mBuffer;
        const BigramListPolicy mBigramListPolicy;
        const ShortcutListPolicy mShortcutListPolicy;
        const Ver6ParticiaTrieNodeReader mPtNodeReader;
        const Ver6PtNodeArrayReader mPtNodeArrayReader;
        std::vector<int> mTerminalPtNodePositionsForIteratingWords;
        mutable bool mIsCorrupted;

        int getCodePointsAndProbabilityAndReturnCodePointCount(int wordId, int maxCodePointCount, int *outCodePoints,
                                                               int *outUnigramProbability) const;

        int getShortcutPositionOfPtNode(int ptNodePos) const;

        int getBigramsPositionOfPtNode(int ptNodePos) const;

        int createAndGetLeavingChildNode(const DicNode *dicNode, int ptNodePos, DicNodeVector *childDicNodes) const;

        int getWordIdFromTerminalPtNodePos(int ptNodePos) const;

        int getTerminalPtNodePosFromWordId(int wordId) const;

        const WordAttributes getWordAttributes(int probability, const PtNodeParams &ptNodeParams) const;

        bool isValidPos(int pos) const;
    };
} // namespace latinime
#endif // LATINIME_PATRICIA_TRIE_POLICY_V6_H
