//
// Created by YakirPayKey on 3/20/18.
//

#ifndef NLP_ENGINE_USERHISTORYDICTIONARY_H
#define NLP_ENGINE_USERHISTORYDICTIONARY_H

#include "ExpandableDictionary.h"

namespace latinime {


    class UserHistoryDictionary : public ExpandableDictionary {

    public:
        static const char *const DICTIONARY_NAME;

        static UserHistoryDictionary *createUserHistoryDictionary(const char *const dictionaryPath, CodePoints locale) {
            int pathLength = static_cast<int>(strlen(dictionaryPath));
            std::string newPath = dictionaryPath;
            if (dictionaryPath[pathLength - 1] != '/')
                newPath += "/";

            for (int i : locale) {
                newPath += (char) i;
            }
            
            newPath += '_';
            newPath += DICTIONARY_NAME;
            newPath += ExpandableDictionary::DICT_FILE_EXTENSION;
            char *c_newPath = new char[sizeof(char) * newPath.size() + 1];
            memmove(c_newPath, newPath.c_str(), newPath.size());
            c_newPath[newPath.size()] = '\0';
            UserHistoryDictionary *userHistoryDictionary = new UserHistoryDictionary(c_newPath);
            userHistoryDictionary->setLocale(locale);
            delete[] c_newPath;
            return userHistoryDictionary;
        }

        bool addToDictionary(const NgramContext *ngramContext, CodePointArrayView word, bool isValid,
                             bool isNotAWord,
                             int timestamp, const CodePointArraysView *shortcuts,
                             const std::vector<int> *frequencies);

    private:

        bool addWordShortcuts(CodePointArrayView word, const CodePointArraysView *shortcuts,
                              const std::vector<int> *frequencies);

        explicit UserHistoryDictionary(const char *const filePath) :
                ExpandableDictionary(DICTIONARY_NAME, filePath) {};

        DictionaryHeaderStructurePolicy::AttributeMap getHeaderAttributes() override;

    };

}

#endif //NLP_ENGINE_USERHISTORYDICTIONARY_H
