/*
 * Copyright (C) 2013, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_PT_NODE_WRITER_H
#define LATINIME_PT_NODE_WRITER_H

#include <unordered_map>

#include "defines.h"
#include "pt_node_params.h"
#include "int_array_view.h"

namespace latinime {

class NgramProperty;
class UnigramProperty;

// Interface class used to write PtNode information.
class PtNodeWriter {
 public:
    typedef std::unordered_map<int, int> PtNodeArrayPositionRelocationMap;
    typedef std::unordered_map<int, int> PtNodePositionRelocationMap;
    struct DictPositionRelocationMap {
     public:
        DictPositionRelocationMap()
                : mPtNodeArrayPositionRelocationMap(), mPtNodePositionRelocationMap() {}

        PtNodeArrayPositionRelocationMap mPtNodeArrayPositionRelocationMap;
        PtNodePositionRelocationMap mPtNodePositionRelocationMap;

     private:
        DISALLOW_COPY_AND_ASSIGN(DictPositionRelocationMap);
    };

    virtual ~PtNodeWriter() {}

    virtual bool markPtNodeAsDeleted(const PtNodeParams *toBeUpdatedPtNodeParams) = 0;

    virtual bool markPtNodeAsMoved(const PtNodeParams *toBeUpdatedPtNodeParams,
                                   int movedPos, int bigramLinkedNodePos) = 0;

    virtual bool markPtNodeAsWillBecomeNonTerminal(
            const PtNodeParams *toBeUpdatedPtNodeParams) = 0;

    virtual bool updatePtNodeUnigramProperty(const PtNodeParams *toBeUpdatedPtNodeParams,
            const UnigramProperty *unigramProperty) = 0;

    virtual bool updatePtNodeProbabilityAndGetNeedsToKeepPtNodeAfterGC(
            const PtNodeParams *toBeUpdatedPtNodeParams,
            bool *outNeedsToKeepPtNode) = 0;

    virtual bool updateChildrenPosition(const PtNodeParams *toBeUpdatedPtNodeParams,
                                        int newChildrenPosition) = 0;

    virtual bool writePtNodeAndAdvancePosition(const PtNodeParams *ptNodeParams,
            int *ptNodeWritingPos) = 0;

    virtual bool writeNewTerminalPtNodeAndAdvancePosition(const PtNodeParams *ptNodeParams,
            const UnigramProperty *unigramProperty, int *ptNodeWritingPos) = 0;

    virtual bool addNgramEntry(WordIdArrayView prevWordIds, int wordId,
            const NgramProperty *ngramProperty, bool *outAddedNewEntry) = 0;

    virtual bool removeNgramEntry(WordIdArrayView prevWordIds, int wordId) = 0;

    virtual bool updateAllBigramEntriesAndDeleteUselessEntries(
            const PtNodeParams *sourcePtNodeParams, int *outBigramEntryCount) = 0;

    virtual bool updateAllPositionFields(const PtNodeParams *toBeUpdatedPtNodeParams,
            const DictPositionRelocationMap *dictPositionRelocationMap,
            int *outBigramEntryCount) = 0;

    virtual bool addShortcutTarget(const PtNodeParams *ptNodeParams,
            const int *targetCodePoints, int targetCodePointCount,
                                   int shortcutProbability) = 0;

 protected:
    PtNodeWriter() {};

 private:
    DISALLOW_COPY_AND_ASSIGN(PtNodeWriter);
};
} // namespace latinime
#endif /* LATINIME_PT_NODE_WRITER_H */
