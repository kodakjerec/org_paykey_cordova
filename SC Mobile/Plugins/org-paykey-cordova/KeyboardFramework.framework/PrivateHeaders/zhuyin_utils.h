//
// Created by YakirPayKey on 10/15/18.
//

#ifndef NLP_ENGINE_ZHUYINUTILS_H
#define NLP_ENGINE_ZHUYINUTILS_H


class ZhuyinUtils {
public:
    /*
     * strict wil include strokes that are not considered "initials" but have cases where they are in the beginning of 
     * a bopomofo
     * */
    static bool isInitial(const int codepoint, const bool strict) {
        if (strict) {
            switch (codepoint){
                case 0x3114:
                case 0x3127:
                case 0x311b:
                case 0x3129:
                case 0x3128:
                    return true;
                default:
                    break;
            }
        }
       
        return (codepoint <= 0x3119 && codepoint >= 0x3105);
    }


    static bool isTone(const int codepoint) {
        switch (codepoint) {
            case 0x02ca:
            case 0x02cb:
            case 0x02c7:
            case 0x02d9:
            case 0x02c9:
                return true;
            default:
                return false;
        }
    }
};


#endif //NLP_ENGINE_ZHUYINUTILS_H
