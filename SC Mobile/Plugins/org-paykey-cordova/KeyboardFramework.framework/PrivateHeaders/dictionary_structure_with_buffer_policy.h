/*
 * Copyright (C) 2013, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_DICTIONARY_STRUCTURE_POLICY_H
#define LATINIME_DICTIONARY_STRUCTURE_POLICY_H

#include <memory>

#include "defines.h"
#include "historical_info.h"
#include "word_attributes.h"
#include "word_property.h"
#include "binary_dictionary_shortcut_iterator.h"
#include "int_array_view.h"

namespace latinime {

class DicNode;
class DicNodeVector;
class DictionaryHeaderStructurePolicy;
class MultiBigramMap;
class NgramListener;
class NgramContext;
class UnigramProperty;

/*
 * This class abstracts the structure of dictionaries.
 * Implement this policy to support additional dictionaries.
 */
    class DictionaryStructureWithBufferPolicy {
    public:
        typedef std::unique_ptr<DictionaryStructureWithBufferPolicy> StructurePolicyPtr;

        virtual ~DictionaryStructureWithBufferPolicy() {}

        virtual int getRootPosition() const = 0;

        virtual void createAndGetAllChildDicNodes(const DicNode *dicNode,
                                                  DicNodeVector *childDicNodes) const = 0;

        virtual int getCodePointsAndReturnCodePointCount(int wordId, int maxCodePointCount,
                                                         int *outCodePoints) const = 0;

        virtual int getWordId(CodePointArrayView wordCodePoints,
                              bool forceLowerCaseSearch) const = 0;

        virtual const WordAttributes getWordAttributesInContext(WordIdArrayView prevWordIds,
                                                                int wordId,
                                                                MultiBigramMap *multiBigramMap) const = 0;

        // TODO: Remove
        virtual int getProbability(int unigramProbability, int bigramProbability) const = 0;

        virtual int getProbabilityOfWord(const WordIdArrayView prevWordIds, const int wordId) const {
            return getProbabilityOfWord(prevWordIds, wordId, false);
        }
        
        virtual int getProbabilityOfWord(WordIdArrayView prevWordIds, int wordId, bool absolute) const = 0;

        virtual void iterateNgramEntries(WordIdArrayView prevWordIds,
                                         NgramListener *listener) const = 0;

        virtual BinaryDictionaryShortcutIterator getShortcutIterator(int wordId) const = 0;

        virtual const DictionaryHeaderStructurePolicy *getHeaderStructurePolicy() const = 0;

        // Returns whether the update was success or not.
        virtual bool addUnigramEntry(CodePointArrayView wordCodePoints,
                                     const UnigramProperty *unigramProperty) = 0;

        // Returns whether the update was success or not.
        virtual bool removeUnigramEntry(CodePointArrayView wordCodePoints) = 0;

        // Returns whether the update was success or not.
        virtual bool addNgramEntry(const NgramProperty *ngramProperty) = 0;

        // Returns whether the update was success or not.
        virtual bool removeNgramEntry(const NgramContext *ngramContext,
                                      CodePointArrayView wordCodePoints) = 0;

        // Returns whether the update was success or not.
        virtual bool updateEntriesForWordWithNgramContext(const NgramContext *ngramContext,
                                                          CodePointArrayView wordCodePoints,
                                                          bool isValidWord,
                                                          bool isNotAWord,
                                                          HistoricalInfo historicalInfo) = 0;
        
        // Returns whether the shortcuts were added to the word ( if shortcuts are empty returns true)
        virtual  bool updateShortcutsEntriesForWord(CodePointArrayView wordCodePoints,
                std::vector<UnigramProperty::ShortcutProperty> shortcuts) = 0;
        
        // Returns whether this policy supports shortcuts individual frequency
        virtual bool supportsShortcutsIndividualFrequency() const = 0;

        // Returns whether the flush was success or not.
        virtual bool flush(const char *filePath) = 0;

        // Returns whether the GC and flush were success or not.
        virtual bool flushWithGC(const char *filePath) = 0;

        virtual bool needsToRunGC(bool mindsBlockByGC) const = 0;

        // Currently, this method is used only for testing. You may want to consider creating new
        // dedicated method instead of this if you want to use this in the production.
        virtual void getProperty(const char *query, int queryLength, char *outResult,
                                 int maxResultLength) = 0;

        virtual bool isOffensive(CodePointArrayView wordCodePoints) const = 0;

        virtual const WordProperty getWordProperty(CodePointArrayView wordCodePoints) const = 0;
        
        // Method to iterate all words in the dictionary.
        // The returned token has to be used to get the next word. If token is 0, this method newly
        // starts iterating the dictionary.
        virtual int getNextWordAndNextToken(int token, int *outCodePoints,
                                            int *outCodePointCount) = 0;

        virtual bool isCorrupted() const = 0;

    protected:
        DictionaryStructureWithBufferPolicy() {}

    private:
        DISALLOW_COPY_AND_ASSIGN(DictionaryStructureWithBufferPolicy);
    };
} // namespace latinime
#endif /* LATINIME_DICTIONARY_STRUCTURE_POLICY_H */
