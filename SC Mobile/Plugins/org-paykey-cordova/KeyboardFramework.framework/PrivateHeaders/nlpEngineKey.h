//
//  NLPEngineKey.h
//
//  Created by paul kalnitz on 14/03/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#ifndef NLP_ENGINE_KEY_H
#define NLP_ENGINE_KEY_H
    
struct nlpEngineKey {
    int code;
    int x;
    int y;
    int width;
    int height;
};

typedef struct nlpEngineKey nlpEngineKey;

typedef std::vector<nlpEngineKey> nlpEngineKeys;

#endif /* NLP_ENGINE_KEY_H */
