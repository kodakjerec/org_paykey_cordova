/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_GESTURE_TRAVERSAL_H
#define LATINIME_GESTURE_TRAVERSAL_H

#include <cstdint>

#include "defines.h"
#include "dic_node.h"
#include "dic_node_vector.h"
#include "proximity_info_state.h"
#include "proximity_info.h"
#include "proximity_info_utils.h"
#include "traversal.h"
#include "dic_traverse_session.h"
#include "suggest_options.h"
#include "gesture_scoring_params.h"
#include "char_utils.h"

namespace latinime {
    class GestureTraversal : public Traversal {
    public:
        static const GestureTraversal *getInstance() { return &sInstance; }

        AK_FORCE_INLINE bool doesSupportIndexDividedTerminals() const override {
            return false;
        }

        AK_FORCE_INLINE int getMaxPointerCount() const override {
            return MAX_POINTER_COUNT;
        }

        AK_FORCE_INLINE bool allowsErrorCorrections(const DicNode *const dicNode) const override {
            return dicNode->getNormalizedSpatialDistance()
                   < GestureScoringParams::NORMALIZED_SPATIAL_DISTANCE_THRESHOLD_FOR_EDIT;
        }

        AK_FORCE_INLINE bool isSkip(const DicTraverseSession *const traverseSession,
                                            const DicNode *const dicNode, const bool allowsErrorCorrections) const override {
            return false;
        }

        AK_FORCE_INLINE bool isOmission(const DicTraverseSession *const traverseSession,
                                        const DicNode *const dicNode,
                                        const DicNode *const childDicNode,
                                        const bool allowsErrorCorrections) const override {

            if (!CORRECT_OMISSION) {
                return false;
            }

            // Note: Always consider intentional omissions (like apostrophes) since they are common.
            const bool canConsiderOmission =
                    allowsErrorCorrections || childDicNode->canBeIntentionalOmission();
            if (!canConsiderOmission) {
                return false;
            }

            const int point0Index = dicNode->getInputIndex(0);
            int keyIndex = traverseSession->getProximityInfo()->getKeyIndexOf(childDicNode->getNodeCodePoint());
            float bestProb = traverseSession->getProximityInfoState(0)->getBestProbBetween(point0Index,
                                                                                           keyIndex, true);

            if (dicNode->getNodeCodePoint() == childDicNode->getNodeCodePoint())
                return true;

            if (childDicNode->canBeIntentionalOmission()) {
                return true;
            }

            float currentProb = traverseSession->getProximityInfoState(0)->getProbability(point0Index, keyIndex, true);

            return keyIndex != NOT_A_CODE_POINT
                       && bestProb < GestureScoringParams::OMISSION_BEST_PROB_LIMIT
                       && currentProb > GestureScoringParams::OMISSION_CURRENT_PROB_LIMIT;
        }

        AK_FORCE_INLINE
        bool isInsertion(const DicTraverseSession *const traverseSession,
                         const DicNode *const dicNode,
                         const DicNode *const childDicNode) const override {
            const int pointIndex = dicNode->getInputIndex(0);
            float skip = traverseSession->getProximityInfoState(0)->getProbability(pointIndex, NOT_AN_INDEX, true);
            float nextSkip = traverseSession->getProximityInfoState(0)->getProbability(pointIndex + 1, NOT_AN_INDEX,
                                                                                       true);

            if (pointIndex == 0)
                return false;

            if (traverseSession->getProximityInfoState(0)->getProbability(pointIndex, NOT_AN_INDEX, true)
                > GestureScoringParams::TRAVERSAL_INSERTION_MAX_SKIP_LOG)
                return false;

            int keyIndex = traverseSession->getProximityInfo()->getKeyIndexOf(childDicNode->getNodeCodePoint());
            float nextProb = traverseSession->getProximityInfoState(0)->getProbability(pointIndex + 1,
                                                                                       keyIndex, true);
            int currentBestChar = traverseSession->getProximityInfoState(0)->getMostProbableCharIndex(pointIndex);
            float currentBestCharProb = traverseSession->getProximityInfoState(0)->getProbability(pointIndex,
                                                                                                  currentBestChar,
                                                                                                  true);
            float nodeCurrentCharProb = traverseSession->getProximityInfoState(0)->getProbability(pointIndex, keyIndex,
                                                                                                  true);

            int delta = traverseSession->getProximityInfoState(0)->getRelativeIndex(pointIndex + 1) -
                        traverseSession->getProximityInfoState(0)->getRelativeIndex(pointIndex);


            return (nextSkip < skip || delta < GestureScoringParams::TRAVERSAL_INSERTION_END_DELTA)
                   && (nextProb < nodeCurrentCharProb / GestureScoringParams::TRAVERSAL_INSERTION_PROB_RATIO_THRESH)
                   && (pointIndex - 1 <= childDicNode->getNodeCodePointCount());
        }

        AK_FORCE_INLINE bool
        isSpaceSubstitutionTerminal(const DicTraverseSession *const traverseSession,
                                    const DicNode *const dicNode) const override {
            if (!CORRECT_NEW_WORD_SPACE_SUBSTITUTION) {
                return false;
            }
            if (traverseSession->getSuggestOptions()->weightForLocale()
                < GestureScoringParams::LOCALE_WEIGHT_THRESHOLD_FOR_SPACE_SUBSTITUTION) {
                // Space substitution is heavy, so we skip doing it if the weight for this language
                // is low because we anticipate the suggestions out of this dictionary are not for
                // the language the user intends to type in.
                return false;
            }
            if (!canDoLookAheadCorrection(traverseSession, dicNode)) {
                return false;
            }
            const int point0Index = dicNode->getInputIndex(0);
            return dicNode->isTerminalDicNode()
                   && traverseSession->getProximityInfoState(0)->
                    hasSpaceProximity(point0Index);
        }

        AK_FORCE_INLINE bool isSpaceOmissionTerminal(
                const DicTraverseSession *const traverseSession,
                const DicNode *const dicNode) const override {
            if (!CORRECT_NEW_WORD_SPACE_OMISSION) {
                return false;
            }
            if (traverseSession->getSuggestOptions()->weightForLocale()
                < GestureScoringParams::LOCALE_WEIGHT_THRESHOLD_FOR_SPACE_OMISSION) {
                // Space omission is heavy, so we skip doing it if the weight for this language
                // is low because we anticipate the suggestions out of this dictionary are not for
                // the language the user intends to type in.
                return false;
            }
            const int inputSize = traverseSession->getInputSize();
            // TODO: Don't refer to isCompletion?
            if (dicNode->isCompletion(inputSize)) {
                return false;
            }
            if (!dicNode->isTerminalDicNode()) {
                return false;
            }
            const int16_t pointIndex = dicNode->getInputIndex(0);
            return pointIndex <= inputSize && !dicNode->isTotalInputSizeExceedingLimit()
                   && !dicNode->shouldBeFilteredBySafetyNetForBigram();
        }

        AK_FORCE_INLINE bool shouldDepthLevelCache(
                const DicTraverseSession *const traverseSession) const override {
            const int inputSize = traverseSession->getInputSize();
            return traverseSession->isCacheBorderForTyping(inputSize, CACHE_BACK_LENGTH);
        }

        AK_FORCE_INLINE bool shouldNodeLevelCache(
                const DicTraverseSession *const traverseSession,
                const DicNode *const dicNode) const override {
            return false;
        }

        AK_FORCE_INLINE bool canDoLookAheadCorrection(
                const DicTraverseSession *const traverseSession,
                const DicNode *const dicNode) const override {
            const int inputSize = traverseSession->getInputSize();
            return dicNode->canDoLookAheadCorrection(inputSize);
        }

        AK_FORCE_INLINE ProximityType getProximityType(
                const DicTraverseSession *const traverseSession, const DicNode *const dicNode,
                const DicNode *const childDicNode) const override {

            const ProximityInfoState *proximityInfoState = traverseSession->
                    getProximityInfoState(0);
            int index = dicNode->getInputIndex(0);
            ProximityType proximityType = traverseSession->getProximityTypeG(
                    index, childDicNode->getNodeCodePoint());
            return proximityType;
        }

        AK_FORCE_INLINE bool needsToTraverseAllUserInput() const override {
            return true;
        }

        AK_FORCE_INLINE float getMaxSpatialDistance() const override {
            return GestureScoringParams::MAX_SPATIAL_DISTANCE;
        }

        AK_FORCE_INLINE int getDefaultExpandDicNodeSize() const override {
            return DicNodeVector::DEFAULT_NODES_SIZE_FOR_OPTIMIZATION;
        }

        AK_FORCE_INLINE int
        getMaxCacheSize(const int inputSize, const float weightForLocale) const override {
            if (inputSize <= 1) {
                return GestureScoringParams::MAX_CACHE_DIC_NODE_SIZE_FOR_SINGLE_POINT;
            }
            if (weightForLocale <
                GestureScoringParams::LOCALE_WEIGHT_THRESHOLD_FOR_SMALL_CACHE_SIZE) {
                return GestureScoringParams::MAX_CACHE_DIC_NODE_SIZE_FOR_LOW_PROBABILITY_LOCALE;
            }
            return GestureScoringParams::MAX_CACHE_DIC_NODE_SIZE;
        }
        
        AK_FORCE_INLINE int
        getCompletionInputSize(const DicTraverseSession *const traverseSession) const override {
            return traverseSession->getInputSize();
        }

        AK_FORCE_INLINE bool isPossibleOmissionChildNode(
                const DicTraverseSession *const traverseSession, const DicNode *const parentDicNode,
                const DicNode *const dicNode) const override {
            bool ans = getProximityType(traverseSession, parentDicNode, dicNode) == MATCH_CHAR;
            return ans;
        }

        AK_FORCE_INLINE bool isGoodToTraverseNextWord(const DicNode *const dicNode,
                                                      const int probability) const override {
            return false;
        }

        AK_FORCE_INLINE float getMaxValueForWeighting() const override {
            return static_cast<float>(MAX_VALUE_FOR_WEIGHTING);
        }

        AK_FORCE_INLINE bool allowLookAhead(const DicTraverseSession *const traverseSession, DicNode *dicNode) const override{
            const int inputSize = traverseSession->getInputSize();
            return !(dicNode->hasMultipleWords()
                     && dicNode->isCompletion(traverseSession->getInputSize()));
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(GestureTraversal);

        static const bool CORRECT_OMISSION;
        static const bool CORRECT_NEW_WORD_SPACE_SUBSTITUTION;
        static const bool CORRECT_NEW_WORD_SPACE_OMISSION;
        static const int CACHE_BACK_LENGTH = 3;
        static const GestureTraversal sInstance;

        GestureTraversal() {}

        ~GestureTraversal() {}
    };
} // namespace latinime
#endif // LATINIME_TYPING_TRAVERSAL_H
