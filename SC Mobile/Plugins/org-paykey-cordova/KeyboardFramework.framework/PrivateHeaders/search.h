//
// Created by YakirPayKey on 2019-04-14.
//

#ifndef NLP_ENGINE_SEARCH_H
#define NLP_ENGINE_SEARCH_H


#include "defines.h"
#include "suggest_interface.h"
#include "suggest_policy.h"
#include "suggest.h"
#include <suggest/policyimpl/search/search_weighting.h>


namespace latinime {

    class Search : public SuggestInterface {
    public:

        AK_FORCE_INLINE Search() = default;

        AK_FORCE_INLINE ~Search() override = default;

        void getSuggestions(ProximityInfo *pInfo, DicTraverseSession *traverseSession, const int *inputXs,
                            const int *inputYs,
                            const int *times, const int *pointerIds, const int *inputCodePoints, int inputSize,
                            float weightOfLangModelVsSpatialModel, int maxResults,
                            SuggestionResults *outSuggestionResults) const override;


        /**
        * Weight child dicNode by aligning it to the key
        */
        void weightChildNode(DicTraverseSession *traverseSession, const int inputSize, DicNode *dicNode) const {
            if (dicNode->isCompletion(inputSize)) {
                Weighting::addCostAndForwardInputIndex(SearchWeighting::getInstance(), CT_COMPLETION,
                                                       traverseSession,
                                                       0 /* parentDicNode */, dicNode, 0 /* multiBigramMap */);
            } else {
                Weighting::addCostAndForwardInputIndex(SearchWeighting::getInstance(), CT_MATCH,
                                                       traverseSession,
                                                       0 /* parentDicNode */, dicNode, 0 /* multiBigramMap */);
            }
        }

        /**
        * Weight child dicNode by aligning it to the key
        */
        void weightTerminalChildNode(DicTraverseSession *traverseSession, const int inputSize, DicNode *terminalDicNode) const {
            if (terminalDicNode->isTerminalDicNode() && terminalDicNode->getInputIndex(0) >= inputSize) {
                Weighting::addCostAndForwardInputIndex(SearchWeighting::getInstance(), CT_TERMINAL, traverseSession, 0,
                                                       terminalDicNode, traverseSession->getMultiBigramMap());
                traverseSession->getDicTraverseCache()->copyPushTerminal(0, terminalDicNode);

            }
        }

    };

}
#endif //NLP_ENGINE_SEARCH_H
