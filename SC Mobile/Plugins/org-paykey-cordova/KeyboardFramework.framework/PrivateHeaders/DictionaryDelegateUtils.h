//
// Created by YakirPayKey on 10/10/18.
//

#ifndef NLP_ENGINE_DICTIONARYDELEGATEUTILS_H
#define NLP_ENGINE_DICTIONARYDELEGATEUTILS_H

#include <suggested_word.h>

namespace latinime {

    static int transformCodePoints(const int *codePoints, const int len, int outCodePoints[MAX_WORD_LENGTH]) {
        int codePointCount = 0;
        for (int i = 0; i < len; i++) {
            const int codePoint = codePoints[i];
            int c = codePoint;
            if (!CharUtils::isInUnicodeSpace(codePoint)) {
                if (codePoint == CODE_POINT_BEGINNING_OF_SENTENCE)
                    continue;
                c = 0xFFFD;
            } else if (codePoint >= 0x01 && codePoint <= 0x1F)
                c = 0xFFFD;
            outCodePoints[codePointCount] = c;
            codePointCount++;
        }

        if (codePointCount < MAX_WORD_LENGTH)
            outCodePoints[codePointCount] = 0;

        return codePointCount;
    }

    static void transformEngineSuggestions(SuggestionResults *res, SuggestedWords *ret) {

        //don't go through the arrays in outputSuggestions
        while (!res->empty()) {
            const SuggestedWord &suggestedWord = res->pop();

            //adjust codePoints (code taken from SuggestionResults.outputCodePoints())
            int codePoints[MAX_WORD_LENGTH] = {0};
            int parentCodePoints[MAX_WORD_LENGTH] = {0};
            int codePointCount = 0;
            int parentCodePointCount = 0;
            
            if (suggestedWord.getCodePointCount() > 0)
                codePointCount = transformCodePoints(suggestedWord.getCodePoints(),
                                                     suggestedWord.getCodePointCount(),
                                                     codePoints);
            if (suggestedWord.getParentCodePointCount() > 0)
                parentCodePointCount = transformCodePoints(suggestedWord.getParentCodePoints(),
                                                           suggestedWord.getParentCodePointCount(), parentCodePoints);

            SuggestedWord suggestion(codePoints, codePointCount,
                                     parentCodePoints, parentCodePointCount,
                                     suggestedWord.getScore(),
                                     suggestedWord.getType(), suggestedWord.getIndexToPartialCommit(),
                                     suggestedWord.getAutoCommitFirstWordConfidence(),
                                     suggestedWord.getLanguageDistance(),
                                     suggestedWord.isValidMultipleWordSuggestion(),
                                     suggestedWord.getLastInputConsumed());

            ret->push_back(suggestion);
        }
    }
}


#endif //NLP_ENGINE_DICTIONARYDELEGATEUTILS_H
