/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_ZH_TYPING_TRAVERSAL_H
#define LATINIME_ZH_TYPING_TRAVERSAL_H

#include <cstdint>
#include <typing_traversal.h>

#include "defines.h"
#include "dic_node.h"
#include "dic_node_vector.h"
#include "proximity_info_state.h"
#include "proximity_info_utils.h"
#include "traversal.h"
#include "dic_traverse_session.h"
#include "suggest_options.h"
#include "scoring_params.h"
#include "char_utils.h"
#include "zhuyin_utils.h"

namespace latinime {
    class TypingZhuyinTraversal : public TypingTraversal {
    public:
        static const TypingZhuyinTraversal *getInstance() { return &sInstance; }

        AK_FORCE_INLINE bool doesSupportIndexDividedTerminals() const override {
            return true;
        }

        AK_FORCE_INLINE bool needsToTraverseAllUserInput() const override {
            return true;
        }

        bool isSkip(const DicTraverseSession *const traverseSession, const DicNode *const dicNode,
                    const bool allowsErrorCorrections) const override {

            const int inputSize = traverseSession->getInputSize();
            const int dicNodeInputIndex = dicNode->getInputIndex(0);
            const int prevCodePoint= dicNode->getPrevCodePointG(0);

            if (dicNodeInputIndex == 0 || ZhuyinUtils::isTone(prevCodePoint))
                return false;

            const int currentCodePoint = traverseSession->getProximityInfoState(0)->getPrimaryCodePointAt(
                    dicNodeInputIndex);
            const int dicNodeCodePoint = dicNode->getNodeCodePoint();

            return ZhuyinUtils::isInitial(currentCodePoint, true) && !ZhuyinUtils::isInitial(dicNodeCodePoint, false);
        }


        ProximityType getProximityType(const DicTraverseSession *const traverseSession, const DicNode *const dicNode,
                                       const DicNode *const childDicNode) const override {
            int index = dicNode->getInputIndex(0);
            int currentCodePoint = traverseSession->getProximityInfoState(0)->getPrimaryCodePointAt(index);
            int dicNodeCodePoint = childDicNode->getNodeCodePoint();

            if (currentCodePoint == dicNodeCodePoint)
                return MATCH_CHAR;
            return UNRELATED_CHAR;
        }

        AK_FORCE_INLINE float getMaxValueForWeighting() const override {
            return MAX_VALUE_FOR_COMPOUND_WEIGHTING;
        }

        bool isGoodToTraverseNextWord(const DicNode *const dicNode, const int probability) const override{
            return false;
        }

        AK_FORCE_INLINE bool allowsErrorCorrections(const DicNode *const dicNode) const override {
            return false;
        }

        AK_FORCE_INLINE bool isSpaceSubstitutionTerminal(const DicTraverseSession *const traverseSession,
                                                         const DicNode *const dicNode) const override {
            if (!CORRECT_NEW_WORD_SPACE_SUBSTITUTION) {
                return false;
            }

            if (dicNode->getNodeCodePointCount() < 2)
                return false;

            return TypingTraversal::isSpaceSubstitutionTerminal(traverseSession, dicNode);
        }

        AK_FORCE_INLINE bool isSpaceOmissionTerminal(
                const DicTraverseSession *const traverseSession, const DicNode *const dicNode) const override {
            if (!CORRECT_NEW_WORD_SPACE_OMISSION) {
                return false;
            }

            if (dicNode->getNodeCodePointCount() < 2)
                return false;

            return TypingTraversal::isSpaceOmissionTerminal(traverseSession, dicNode);
        }

        AK_FORCE_INLINE bool isOmission(const DicTraverseSession *const traverseSession,
                                        const DicNode *const dicNode, const DicNode *const childDicNode,
                                        const bool allowsErrorCorrections) const override {
            if (!CORRECT_OMISSION) {
                return false;
            }

            if (childDicNode->canBeIntentionalOmission() && dicNode->getInputIndex(0) == 0)
                return false;

            return TypingTraversal::isOmission(traverseSession, dicNode, childDicNode, allowsErrorCorrections);
        }

        AK_FORCE_INLINE bool shouldDepthLevelCache(
                const DicTraverseSession *const traverseSession) const override {
            const int inputSize = traverseSession->getInputSize();
            return traverseSession->isCacheBorderForTyping(inputSize, CACHE_BACK_LENGTH);
        }


        AK_FORCE_INLINE bool isPossibleOmissionChildNode(
                const DicTraverseSession *const traverseSession, const DicNode *const parentDicNode,
                const DicNode *const dicNode) const override {
            const ProximityType proximityType =
                    getProximityType(traverseSession, parentDicNode, dicNode);
            return proximityType == MATCH_CHAR;
        }

        AK_FORCE_INLINE bool
        allowLookAhead(const DicTraverseSession *const traverseSession, DicNode *dicNode) const override {
            if(dicNode->hasMultipleWords() &&  !ZhuyinUtils::isInitial(dicNode->getNodeCodePoint(), false) &&
               ErrorTypeUtils::isExactMatchWithIntentionalOmissionAndNewWord(dicNode->getContainedErrorTypes())){
                return true;
            }

            return TypingTraversal::allowLookAhead(traverseSession, dicNode);
        }


    private:
        DISALLOW_COPY_AND_ASSIGN(TypingZhuyinTraversal);

        static const bool CORRECT_OMISSION;
        static const bool CORRECT_NEW_WORD_SPACE_SUBSTITUTION;
        static const bool CORRECT_NEW_WORD_SPACE_OMISSION;
        static const float MAX_VALUE_FOR_COMPOUND_WEIGHTING;
        static const int CACHE_BACK_LENGTH;
        static const TypingZhuyinTraversal sInstance;

        TypingZhuyinTraversal() {}

        ~TypingZhuyinTraversal() {}
    };
} // namespace latinime
#endif // LATINIME_ZH_TYPING_TRAVERSAL_H
