/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_SUGGESTED_WORD_H
#define LATINIME_SUGGESTED_WORD_H

#include <vector>
#include "defines.h"
#include "dictionary.h"

namespace latinime {

    class SuggestedWord {
    public:
        class Comparator {
        public:
            bool operator()(const SuggestedWord &left, const SuggestedWord &right) {
                if (left.getScore() != right.getScore()) {
                    return left.getScore() > right.getScore();
                }
                return left.getCodePointCount() < right.getCodePointCount();
            }

        private:
            DISALLOW_ASSIGNMENT_OPERATOR(Comparator);
        };
        
        
        SuggestedWord() : mCodePoints(0, 0),
                          mParentCodePoints(0, 0), mScore(NOT_AN_INDEX),
                          mType(-1), mIndexToPartialCommit(NOT_AN_INDEX),
                          mAutoCommitFirstWordConfidence(-1), mLanguageDistance(-1),
                          mIsValidMultipleWordSuggestion(false),
                          mLastInputConsumed(NOT_AN_INDEX) {}
        
        SuggestedWord(const int *const codePoints, const int codePointCount, const int *const parentCodePoints,
                      const int parentCodePointCount,
                      const int score, const int type, const int indexToPartialCommit,
                      const int autoCommitFirstWordConfidence, const float languageDistance,
                      const bool isValidMultipleWordSuggestion, const int lastInputConsumed)
                : mScore(score),
                  mType(type), mIndexToPartialCommit(indexToPartialCommit),
                  mAutoCommitFirstWordConfidence(autoCommitFirstWordConfidence), mLanguageDistance(languageDistance),
                  mIsValidMultipleWordSuggestion(isValidMultipleWordSuggestion),
                  mLastInputConsumed(lastInputConsumed) {
            setCodePoints(codePoints, codePointCount);
            setParentCodePoints(parentCodePoints, parentCodePointCount);
        }
        
        SuggestedWord(const SuggestedWord &other){
            clone(&other);
        }
        
        void clone(const SuggestedWord * other){
            setCodePoints(other->getCodePoints(), other->getCodePointCount());
            setParentCodePoints(other->getParentCodePoints(), other->getParentCodePointCount());
            mScore = other->mScore;
            mType = other->mType;
            mAutoCommitFirstWordConfidence = other->mAutoCommitFirstWordConfidence;
            mIndexToPartialCommit = other->mIndexToPartialCommit;
            mLastInputConsumed = other->mLastInputConsumed;
            mLanguageDistance = other->mLanguageDistance;
            mIsValidMultipleWordSuggestion = other->mIsValidMultipleWordSuggestion;
        }
        
        const int *getCodePoints() const {
            if(mCodePoints.empty())
                return nullptr;
            return &mCodePoints.at(0);
        }

        int getCodePointCount() const {
            return static_cast<int>(mCodePoints.size());
        }

        const int *getParentCodePoints() const {
            if(mParentCodePoints.empty())
                return nullptr;
            return &mParentCodePoints.at(0);
        }

        int getParentCodePointCount() const {
            return static_cast<int>(mParentCodePoints.size());
        }

        int getScore() const {
            return mScore;
        }

        float getLanguageDistance() const {
            return mLanguageDistance;
        }

        bool isValidMultipleWordSuggestion() const {
            return mIsValidMultipleWordSuggestion;
        }

        int getType() const {
            return mType;
        }

        void setType(const int type) {
            mType = type;
        }

        int getLastInputConsumed() const {
            return mLastInputConsumed;
        }

        int getIndexToPartialCommit() const {
            return mIndexToPartialCommit;
        }

        int getAutoCommitFirstWordConfidence() const {
            return mAutoCommitFirstWordConfidence;
        }

        void setCodePoint(int position, int codePoint) {
            if (static_cast<unsigned int> (position) < mCodePoints.size() && position >= 0)
                mCodePoints[position] = codePoint;
            intArrayToCharArray(mCodePoints.data(), mCodePoints.size(), mWord, MAX_WORD_LENGTH);
        }

        void setCodePoints(const int *codePoints, const int codePointslength) {
            mCodePoints.clear();
            for (int i = 0; i < codePointslength; i++)
                mCodePoints.push_back(codePoints[i]);
            intArrayToCharArray(mCodePoints.data(), mCodePoints.size(), mWord, MAX_WORD_LENGTH);
        }

        char * getWord(){
            return mWord;
        }

        void setParentCodePoints(const int *codePoints, int codePointslength) {
            mParentCodePoints.clear();
            for (int i = 0; i < codePointslength; i++)
                mParentCodePoints.push_back(codePoints[i]);
        }

        bool isSame(const int *codePoints, const int len) const {
            if (static_cast<unsigned int> (len) != mCodePoints.size())
                return false;
            for (int i = 0; i < len; ++i) {
                if (mCodePoints.at(i) != codePoints[i])
                    return false;
            }
            return true;
        }

        static bool sortSuggestedWord(const SuggestedWord &o1, const SuggestedWord &o2) {
            if (o1.mScore > o2.mScore) return false;
            if (o1.mScore < o2.mScore) return true;
            if (o1.mCodePoints.size() < o2.mCodePoints.size()) return false;
            if (o1.mCodePoints.size() > o2.mCodePoints.size()) return true;
            return strcmp(o1.mWord, o2.mWord) > 0;
        }

        static void sort(std::vector<SuggestedWord> &v) {
            std::sort(v.begin(), v.end(), sortSuggestedWord);
        }

        static void removeDuplicates(std::vector<SuggestedWord> &v) {

            if (v.size() > 1) {
                std::vector<SuggestedWord> tmp;
                tmp.push_back(*(v.end() - 1));

                for (auto it = v.end() - 2;
                     it != v.begin() - 1; it--) {
                    bool isIn = false;
                    SuggestedWord sug1 = *it;
                    for (auto itmp = tmp.begin(); itmp != tmp.end(); itmp++) {
                        SuggestedWord sug2 = *itmp;
                        if (sug1.isSame(sug2.getCodePoints(), sug2.getCodePointCount())) {
                            isIn = true;
                        }
                    }
                    if (!isIn)
                        tmp.push_back(*it);
                }
                v.clear();
                for (auto itmp = tmp.end() - 1; itmp != tmp.begin() - 1; itmp--) {
                    v.push_back(*itmp);
                }
            }
        }


        static void trimSuggestions(std::vector<SuggestedWord> &v, int newSize) {
            if (v.size() > static_cast<unsigned int> (newSize)) {
                v.erase(v.begin(), v.begin() + (v.size() - newSize));
            }
        }

    private:

        std::vector<int> mCodePoints;
        std::vector<int> mParentCodePoints;
        char mWord[MAX_WORD_LENGTH] = {0};
        int mScore;
        int mType;
        int mIndexToPartialCommit;
        int mAutoCommitFirstWordConfidence;
        float mLanguageDistance;
        bool mIsValidMultipleWordSuggestion;
        int mLastInputConsumed = NOT_AN_INDEX;
    };

    typedef std::vector<SuggestedWord> SuggestedWords;
} // namespace latinime
#endif /* LATINIME_SUGGESTED_WORD_H */
