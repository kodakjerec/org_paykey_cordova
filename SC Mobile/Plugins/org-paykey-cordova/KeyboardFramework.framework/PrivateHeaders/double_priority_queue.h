//
// Created by YakirPayKey on 2018-12-24.
//

#ifndef NLP_ENGINE_DOUBLE_PRIORITY_QUEUE_H
#define NLP_ENGINE_DOUBLE_PRIORITY_QUEUE_H


#include <vector>
#include "suggestion_results.h"


namespace latinime {

    class DoublePriorityQueue {
    public:


        AK_FORCE_INLINE void push(SuggestedWord &suggestedWord) {
            mQueue.push_back(suggestedWord);
        }

        AK_FORCE_INLINE SuggestedWord peekBest() {
            SuggestedWord suggestedWord;
            peekBest(suggestedWord);
            return suggestedWord;
        }

        AK_FORCE_INLINE SuggestedWord peekWorst() const {
            SuggestedWord suggestedWord;
            peekWorst(suggestedWord);
            return suggestedWord;
        }

        AK_FORCE_INLINE SuggestedWord popBest() {
            SuggestedWord suggestedWord;
            remove(peekBest(suggestedWord));
            return suggestedWord;
        }

        AK_FORCE_INLINE SuggestedWord popWorst() {
            SuggestedWord suggestedWord;
            remove(peekWorst(suggestedWord));
            return suggestedWord;
        }

        AK_FORCE_INLINE int size() const {
            return static_cast<int>(mQueue.size());
        }

        AK_FORCE_INLINE bool empty() const {
            return mQueue.empty();
        }

    private:

        AK_FORCE_INLINE const int peekWorst(SuggestedWord &suggestedWord) const {
            const SuggestedWord *worst = nullptr;
            int worstIndex = -1;
            for (int i = 0; i < (int) mQueue.size(); ++i) {
                const SuggestedWord *candidate = &mQueue.at(i);
                if (!worst) {
                    worst = candidate;
                    worstIndex = i;
                    continue;
                }

                if (candidate->getScore() < worst->getScore()) {
                    worst = candidate;
                    worstIndex = i;
                } else if (candidate->getScore() == worst->getScore() &&
                           candidate->getCodePointCount() > worst->getCodePointCount()) {
                    worst = candidate;
                    worstIndex = i;
                }
            }
            suggestedWord.clone(worst);
            return worstIndex;
        }

        AK_FORCE_INLINE const int peekBest(SuggestedWord &suggestedWord) const {
            const SuggestedWord *best = nullptr;
            int bestIndex = -1;
            for (int i = 0; i < (int) mQueue.size(); ++i) {
                const SuggestedWord *candidate = &mQueue.at(i);
                if (!best) {
                    best = candidate;
                    bestIndex = i;
                    continue;
                }

                if (candidate->getScore() > best->getScore()) {
                    best = candidate;
                    bestIndex = i;
                } else if (candidate->getScore() == best->getScore() &&
                           candidate->getCodePointCount() < best->getCodePointCount()) {
                    best = candidate;
                    bestIndex = i;
                }
            }
            suggestedWord.clone(best);
            return bestIndex;
        }

        AK_FORCE_INLINE void remove(const int i) {
            mQueue.erase(mQueue.begin() + i);
        }

        std::vector<SuggestedWord> mQueue;
    };


};


#endif //NLP_ENGINE_DOUBLE_PRIORITY_QUEUE_H
