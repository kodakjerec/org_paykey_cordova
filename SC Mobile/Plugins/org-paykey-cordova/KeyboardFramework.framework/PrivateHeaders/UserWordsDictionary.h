//
// Created by YakirPayKey on 3/22/18.
//

#ifndef NLP_ENGINE_USERWORDSDICTIONARY_H
#define NLP_ENGINE_USERWORDSDICTIONARY_H


#include "ExpandableDictionary.h"


namespace latinime {

    class UserWordsDictionary : public ExpandableDictionary {


    public:
        static const char *const DICTIONARY_NAME;
        UserWordsDictionary()
                : ExpandableDictionary(DICTIONARY_NAME, "") {};

        bool addWord(CodePointArrayView word, CodePointArrayView shortcut);
    };
}

#endif //NLP_ENGINE_USERWORDSDICTIONARY_H
