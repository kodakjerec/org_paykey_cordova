/*
 * Copyright (C) 2013, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_VER4_PATRICIA_TRIE_NODE_WRITER_H
#define LATINIME_VER4_PATRICIA_TRIE_NODE_WRITER_H

#include "defines.h"
#include "dynamic_pt_reading_helper.h"
#include "pt_node_params.h"
#include "pt_node_writer.h"
#include "probability_entry.h"

namespace latinime {

class BufferWithExtendableBuffer;
class HeaderPolicy;
class Ver4DictBuffers;
class Ver4PatriciaTrieNodeReader;
class Ver4PtNodeArrayReader;
class Ver4ShortcutListPolicy;

/*
 * This class is used for helping to writes nodes of ver4 patricia trie.
 */
class Ver4PatriciaTrieNodeWriter : public PtNodeWriter {
 public:
    Ver4PatriciaTrieNodeWriter(BufferWithExtendableBuffer *const trieBuffer,
            Ver4DictBuffers *const buffers, const PtNodeReader *const ptNodeReader,
            const PtNodeArrayReader *const ptNodeArrayReader,
            Ver4ShortcutListPolicy *const shortcutPolicy)
            : mTrieBuffer(trieBuffer), mBuffers(buffers),
              mReadingHelper(ptNodeReader, ptNodeArrayReader), mShortcutPolicy(shortcutPolicy) {}

    virtual ~Ver4PatriciaTrieNodeWriter() {}

    virtual bool markPtNodeAsDeleted(const PtNodeParams *toBeUpdatedPtNodeParams);

    virtual bool markPtNodeAsMoved(const PtNodeParams *toBeUpdatedPtNodeParams,
            const int movedPos, int bigramLinkedNodePos);

    virtual bool markPtNodeAsWillBecomeNonTerminal(
            const PtNodeParams *toBeUpdatedPtNodeParams);

    virtual bool updatePtNodeUnigramProperty(const PtNodeParams *toBeUpdatedPtNodeParams,
            const UnigramProperty *unigramProperty);

    virtual bool updatePtNodeProbabilityAndGetNeedsToKeepPtNodeAfterGC(
            const PtNodeParams *toBeUpdatedPtNodeParams, bool *const outNeedsToKeepPtNode);

    virtual bool updateChildrenPosition(const PtNodeParams *toBeUpdatedPtNodeParams,
                                        int newChildrenPosition);

    bool updateTerminalId(const PtNodeParams *toBeUpdatedPtNodeParams,
                          int newTerminalId);

    virtual bool writePtNodeAndAdvancePosition(const PtNodeParams *ptNodeParams,
            int *ptNodeWritingPos);

    virtual bool writeNewTerminalPtNodeAndAdvancePosition(const PtNodeParams *ptNodeParams,
            const UnigramProperty *unigramProperty, int *ptNodeWritingPos);

    virtual bool addNgramEntry(WordIdArrayView prevWordIds, int wordId,
            const NgramProperty *ngramProperty, bool *outAddedNewEntry);

    virtual bool removeNgramEntry(WordIdArrayView prevWordIds, int wordId);

    virtual bool updateAllBigramEntriesAndDeleteUselessEntries(
            const PtNodeParams *const sourcePtNodeParams, int *outBigramEntryCount);

    virtual bool updateAllPositionFields(const PtNodeParams *toBeUpdatedPtNodeParams,
            const DictPositionRelocationMap *dictPositionRelocationMap,
            int *outBigramEntryCount);

    virtual bool addShortcutTarget(const PtNodeParams *ptNodeParams, const int *targetCodePoints,
            int targetCodePointCount, int shortcutProbability);

 private:
    DISALLOW_COPY_AND_ASSIGN(Ver4PatriciaTrieNodeWriter);

    bool writePtNodeAndGetTerminalIdAndAdvancePosition(
            const PtNodeParams *ptNodeParams, int *outTerminalId,
            int *ptNodeWritingPos);

    bool updatePtNodeFlags(int ptNodePos, bool isTerminal, bool hasMultipleChars);

    static const int CHILDREN_POSITION_FIELD_SIZE;

    BufferWithExtendableBuffer *const mTrieBuffer;
    Ver4DictBuffers *const mBuffers;
    DynamicPtReadingHelper mReadingHelper;
    Ver4ShortcutListPolicy *const mShortcutPolicy;
};
} // namespace latinime
#endif /* LATINIME_VER4_PATRICIA_TRIE_NODE_WRITER_H */
