//
// Created by YakirPayKey on 6/3/18.
//

#ifndef NLP_ENGINE_EMOJIDICTIONARY_H
#define NLP_ENGINE_EMOJIDICTIONARY_H


#include "ReadOnlyDictionary.h"

namespace latinime {

    class EmojiDictionary : public ReadOnlyDictionary {
    public:
        EmojiDictionary(const char *name, const char *pathToDict, long dictionarySize, long offset,
                        bool shouldUseForPrediction);

        bool isValidWord(CodePointArrayView word) override;

        bool isOffensive(CodePointArrayView word) override;

        void predict(NgramContext *ngramContext, CodePointArrayView codePointArrayView,
                     int maxResults, SuggestedWords *predictions) override;

        void
        correct(ProximityInfo *proximityInfo, NgramContext *ngramContext, int *codePoints, int typedInputLen, int *x,
                int *y, int *times, int *pointer, SuggestOptions *options, SuggestedWords *corrections) override;

        void search(CodePointArrayView codePointArrayView, SuggestOptions *options,
                    SuggestedWords *searchSuggestions) override;

    private:

        static void
        transformToEmojiResults(latinime::SuggestedWords &suggestions, latinime::SuggestedWords *outSuggestions,
                                bool shouldBeExactMatch);

        const bool mShouldUseForPrediction;
    };
}
#endif //NLP_ENGINE_EMOJIDICTIONARY_H
