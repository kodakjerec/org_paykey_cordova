/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_GESTURE_SCORING_H
#define LATINIME_GESTURE_SCORING_H

#include "defines.h"
#include "error_type_utils.h"
#include "scoring.h"
#include "dic_traverse_session.h"
#include "gesture_scoring_params.h"


namespace latinime {

    class DicNode;

    class DicTraverseSession;

    class GestureScoring : public Scoring {
    public:
        static const GestureScoring *getInstance() { return &sInstance; }

        AK_FORCE_INLINE void getMostProbableString(const DicTraverseSession *const traverseSession,
                                                   const float weightOfLangModelVsSpatialModel,
                                                   SuggestionResults *const outSuggestionResults) const {}

        AK_FORCE_INLINE float getAdjustedWeightOfLangModelVsSpatialModel(
                DicTraverseSession *const traverseSession, DicNode *const terminals,
                const int size) const {
            return 1.0f;
        }

        AK_FORCE_INLINE int calculateFinalScore(const float compoundDistance, const int inputSize,
                                                const ErrorTypeUtils::ErrorType containedErrorTypes,
                                                const bool forceCommit,
                                                const bool boostExactMatches,
                                                const bool hasProbabilityZero, int spaces,
                                                int edits) const {

            const float maxDistance = GestureScoringParams::DISTANCE_WEIGHT_LANGUAGE //*(spaces+1)
                                      + static_cast<float>(inputSize) *
                                        GestureScoringParams::GESTURE_MAX_OUTPUT_SCORE_PER_INPUT;

            //----maxDistance = 1.1214 + inputSize*0.1


            if (DEBUG_SCORING) {
                AKLOGI("CALCULATE_FINAL_SCORE maxDistance=%.4f, compoundDistance=%.4f\n", maxDistance,
                       compoundDistance);
            }

            float score =
                    GestureScoringParams::GESTURE_BASE_OUTPUT_SCORE -
                    compoundDistance / maxDistance;

            //----score = 1.0 - compoundDistance / (1.1214 + inputSize*0.1)
            if (DEBUG_SCORING)
                AKLOGI("CALCULATE_FINAL_SCORE score=%.4f\n", score);

            if (forceCommit) {
                score += GestureScoringParams::AUTOCORRECT_OUTPUT_THRESHOLD;

                if (DEBUG_SCORING)
                    AKLOGI("CALCULATE_FINAL_SCORE forceCommit score=%.4f\n", score);
            }
            if (hasProbabilityZero) {
                // We do nothing here.
                // 0 probability words should by typed for now, so we give no boost.
            } else {
                if (boostExactMatches && ErrorTypeUtils::isPerfectMatch(containedErrorTypes)) {
                    score += GestureScoringParams::PERFECT_MATCH_PROMOTION;

                    if (DEBUG_SCORING)
                        AKLOGI("CALCULATE_FINAL_SCORE boostExactMatches(perfect) score=%.4f\n",
                               score);

                } else if (boostExactMatches && ErrorTypeUtils::isExactMatchWithIntentionalOmission(
                        containedErrorTypes)) {
                    score += GestureScoringParams::EXACT_MATCH_PROMOTION;
                    if (DEBUG_SCORING)
                        AKLOGI("CALCULATE_FINAL_SCORE boostExactMatches(Exact+Omission) score=%.4f\n",
                               score);
                }
            }
            if (DEBUG_SCORING) {
                AKLOGI("CALCULATE_FINAL_SCORE FINALSCORE= %.4f\n",
                       (score * SUGGEST_INTERFACE_OUTPUT_SCALE));
            }

            return static_cast<int>(score * SUGGEST_INTERFACE_OUTPUT_SCALE);

        }

        AK_FORCE_INLINE float getDoubleLetterDemotionDistanceCost(
                const DicNode *const terminalDicNode) const {
            return 0.0f;
        }

        AK_FORCE_INLINE bool autoCorrectsToMultiWordSuggestionIfTop() const {
            return false;
        }

        AK_FORCE_INLINE bool sameAsTyped(const DicTraverseSession *const traverseSession,
                                         const DicNode *const dicNode) const {
            return traverseSession->getProximityInfoState(0)->sameAsTypedWithProb(
                    dicNode->getOutputWordBuf(), dicNode->getNodeCodePointCount());
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(GestureScoring);

        static const GestureScoring sInstance;

        GestureScoring() {}

        ~GestureScoring() {}
    };
} // namespace latinime
#endif // LATINIME_TYPING_SCORING_H
