/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_SEARCH_WEIGHTING_H
#define LATINIME_SEARCH_WEIGHTING_H

#include "defines.h"
#include "dic_node_utils.h"
#include "error_type_utils.h"
#include "touch_position_correction_utils.h"
#include "weighting.h"
#include "dic_traverse_session.h"
#include "scoring_params.h"
#include "char_utils.h"
#include "search_scoring_params.h"

namespace latinime {

    class DicNode;

    struct DicNode_InputStateG;

    class MultiBigramMap;

    class SearchWeighting : public Weighting {
    public:
        static const SearchWeighting *getInstance() { return &sInstance; }
        
    protected:
        float getTerminalSpatialCost(const DicTraverseSession *const traverseSession,
                                     const DicNode *const parentDicNode,
                                     const DicNode *const dicNode) const override {
            return SearchScoringParams::COST_TERMINAL;
        }

        virtual float
        getOmissionCost(const DicTraverseSession *const traverseSession, const DicNode *const parentDicNode,
                        const DicNode *const dicNode) const override {
            ASSERT(false);
        }

        float getMatchedCost(const DicTraverseSession *const traverseSession,
                             const DicNode *const parentDicNode,
                             const DicNode *const dicNode, DicNode_InputStateG *inputStateG) const override {
            return SearchScoringParams::COST_MATCH;
        }

        bool isProximityDicNode(const DicTraverseSession *const traverseSession,
                                const DicNode *const dicNode) const override {
            ASSERT(false);
        }

        float getTranspositionCost(const DicTraverseSession *const traverseSession,
                                   const DicNode *const parentDicNode, const DicNode *const dicNode) const override {
            ASSERT(false);
        }

        float getInsertionCost(const DicTraverseSession *const traverseSession,
                               const DicNode *const parentDicNode, const DicNode *const dicNode) const override {
            ASSERT(false);
        }

        AK_FORCE_INLINE float
        getSkipCost(const DicTraverseSession *const traverseSession, const DicNode *const parentDicNode,
                    const DicNode *const dicNode) const override {
            ASSERT(false);
        }

        float getSpaceOmissionCost(const DicTraverseSession *const traverseSession,
                                   const DicNode *const dicNode, DicNode_InputStateG *inputStateG) const override {
            ASSERT(false);
        }

        float getNewWordBigramLanguageCost(const DicTraverseSession *const traverseSession,
                                           const DicNode *const dicNode,
                                           MultiBigramMap *const multiBigramMap) const override {
            ASSERT(false);
        }

        float getCompletionCost(const DicTraverseSession *const traverseSession,
                                const DicNode *const dicNode) const override {
            return SearchScoringParams::COST_COMPLETION;
        }

        float getTerminalLanguageCost(Improbability improbability, bool hasMultipleWords) const override {
            float cost = improbability.getLanguageImprobability() * ScoringParams::DISTANCE_WEIGHT_LANGUAGE;
            if (improbability.getLanguagePOSImprobability() != NOT_A_PROBABILITY)
                cost += improbability.getLanguagePOSImprobability() *
                        (hasMultipleWords ?
                         ScoringParams::DISTANCE_WEIGHT_LANGUAGE_POS_SPLIT :
                         ScoringParams::DISTANCE_WEIGHT_LANGUAGE_POS);
            return cost;
        }

        float getTerminalInsertionCost(const DicTraverseSession *const traverseSession,
                                       const DicNode *const dicNode) const override {
            ASSERT(false);
        }

        AK_FORCE_INLINE bool needsToNormalizeCompoundDistance() const override {
            return false;
        }

        AK_FORCE_INLINE float getAdditionalProximityCost() const override {
            ASSERT(false);
        }

        virtual AK_FORCE_INLINE float
        getSubstitutionCost(const DicTraverseSession *const pSession, const DicNode *const parentDicNode,
                            const DicNode *const dicNode,
                            DicNode_InputStateG *const pG) const override {
            ASSERT(false);
        }

        AK_FORCE_INLINE float getSpaceSubstitutionCost(const DicTraverseSession *const traverseSession,
                                                       const DicNode *const dicNode) const override {
            ASSERT(false);
        }

        ErrorTypeUtils::ErrorType getErrorType(CorrectionType correctionType,
                                               const DicTraverseSession *traverseSession,
                                               const DicNode *parentDicNode, const DicNode *dicNode) const override;

    protected:
        SearchWeighting() {}

        ~SearchWeighting() {}

    private:
        DISALLOW_COPY_AND_ASSIGN(SearchWeighting);
        static SearchWeighting sInstance;
    };
} // namespace latinime
#endif // LATINIME_SEARCH_WEIGHTING_H
