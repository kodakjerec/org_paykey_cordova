//
//  KeyboardKey
//
//  Created by paul kalnitz on 01/03/2017.
//  Copyright © 2017 paul kalnitz. All rights reserved.
//

#ifndef KEYBOARD_KEY_H
#define KEYBOARD_KEY_H

#define DEFAULT_TOUCH_POSITION_CORRECTION_RADIUS 0.15f

namespace latinime {

    struct Rectangle {
        int x;
        int y;
        int width;
        int height;
    };
    typedef struct Rectangle Rectangle;

    class KeyboardKey {
    public:
        KeyboardKey(int code, int x, int y, int width, int height);

        ~KeyboardKey();

        int squaredDistanceToEdge(int x, int y);

        float sweetSpotX();

        float sweetSpotY();

        float sweetSpotRadii();

        Rectangle hitBox();

        int detectKey(int x, int y);

        int mCode;
        int mX;
        int mY;
        int mWidth;
        int mHeight;

    };
} // namespace latinime

#endif /* KEYBOARD_KEY_H */

