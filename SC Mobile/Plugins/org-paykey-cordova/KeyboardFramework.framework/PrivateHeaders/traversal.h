/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_TRAVERSAL_H
#define LATINIME_TRAVERSAL_H

#include <dic_node.h>
#include "defines.h"

namespace latinime {

class DicTraverseSession;

class Traversal {
 public:
    virtual bool doesSupportIndexDividedTerminals() const = 0;
    virtual int getMaxPointerCount() const = 0;
    virtual bool allowsErrorCorrections(const DicNode *dicNode) const = 0;
    virtual bool isOmission(const DicTraverseSession *traverseSession,
            const DicNode *dicNode, const DicNode *childDicNode,
                            bool allowsErrorCorrections) const = 0;
    virtual bool isSkip(const DicTraverseSession *traverseSession,
            const DicNode *dicNode, bool allowsErrorCorrections) const = 0;
    virtual bool isInsertion(const DicTraverseSession *traverseSession,
                            const DicNode *dicNode, const DicNode *childDicNode) const = 0;
    virtual bool isSpaceSubstitutionTerminal(const DicTraverseSession *traverseSession,
            const DicNode *dicNode) const = 0;
    virtual bool isSpaceOmissionTerminal(const DicTraverseSession *traverseSession,
               const DicNode *dicNode) const = 0;
    virtual bool shouldDepthLevelCache(const DicTraverseSession *traverseSession) const = 0;
    virtual bool shouldNodeLevelCache(const DicTraverseSession *traverseSession,
            const DicNode *dicNode) const = 0;
    virtual bool canDoLookAheadCorrection(const DicTraverseSession *traverseSession,
            const DicNode *dicNode) const = 0;
    virtual ProximityType getProximityType(const DicTraverseSession *traverseSession,
            const DicNode *dicNode, const DicNode *childDicNode) const = 0;
    virtual bool needsToTraverseAllUserInput() const = 0;
    virtual float getMaxSpatialDistance() const = 0;
    virtual int getDefaultExpandDicNodeSize() const = 0;
    virtual int getMaxCacheSize(int inputSize, float weightForLocale) const = 0;
    virtual bool isPossibleOmissionChildNode(const DicTraverseSession *traverseSession,
            const DicNode *parentDicNode, const DicNode *dicNode) const = 0;
    virtual bool isGoodToTraverseNextWord(const DicNode *dicNode,
                                          int probability) const = 0;

    virtual int getCompletionInputSize(const DicTraverseSession *traverseSession) const = 0;
    
    virtual float getMaxValueForWeighting() const = 0;

    virtual bool allowLookAhead(const DicTraverseSession *traverseSession, DicNode *dicNode) const = 0;

protected:
    Traversal() {}
    virtual ~Traversal() {}

 private:
    DISALLOW_COPY_AND_ASSIGN(Traversal);
};
} // namespace latinime
#endif // LATINIME_TRAVERSAL_H
