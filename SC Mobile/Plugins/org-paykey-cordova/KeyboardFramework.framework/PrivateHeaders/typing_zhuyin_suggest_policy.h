/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_TYPING_ZH_SUGGEST_POLICY_H
#define LATINIME_TYPING_ZH_SUGGEST_POLICY_H

#include <typing_suggest_policy.h>
#include <iostream>
#include "defines.h"
#include "suggest_policy.h"
#include "typing_scoring.h"
#include "typing_traversal.h"
#include "typing_weighting.h"
#include "typing_zhuyin_weighting.h"
#include "typing_zhuyin_traversal.h"

namespace latinime {

class Scoring;
class Traversal;
class Weighting;

class TypingZhuyinSuggestPolicy : public TypingSuggestPolicy {
 public:
    static const TypingZhuyinSuggestPolicy *getInstance() { return &sInstance; }

    TypingZhuyinSuggestPolicy() {}
    ~TypingZhuyinSuggestPolicy() {}
    AK_FORCE_INLINE const Traversal *getTraversal() const override {
        return TypingZhuyinTraversal::getInstance();
    }

    AK_FORCE_INLINE const Scoring *getScoring() const override {
        return TypingScoring::getInstance();
    }

    AK_FORCE_INLINE const Weighting *getWeighting() const override {
        return TypingZhuyinWeighting::getInstance();
    }

 private:
    DISALLOW_COPY_AND_ASSIGN(TypingZhuyinSuggestPolicy);
    static const TypingZhuyinSuggestPolicy sInstance;
};
} // namespace latinime
#endif // LATINIME_TYPING_ZH_SUGGEST_POLICY_H
