//
//  nlpResults.h
//
//  Created by paul kalnitz on 05/03/2017.
//  Copyright © 2017 paul kalnitz. All rights reserved.
//

#ifndef NLP_RESULTS_H
#define NLP_RESULTS_H

#include <suggested_word.h>

namespace latinime {

    struct NlpResults {
        NlpResults();
        NlpResults(int status, SuggestedWords nlpSuggestions);
        ~NlpResults();

        int mStatus;
        SuggestedWords suggestions;
};
    
} // namespace latinime

#endif /* NLP_RESULTS_H */
