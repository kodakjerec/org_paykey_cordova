//
//  INTEGRATOR
//
//  Created by paul kalnitz on 27/03/2017.
//  Copyright © 2017 paul kalnitz. All rights reserved.
//

#ifndef INTEGRATOR_H
#define INTEGRATOR_H

#include <locale>
#include <unordered_set>
#include "nlpDefines.h"
#include "dictionary.h"
#include "NlpResults.h"

namespace latinime {

    class Integrator {
    public:
        Integrator();

        Integrator(int capMode);

        void append(SuggestedWords *suggestions);

        void removeDuplicates();

        bool inputEqualToSuggestion(const int *input, int inputLen, const SuggestedWord *suggestion);

        bool isInSuggestions(const int *typedInputCodePoints, int length);

        void setStatus(int status);

        void calcStatus(CodePointArrayView codePointArrayView, bool allowAutoCorrection);

        void transformSuggestions(const CodePointArrayView codePointArrayView, bool isGesture, int shiftMode);

        void trimSuggestions(int newSize);

        void dump(const char *header);

        void sortResult();

        int getStatus();

        SuggestedWords getSuggestions();

        void merge(Integrator integrator, bool inheritStatus);

    private:

        static bool const SHOULD_AUTO_CORRECT_USING_NON_WHITE_LISTED_SUGGESTION = false;
        SuggestedWords mCombinedSuggestions;
        int mStatus = 0;

        const SuggestedWord *getFirstSuggestion(const int kindFlagFilter) const;
    };
} // namespace latinime

#endif /* INTEGRATOR_H */
