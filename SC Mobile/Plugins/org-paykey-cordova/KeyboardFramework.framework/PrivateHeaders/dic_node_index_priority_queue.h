/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_DIC_NODE_INDEX_PRIORITY_QUEUE_H
#define LATINIME_DIC_NODE_INDEX_PRIORITY_QUEUE_H

#include <algorithm>
#include <queue>
#include <vector>
#include <map>

#include "defines.h"
#include "dic_node.h"
#include "dic_node_pool.h"
#include "dic_node_priority_queue.h"

namespace latinime {

    typedef std::map<int, DicNodePriorityQueue *> IndexToQueueMap;
    typedef std::pair<int, latinime::DicNodePriorityQueue *> IndexQueuePair;

    class DicNodeIndexPriorityQueue {
    public:
        AK_FORCE_INLINE explicit DicNodeIndexPriorityQueue(const int capacity)
                : mMaxTerminalCount(capacity), mDicNodesQueueMap(), mMaxBucketSize(capacity) {
            clear();
        }

        // Non virtual inline destructor -- never inherit this class
        AK_FORCE_INLINE ~DicNodeIndexPriorityQueue() {
            IndexToQueueMap::iterator p;
            for (p = mDicNodesQueueMap.begin(); p != mDicNodesQueueMap.end(); ++p) {
                delete p->second;
            }
        }

        AK_FORCE_INLINE int getTotalTerminalSize() {
            int totalSize = 0;
            IndexToQueueMap::iterator p;
            for (p = mDicNodesQueueMap.begin(); p != mDicNodesQueueMap.end(); ++p) {
                totalSize += p->second->getSize();
            }
            return totalSize;
        }

        AK_FORCE_INLINE int getSize(const int bucketIndex) const {
            DicNodePriorityQueue *queue = get(bucketIndex);
            if (queue)
                return queue->getSize();
            else return 0;
        }

        AK_FORCE_INLINE int getBucketCount() const {
            return static_cast<int>(mDicNodesQueueMap.size());
        }

        AK_FORCE_INLINE std::vector<int> getBucketsKeys() const {
            std::vector<int> outKeys;
            IndexToQueueMap::const_iterator p;
            for (p = mDicNodesQueueMap.begin(); p != mDicNodesQueueMap.end(); ++p) {
                outKeys.push_back(p->first);
            }
            return outKeys;
        }

        AK_FORCE_INLINE int getMaxSize() const {
            return mMaxTerminalCount;
        }

        AK_FORCE_INLINE void resetAndReduceMaxSize(const int maxSize) {
            ASSERT(maxSize <= getMaxSize());
            setMaxTerminalCount(maxSize);
            IndexToQueueMap::iterator p;
            for (p = mDicNodesQueueMap.begin(); p != mDicNodesQueueMap.end(); ++p) {
                p->second->setMaxSizeAndReset(mMaxBucketSize);
            }
        }

        AK_FORCE_INLINE void clear() {
            clearAndResize(mMaxTerminalCount);
        }

        AK_FORCE_INLINE void clearAndResize(const int maxSize) {
            setMaxTerminalCount(maxSize);
            IndexToQueueMap::iterator p;
            for (p = mDicNodesQueueMap.begin(); p != mDicNodesQueueMap.end(); ++p) {
                p->second->clearAndResize(mMaxBucketSize);
            }
        }

        void copyPush(const int index, const DicNode *const dicNode) {
            DicNodePriorityQueue *queue = getOrCreate(index);
            updateMaxTerminalCount();
            queue->copyPush(dicNode);
        }

        AK_FORCE_INLINE void copyPop(const int index, DicNode *const dest) {
            DicNodePriorityQueue *queue = getOrCreate(index);
            queue->copyPop(dest);
        }

        AK_FORCE_INLINE int copyPopAll(std::vector<DicNode> &outTerminal) {
            const int vectorSize = static_cast<const int>(outTerminal.size());
            int currentVectorIndex = 0;
            IndexToQueueMap::iterator p;
            for (p = mDicNodesQueueMap.begin();
                 p != mDicNodesQueueMap.end() && currentVectorIndex < (int) outTerminal.size(); ++p) {
                while (p->second->getSize() > 0 && currentVectorIndex < (int) outTerminal.size()) {
                    p->second->copyPop(&outTerminal[currentVectorIndex++]);
                }
            }
            return currentVectorIndex;
        }

        AK_FORCE_INLINE void dump() const {
            IndexToQueueMap::const_iterator p;
            for (p = mDicNodesQueueMap.begin(); p != mDicNodesQueueMap.end(); ++p) {
                p->second->dump();
            }
        }

        AK_FORCE_INLINE int computeDividedBucketSize() {
            const int tempBucketCount = getBucketCount() < 1 ? 1 : getBucketCount();
            int bucketSize = mMaxTerminalCount / tempBucketCount;
            bucketSize = bucketSize < 1 ? 1 : bucketSize;
            ASSERT(mMaxBucketSize >= 0 && mMaxBucketSize <= mMaxTerminalCount);
            return bucketSize;
        }

    private:
        DISALLOW_IMPLICIT_CONSTRUCTORS(DicNodeIndexPriorityQueue);

        AK_FORCE_INLINE void updateMaxTerminalCount() {
            int tempBucketCount = getBucketCount() < 1 ? 1 : getBucketCount();
            if (mMaxBucketSize != (mMaxTerminalCount / tempBucketCount))
                resetAndReduceMaxSize(mMaxTerminalCount);
        }

        AK_FORCE_INLINE int computeMaxBucketSize() {
            mMaxBucketSize = mUseMaxSizeForBucketsInner ? mMaxTerminalCount : computeDividedBucketSize();
            return mMaxBucketSize;
        }

        AK_FORCE_INLINE void setMaxTerminalCount(const int maxCount) {
            mMaxTerminalCount = maxCount;
            computeMaxBucketSize();
        }

        AK_FORCE_INLINE DicNodePriorityQueue *getOrCreate(const int index) {
            IndexToQueueMap::iterator iterator = mDicNodesQueueMap.find(index);
            DicNodePriorityQueue *queue;
            if (iterator == mDicNodesQueueMap.end()) {
                queue = new DicNodePriorityQueue(mMaxBucketSize);
                IndexQueuePair pair(index, queue);
                mDicNodesQueueMap.insert(pair);
            } else {
                queue = iterator->second;
            }

            return queue;
        }

        AK_FORCE_INLINE DicNodePriorityQueue *get(const int index) const {
            IndexToQueueMap::const_iterator iterator = mDicNodesQueueMap.find(index);
            DicNodePriorityQueue *queue;
            if (iterator == mDicNodesQueueMap.end()) {
                queue = nullptr;
            } else {
                queue = iterator->second;
            }

            return queue;
        }


        int mMaxTerminalCount = 0;
        int mMaxBucketSize = 0;
        const bool mUseMaxSizeForBucketsInner = true; /* Always true for now */
        IndexToQueueMap mDicNodesQueueMap;

    };
} // namespace latinime
#endif // LATINIME_DIC_NODE_INDEX_PRIORITY_QUEUE_H
