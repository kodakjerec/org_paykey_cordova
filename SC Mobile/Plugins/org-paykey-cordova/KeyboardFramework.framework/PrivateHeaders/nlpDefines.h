//
//  NLPDefines.h
//
//  Created by paul kalnitz on 14/03/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#ifndef NLP_DEFINES_H
#define NLP_DEFINES_H

#include <vector>
#include <map>

#define PK_NLP_LIBRARY_VERSION "237.3.0"

#define MIN_CORRECTION_THRESH       0.185
#define MIN_SPLIT_WORD_THRESH       0.430
#define MIN_MULTI_SPLIT_WORD_THRESH 1.250

#define IS_AUTO_CORRECTION                      0x01
#define TYPED_INPUT_IN_DICTIONARY               0x02
#define TYPED_INPUT_IS_SHORTCUT                 0x04
#define TYPED_INPUT_IS_WHITELISTED              0x08
#define TYPED_INPUT_ALLOWED_TO_BE_AUTOCORRECTED 0x10

typedef std::vector<int> CodePoints;
typedef std::vector<CodePoints> CodePointsList;

#define HAS_AUTO_CORRECTION(status) ((status & IS_AUTO_CORRECTION)==IS_AUTO_CORRECTION)
#define IS_IN_DICTIONARY(status) ((status & TYPED_INPUT_IN_DICTIONARY)==TYPED_INPUT_IN_DICTIONARY)
#define IS_SHORTCUT(status) ((status & TYPED_INPUT_IS_SHORTCUT)==TYPED_INPUT_IS_SHORTCUT)
#define IS_WHITELISTED(status) ((status & TYPED_INPUT_IS_WHITELISTED)==TYPED_INPUT_IS_WHITELISTED)
#define IS_ALLOWED_TO_BE_AUTOCORRECTED(status) ((status & TYPED_INPUT_ALLOWED_TO_BE_AUTOCORRECTED)==TYPED_INPUT_ALLOWED_TO_BE_AUTOCORRECTED)


#endif /* NLP_DEFINES_H */
