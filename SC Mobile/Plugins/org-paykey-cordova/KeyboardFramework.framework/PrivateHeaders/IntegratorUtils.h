//
// Created by YakirPayKey on 3/19/18.
//

#ifndef NLP_ENGINE_INTEGRATORUTILS_H
#define NLP_ENGINE_INTEGRATORUTILS_H

#include <dictionary.h>
#include <autocorrection_threshold_utils.h>

namespace latinime {

    class IntegratorUtils {

    public:
        static constexpr float AUTO_CORRECTION_THRESH = 0.185f;

        static float
        calcNormalizedScore(const int *beforeWord, int beforeWordLen, const int *afterword, int afterWordLen,
                            int score) {
            float ret = latinime::AutocorrectionThresholdUtils::calcNormalizedScore(beforeWord,
                                                                                    beforeWordLen,
                                                                                    afterword,
                                                                                    afterWordLen,
                                                                                    score);
            return ret;
        }

        static bool suggestionExceedsThreshold(const SuggestedWord *firstSuggestion, const int *inputCodePoints,
                                               const int inputLen,
                                               float *normScoreRes) {
            if (firstSuggestion != nullptr) {
                if ((firstSuggestion->getType() & latinime::Dictionary::KIND_MASK_KIND) ==
                    latinime::Dictionary::KIND_WHITELIST)
                    return true;

                if ((firstSuggestion->getType() & latinime::Dictionary::KIND_FLAG_APPROPRIATE_FOR_AUTOCORRECTION) == 0)
                    return false;

                float normScore = calcNormalizedScore(inputCodePoints, inputLen, firstSuggestion->getCodePoints(),
                                                      firstSuggestion->getCodePointCount(),
                                                      firstSuggestion->getScore());
                (*normScoreRes) = normScore;
                if (normScore >= AUTO_CORRECTION_THRESH)
                    return true;
            }
            return false;
        }

        static SuggestedWord *getWhitelistedWordInfoOrNull(SuggestedWords *suggestions) {
            if (suggestions->empty())
                return nullptr;

            SuggestedWord *first = &suggestions->back();

            if ((first->getType() & latinime::Dictionary::KIND_MASK_KIND) != latinime::Dictionary::KIND_WHITELIST)
                return nullptr;

            return first;
        }

        static bool isMostlyUpper(const int *codePoints, const int wordLen) {
            int capCount = 0;
            for (int i = 0; i < wordLen; ++i) {
                capCount += latinime::CharUtils::isAsciiUpper(codePoints[i]) ? 1 : 0;
            }
            return capCount > 1;
        }

        static bool isAllUpper(const int *codePoints, const int wordLen) {
            for (int i = 0; i < wordLen; ++i) {
                if (!latinime::CharUtils::isUpper(codePoints[i])) {
                    return false;
                }
            }
            return true;
        }

        static bool isOnlyFirstLetterCapitalized(const int *codePoints, int wordLen) {

            if (wordLen == 0 || !CharUtils::isAsciiUpper(codePoints[0])) {
                return false;
            }

            for (int i = 1; i < wordLen; ++i) {
                if (CharUtils::isAsciiUpper(codePoints[i]))
                    return false;
            }

            return true;
        }

        static bool hasDigits(const int *codePoints, int len) {
            int digitsCount = 0;
            for (int i = 0; i < len; ++i) {
                digitsCount += (codePoints[i] >= 48 && codePoints[i] <= 57) ? 1 : 0;
            }
            return digitsCount > 0;
        }

        static bool isShortcutSuggestion(const SuggestedWord *first) {
            if (first == nullptr)
                return false;

            return (first->getType() & latinime::Dictionary::KIND_MASK_KIND) == latinime::Dictionary::KIND_SHORTCUT;
        }
    };
}

#endif //NLP_ENGINE_INTEGRATORUTILS_H
