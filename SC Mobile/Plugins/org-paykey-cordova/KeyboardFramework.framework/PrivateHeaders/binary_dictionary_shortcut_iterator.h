/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_BINARY_DICTIONARY_SHORTCUT_ITERATOR_H
#define LATINIME_BINARY_DICTIONARY_SHORTCUT_ITERATOR_H

#include "defines.h"
#include "dictionary_shortcuts_structure_policy.h"

namespace latinime {

    class BinaryDictionaryShortcutIterator;

    struct ShortcutOutput {
        int codePoints[MAX_WORD_LENGTH] = {0};
        bool isWhitelist = false;
        int codePointsLength = 0;
        int frequency = NOT_A_PROBABILITY;
        int wordId = NOT_A_WORD_ID;

        ShortcutOutput(int parentWordId) {
            wordId = parentWordId;
        }

        bool isEmpty() {
            return wordId == NOT_A_WORD_ID &&
                   frequency == NOT_A_PROBABILITY &&
                   codePointsLength == 0 ;
        }

        bool operator==(const ShortcutOutput &other) {
            if (!(wordId == other.wordId &&
                  frequency == other.frequency &&
                  isWhitelist == other.isWhitelist &&
                  codePointsLength == other.codePointsLength))
                return false;

            for (int i = 0; i < codePointsLength; ++i) {
                if (codePoints[i] != other.codePoints[i])
                    return false;
            }

            return true;
        }
    };

    class BinaryDictionaryShortcutIterator {
    public:

        BinaryDictionaryShortcutIterator(
                const DictionaryShortcutsStructurePolicy *const shortcutStructurePolicy,
                const int shortcutPos, const int bias)
                :mShortcutStructurePolicy(shortcutStructurePolicy),
                 mPos(shortcutStructurePolicy->getStartPos(shortcutPos)),
                 mHasNextShortcutTarget(shortcutPos != NOT_A_DICT_POS), mBias(bias) {}

        BinaryDictionaryShortcutIterator(
                const DictionaryShortcutsStructurePolicy *const shortcutStructurePolicy,
                const int shortcutPos)
                : mShortcutStructurePolicy(shortcutStructurePolicy),
                  mPos(shortcutStructurePolicy->getStartPos(shortcutPos)),
                  mHasNextShortcutTarget(shortcutPos != NOT_A_DICT_POS) {}

        BinaryDictionaryShortcutIterator(const BinaryDictionaryShortcutIterator &&shortcutIterator)
                : mShortcutStructurePolicy(shortcutIterator.mShortcutStructurePolicy),
                  mPos(shortcutIterator.mPos),
                  mHasNextShortcutTarget(shortcutIterator.mHasNextShortcutTarget) {}

        AK_FORCE_INLINE bool hasNextShortcutTarget() const {
            return mHasNextShortcutTarget;
        }

        // Gets the shortcut target itself as an int string and put it to outTarget, put its length
        // to outTargetLength, put whether it is whitelist to outIsWhitelist.
        virtual AK_FORCE_INLINE void nextShortcutTarget(
                const int maxDepth, int *const outTarget, int *const outTargetLength,
                int *const outFrequency,
                bool *const outIsWhitelist) {
            mShortcutStructurePolicy->getNextShortcut(maxDepth, outTarget, outTargetLength,
                                                      outIsWhitelist, &mHasNextShortcutTarget, outFrequency, &mPos);

            if (mBias != NOT_A_PROBABILITY && outFrequency && (*outFrequency) != NOT_A_PROBABILITY) {
                *outFrequency = (*outFrequency + mBias) / 2;
            }
        }

        // Gets the shortcut target itself as an int string and put it to outTarget, put its length
        // to outTargetLength, put whether it is whitelist to outIsWhitelist.
        virtual AK_FORCE_INLINE void nextShortcutTarget(
                const int maxDepth, ShortcutOutput *shortcutOutputOut) {
            nextShortcutTarget(maxDepth, shortcutOutputOut->codePoints,
                               &shortcutOutputOut->codePointsLength,
                               &shortcutOutputOut->frequency, &shortcutOutputOut->isWhitelist);
        }

    private:
        DISALLOW_DEFAULT_CONSTRUCTOR(BinaryDictionaryShortcutIterator);

        DISALLOW_ASSIGNMENT_OPERATOR(BinaryDictionaryShortcutIterator);

        const DictionaryShortcutsStructurePolicy *const mShortcutStructurePolicy;
        int mPos;
        bool mHasNextShortcutTarget;
        const int mBias = NOT_A_PROBABILITY;
    };
} // namespace latinime
#endif // LATINIME_BINARY_DICTIONARY_SHORTCUT_ITERATOR_H
