//
// Created by YakirPayKey on 4/9/18.
//

#ifndef NLP_ENGINE_DICTIONARYFACILITATORUTILS_H
#define NLP_ENGINE_DICTIONARYFACILITATORUTILS_H

#include <algorithm>
#include <int_array_view.h>
#include "DictionaryFacilitator.h"

#define CAPITALIZED_FORM_MAX_PROBABILITY_FOR_INSERT 140

namespace latinime {

    class DictionaryFacilitatorUtils {
    public:

        static void
        getWordToLearn(DictionaryFacilitator *dictionaryFacilitator, CodePointArrayView word, bool wasAutoCapitalized,
                       CodePoints &wordToLearn);

    private:

    };
}


#endif //NLP_ENGINE_DICTIONARYFACILITATORUTILS_H
