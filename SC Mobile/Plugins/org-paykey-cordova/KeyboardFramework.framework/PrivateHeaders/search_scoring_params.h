//
// Created by YakirPayKey on 2019-04-16.
//

#ifndef NLP_ENGINE_SEARCH_SCORING_PARAMS_H
#define NLP_ENGINE_SEARCH_SCORING_PARAMS_H

namespace latinime {
 
 
    class SearchScoringParams {
    public:
        static const float COST_COMPLETION;
        static const float COST_TERMINAL_INSERTION;
        static const float COST_MATCH;
        static const float COST_TERMINAL;
    };

}

#endif //NLP_ENGINE_SEARCH_SCORING_PARAMS_H
