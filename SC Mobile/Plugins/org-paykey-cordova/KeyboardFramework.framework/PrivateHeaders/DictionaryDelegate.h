//
// Created by YakirPayKey on 3/21/18.
//

#ifndef DICTIONARY_DELEGATE_H
#define DICTIONARY_DELEGATE_H

#include <dictionary.h>
#include <nlpDefines.h>
#include <dictionary_structure_with_buffer_policy_factory.h>
#include <suggestion_results.h>
#include <DictionaryDelegateUtils.h>

namespace latinime {

    class DictionaryDelegate {

    public:
        static const int MAX_DICTIONARY_INFO_LENGTH = 512;

        DictionaryDelegate(const char *name, const char *pathToDict, const int dictionarySize, const long offset)
                : mFileSize(
                dictionarySize), mOffset(offset), mName(name), mFilePath(pathToDict) {
            if (pathToDict) {
                mFilePath = strdup(pathToDict);
            } else mFilePath = nullptr;

            if (name) {
                mName = strdup(name);
            } else mName = nullptr;
        }

        bool loadCreateIfNeeded() {
            bool success = false;
            if (exists()) {
                AKLOGIC("%s: Dictionary %s exists, loading...\n", getName(), getFilePath());
                DictionaryStructureWithBufferPolicy::StructurePolicyPtr ptr =
                        DictionaryStructureWithBufferPolicyFactory::
                        newPolicyForExistingDictFile(getFilePath(), static_cast<const int>(getOffset()), getFileSize(),
                                                     isUpdatable());
                if (ptr.get() != nullptr) {
                    success = true;
                    setDictionary(Dictionary::createNewDictionary(std::move(ptr)));
                }
            } else {
                AKLOGIC("%s: in path %s, Does NOT exist, trying to create\n", getName(), getFilePath());
                success = create();
            }

            if (!success) {
                AKLOGEC("%s: loadCreateIfNeeded FAILED!: %s, %ld, %ld\n", getName(), getFilePath(), getOffset(),
                        (long) getFileSize());
            } else {
                AKLOGIC("%s: loadCreateIfNeeded sucess!\n", getName());
            }
            return success;
        }

        virtual void close() {
            deleteDictionary();

            delete mFilePath;
            mFilePath = nullptr;
            delete mName;
            mName = nullptr;

            deleteDicTraverseSessions();
        }

        void deleteDictionary() {
            delete mDictionary;
            mDictionary = nullptr;
        }

        void reopenDictionary() {
            deleteDictionary();
            loadCreateIfNeeded();
        }

        DicTraverseSession *refreshDicTraverseSession(bool isGesture, int terminalCacheSize) {
            DicTraverseSession **pTraverseSession = getDicTraverseSession(isGesture);
            int maxTerminals = -1;
            if (!*pTraverseSession ||
                terminalCacheSize >= (maxTerminals = (*pTraverseSession)->getTerminalCacheMaxSize())) {
                deleteDicTraverseSession(isGesture);
                *pTraverseSession = new DicTraverseSession(true, terminalCacheSize);
            } else if (maxTerminals < terminalCacheSize) {
                (*pTraverseSession)->reduceTerminalCacheMaxSize(terminalCacheSize);
            }
            return *pTraverseSession;
        }

        void deleteDicTraverseSessions() {
            deleteDicTraverseSession(true /* gesture */);
            deleteDicTraverseSession(false /* typing */);
        }

        void deleteDicTraverseSession(bool isGesture) {
            DicTraverseSession **pDicTraverseSession = getDicTraverseSession(isGesture);

            if (*pDicTraverseSession)
                DicTraverseSession::releaseSessionInstance(*pDicTraverseSession);
            *pDicTraverseSession = nullptr;
        }

        DicTraverseSession **getDicTraverseSession(bool isGesture) {
            return isGesture ? &mTraverseSessionGesture : &mTraverseSessionTyping;
        }

        CodePoints getLocale() const {
            return mLocale;
        }

        const char *getName() const {
            return mName;
        }

        const char *getFilePath() const {
            return mFilePath;
        }

        int getFileSize() const {
            return mFileSize;
        }

        long getOffset() const {
            return mOffset;
        }

        void setLocale(const CodePoints mLocale) {
            DictionaryDelegate::mLocale = mLocale;
        }

        void setDictionary(Dictionary *mDictionary) {
            DictionaryDelegate::mDictionary = mDictionary;
        }

        virtual bool isValid() {
            return mDictionary != nullptr;
        }

        virtual bool exists() = 0;

        virtual bool isUpdatable() = 0;

        virtual bool isValidWord(CodePointArrayView word) {
            return getProbability(word) != NOT_A_PROBABILITY;
        }

        int getProbability(CodePointArrayView word) {
            if (word.empty()) {
                return NOT_A_PROBABILITY;
            }
            return mDictionary->getProbability(word);
        }

        int getProbability(CodePointArrayView word, CodePointArrayView shortcut) {
            if (word.empty()) {
                return NOT_A_PROBABILITY;
            }
            if (shortcut.empty()) {
                return getProbability(word);
            }

            std::vector<int> shortcutVector = shortcut.toVector();
            WordProperty wordProperty = mDictionary->getWordProperty(word);
            if (wordProperty.getUnigramProperty().hasShortcuts()) {
                for (auto &wordShortcut : wordProperty.getUnigramProperty().getShortcuts()) {
                    if (std::equal(shortcutVector.begin(), shortcutVector.end(),
                                   wordShortcut.getTargetCodePoints()->begin())) {
                        return wordShortcut.getProbability();
                    }
                }
            }
            return NOT_A_PROBABILITY;
        }

        virtual bool isOffensive(CodePointArrayView word) {
            return mDictionary->isOffensive(word);
        }

        virtual void
        search(const CodePointArrayView codePointArrayView, SuggestOptions *options,
               SuggestedWords *searchSuggestions) {
            DicTraverseSession *traverseSession = refreshDicTraverseSession(options->isGesture(),
                                                                            options->getMaxResultsSize());
            SuggestionResults res(options->getMaxResultsSize());
            mDictionary->search(mTraverseSessionTyping, codePointArrayView, options, &res);
            transformEngineSuggestions(&res, searchSuggestions);
        }

        virtual void
        predict(NgramContext *ngramContext, CodePointArrayView codePointArrayView,
                const int maxResults, SuggestedWords *predictions) {
            SuggestionResults res(maxResults);
            mDictionary->getPredictions(ngramContext, &res, codePointArrayView);
            transformEngineSuggestions(&res, predictions);
        }

        virtual void correct(ProximityInfo *proximityInfo,
                             NgramContext *ngramContext,
                             int codePoints[],
                             int typedInputLen,
                             int x[], int y[],
                             int times[],
                             int pointer[],
                             SuggestOptions *options,
                             SuggestedWords *corrections) {

            DicTraverseSession *traverseSession = refreshDicTraverseSession(options->isGesture(),
                                                                            options->getMaxResultsSize());

            if (!options->isGesture() && typedInputLen > MAX_WORD_LENGTH) {
                AKLOGEC("Word is too long for typing\n");
                return;
            }

            if (!mDictionary) {
                AKLOGEC("Dictionary is missing\n");
                return;
            }
            if (!traverseSession) {
                AKLOGEC("DicTraverseSession is missing\n");
                return;
            }

            SuggestionResults res(options->getMaxResultsSize());
            const float weightOfLangModelVsSpatialModel = -1;

            mDictionary->getSuggestions(proximityInfo, traverseSession,
                                        x, y, times, pointer, codePoints,
                                        typedInputLen,
                                        ngramContext,
                                        options,
                                        weightOfLangModelVsSpatialModel,
                                        &res,
                                        nullptr);

            transformEngineSuggestions(&res, corrections);
        }


        std::string dumpAllWordsForDebug() {
            std::ostringstream stringStream;
            std::vector<std::string> unigrams;
            std::vector<std::string> bigrams;
            std::vector<std::string> trigrams;
            std::vector<std::string> fourgrams;

            int token = 0;
            do {
                char unigram[MAX_WORD_LENGTH] = {0};
                int wordCodePoints[MAX_WORD_LENGTH];
                int wordCodePointCount = 0;

                token = getDictionary()->getNextWordAndNextToken(token, wordCodePoints, &wordCodePointCount);

                if (wordCodePointCount == 0) {
                    continue;
                }

                intArrayToCharArray(&wordCodePoints[0], wordCodePointCount, unigram, MAX_WORD_LENGTH);

                const WordProperty wordProperty = mDictionary->getWordProperty(
                        CodePointArrayView(wordCodePoints, static_cast<const size_t>(wordCodePointCount)));
                const UnigramProperty *unigramProperty = &wordProperty.getUnigramProperty();

                if (!unigramProperty->representsBeginningOfSentence()) {
                    std::stringstream u;
                    u << unigram << std::string(", ")
                      << std::to_string(unigramProperty->getHistoricalInfo().getCount());
                    u << std::string(", ") << std::to_string(unigramProperty->getProbability());
                    u << std::string(", ") << std::to_string(unigramProperty->isNotAWord());
                    std::vector<UnigramProperty::ShortcutProperty> shortcuts = unigramProperty->getShortcuts();
                    for (int i = 0; i < (int) shortcuts.size(); ++i) {
                        if (i == 0) {
                            u << " | ";
                        }

                        char shortcutCharArr[MAX_WORD_LENGTH] = {0};
                        const std::vector<int> *shorVexc = shortcuts[i].getTargetCodePoints();
                        const std::vector<int> svv = *shorVexc;
                        intArrayToCharArray(&svv[0], static_cast<const int>(shortcuts[i].getTargetCodePoints()->size()),
                                            shortcutCharArr, MAX_WORD_LENGTH);
                        u << "(" << shortcutCharArr << "[" << shortcuts[i].getProbability() << "]" << ") ";
                    }
                    unigrams.push_back(u.str());
                }

                const std::vector<NgramProperty> *ngrams = &wordProperty.getNgramProperties();

                for (auto it = ngrams->begin(); it != ngrams->end(); ++it) {
                    std::string ngram;
                    const NgramContext *c = it->getNgramContext();
                    int prevWordCount = (int) c->getPrevWordCount();
                    for (int i = prevWordCount; i > 0; i--) {
                        CodePointArrayView prevCodePointView = c->getNthPrevWordCodePoints(
                                static_cast<const size_t>(i));
                        char prevWord[MAX_WORD_LENGTH] = {0};
                        intArrayToCharArray(prevCodePointView.data(), (int) prevCodePointView.size(), prevWord,
                                            MAX_WORD_LENGTH);
                        if (c->isNthPrevWordBeginningOfSentence(i))
                            ngram = ngram + "(b)";

                        ngram = ngram + std::string(prevWord) + std::string(" ");

                    }

                    char targetWord[MAX_WORD_LENGTH] = {0};
                    const std::vector<int> *t = it->getTargetCodePoints();

                    if (t->size() == 0) {
                        //unigram was deleted
                        ngram = ngram + std::string(targetWord) + std::string("(deleted) ") +
                                std::to_string(it->getProbability());
                    } else {
                        intArrayToCharArray(&t->at(0), (int) t->size(), targetWord, MAX_WORD_LENGTH);
                        ngram = ngram + std::string(" -> ") + std::string(targetWord) + std::string(" ,") +
                                std::to_string(it->getHistoricalInfo().getCount()) + std::string(",") +
                                std::to_string(it->getProbability());
                    }


                    switch (prevWordCount) {
                        case 1:
                            bigrams.push_back(ngram);
                            break;
                        case 2:
                            trigrams.push_back(ngram);
                            break;
                        case 3:
                            fourgrams.push_back(ngram);
                            break;
                        default:
                            break;
                    }
                }
            } while (token != 0);

            sort(unigrams.begin(), unigrams.end());

            stringStream << "------Unigrams------\n";
            for (auto it = unigrams.begin(); it != unigrams.end(); ++it)
                stringStream << it->c_str() << "\n";
            stringStream << "------Bigrams------\n";

            for (auto it = bigrams.begin(); it != bigrams.end(); ++it)
                stringStream << it->c_str() << "\n";
            stringStream << "------------\n";

            for (auto it = trigrams.begin(); it != trigrams.end(); ++it)
                stringStream << it->c_str() << "\n";
            stringStream << "------------\n";

            for (auto it = fourgrams.begin(); it != fourgrams.end(); ++it)
                stringStream << it->c_str() << "\n";
            stringStream << "------------\n";

            AKLOGI("%s", stringStream.str().c_str());
            return stringStream.str();
        }

        std::string getDictionaryInfo() {
            std::stringstream ss;

            if (!mDictionary) {
                ss << "Dictionary wasn't initialized";
                return ss.str();
            }

            const DictionaryHeaderStructurePolicy *const headerPolicy =
                    mDictionary->getDictionaryStructurePolicy()->getHeaderStructurePolicy();

            int outFormatVersion = headerPolicy->getFormatVersionNumber();

            const DictionaryHeaderStructurePolicy::AttributeMap *const attributeMap =
                    headerPolicy->getAttributeMap();

            for (DictionaryHeaderStructurePolicy::AttributeMap::const_iterator it = attributeMap->begin();
                 it != attributeMap->end(); ++it) {

                const int keyLen = static_cast<int>(it->first.size());
                int keyCodePoints[keyLen];
                CharUtils::outputCodePoints(keyCodePoints, it->first.data(), static_cast<unsigned long>(keyLen));
                char keyBuf[keyLen + 1];
                intArrayToCharArray(keyCodePoints, keyLen, keyBuf, keyLen + 1);

                const int valueLen = static_cast<int>(it->second.size());
                int valueCodePoints[valueLen];
                CharUtils::outputCodePoints(valueCodePoints, it->second.data(), static_cast<unsigned long>(valueLen));
                char valueBuf[valueLen + 1];
                intArrayToCharArray(valueCodePoints, valueLen, valueBuf, valueLen + 1);

                if (ss.str().size() + keyLen + valueLen + 2 < MAX_DICTIONARY_INFO_LENGTH) {
                    ss << keyBuf << "=" << valueBuf << ",";
                } else {
                    AKLOGE("header info doesn't fit in buffer");
                    return ss.str();
                }
            }
            ss << "FORMAT_VERSION_" << outFormatVersion;
            return ss.str();
        }

    protected:
        Dictionary *getDictionary() {
            return mDictionary;
        };


    private:

        virtual bool create() = 0;

        CodePoints mLocale;
        Dictionary *mDictionary = nullptr;
        const char *mName = nullptr;
        const char *mFilePath = nullptr;
        const int mFileSize;
        const long mOffset;
        DicTraverseSession *mTraverseSessionTyping = nullptr;
        DicTraverseSession *mTraverseSessionGesture = nullptr;
    };


};


#endif //DICTIONARY_DELEGATE_H
