//
// Created by YakirPayKey on 3/20/18.
//

#ifndef NLP_ENGINE_CONTACTSDICTIONARY_H
#define NLP_ENGINE_CONTACTSDICTIONARY_H

#include "ExpandableDictionary.h"

namespace latinime {

    class ContactsDictionary : public ExpandableDictionary {
    public:

        const int FREQUENCY_FOR_CONTACTSS = 120;
        const int FREQUENCY_FOR_CONTACTS_BIGRAMM = 190;
        static const char *const DICTIONARY_NAME;

        ContactsDictionary() : ExpandableDictionary(DICTIONARY_NAME, nullptr) {};

        bool addContacts(CodePointsList fullName);

    public:

        static NgramContext constructNgramContextForNames(CodePointsList previousNames) {
            if (previousNames.empty()) {
                return NgramContext();
            }

            int prevNameCodePoints[MAX_PREV_WORD_COUNT_FOR_N_GRAM][MAX_WORD_LENGTH];
            int prevNameCodePointCount[MAX_PREV_WORD_COUNT_FOR_N_GRAM];
            bool isBeginningOfSentence[MAX_PREV_WORD_COUNT_FOR_N_GRAM];

            for (int i = 0; i < (int) previousNames.size(); i++) {

                int *prevName = &previousNames[i][0];
                if (!prevName) {
                    continue;
                }

                auto prevNameLength = static_cast<int>(previousNames[i].size());

                memcpy(prevNameCodePoints[i], prevName, sizeof(int) * prevNameLength);

                prevNameCodePointCount[i] = prevNameLength;

                isBeginningOfSentence[i] = false;
            }

            NgramContext ngramContext = NgramContext(prevNameCodePoints, prevNameCodePointCount, isBeginningOfSentence,
                                                     previousNames.size());

            return ngramContext;
        }
    };

}


#endif //NLP_ENGINE_CONTACTSDICTIONARY_H
