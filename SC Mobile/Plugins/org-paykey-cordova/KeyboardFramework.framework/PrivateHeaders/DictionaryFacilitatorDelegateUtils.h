//
// Created by Paul PayKey on 3/21/18.
//

#ifndef DICTIONARY_FACILITATOR_DELEGATE_UTILS_H
#define DICTIONARY_FACILITATOR_DELEGATE_UTILS_H

namespace latinime {

    class DictionaryFacilitatorDelegateUtils {

    public:
        static void convertNgramContextParams(CodePointsList prevWordCodePointArrays,
                                              int prevWordCodePointCountArray[],
                                              bool isBeginningOfSentenceArray[],
                                              int prevWordCodePointsOut[][MAX_WORD_LENGTH],
                                              int prevWordCodePointCountOut[],
                                              bool isBeginningOfSentenceOut[],
                                              int prevWordCount) {

            int prevWordCodePointCount[prevWordCount];
            for (int i = 0; i < prevWordCount; i++) {
                prevWordCodePointCount[i] = prevWordCodePointCountArray[i];
            }

            for (int i = 0; i < prevWordCount; ++i) {
                prevWordCodePointCountOut[i] = 0;
                isBeginningOfSentenceOut[i] = false;
                int *prevWord = &prevWordCodePointArrays[i][0];
                if (!prevWord) {
                    continue;
                }
                int prevWordLength = prevWordCodePointCount[i];
                if (prevWordLength > MAX_WORD_LENGTH) {
                    continue;
                }
                for (int j = 0; j < prevWordLength; j++) {
                    prevWordCodePointsOut[i][j] = prevWord[j];
                }
                prevWordCodePointCountOut[i] = prevWordLength;
                isBeginningOfSentenceOut[i] = isBeginningOfSentenceArray[i];
            }
        }
    };
}


#endif //DICTIONARY_FACILITATOR_DELEGATE_UTILS_H
