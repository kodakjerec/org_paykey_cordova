/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_ADDITIONAL_LOCALE_INFORMATION_H
#define LATINIME_ADDITIONAL_LOCALE_INFORMATION_H

#include <cstring>
#include <vector>
#include <map>

#include "defines.h"

namespace latinime {

    class AdditionalLocaleInformation {
    public:
        static int getAdditionalCharsSize(const std::map<int, std::vector<int>> *altProximitiesMap,
                                          const std::vector<int> *locale,
                                          const int primaryKey) {
            if (!altProximitiesMap)
                return AdditionalLocaleInformation::getAdditionalCharsSize(locale, primaryKey);

            return getAdditionalCharsSize(altProximitiesMap, primaryKey);
        }
        static const int * getAdditionalChars(const std::map<int, std::vector<int>> *altProximitiesMap,
                                              const std::vector<int> *locale,
                                              const int primaryKey) {
            if (!altProximitiesMap)
                return AdditionalLocaleInformation::getAdditionalChars(locale, primaryKey);

            return getAdditionalChars(altProximitiesMap, primaryKey);
        }
    private:
        static int getAdditionalCharsSize(const std::map<int, std::vector<int>> *altProximitiesMap, const int primaryKey) {
            auto search = altProximitiesMap->find(primaryKey);
            if (search != altProximitiesMap->end()) {
                return static_cast<int>(search->second.size());
            }

            return 0;
        }
        static const int * getAdditionalChars(const std::map<int, std::vector<int>> *altProximitiesMap, const int primaryKey) {
            auto search = altProximitiesMap->find(primaryKey);

            if (search != altProximitiesMap->end()) {
                return &search->second[0];
            }
            return nullptr;
        }
    private:
        DISALLOW_IMPLICIT_CONSTRUCTORS(AdditionalLocaleInformation);

        static const int LOCALE_EN_US_SIZE = 2;
        static const int LOCALE_EN_US[LOCALE_EN_US_SIZE];
        static const int LOCALE_CH_ZH_SIZE = 2;
        static const int LOCALE_CH_ZH[LOCALE_CH_ZH_SIZE];
        static const int LOCALE_ID_ID_SIZE = 2;
        static const int LOCALE_ID_ID[LOCALE_ID_ID_SIZE];
        static const int LOCALE_HE_IL_SIZE = 2;
        static const int LOCALE_HE_IL[LOCALE_HE_IL_SIZE];
        static const int LOCALE_RO_RO_SIZE = 2;
        static const int LOCALE_RO_RO[LOCALE_RO_RO_SIZE];
        static const int LOCALE_SK_SK_SIZE = 2;
        static const int LOCALE_SK_SK[LOCALE_SK_SK_SIZE];
        static const int LOCALE_PL_PL_SIZE = 2;
        static const int LOCALE_PL_PL[LOCALE_PL_PL_SIZE];
        static const int LOCALE_RU_RU_SIZE = 2;
        static const int LOCALE_RU_RU[LOCALE_RU_RU_SIZE];
        static const int LOCALE_TR_TR_SIZE = 2;
        static const int LOCALE_TR_TR[LOCALE_TR_TR_SIZE];
        static const int LOCALE_ES_ES_SIZE = 2;
        static const int LOCALE_ES_ES[LOCALE_ES_ES_SIZE];


        static const int EN_US_ADDITIONAL_A_SIZE = 4;
        static const int EN_US_ADDITIONAL_A[];
        static const int EN_US_ADDITIONAL_E_SIZE = 4;
        static const int EN_US_ADDITIONAL_E[];
        static const int EN_US_ADDITIONAL_I_SIZE = 4;
        static const int EN_US_ADDITIONAL_I[];
        static const int EN_US_ADDITIONAL_O_SIZE = 4;
        static const int EN_US_ADDITIONAL_O[];
        static const int EN_US_ADDITIONAL_U_SIZE = 4;
        static const int EN_US_ADDITIONAL_U[];

        static const int RO_RO_ADDITIONAL_A_SIZE = 6;
        static const int RO_RO_ADDITIONAL_A[];
        static const int RO_RO_ADDITIONAL_E_SIZE = 4;
        static const int RO_RO_ADDITIONAL_E[];
        static const int RO_RO_ADDITIONAL_I_SIZE = 5;
        static const int RO_RO_ADDITIONAL_I[];
        static const int RO_RO_ADDITIONAL_O_SIZE = 4;
        static const int RO_RO_ADDITIONAL_O[];
        static const int RO_RO_ADDITIONAL_U_SIZE = 4;
        static const int RO_RO_ADDITIONAL_U[];
        static const int RO_RO_ADDITIONAL_S_SIZE = 2;
        static const int RO_RO_ADDITIONAL_S[];
        static const int RO_RO_ADDITIONAL_T_SIZE = 1;
        static const int RO_RO_ADDITIONAL_T[];

        static const int RO_A_with_Circumflex = 0x00E2; // “â”
        static const int RO_A_with_Breve = 0x0103; // “ă”
        static const int RO_I_with_Circumflex = 0x00EE; // “î”
        static const int RO_S_with_Cedilla = 0x015F; // “ş”
        static const int RO_S_with_Comma_Below = 0x0219; // “ş”
        static const int RO_T_with_Comma_Below = 0x021B; // “ț”

        static const int SK_SK_ADDITIONAL_A_SIZE = 6;
        static const int SK_SK_ADDITIONAL_A[];
        static const int SK_SK_ADDITIONAL_E_SIZE = 5;
        static const int SK_SK_ADDITIONAL_E[];
        static const int SK_SK_ADDITIONAL_I_SIZE = 5;
        static const int SK_SK_ADDITIONAL_I[];
        static const int SK_SK_ADDITIONAL_O_SIZE = 6;
        static const int SK_SK_ADDITIONAL_O[];
        static const int SK_SK_ADDITIONAL_U_SIZE = 5;
        static const int SK_SK_ADDITIONAL_U[];
        static const int SK_SK_ADDITIONAL_C_SIZE = 1;
        static const int SK_SK_ADDITIONAL_C[];
        static const int SK_SK_ADDITIONAL_D_SIZE = 1;
        static const int SK_SK_ADDITIONAL_D[];
        static const int SK_SK_ADDITIONAL_L_SIZE = 2;
        static const int SK_SK_ADDITIONAL_L[];
        static const int SK_SK_ADDITIONAL_N_SIZE = 1;
        static const int SK_SK_ADDITIONAL_N[];
        static const int SK_SK_ADDITIONAL_R_SIZE = 1;
        static const int SK_SK_ADDITIONAL_R[];
        static const int SK_SK_ADDITIONAL_S_SIZE = 1;
        static const int SK_SK_ADDITIONAL_S[];
        static const int SK_SK_ADDITIONAL_T_SIZE = 1;
        static const int SK_SK_ADDITIONAL_T[];
        static const int SK_SK_ADDITIONAL_Y_SIZE = 1;
        static const int SK_SK_ADDITIONAL_Y[];
        static const int SK_SK_ADDITIONAL_Z_SIZE = 1;
        static const int SK_SK_ADDITIONAL_Z[];

        static const int SK_A_with_Acute = 0x00E1; // “á”
        static const int SK_A_with_Diaeresis = 0x00E4; // “ä”
        static const int SK_C_with_Caron = 0x010E; // “č”
        static const int SK_D_with_Caron = 0x010F; // “ď”
        static const int SK_E_with_Acute = 0x00E9; // “é”
        static const int SK_I_with_Acute = 0x00ED; // “í”
        static const int SK_L_with_Acute = 0x013A; // “ĺ”
        static const int SK_L_with_Caron = 0x013E; // “ľ”
        static const int SK_N_with_Caron = 0x0148; // “ň”
        static const int SK_O_with_Acute = 0x00F3; // “ó”
        static const int SK_O_with_Circumflex = 0x00F4; // “ô”
        static const int SK_R_with_Acute = 0x0155; // “ŕ”
        static const int SK_S_with_Caron = 0x0161; // “š”
        static const int SK_T_with_Caron = 0x0165; // “ť”
        static const int SK_U_with_Acute = 0x00FA; // “ú”
        static const int SK_Y_with_Acute = 0x00FD; // “ý”
        static const int SK_Z_with_Caron = 0x017E; // “ž”

        static const int CH_ZU_ADDITIONAL_TONES_SIZE = 3;
        static const int CH_ZU_ADDITIONAL_ACUTE[];
        static const int CH_ZU_ADDITIONAL_CARON[];
        static const int CH_ZU_ADDITIONAL_GRAVE[];
        static const int CH_ZU_ADDITIONAL_DOT[];

        static const int CH_Caron = 0x02C7; // “ˇ”
        static const int CH_Dot_Above = 0x02D9; // “˙”
        static const int CH_Acute_Accent = 0x02CA; // “ˊ”
        static const int CH_Grave_Accent = 0x02CB; // “ˋ”

        static const int HE_IL_ADDITIONAL_ALEF_SIZE = 2;
        static const int HE_IL_ADDITIONAL_ALEF[];
        static const int HE_IL_ADDITIONAL_AYIN_SIZE = 1;
        static const int HE_IL_ADDITIONAL_AYIN[];
        static const int HE_IL_ADDITIONAL_TAV_SIZE = 1;
        static const int HE_IL_ADDITIONAL_TAV[];
        static const int HE_IL_ADDITIONAL_TET_SIZE = 1;
        static const int HE_IL_ADDITIONAL_TET[];
        static const int HE_IL_ADDITIONAL_QOF_SIZE = 2;
        static const int HE_IL_ADDITIONAL_QOF[];
        static const int HE_IL_ADDITIONAL_KAF_SIZE = 2;
        static const int HE_IL_ADDITIONAL_KAF[];
        static const int HE_IL_ADDITIONAL_FINAL_KAF_SIZE = 2;
        static const int HE_IL_ADDITIONAL_FINAL_KAF[];
        static const int HE_IL_ADDITIONAL_SHIN_SIZE = 1;
        static const int HE_IL_ADDITIONAL_SHIN[];
        static const int HE_IL_ADDITIONAL_SAMEKH_SIZE = 1;
        static const int HE_IL_ADDITIONAL_SAMEKH[];
        static const int HE_IL_ADDITIONAL_VAV_SIZE = 1;
        static const int HE_IL_ADDITIONAL_VAV[];
        static const int HE_IL_ADDITIONAL_BET_SIZE = 1;
        static const int HE_IL_ADDITIONAL_BET[];
        static const int HE_IL_ADDITIONAL_HET_SIZE = 2;
        static const int HE_IL_ADDITIONAL_HET[];
        static const int HE_IL_ADDITIONAL_HE_SIZE = 1;
        static const int HE_IL_ADDITIONAL_HE[];
        static const int HE_IL_ADDITIONAL_APOSTROPHE_SIZE = 1;
        static const int HE_IL_ADDITIONAL_APOSTROPHE[];
        static const int HE_IL_ADDITIONAL_QUOTATIONMARKS_SIZE = 1;
        static const int HE_IL_ADDITIONAL_QUOTATIONMARKS[];

        static const int HE_ALEF = 0x05D0;
        static const int HE_BET = 0x05D1;
        static const int HE_GIMEL = 0x05D2;
        static const int HE_DALET = 0x05D3;
        static const int HE_HE = 0x05D4;
        static const int HE_VAV = 0x05D5;
        static const int HE_ZAYIN = 0x05D6;
        static const int HE_HET = 0x05D7;
        static const int HE_TET = 0x05D8;
        static const int HE_YOD = 0x05D9;
        static const int HE_FINAL_KAF = 0x05DA;
        static const int HE_KAF = 0x05DB;
        static const int HE_LAMED = 0x05DC;
        static const int HE_FINAL_MEM = 0x05DD;
        static const int HE_MEM = 0x05DE;
        static const int HE_FINAL_NUN = 0x05DF;
        static const int HE_NUN = 0x05E0;
        static const int HE_SAMEKH = 0x05E1;
        static const int HE_AYIN = 0x05E2;
        static const int HE_FINAL_PE = 0x05E3;
        static const int HE_PE = 0x05E4;
        static const int HE_FINAL_TSADI = 0x05E5;
        static const int HE_TSADI = 0x05E6;
        static const int HE_QOF = 0x05E7;
        static const int HE_RESH = 0x05E8;
        static const int HE_SHIN = 0x05E9;
        static const int HE_TAV = 0x05EA;
        static const int HE_GERASH = 0x05F3;
        static const int HE_GERSHAYIM = 0x05F4;
        static const int EN_APOSTROPHE = 0x27;
        static const int EN_QUOTATIONMARKS = 0x22;

        static const int ES_ES_ADDITIONAL_A_SIZE = 5;
        static const int ES_ES_ADDITIONAL_A[];
       static const int ES_ES_ADDITIONAL_E_SIZE = 5;
        static const int ES_ES_ADDITIONAL_E[];
       static const int ES_ES_ADDITIONAL_I_SIZE = 5;
        static const int ES_ES_ADDITIONAL_I[];
       static const int ES_ES_ADDITIONAL_N_SIZE = 1;
        static const int ES_ES_ADDITIONAL_N[];
       static const int ES_ES_ADDITIONAL_O_SIZE = 5;
        static const int ES_ES_ADDITIONAL_O[];
       static const int ES_ES_ADDITIONAL_U_SIZE = 6;
        static const int ES_ES_ADDITIONAL_U[];

        static const int ES_A_with_Acute = 0x00E1; // “á”
        static const int ES_E_with_Acute = 0x00E9; // “é”
        static const int ES_I_with_Acute = 0x00ED; // “í”
        static const int ES_N_with_Tilde = 0x00F1; // “ñ”
        static const int ES_O_with_Acute = 0x00F3; // “ó”
        static const int ES_U_with_Acute = 0x00FA; // “ú”
        static const int ES_U_with_Diaeresis = 0x00FC; // “ü”

        static const int PL_PL_ADDITIONAL_A_SIZE = 5;
        static const int PL_PL_ADDITIONAL_A[];
        static const int PL_PL_ADDITIONAL_C_SIZE = 1;
        static const int PL_PL_ADDITIONAL_C[];
        static const int PL_PL_ADDITIONAL_E_SIZE = 5;
        static const int PL_PL_ADDITIONAL_E[];
        static const int PL_PL_ADDITIONAL_I_SIZE = 5;
        static const int PL_PL_ADDITIONAL_I[];
        static const int PL_PL_ADDITIONAL_U_SIZE = 4;
        static const int PL_PL_ADDITIONAL_U[];
        static const int PL_PL_ADDITIONAL_L_SIZE = 1;
        static const int PL_PL_ADDITIONAL_L[];
        static const int PL_PL_ADDITIONAL_N_SIZE = 1;
        static const int PL_PL_ADDITIONAL_N[];
        static const int PL_PL_ADDITIONAL_O_SIZE = 5;
        static const int PL_PL_ADDITIONAL_O[];
        static const int PL_PL_ADDITIONAL_S_SIZE = 1;
        static const int PL_PL_ADDITIONAL_S[];
        static const int PL_PL_ADDITIONAL_Z_SIZE = 2;
        static const int PL_PL_ADDITIONAL_Z[];

        static const int PL_A_with_Ogonek = 0x0105; // “á”
        static const int PL_A_with_Acute = 0x0107; // “ć”
        static const int PL_E_with_Ogonek = 0x0119; // “ę”
        static const int PL_I_with_Circumflex = 0x0119; // “î”
        static const int PL_L_with_Stroke = 0x0142; // “ł”
        static const int PL_N_with_Acute = 0x0144; // “ń”
        static const int PL_O_with_Acute = 0x00F3; // “ó”
        static const int PL_S_with_Acute = 0x015B; // “ś”
        static const int PL_Z_with_Acute = 0x017A; // “ź”
        static const int PL_Z_with_Dot_Above = 0x017C; // “ź”

        static const int TR_TR_ADDITIONAL_A_SIZE = 5;
        static const int TR_TR_ADDITIONAL_A[];
        static const int TR_TR_ADDITIONAL_C_SIZE = 1;
        static const int TR_TR_ADDITIONAL_C[];
        static const int TR_TR_ADDITIONAL_E_SIZE = 5;
        static const int TR_TR_ADDITIONAL_E[];
        static const int TR_TR_ADDITIONAL_G_SIZE = 1;
        static const int TR_TR_ADDITIONAL_G[];
        static const int TR_TR_ADDITIONAL_I_SIZE = 7;
        static const int TR_TR_ADDITIONAL_I[];
        static const int TR_TR_ADDITIONAL_O_SIZE = 5;
        static const int TR_TR_ADDITIONAL_O[];
        static const int TR_TR_ADDITIONAL_S_SIZE = 1;
        static const int TR_TR_ADDITIONAL_S[];
        static const int TR_TR_ADDITIONAL_U_SIZE = 6;
        static const int TR_TR_ADDITIONAL_U[];

        static const int TR_A_with_Circumflex = 0x00E2; // “â”
        static const int TR_C_with_Cedilla = 0x00E7; // “ç”
        static const int TR_G_with_Breve = 0x011F; // “ğ”
        static const int TR_I_with_Dot_Above = 0x0131; // “ı”
        static const int TR_I_with_Dot_Above_Capital = 0x0130; // “İ”
        static const int TR_I_with_Circumflex = 0x00EE; // “î”
        static const int TR_O_with_Diaeresis = 0x00F6; // “ö”
        static const int TR_S_with_Cedilla = 0x015F; // “ş”
        static const int TR_U_with_Diaeresis = 0x00FC; // “ü”
        static const int TR_U_with_Circumflex = 0x00FB; // “û”

        static const int RU_RU_ADDITIONAL_A_SIZE = 2;
        static const int RU_RU_ADDITIONAL_A[];
        static const int RU_RU_ADDITIONAL_O_SIZE = 2;
        static const int RU_RU_ADDITIONAL_O[];
        static const int RU_RU_ADDITIONAL_Io_SIZE = 1;
        static const int RU_RU_ADDITIONAL_Io[];
        static const int RU_RU_ADDITIONAL_E_SIZE = 1;
        static const int RU_RU_ADDITIONAL_E[];
        static const int RU_RU_ADDITIONAL_Ie_SIZE = 4;
        static const int RU_RU_ADDITIONAL_Ie[];
        static const int RU_RU_ADDITIONAL_I_SIZE = 4;
        static const int RU_RU_ADDITIONAL_I[];
        static const int RU_RU_ADDITIONAL_Short_I_SIZE = 3;
        static const int RU_RU_ADDITIONAL_Short_I[];
        static const int RU_RU_ADDITIONAL_RU_Sha_SIZE = 1;
        static const int RU_RU_ADDITIONAL_RU_Sha[];
        static const int RU_RU_ADDITIONAL_RU_Shcha_SIZE = 1;
        static const int RU_RU_ADDITIONAL_RU_Shcha[];

        static const int RU_A = 0x0430;         // “а”
        static const int RU_Be = 0x0431;        // “б”
        static const int RU_Ve = 0x0432;        // “в”
        static const int RU_Ghe = 0x0433;       // “г”
        static const int RU_De = 0x0434;        // “д”
        static const int RU_Ie = 0x0435;        // “е”
        static const int RU_Zhe = 0x0436;       // “ж”
        static const int RU_Ze = 0x0437;        // “з”
        static const int RU_I = 0x0438;         // “и”
        static const int RU_Short_I = 0x0439;   // “й”
        static const int RU_Ka = 0x043A;        // “к”
        static const int RU_El = 0x043B;        // “л”
        static const int RU_Em = 0x043C;        // “м”
        static const int RU_En = 0x043D;        // “н”
        static const int RU_O = 0x043E;         // “о”
        static const int RU_Pe = 0x043F;        // “п”
        static const int RU_Er = 0x0440;        // “р”
        static const int RU_Es = 0x0441;        // “с”
        static const int RU_Te = 0x0442;        // “т”
        static const int RU_U = 0x0443;         // “у”
        static const int RU_Ef = 0x0444;        // “ф”
        static const int RU_Ha = 0x0445;        // “х”
        static const int RU_Tse = 0x0446;       // “ц”
        static const int RU_Che = 0x0447;       // “ч”
        static const int RU_Sha = 0x0448;       // “ш”
        static const int RU_Shcha = 0x0449;     // “щ”
        static const int RU_Hard_Sign = 0x044A; // “ъ”
        static const int RU_Yeru = 0x044B;      // “ы”
        static const int RU_Soft_Sign = 0x044C; // “ь”
        static const int RU_E = 0x044D;         // “э”
        static const int RU_Yu = 0x044E;        // “ю”
        static const int RU_Ya = 0x044F;        // “я”
        static const int RU_Ie_with_Grave = 0x0450; // “ѐ”
        static const int RU_Io = 0x0451;        // “ё”



        AK_FORCE_INLINE static bool isEnLocale(const std::vector<int> *locale) {
            return isLocale(locale, LOCALE_EN_US, LOCALE_EN_US_SIZE);
        }

        AK_FORCE_INLINE static bool isInLocale(const std::vector<int> *locale) {
            return isLocale(locale, LOCALE_ID_ID, LOCALE_ID_ID_SIZE);
        }

    public:

        AK_FORCE_INLINE static bool isZhLocale(const std::vector<int> *locale) {
            return isLocale(locale, LOCALE_CH_ZH, LOCALE_CH_ZH_SIZE);
        }

        AK_FORCE_INLINE static bool isHeLocale(const std::vector<int> *locale) {
            return isLocale(locale, LOCALE_HE_IL, LOCALE_HE_IL_SIZE);
        }

        AK_FORCE_INLINE static bool isRoLocale(const std::vector<int> *locale) {
            return isLocale(locale, LOCALE_RO_RO, LOCALE_RO_RO_SIZE);
        }

        AK_FORCE_INLINE static bool isSkLocale(const std::vector<int> *locale) {
            return isLocale(locale, LOCALE_SK_SK, LOCALE_SK_SK_SIZE);
        }

        AK_FORCE_INLINE static bool isPlLocale(const std::vector<int> *locale) {
            return isLocale(locale, LOCALE_PL_PL, LOCALE_PL_PL_SIZE);
        }
        AK_FORCE_INLINE static bool isRuLocale(const std::vector<int> *locale) {
            return isLocale(locale, LOCALE_RU_RU, LOCALE_RU_RU_SIZE);
        }
        AK_FORCE_INLINE static bool isTrLocale(const std::vector<int> *locale) {
            return isLocale(locale, LOCALE_TR_TR, LOCALE_TR_TR_SIZE);
        }
        AK_FORCE_INLINE static bool isEsLocale(const std::vector<int> *locale) {
            return isLocale(locale, LOCALE_ES_ES, LOCALE_ES_ES_SIZE);
        }

        AK_FORCE_INLINE static bool isLocale(const std::vector<int> *locale, const int LOCALE_Name[], const int NCHARS) {
            if (locale->size() < NCHARS) {
                return false;
            }
            for (int i = 0; i < NCHARS; ++i) {
                if ((*locale)[i] != LOCALE_Name[i]) {
                    return false;
                }
            }
            return true;
        }


    public:
        static int getAdditionalCharsSize(const std::vector<int> *locale, const int c) {
            if (isEnLocale(locale) || isInLocale(locale)) {
                switch (c) {
                    case 'a':
                        return EN_US_ADDITIONAL_A_SIZE;
                    case 'e':
                        return EN_US_ADDITIONAL_E_SIZE;
                    case 'i':
                        return EN_US_ADDITIONAL_I_SIZE;
                    case 'o':
                        return EN_US_ADDITIONAL_O_SIZE;
                    case 'u':
                        return EN_US_ADDITIONAL_U_SIZE;
                    default:
                        return 0;
                }
            }

            if (isZhLocale(locale)) {
               /* if (isZhLocale(locale)) {
                    switch (c) {
                        case 0x02CA:
                        case 0x02C7:
                        case 0x02CB:
                        case 0x02D9:
                            return CH_ZU_ADDITIONAL_TONES_SIZE;
                    }
                }*/
               return 0;
            } else if (isHeLocale(locale)) {
                switch (c) {
                    case HE_ALEF:
                        return HE_IL_ADDITIONAL_ALEF_SIZE;
                    case HE_AYIN:
                        return HE_IL_ADDITIONAL_AYIN_SIZE;
                    case HE_TAV:
                        return HE_IL_ADDITIONAL_TAV_SIZE;
                    case HE_TET:
                        return HE_IL_ADDITIONAL_TET_SIZE;
                    case HE_QOF:
                        return HE_IL_ADDITIONAL_QOF_SIZE;
                    case HE_KAF:
                        return HE_IL_ADDITIONAL_KAF_SIZE;
                    case HE_FINAL_KAF:
                        return HE_IL_ADDITIONAL_FINAL_KAF_SIZE;
                    case HE_SHIN:
                        return HE_IL_ADDITIONAL_SHIN_SIZE;
                    case HE_SAMEKH:
                        return HE_IL_ADDITIONAL_SAMEKH_SIZE;
                    case HE_VAV:
                        return HE_IL_ADDITIONAL_VAV_SIZE;
                    case HE_BET:
                        return HE_IL_ADDITIONAL_BET_SIZE;
                    case HE_HET:
                        return HE_IL_ADDITIONAL_HET_SIZE;
                    case HE_HE:
                        return HE_IL_ADDITIONAL_HE_SIZE;
                    case EN_APOSTROPHE:
                        return HE_IL_ADDITIONAL_APOSTROPHE_SIZE;
                    case EN_QUOTATIONMARKS:
                        return HE_IL_ADDITIONAL_QUOTATIONMARKS_SIZE;
                    default:
                        return 0;
                }
            } else if (isRoLocale(locale)) {
                switch (c) {
                    case 'a':
                        return RO_RO_ADDITIONAL_A_SIZE;
                    case 'e':
                        return RO_RO_ADDITIONAL_E_SIZE;
                    case 'i':
                        return RO_RO_ADDITIONAL_I_SIZE;
                    case 'o':
                        return RO_RO_ADDITIONAL_O_SIZE;
                    case 'u':
                        return RO_RO_ADDITIONAL_U_SIZE;
                    case 's':
                        return RO_RO_ADDITIONAL_S_SIZE;
                    case 't':
                        return RO_RO_ADDITIONAL_T_SIZE;
                    default:
                        return 0;
                }
            } else if (isSkLocale(locale)) {
                switch (c) {
                    case 'a':
                        return SK_SK_ADDITIONAL_A_SIZE;
                    case 'e':
                        return SK_SK_ADDITIONAL_E_SIZE;
                    case 'i':
                        return SK_SK_ADDITIONAL_I_SIZE;
                    case 'o':
                        return SK_SK_ADDITIONAL_O_SIZE;
                    case 'u':
                        return SK_SK_ADDITIONAL_U_SIZE;
                    case 'c':
                        return SK_SK_ADDITIONAL_C_SIZE;
                    case 'd':
                        return SK_SK_ADDITIONAL_D_SIZE;
                    case 'l':
                        return SK_SK_ADDITIONAL_L_SIZE;
                    case 'n':
                        return SK_SK_ADDITIONAL_N_SIZE;
                    case 'r':
                        return SK_SK_ADDITIONAL_R_SIZE;
                    case 's':
                        return SK_SK_ADDITIONAL_S_SIZE;
                    case 't':
                        return SK_SK_ADDITIONAL_T_SIZE;
                    case 'y':
                        return SK_SK_ADDITIONAL_Y_SIZE;
                    case 'z':
                        return SK_SK_ADDITIONAL_Z_SIZE;
                    default:
                        return 0;
                }
            } else if (isPlLocale(locale)) {
                switch (c) {
                    case 'a':
                        return PL_PL_ADDITIONAL_A_SIZE;
                    case 'c':
                        return PL_PL_ADDITIONAL_C_SIZE;
                    case 'e':
                        return PL_PL_ADDITIONAL_E_SIZE;
                    case 'i':
                        return PL_PL_ADDITIONAL_I_SIZE;
                    case 'l':
                        return PL_PL_ADDITIONAL_L_SIZE;
                    case 'n':
                        return PL_PL_ADDITIONAL_N_SIZE;
                    case 'o':
                        return PL_PL_ADDITIONAL_O_SIZE;
                    case 's':
                        return PL_PL_ADDITIONAL_S_SIZE;
                    case 'u':
                        return PL_PL_ADDITIONAL_U_SIZE;
                    case 'z':
                        return PL_PL_ADDITIONAL_Z_SIZE;
                    default:
                        return 0;
                }
            } else if (isRuLocale(locale)) {
                switch (c) {
                case RU_A:
                    return RU_RU_ADDITIONAL_A_SIZE;
                case RU_O:
                    return RU_RU_ADDITIONAL_O_SIZE;
                case RU_Io:
                    return RU_RU_ADDITIONAL_Io_SIZE;
                case RU_E:
                    return RU_RU_ADDITIONAL_E_SIZE;
                case RU_Ie:
                    return RU_RU_ADDITIONAL_Ie_SIZE;
                case RU_I:
                    return RU_RU_ADDITIONAL_I_SIZE;
                case RU_Short_I:
                    return RU_RU_ADDITIONAL_Short_I_SIZE;
                case RU_Sha:
                    return RU_RU_ADDITIONAL_RU_Sha_SIZE;
                case RU_Shcha:
                    return RU_RU_ADDITIONAL_RU_Shcha_SIZE;


                default:
                        return 0;
                }
            }else if (isTrLocale(locale)) {
                switch (c) {
                    case 'a':
                        return TR_TR_ADDITIONAL_A_SIZE;
                    case 'c':
                        return TR_TR_ADDITIONAL_C_SIZE;
                    case 'e':
                        return TR_TR_ADDITIONAL_E_SIZE;
                    case 'g':
                        return TR_TR_ADDITIONAL_G_SIZE;
                    case 'i':
                        return TR_TR_ADDITIONAL_I_SIZE;
                    case 'o':
                        return TR_TR_ADDITIONAL_O_SIZE;
                    case 's':
                        return TR_TR_ADDITIONAL_S_SIZE;
                    case 'u':
                        return TR_TR_ADDITIONAL_U_SIZE;
                    default:
                        return 0;
                }
            }else if (isEsLocale(locale)) {
                switch (c) {
                    case 'a':
                        return ES_ES_ADDITIONAL_A_SIZE;
                    case 'e':
                        return ES_ES_ADDITIONAL_E_SIZE;
                    case 'i':
                        return ES_ES_ADDITIONAL_I_SIZE;
                    case 'n':
                        return ES_ES_ADDITIONAL_N_SIZE;
                    case 'o':
                        return ES_ES_ADDITIONAL_O_SIZE;
                    case 'u':
                        return ES_ES_ADDITIONAL_U_SIZE;
                    default:
                        return 0;
                }
            }
            return 0;
        }

        static const int *getAdditionalChars(const std::vector<int> *locale, const int c) {
            if (isEnLocale(locale) || isInLocale(locale)) {
                switch (c) {
                    case 'a':
                        return EN_US_ADDITIONAL_A;
                    case 'e':
                        return EN_US_ADDITIONAL_E;
                    case 'i':
                        return EN_US_ADDITIONAL_I;
                    case 'o':
                        return EN_US_ADDITIONAL_O;
                    case 'u':
                        return EN_US_ADDITIONAL_U;
                    default:
                        return nullptr;
                }
            }
            if (isZhLocale(locale)) {
                switch (c) {
                    case 0x02CA:
                        return CH_ZU_ADDITIONAL_ACUTE;
                    case 0x02C7:
                        return CH_ZU_ADDITIONAL_CARON;
                    case 0x02CB:
                        return CH_ZU_ADDITIONAL_GRAVE;
                    case 0x02D9:
                        return CH_ZU_ADDITIONAL_DOT;
                    default:
                        return nullptr;
                }
            } else if (isHeLocale(locale)) {
                switch (c) {
                    case HE_ALEF:
                        return HE_IL_ADDITIONAL_ALEF;
                    case HE_AYIN:
                        return HE_IL_ADDITIONAL_AYIN;
                    case HE_TAV:
                        return HE_IL_ADDITIONAL_TAV;
                    case HE_TET:
                        return HE_IL_ADDITIONAL_TET;
                    case HE_QOF:
                        return HE_IL_ADDITIONAL_QOF;
                    case HE_KAF:
                        return HE_IL_ADDITIONAL_KAF;
                    case HE_FINAL_KAF:
                        return HE_IL_ADDITIONAL_FINAL_KAF;
                    case HE_SHIN:
                        return HE_IL_ADDITIONAL_SHIN;
                    case HE_SAMEKH:
                        return HE_IL_ADDITIONAL_SAMEKH;
                    case HE_VAV:
                        return HE_IL_ADDITIONAL_VAV;
                    case HE_BET:
                        return HE_IL_ADDITIONAL_BET;
                    case HE_HET:
                        return HE_IL_ADDITIONAL_HET;
                    case HE_HE:
                        return HE_IL_ADDITIONAL_HE;
                    case EN_APOSTROPHE:
                        return HE_IL_ADDITIONAL_APOSTROPHE;
                    case EN_QUOTATIONMARKS:
                        return HE_IL_ADDITIONAL_QUOTATIONMARKS;
                    default:
                        return nullptr;
                }
            } else if (isRoLocale(locale)) {
                switch (c) {
                    case 'a':
                        return RO_RO_ADDITIONAL_A;
                    case 'e':
                        return RO_RO_ADDITIONAL_E;
                    case 'i':
                        return RO_RO_ADDITIONAL_I;
                    case 'o':
                        return RO_RO_ADDITIONAL_O;
                    case 'u':
                        return RO_RO_ADDITIONAL_U;
                    case 's':
                        return RO_RO_ADDITIONAL_S;
                    case 't':
                        return RO_RO_ADDITIONAL_T;
                    default:
                        return nullptr;
                }
            } else if (isSkLocale(locale)) {
                switch (c) {
                    case 'a':
                        return SK_SK_ADDITIONAL_A;
                    case 'e':
                        return SK_SK_ADDITIONAL_E;
                    case 'i':
                        return SK_SK_ADDITIONAL_I;
                    case 'o':
                        return SK_SK_ADDITIONAL_O;
                    case 'u':
                        return SK_SK_ADDITIONAL_U;
                    case 'c':
                        return SK_SK_ADDITIONAL_C;
                    case 'd':
                        return SK_SK_ADDITIONAL_D;
                    case 'l':
                        return SK_SK_ADDITIONAL_L;
                    case 'n':
                        return SK_SK_ADDITIONAL_N;
                    case 'r':
                        return SK_SK_ADDITIONAL_R;
                    case 's':
                        return SK_SK_ADDITIONAL_S;
                    case 't':
                        return SK_SK_ADDITIONAL_T;
                    case 'y':
                        return SK_SK_ADDITIONAL_Y;
                    case 'z':
                        return SK_SK_ADDITIONAL_Z;
                    default:
                        return nullptr;
                }
            } else if (isPlLocale(locale)) {
                switch (c) {
                case 'a':
                    return PL_PL_ADDITIONAL_A;
                case 'c':
                    return PL_PL_ADDITIONAL_C;
                case 'e':
                    return PL_PL_ADDITIONAL_E;
                case 'i':
                    return PL_PL_ADDITIONAL_I;
                case 'l':
                    return PL_PL_ADDITIONAL_L;
                case 'n':
                    return PL_PL_ADDITIONAL_N;
                case 'o':
                    return PL_PL_ADDITIONAL_O;
                case 's':
                    return PL_PL_ADDITIONAL_S;
                case 'u':
                    return PL_PL_ADDITIONAL_U;
                case 'z':
                    return PL_PL_ADDITIONAL_Z;
                default:
                    return nullptr;
                }
            } else if (isRuLocale(locale)) {
                switch (c) {
                case RU_A:
                    return RU_RU_ADDITIONAL_A;
                case RU_O:
                    return RU_RU_ADDITIONAL_O;
                case RU_Io:
                    return RU_RU_ADDITIONAL_Io;
                case RU_E:
                    return RU_RU_ADDITIONAL_E;
                case RU_Ie:
                    return RU_RU_ADDITIONAL_Ie;
                case RU_I:
                    return RU_RU_ADDITIONAL_I;
                case RU_Short_I:
                    return RU_RU_ADDITIONAL_Short_I;
                case RU_Sha:
                    return RU_RU_ADDITIONAL_RU_Sha;
                case RU_Shcha:
                    return RU_RU_ADDITIONAL_RU_Shcha;

                default:
                    return nullptr;
                }
            }else if (isTrLocale(locale)) {
                switch (c) {
                case 'a':
                    return TR_TR_ADDITIONAL_A;
                case 'c':
                    return TR_TR_ADDITIONAL_C;
                case 'e':
                    return TR_TR_ADDITIONAL_E;
                case 'g':
                    return TR_TR_ADDITIONAL_G;
                case 'i':
                    return TR_TR_ADDITIONAL_I;
                case 'o':
                    return TR_TR_ADDITIONAL_O;
                case 's':
                    return TR_TR_ADDITIONAL_S;
                case 'u':
                    return TR_TR_ADDITIONAL_U;
                default:
                    return nullptr;
                }
            }else if (isEsLocale(locale)) {
                switch (c) {
                case 'a':
                    return ES_ES_ADDITIONAL_A;
                case 'e':
                    return ES_ES_ADDITIONAL_E;
                case 'i':
                    return ES_ES_ADDITIONAL_I;
                case 'n':
                    return ES_ES_ADDITIONAL_N;
                case 'o':
                    return ES_ES_ADDITIONAL_O;
                case 'u':
                    return ES_ES_ADDITIONAL_U;
                default:
                    return nullptr;
                }
            }

            return nullptr;
        }

        AK_FORCE_INLINE static bool filterByLocale(const std::vector<int> *locale, int codepoint) {
            if (AdditionalLocaleInformation::isZhLocale(locale)) {
                if (codepoint >= 0x4e00 && codepoint <= 0x9fef)
                    return true;
                if (codepoint >= 0x3000 && codepoint <= 0x303f)
                    return true;
                if (codepoint >= 0x2600 && codepoint <= 0x26ff)
                    return true;
                if (codepoint >= 0x1f200 && codepoint <= 0x1f2ff)
                    return true;
                if (codepoint >= 0x3300 && codepoint <= 0x33ff)
                    return true;
                if (codepoint >= 0x3200 && codepoint <= 0x32ff)
                    return true;
                if (codepoint >= 0xF900 && codepoint <= 0xFAFF)
                    return true;
                if (codepoint >= 0x2200 && codepoint <= 0x23ff)
                    return true;
                if (codepoint >= 0x2700 && codepoint <= 0x27BF)
                    return true;
                if (codepoint >= 0x25a0 && codepoint <= 0x25ff)
                    return true;
                if (codepoint >= 0x0080 && codepoint <= 0x00ff)
                    return true;
                if (codepoint >= 0x1f300 && codepoint <= 0x1f5ff)
                    return true;
                if (codepoint >= 0xff00 && codepoint <= 0xffef)
                    return true;
                if (codepoint >= 0xe000 && codepoint <= 0xf8ff)
                    return true;
                if (codepoint >= 0x20A0 && codepoint <= 0x20CF)
                    return true;

            }
            return false;
        }
    };
}// namespace latinime
#endif // LATINIME_ADDITIONAL_LOCALE_INFORMATION_H
