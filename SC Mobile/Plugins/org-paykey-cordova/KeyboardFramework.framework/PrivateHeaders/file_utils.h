/*
 * Copyright (C) 2013, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_FILE_UTILS_H
#define LATINIME_FILE_UTILS_H

#include "defines.h"

namespace latinime {

class FileUtils {
 public:
    // Returns -1 on error.
    static int getFileSize(const char *filePath);

    static bool existsDir(const char *dirPath);

    static bool existsFile(const char *const &name);

    // Remove a directory and all files in the directory.
    static bool removeDirAndFiles(const char *dirPath);

    static int getFilePathWithSuffixBufSize(const char *filePath, const char *suffix);

    static void getFilePathWithSuffix(const char *filePath, const char *suffix,
                                      int filePathBufSize, char *outFilePath);

    static int getFilePathBufSize(const char *dirPath, const char *fileName);

    static void getFilePath(const char *dirPath, const char *fileName,
                            int filePathBufSize, char *outFilePath);

    // Returns whether the filePath have the suffix.
    static bool getFilePathWithoutSuffix(const char *filePath, const char *suffix,
                                         int dirPathBufSize, char *outDirPath);

    static void getDirPath(const char *filePath, int dirPathBufSize,
            char *outDirPath);

    static void getBasename(const char *filePath, int outNameBufSize,
            char *outName);

    static bool deleteFileOrDir(const char *path);

private:
    DISALLOW_IMPLICIT_CONSTRUCTORS(FileUtils);

    static bool removeDirAndFiles(const char *dirPath, int maxTries);
};
} // namespace latinime
#endif /* LATINIME_FILE_UTILS_H */
