/*
 *  improbability.h
 *
 *  Created by paul on 31/12/2018.
 *  Copyright © 2017, 2018 PayKey. All rights reserved.
 *
 */

#ifndef LATINIME_IMPROBABILITY_H
#define LATINIME_IMPROBABILITY_H

#include <scoring_params.h>

namespace latinime {

class Improbability {
 public:
    Improbability(float languageImprobability, float languagePOSImprobability);

    float getLanguageImprobability();
    float getLanguagePOSImprobability();

private:
    const float mLanguageImprobability;
    const float mLanguagePOSImprobability;

};
} // namespace latinime
#endif // LATINIME_WEIGHTING_H
