/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_ZH_TYPING_WEIGHTING_H
#define LATINIME_ZH_TYPING_WEIGHTING_H

#include <typing_weighting.h>
#include "defines.h"
#include "dic_node_utils.h"
#include "error_type_utils.h"
#include "touch_position_correction_utils.h"
#include "weighting.h"
#include "dic_traverse_session.h"
#include "scoring_params.h"
#include "char_utils.h"
#include "zhuyin_utils.h"

namespace latinime {

    class DicNode;

    struct DicNode_InputStateG;

    class MultiBigramMap;

    class TypingZhuyinWeighting : public TypingWeighting {
    public:
        static const TypingZhuyinWeighting *getInstance() { return &sInstance; }

        Improbability getBigramNodeImprobability(
                const DictionaryStructureWithBufferPolicy *const dictionaryStructurePolicy,
                const DicNode *const dicNode, MultiBigramMap *const multiBigramMap) const override {
            return DicNodeUtils::getBigramNodeImprobability(
                    dictionaryStructurePolicy,
                    dicNode, multiBigramMap, true);
        }

    protected:

        float getTerminalSpatialCost(const DicTraverseSession *const traverseSession,
                                     const DicNode *const parentDicNode,
                                     const DicNode *const dicNode) const override {
            float cost = 0.0f;
            if (dicNode->hasMultipleWords()) {
                cost += ScoringParams::HAS_MULTI_WORD_TERMINAL_COST;
            }
            if (dicNode->getProximityCorrectionCount() > 0) {
                cost += ScoringParams::HAS_PROXIMITY_TERMINAL_COST;
            }
            if (dicNode->getEditCorrectionCount() > 0) {
                cost += ScoringParams::HAS_EDIT_CORRECTION_TERMINAL_COST;
            }

            int initialCount = 0;
            for (int i = 0; i < traverseSession->getInputSize(); ++i) {
                if (ZhuyinUtils::isInitial(traverseSession->getProximityInfoState(0)->getPrimaryCodePointAt(i), false))
                    initialCount++;
            }
            int terminalInitialCount = 0;
            const int *terminalWord = dicNode->getOutputWordBuf();
            for (int i = 0; i < dicNode->getTotalNodeCodePointCount(); ++i) {
                if (ZhuyinUtils::isInitial(terminalWord[i], false))
                    terminalInitialCount++;
            }

            //ALEX: Why do we only penalize in one way ?
            int initialDiff = initialCount > terminalInitialCount ? initialCount - terminalInitialCount : 0;
            return cost + initialDiff * ScoringParams::DISTANCE_WEIGHT_LENGTH * 2;
        }

        float getMatchedCost(const DicTraverseSession *const traverseSession, const DicNode *const parentDicNode,
                             const DicNode *const dicNode, DicNode_InputStateG *inputStateG) const override {
            int index = dicNode->getInputIndex(0);
            int inputCodePoint = traverseSession->getProximityInfoState(0)->getPrimaryCodePointAt(index);
            int dicCodePoint = dicNode->getNodeCodePoint();

            if (ZhuyinUtils::isInitial(inputCodePoint, false) && ZhuyinUtils::isInitial(dicCodePoint, false)) {
                if (dicCodePoint == inputCodePoint)
                    return 0;

                return index == 0 ? ScoringParams::DISTANCE_WEIGHT_LENGTH * 3 :
                       ScoringParams::DISTANCE_WEIGHT_LENGTH * 2;
            }
            float multi = 1;
            if (ZhuyinUtils::isInitial(inputCodePoint, false)) {
                multi += 0.2;
            }

            if (ZhuyinUtils::isInitial(dicCodePoint, false)) {
                multi += 0.2;
            }

            return TypingWeighting::getMatchedCost(traverseSession, parentDicNode, dicNode, inputStateG) * multi;
        }


        float getNewWordBigramLanguageCost(const DicTraverseSession *const traverseSession,
                                           const DicNode *const dicNode,
                                           MultiBigramMap *const multiBigramMap) const override {
            const WordAttributes wordAttributes = traverseSession->getDictionaryStructurePolicy()
                    ->getWordAttributesInContext(dicNode->getPrevWordIds(), dicNode->getWordId(), multiBigramMap);
            return DicNodeUtils::getImprobability(wordAttributes.getProbability()) *
                   ScoringParams::DISTANCE_WEIGHT_LANGUAGE;
        }

        float getSkipCost(const DicTraverseSession *const traverseSession, const DicNode *const parentDicNode,
                          const DicNode *const dicNode) const override {
            //TODO: make a contant
            return 0.00144f;
        }


        float
        getCompletionCost(const DicTraverseSession *const traverseSession,
                          const DicNode *const dicNode) const override {
            // The auto completion starts when the input index is same as the input size
            const float cost = 0.0624f;
            if (ZhuyinUtils::isInitial(dicNode->getNodeCodePoint(), false)) {
                return cost * 10;
            }
            return cost;
        }


        float getOmissionCost(const DicTraverseSession *const traverseSession, const DicNode *const parentDicNode,
                              const DicNode *const dicNode) const override {
            const bool isIntentionalOmission = parentDicNode->canBeIntentionalOmission();
            if (isIntentionalOmission)
                return 0.0f;

            return TypingWeighting::getOmissionCost(traverseSession, parentDicNode, dicNode);
        }


        AK_FORCE_INLINE float
        getSubstitutionCost(const DicTraverseSession *const pSession, const DicNode *const parentDicNode,
                            const DicNode *const dicNode,
                            DicNode_InputStateG *const pG) const override {
            if (dicNode->canBeIntentionalOmission())
                return 0.0;
            else return ScoringParams::SUBSTITUTION_COST + getMatchedCost(pSession, parentDicNode, dicNode, pG);
        }

        float getSpaceOmissionCost(const DicTraverseSession *const traverseSession,
                                   const DicNode *const dicNode, DicNode_InputStateG *inputStateG) const override {
            const auto cost = static_cast<const float>(ScoringParams::SPACE_OMISSION_COST / 1.6);
            return cost * traverseSession->getMultiWordCostMultiplier();
        }

        ErrorTypeUtils::ErrorType
        getErrorType(CorrectionType correctionType, const DicTraverseSession *traverseSession,
                     const DicNode *parentDicNode, const DicNode *dicNode) const override;


        float getTerminalInsertionCost(const DicTraverseSession *const traverseSession,
                                       const DicNode *const dicNode) const override {
            const int inputIndex = dicNode->getInputIndex(0);
            const int inputSize = traverseSession->getInputSize();
            ASSERT(inputIndex < inputSize);
            // TODO: Implement more efficient logic
            return  ScoringParams::TERMINAL_INSERTION_COST * (inputSize - inputIndex);
        }

    private:
        DISALLOW_COPY_AND_ASSIGN(TypingZhuyinWeighting);

        TypingZhuyinWeighting() {}

        ~TypingZhuyinWeighting() {}

        static const TypingZhuyinWeighting sInstance;

    };
} // namespace latinime
#endif // LATINIME_ZH_TYPING_WEIGHTING_H
