/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_PROXIMITY_INFO_STATE_UTILS_H
#define LATINIME_PROXIMITY_INFO_STATE_UTILS_H

#include <bitset>
#include <unordered_map>
#include <vector>

#include "defines.h"

namespace latinime {
    class ProximityInfo;

    class ProximityInfoParams;

    class ProximityInfoStateUtils {
    public:
        typedef std::unordered_map<int, float> NearKeysDistanceMap;
        typedef std::bitset<MAX_KEY_COUNT_IN_A_KEYBOARD> NearKeycodesSet;

        static int trimLastTwoTouchPoints(std::vector<int> *sampledInputXs,
                                          std::vector<int> *sampledInputYs,
                                          std::vector<int> *sampledInputTimes,
                                          std::vector<int> *sampledLengthCache,
                                          std::vector<int> *sampledInputIndice);

        static int updateTouchPoints(const ProximityInfo *proximityInfo,
                                     int maxPointToKeyLength,
                                     const int *inputProximities,
                                     const int *inputXCoordinates,
                                     const int *inputYCoordinates,
                                     const int *times, const int *pointerIds,
                                     int inputSize,
                                     bool isGeometric, int pointerId,
                                     int pushTouchPointStartIndex,
                                     std::vector<int> *sampledInputXs,
                                     std::vector<int> *sampledInputYs,
                                     std::vector<int> *sampledInputTimes,
                                     std::vector<int> *sampledLengthCache,
                                     std::vector<int> *sampledInputIndice);

        static const int *
        getProximityCodePointsAt(const int *inputProximities, int index);

        static int getPrimaryCodePointAt(const int *inputProximities, int index);

        static void popInputData(std::vector<int> *sampledInputXs, std::vector<int> *sampledInputYs,
                                 std::vector<int> *sampledInputTimes,
                                 std::vector<int> *sampledLengthCache,
                                 std::vector<int> *sampledInputIndice);

        static float refreshSpeedRates(int inputSize, const int *xCoordinates,
                                       const int *yCoordinates, const int *times,
                                       int lastSavedInputSize,
                                       int sampledInputSize,
                                       const std::vector<int> *sampledInputXs,
                                       const std::vector<int> *sampledInputYs,
                                       const std::vector<int> *sampledInputTimes,
                                       const std::vector<int> *sampledLengthCache,
                                       const std::vector<int> *sampledInputIndice,
                                       std::vector<float> *sampledSpeedRates,
                                       std::vector<float> *sampledDirections);

        static void refreshBeelineSpeedRates(int mostCommonKeyWidth, float averageSpeed,
                                             int inputSize, const int *xCoordinates,
                                             const int *yCoordinates,
                                             const int *times, int sampledInputSize,
                                             const std::vector<int> *sampledInputXs,
                                             const std::vector<int> *sampledInputYs,
                                             const std::vector<int> *inputIndice,
                                             std::vector<int> *beelineSpeedPercentiles);

        static float getDirection(const std::vector<int> *sampledInputXs,
                                  const std::vector<int> *sampledInputYs, int index0,
                                  int index1);

        static void
        updateAlignPointProbabilities(float maxPointToKeyLength, int mostCommonKeyWidth,
                                      int keyCount, int start,
                                      int sampledInputSize,
                                      const std::vector<int> *sampledInputXs,
                                      const std::vector<int> *sampledInputYs,
                                      const std::vector<float> *sampledSpeedRates,
                                      const std::vector<int> *sampledLengthCache,
                                      const std::vector<float> *sampledNormalizedSquaredLengthCache,
                                      const ProximityInfo *proximityInfo,
                                      std::vector<std::unordered_map<int, float>> *charProbabilities,
                                      std::vector<std::unordered_map<int, float>> *originalCharProbs);

        static void updateSampledSearchKeySets(const ProximityInfo *proximityInfo,
                                               int sampledInputSize,
                                               int lastSavedInputSize,
                                               const std::vector<int> *sampledLengthCache,
                                               const std::vector<std::unordered_map<int, float>> *charProbabilities,
                                               std::vector<NearKeycodesSet> *sampledSearchKeySets,
                                               std::vector<std::vector<int>> *sampledSearchKeyVectors);

        static float getPointToKeyByIdLength(float maxPointToKeyLength,
                                             const std::vector<float> *sampledNormalizedSquaredLengthCache,
                                             int keyCount,
                                             int inputIndex, int keyId);

        static void
        printSamplePointsProbabilities(int sampledInputSize,
                                       const std::vector<int, std::allocator<int>> *sampledInputXs,
                                       const std::vector<int, std::allocator<int>> *sampledInputYs,
                                       const std::vector<float, std::allocator<float>> *sampledSpeedRates,
                                       const ProximityInfo *proximityInfo,
                                       const std::vector<std::unordered_map<int, float>> *charProbabilities);


        static void printSamplePointsProbabilitiesAsTable(const ProximityInfo *proximityInfo, int sampleSize,
                                                          std::vector<int> *mIndexToRelativeIndex,
                                                          std::vector<std::unordered_map<int, float>> *charProbabilities);

        static void initGeometricDistanceInfos(const ProximityInfo *proximityInfo,
                                               int sampledInputSize,
                                               int lastSavedInputSize, bool isGeometric,
                                               const std::vector<int> *sampledInputXs,
                                               const std::vector<int> *sampledInputYs,
                                               std::vector<float> *sampledNormalizedSquaredLengthCache);

        static void initPrimaryInputWord(int inputSize, const int *inputProximities,
                                         int *primaryInputWord);

        static void dump(bool isGeometric, int inputSize,
                         const int *inputXCoordinates, const int *inputYCoordinates,
                         int sampledInputSize, const std::vector<int> *sampledInputXs,
                         const std::vector<int> *sampledInputYs,
                         const std::vector<int> *sampledTimes,
                         const std::vector<float> *sampledSpeedRates,
                         const std::vector<int> *sampledBeelineSpeedPercentiles);

        static bool checkAndReturnIsContinuousSuggestionPossible(int inputSize,
                                                                 const int *xCoordinates,
                                                                 const int *yCoordinates,
                                                                 const int *times,
                                                                 int sampledInputSize,
                                                                 const std::vector<int> *sampledInputXs,
                                                                 const std::vector<int> *sampledInputYs,
                                                                 const std::vector<int> *sampledTimes,
                                                                 const std::vector<int> *sampledInputIndices);

        // TODO: Move to most_probable_string_utils.h
        static float getMostProbableString(const ProximityInfo *proximityInfo,
                                           int sampledInputSize,
                                           const std::vector<std::unordered_map<int, float>> *charProbabilities,
                                           int *codePointBuf, int *probableStringLength);

        static int getMostProbableChar(const ProximityInfo *proximityInfo,
                                       int index,
                                       const std::vector<std::unordered_map<int, float>> *charProbabilities);

        static void computeRelativeIndexs(const ProximityInfo *proximityInfo,
                                          int sampledInputSize,
                                          const std::vector<std::unordered_map<int, float>> *charProbabilities,
                                          std::vector<int> *relativeIndexs);


    private:
        DISALLOW_IMPLICIT_CONSTRUCTORS(ProximityInfoStateUtils);

        static float updateNearKeysDistances(const ProximityInfo *proximityInfo,
                                             float maxPointToKeyLength, int x, int y,
                                             bool isGeometric,
                                             NearKeysDistanceMap *currentNearKeysDistances);

        static bool isPrevLocalMin(const NearKeysDistanceMap *currentNearKeysDistances,
                                   const NearKeysDistanceMap *prevNearKeysDistances,
                                   const NearKeysDistanceMap *prevPrevNearKeysDistances);

        static float getPointScore(int mostCommonKeyWidth, int x, int y,
                                   int time, bool lastPoint, float nearest, float sumAngle,
                                   const NearKeysDistanceMap *currentNearKeysDistances,
                                   const NearKeysDistanceMap *prevNearKeysDistances,
                                   const NearKeysDistanceMap *prevPrevNearKeysDistances,
                                   std::vector<int> *sampledInputXs, std::vector<int> *sampledInputYs);

        static bool pushTouchPoint(const ProximityInfo *proximityInfo,
                                   int maxPointToKeyLength, int inputIndex, int nodeCodePoint, int x,
                                   int y, int time, bool isGeometric,
                                   bool doSampling, bool isLastPoint,
                                   float sumAngle, NearKeysDistanceMap *currentNearKeysDistances,
                                   const NearKeysDistanceMap *prevNearKeysDistances,
                                   const NearKeysDistanceMap *prevPrevNearKeysDistances,
                                   std::vector<int> *sampledInputXs, std::vector<int> *sampledInputYs,
                                   std::vector<int> *sampledInputTimes, std::vector<int> *sampledLengthCache,
                                   std::vector<int> *sampledInputIndice);

        static float calculateBeelineSpeedRate(int mostCommonKeyWidth, float averageSpeed,
                                               int id, int inputSize, const int *xCoordinates,
                                               const int *yCoordinates, const int *times,
                                               int sampledInputSize,
                                               const std::vector<int> *sampledInputXs,
                                               const std::vector<int> *sampledInputYs,
                                               const std::vector<int> *inputIndice);

        static float getPointAngle(const std::vector<int> *sampledInputXs,
                                   const std::vector<int> *sampledInputYs, int index);

        static float getPointsAngle(const std::vector<int> *sampledInputXs,
                                    const std::vector<int> *sampledInputYs, int index0, int index1,
                                    int index2);

        static bool suppressCharProbabilities(int mostCommonKeyWidth,
                                              int sampledInputSize, const std::vector<int> *lengthCache,
                                              int index0,
                                              int index1,
                                              std::vector<std::unordered_map<int, float>> *charProbabilities);

        static float calculateSquaredDistanceFromSweetSpotCenter(
                const ProximityInfo *proximityInfo, const std::vector<int> *sampledInputXs,
                const std::vector<int> *sampledInputYs, int keyIndex,
                int inputIndex);

        static float calculateNormalizedSquaredDistance(const ProximityInfo *proximityInfo,
                                                        const std::vector<int> *sampledInputXs,
                                                        const std::vector<int> *sampledInputYs,
                                                        int keyIndex, int inputIndex);
    };
} // namespace latinime
#endif // LATINIME_PROXIMITY_INFO_STATE_UTILS_H
