/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_GESTURE_WEIGHTING_H
#define LATINIME_GESTURE_WEIGHTING_H

#include <sstream>
#include <scoring_params.h>
#include "defines.h"
#include "dic_node_utils.h"
#include "error_type_utils.h"
#include "touch_position_correction_utils.h"
#include "weighting.h"
#include "proximity_info.h"
#include "proximity_info_state.h"
#include "dic_traverse_session.h"
#include "gesture_scoring_params.h"
#include "char_utils.h"
#include "profiler.h"

namespace latinime {

    class DicNode;

    struct DicNode_InputStateG;

    class MultiBigramMap;

    class GestureWeighting : public Weighting {
    public:
        static const GestureWeighting *getInstance() { return &sInstance; }

    protected:
        float getTerminalSpatialCost(const DicTraverseSession *const traverseSession,
                                     const DicNode *const parentDicNode,
                                     const DicNode *const dicNode) const override {
            return 0.0;//diff * 0.2;
        }

        float
        getOmissionCost(const DicTraverseSession *const traverseSession, const DicNode *const parentDicNode,
                        const DicNode *const dicNode) const override {
            const bool isZeroCostOmission = parentDicNode->isZeroCostOmission();
            const bool isIntentionalOmission = parentDicNode->canBeIntentionalOmission();
            const bool sameCodePoint =
                    parentDicNode->getNodeCodePoint() == parentDicNode->getPrevCodePointG(0);
            float cost;
            if (isZeroCostOmission) {
                cost = 0.0f;
            } else if (isIntentionalOmission) {
                cost = GestureScoringParams::INTENTIONAL_OMISSION_COST;
            } else {
                cost = sameCodePoint ? GestureScoringParams::OMISSION_COST_SAME_CHAR
                                     : GestureScoringParams::OMISSION_COST;
            }

            return cost;
        }

        float getMatchedCost(const DicTraverseSession *const traverseSession,
                             const DicNode *const parentDicNode, const DicNode *const dicNode,
                             DicNode_InputStateG *inputStateG) const override {
            int index = dicNode->getInputIndex(0);
            return matchCostByIndex(traverseSession, dicNode, index);
        }

        float matchCostByIndex(const DicTraverseSession *traverseSession, const DicNode *dicNode, int index) const {
            const ProximityInfoState *proximityInfoState = traverseSession->getProximityInfoState(
                    0);
            float weight = 0;
            int keyIndex = traverseSession->getProximityInfo()->getKeyIndexOf(
                    dicNode->getNodeCodePoint());
            float prob = proximityInfoState->getProbability(index, keyIndex, false);

            if ((int) prob == MAX_VALUE_FOR_WEIGHTING) {
                weight = GestureScoringParams::DISTANCE_WEIGHT_LENGTH_NO_PROB;
            } else {
                float normalizedProb = 1 - prob;
                normalizedProb = TouchPositionCorrectionUtils::getSweetSpotProbFactor(normalizedProb);
                weight = normalizedProb * ((index == 0 || index == traverseSession->getInputSize() - 1) ?
                                        GestureScoringParams::DISTANCE_WEIGHT_LENGTH_FIRST_CHAR :
                                        GestureScoringParams::DISTANCE_WEIGHT_LENGTH);
            }

            return weight;
        }

        bool isProximityDicNode(const DicTraverseSession *const traverseSession,
                                const DicNode *const dicNode) const override {
            return false;
        }

        float getTranspositionCost(const DicTraverseSession *const traverseSession,
                                   const DicNode *const parentDicNode,
                                   const DicNode *const dicNode) const override {
            ASSERT(false);
            return 0;
        }

        float getInsertionCost(const DicTraverseSession *const traverseSession,
                               const DicNode *const parentDicNode,
                               const DicNode *const dicNode) const override {
            int index = dicNode->getInputIndex(0) + 1;
            float matchWeight = matchCostByIndex(traverseSession, dicNode, index);
            return matchWeight + GestureScoringParams::INSERTION_COST;
        }

        float getSpaceOmissionCost(const DicTraverseSession *const traverseSession,
                                   const DicNode *const dicNode,
                                   DicNode_InputStateG *inputStateG) const override {
            const float cost = GestureScoringParams::SPACE_OMISSION_COST;
            return cost * traverseSession->getMultiWordCostMultiplier();
        }

        float getNewWordBigramLanguageCost(const DicTraverseSession *const traverseSession,
                                           const DicNode *const dicNode,
                                           MultiBigramMap *const multiBigramMap) const override {
            Improbability improbability = DicNodeUtils::getBigramNodeImprobability(
                    traverseSession->getDictionaryStructurePolicy(),
                    dicNode, multiBigramMap);
            return getTerminalLanguageCost(improbability, true);
        }

        float getCompletionCost(const DicTraverseSession *const traverseSession, const DicNode *const dicNode) const override {

            // The auto completion starts when the input index is same as the input size
            const bool firstCompletion = dicNode->getInputIndex(0)
                                         == traverseSession->getInputSize();

            // TODO: Change the cost for the first completion for the gesture?
            const float cost = firstCompletion ? GestureScoringParams::COST_FIRST_COMPLETION
                                               : GestureScoringParams::COST_COMPLETION;

            const bool sameCodePoint = dicNode->getNodeCodePoint() == dicNode->getPrevCodePointG(0);
            int pointIndex = sameCodePoint ? dicNode->getInputIndex(0) : dicNode->getInputIndex(0) + 1;
            int validDiff = getCurrentRangeDiff(traverseSession, pointIndex, true);

            if (validDiff)
                return MAX_VALUE_FOR_WEIGHTING;
            else if (sameCodePoint) {
                return 0.01f;
            }

            float prob;
            int tailStart = traverseSession->getProximityInfoState(0)->getRelativeIndex(
                    traverseSession->getInputSize() - 1);
            if (tailStart + 1 < traverseSession->getProximityInfoState(0)->getFullSampleSize() /* We have a tail */) {
                int tailEnd = traverseSession->getProximityInfoState(0)->getFullSampleSize();
                int keyIndex = traverseSession->getProximityInfo()->getKeyIndexOf(dicNode->getNodeCodePoint());
                prob = traverseSession->getProximityInfoState(0)->getBestProbBetweenRel(tailStart + 1, tailEnd,
                                                                                        true, keyIndex);
            } else prob = MAX_VALUE_FOR_WEIGHTING;

            return (float) (prob < MAX_VALUE_FOR_WEIGHTING ? 0.1 : cost);
        }

        int getCurrentRangeDiff(const DicTraverseSession *traverseSession, int pointIndex, bool checkUpperLimit) const {
            int icc = traverseSession->getProximityInfoState(0)->getImportantCharsCount();
            int mpsc = traverseSession->getProximityInfoState(0)->getMostProbableStringLen()/* -1 *//*padding*/ ;

            if ((pointIndex <= mpsc)) {
                return pointIndex - mpsc;
            } else if (checkUpperLimit && pointIndex > icc) {
                return pointIndex - icc;
            }

            return 0;
        }

        float getTerminalLanguageCost(Improbability improbability, bool hasMultipleWords) const override {
            float cost = improbability.getLanguageImprobability() * GestureScoringParams::DISTANCE_WEIGHT_LANGUAGE;
            if(improbability.getLanguagePOSImprobability() != NOT_A_PROBABILITY) {
                float posCost = improbability.getLanguagePOSImprobability() *
                                    (hasMultipleWords ?
                                        ScoringParams::DISTANCE_WEIGHT_LANGUAGE_POS_SPLIT :
                                        ScoringParams::DISTANCE_WEIGHT_LANGUAGE_POS);
                cost = (cost + posCost) / 2.0;
            }
            return cost;
        }

        float getTerminalInsertionCost(const DicTraverseSession *const traverseSession,
                                       const DicNode *const dicNode) const override {
            const bool sameCodePoint = dicNode->getNodeCodePoint() == dicNode->getPrevCodePointG(0);
            int pointIndex = sameCodePoint ? dicNode->getInputIndex(0) - 1 : dicNode->getInputIndex(0);
            int diff = getCurrentRangeDiff(traverseSession, pointIndex, false);
            diff = abs(diff);

            float cost = 0.0f;
            if (diff > 1) {
                cost += GestureScoringParams::TERMINAL_INSERTION_COST * (diff - 1);
            }
            return cost + GestureScoringParams::TERMINAL_INSERTION_COST * 2 * diff;
        }

        AK_FORCE_INLINE bool needsToNormalizeCompoundDistance() const override {
            return false;
        }

        AK_FORCE_INLINE float getAdditionalProximityCost() const override {
            return GestureScoringParams::ADDITIONAL_PROXIMITY_COST;
        }

        AK_FORCE_INLINE float getSkipCost(const DicTraverseSession *const traverseSession, const DicNode *const parentDicNode,
                          const DicNode *const dicNode) const override {
            ASSERT(false);
            return 0.0;
        }

        AK_FORCE_INLINE float
        getSubstitutionCost(const DicTraverseSession *const traverseSession, const DicNode *const parentDicNode,
                            const DicNode *const dicNode,
                            DicNode_InputStateG *const pG) const override {

            const ProximityInfoState *proximityInfoState = traverseSession->getProximityInfoState(
                    0);

            int index = dicNode->getInputIndex(0) + 1; // important!;
            // This code should be the same like match.
            float weight = 0;
            float prob;

            int keyIndex = traverseSession->getProximityInfo()->getKeyIndexOf(
                    dicNode->getNodeCodePoint());
            prob = proximityInfoState->getBestProbBetween(index, keyIndex, true);

            if ((int) prob == MAX_VALUE_FOR_WEIGHTING) {
                weight = GestureScoringParams::DISTANCE_WEIGHT_LENGTH_NO_PROB;
            } else {
                float normalizedProb = 1 - prob;
                normalizedProb = TouchPositionCorrectionUtils::getSweetSpotProbFactor(normalizedProb);
                weight = normalizedProb * ((index == 0 || index == traverseSession->getInputSize() - 1) ?
                                           GestureScoringParams::DISTANCE_WEIGHT_LENGTH_FIRST_CHAR :
                                           GestureScoringParams::DISTANCE_WEIGHT_LENGTH);
            }
            return weight + GestureScoringParams::SUBSTITUTION_COST;
        }

        AK_FORCE_INLINE float
        getSpaceSubstitutionCost(const DicTraverseSession *const traverseSession,
                                 const DicNode *const dicNode) const override {
            const int inputIndex = dicNode->getInputIndex(0);
            const float distanceToSpaceKey = traverseSession->getProximityInfoState(0)
                    ->getPointToKeyLength(inputIndex, KEYCODE_SPACE);
            const float cost = GestureScoringParams::SPACE_SUBSTITUTION_COST * distanceToSpaceKey;
            return cost * traverseSession->getMultiWordCostMultiplier();
        }

        ErrorTypeUtils::ErrorType getErrorType(CorrectionType correctionType,
                                               const DicTraverseSession *traverseSession,
                                               const DicNode *parentDicNode,
                                               const DicNode *dicNode) const override;

    private:
        DISALLOW_COPY_AND_ASSIGN(GestureWeighting);

        static const GestureWeighting sInstance;

        GestureWeighting() {}

        ~GestureWeighting() {}
    };
} // namespace latinime
#endif // LATINIME_TYPING_WEIGHTING_H
