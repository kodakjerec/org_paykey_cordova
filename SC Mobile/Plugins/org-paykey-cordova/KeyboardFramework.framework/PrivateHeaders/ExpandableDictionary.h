//
// Created by YakirPayKey on 3/20/18.
//

#ifndef NLP_ENGINE_EXPANDABLEDICTIONARY_H
#define NLP_ENGINE_EXPANDABLEDICTIONARY_H

#include <dictionary.h>
#include <format_utils.h>
#include <header_read_write_utils.h>
#include <nlpDefines.h>
#include <dictionary_structure_with_buffer_policy_factory.h>
#include <header_policy.h>
#include <file_utils.h>
#include "DictionaryDelegate.h"

namespace latinime {

    class ExpandableDictionary : public DictionaryDelegate {

    public:

        static std::string DICT_FILE_EXTENSION;

        ExpandableDictionary(const char *name, const char *pathToDict)
                : DictionaryDelegate(name, pathToDict, 0, 0) {}

        virtual DictionaryHeaderStructurePolicy::AttributeMap getHeaderAttributes() {

            latinime::DictionaryHeaderStructurePolicy::AttributeMap attributeMap;
            latinime::DictionaryHeaderStructurePolicy::AttributeMap::key_type key;
            latinime::DictionaryHeaderStructurePolicy::AttributeMap::mapped_type value;

            HeaderReadWriteUtils::insertCharactersIntoVector(HeaderPolicy::DICTIONARY_NAME_KEY, &key);
            HeaderReadWriteUtils::insertCharactersIntoVector(getName(), &value);
            attributeMap[key] = value;

            key.clear();
            value.clear();

            char localeBuf[10];
            const CodePoints localeVec = DictionaryDelegate::getLocale();
            const int *localeArray = &localeVec.front();
            intArrayToCharArray(localeArray, static_cast<const int>(localeVec.size()), localeBuf, NELEMS(localeBuf));
            HeaderReadWriteUtils::insertCharactersIntoVector(HeaderPolicy::LOCALE_KEY, &key);
            HeaderReadWriteUtils::insertCharactersIntoVector(localeBuf, &value);
            attributeMap[key] = value;

            return attributeMap;
        }


        bool exists() override {
            return getFilePath() != nullptr && FileUtils::existsDir(getFilePath());
        }

        bool isUpdatable() override {
            return true;
        }

        bool flushBinaryDictionary() {
            bool success;

            if (getDictionary() == nullptr || getFilePath() == nullptr) {
                return false;
            }

            if (getDictionary()->needsToRunGC(
                    getDictionary()->getDictionaryStructurePolicy()->getHeaderStructurePolicy()->isDecayingDict() /* mindsBlockByGC */)) {
                success = getDictionary()->flushWithGC(getFilePath());
            } else {
                success = getDictionary()->flush(getFilePath());
            }
            reopenDictionary();
            return success;
        }

        virtual void clear() {
            //remove the dictionary object (don't need to flush)
            deleteDictionary();
            //delete file(s) from disk
            if (getFilePath() && !FileUtils::deleteFileOrDir(getFilePath())) {
                AKLOGE("Can't remove file or dir: %s\n", getFilePath());
            }
            deleteDicTraverseSessions();
            //recreate in memory (the client must call save)
            loadCreateIfNeeded();
        }

        bool addUnigramEntry(const CodePointArrayView wordCodePoints, const UnigramProperty *unigramProperty) {
            return getDictionary()->addUnigramEntry(wordCodePoints, unigramProperty);
        }

        bool addNgramEntry(const NgramProperty *const ngramProperty) {
            return getDictionary()->addNgramEntry(ngramProperty);
        }

        bool removeUnigramEntry(const CodePointArrayView codePointArrayView) {
            return getDictionary()->removeUnigramEntry(codePointArrayView);
        }

        bool removeNgramEntry(const NgramContext *const context, CodePointArrayView codePointArrayView) {
            return getDictionary()->removeNgramEntry(context, codePointArrayView);
        }


        bool updateEntriesForWordWithNgramContext(const NgramContext *const ngramContext,
                                                  const CodePointArrayView codePoints, const bool isValidWord,
                                                  const bool isNotAWord,
                                                  const HistoricalInfo historicalInfo) {
            return getDictionary()->updateEntriesForWordWithNgramContext(ngramContext, codePoints, isValidWord,
                                                                         isNotAWord, historicalInfo);
        }

        bool updateShortcutsEntriesForWord(const CodePointArrayView wordCodePoints,
                                           std::vector<UnigramProperty::ShortcutProperty> shortcuts) {
            return getDictionary()->updateShortcutsEntriesForWord(wordCodePoints, shortcuts);
        }


    private:
        
        bool create() override {
            latinime::DictionaryHeaderStructurePolicy::AttributeMap attributeMap = getHeaderAttributes();
            latinime::DictionaryStructureWithBufferPolicy::StructurePolicyPtr ptr =
                    latinime::DictionaryStructureWithBufferPolicyFactory::newPolicyForOnMemoryDict(
                            FormatUtils::VERSION_403, getLocale(), &attributeMap);

            DictionaryDelegate::setDictionary(Dictionary::createNewDictionary(std::move(ptr)));
            return true;
        }

    };


}

#endif //NLP_ENGINE_EXPANDABLEDICTIONARY_H
