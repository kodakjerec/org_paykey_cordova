//
//  NLPEngine.h
//
//  Created by paul kalnitz on 14/03/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#ifndef NLP_FACILITATOR_H
#define NLP_FACILITATOR_H

#include <memory>
#include "nlpDefines.h"
#include "KeyboardData.h"
#include "Integrator.h"
#include "ReadOnlyDictionary.h"
#include "UserHistoryDictionary.h"
#include "ContactsDictionary.h"
#include "UserWordsDictionary.h"
#include "EmojiDictionary.h"

namespace latinime {

    class DictionaryFacilitator {

    public:

        static int const DIC_INDEX_MAIN;
        static int const DIC_INDEX_USER_HISTORY;
        static int const DIC_INDEX_CONTACTS;
        static int const DIC_INDEX_USER_WORDS;
        static int const DIC_INDEX_EMOJI;
        static int const DIC_SIZE;
        static int const DIC_SUGGESTIONS_COUNT;
        static int const DIC_SUGGESTIONS[];

        DictionaryFacilitator();

        ~DictionaryFacilitator();

        void loadKeyboardData(int keyboardId, int gridWidth, int gridHeight, int kbdWidth, int kbdHeight,
                              nlpEngineKeys *keys,
                              int mostCommonKeyWidth, int mostCommonKeyHeight);

        void setProximityInfo(ProximityInfo *p);

        bool loadMainDictionary(const char *dictionaryPath, long dictionarySize, long offset);

        bool loadUserWordsDictionary(std::vector<int> localeCodePoints);

        bool loadEmojiDictionary(const char *dictionaryPath, long dictionarySize, long offset,
                                 bool alsoUseForPrediction);

        bool loadContactsDictionary();

        bool loadUserHistoryDictionary(const char *dictionaryPath, std::vector<int> localeCodepoints);

        void unloadMainDictionary();

        void unloadUserWordsDictionary();

        void unloadEmojiDictionary();

        void unloadContactsDictionary();

        void unloadUserHistoryDictionary();

        bool addContact(CodePointsList fullName);

        bool addToUserHistory(const NgramContext *ngramContext,
                              CodePointArrayView wordsSuggested,
                              bool wasAutoCapitalized,
                              int timestamp,
                              const CodePointArraysView *shortcuts,
                              const std::vector<int> *frequencies,
                              bool blockOffensive,
                              std::vector<int> &wordLearned);

        bool addUserWord(CodePointArrayView word, CodePointArrayView shortcut);

        void clearUserHistoryDictionary();

        bool saveUserHistoryDictionary();

        std::string getVersion();

        bool unlearnFromUserHistory(CodePointArrayView view);

        bool unlearnFromUserHistory(const NgramContext *context, CodePointArrayView view);

        bool isWordExists(CodePointArrayView view);

        int getProbabilityFromMainDictionary(CodePointArrayView word);

        int getProbabilityFromMainDictionary(CodePointArrayView word, CodePointArrayView shortcut);

        int getProbability(CodePointArrayView word);

        bool isOffensive(CodePointArrayView word);

        bool isValidWord(CodePointArrayView word);

        bool isLanguageValidWord(CodePointArrayView word);

        std::string getDictionaryInfo(int index);

        std::string getDictionaryDump(int index);

        void clearUserWordsDictionaryNative();

        void clearContactsDictionary();

        NlpResults getSuggestionResults(NgramContext *ngramContext,
                                        CodePoints wordCodePoints, int x[], int y[], int times[], int pointer[],
                                        SuggestOptions *options);

        NlpResults getEmojiSuggestions(NgramContext *ngramContext,
                                       CodePointArrayView codePoints, int *x, int *y, int *times, int *pointer,
                                       SuggestOptions *options);

    private:

        KeyboardData *mKeyboardData = nullptr;
        ProximityInfo *mProximityInfo = nullptr;

        UserWordsDictionary *mUserWordsDictionary = nullptr;
        ReadOnlyDictionary *mMainDictionary = nullptr;
        EmojiDictionary *mEmojiDictionary = nullptr;
        UserHistoryDictionary *mUserHistoryDictionary = nullptr;
        ContactsDictionary *mContactsDictionary = nullptr;

        Integrator predict(NgramContext *ngramContext,
                           CodePointArrayView codePointArrayView,
                           SuggestOptions *pOptions);

        Integrator correct(NgramContext *ngramContext,
                           CodePointArrayView codePoints, int *x, int *y, int *times, int *pointer,
                           SuggestOptions *options);
        
        bool closeDictionary(int index);

        DictionaryDelegate *getDictionary(int index);

        void prettyPrint(std::string arg);

    };
} // namespace latinime

#endif /* NLP_FACILITATOR_H */
