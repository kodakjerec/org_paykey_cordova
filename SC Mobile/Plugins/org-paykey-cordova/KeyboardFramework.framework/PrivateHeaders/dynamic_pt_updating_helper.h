/*
 * Copyright (C) 2013, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_DYNAMIC_PT_UPDATING_HELPER_H
#define LATINIME_DYNAMIC_PT_UPDATING_HELPER_H

#include "defines.h"
#include "pt_node_params.h"
#include "int_array_view.h"

namespace latinime {

class NgramProperty;
class BufferWithExtendableBuffer;
class DynamicPtReadingHelper;
class PtNodeReader;
class PtNodeWriter;
class UnigramProperty;

class DynamicPtUpdatingHelper {
 public:
    DynamicPtUpdatingHelper(BufferWithExtendableBuffer *const buffer,
            const PtNodeReader *const ptNodeReader, PtNodeWriter *const ptNodeWriter)
            : mBuffer(buffer), mPtNodeReader(ptNodeReader), mPtNodeWriter(ptNodeWriter) {}

    ~DynamicPtUpdatingHelper() {}

    // Add a word to the dictionary. If the word already exists, update the probability.
    bool addUnigramWord(DynamicPtReadingHelper *readingHelper,
                        CodePointArrayView wordCodePoints, const UnigramProperty *unigramProperty,
            bool *outAddedNewUnigram);

    // TODO: Remove after stopping supporting v402.
    // Add an n-gram entry.
    bool addNgramEntry(PtNodePosArrayView prevWordsPtNodePos, int wordPos,
            const NgramProperty *ngramProperty, bool *outAddedNewEntry);

    // TODO: Remove after stopping supporting v402.
    // Remove an n-gram entry.
    bool removeNgramEntry(PtNodePosArrayView prevWordsPtNodePos, int wordPos);

    // Add a shortcut target.
    bool addShortcutTarget(int wordPos, CodePointArrayView targetCodePoints,
                           int shortcutProbability);

 private:
    DISALLOW_IMPLICIT_CONSTRUCTORS(DynamicPtUpdatingHelper);

    static const int CHILDREN_POSITION_FIELD_SIZE;

    BufferWithExtendableBuffer *const mBuffer;
    const PtNodeReader *const mPtNodeReader;
    PtNodeWriter *const mPtNodeWriter;

    bool createAndInsertNodeIntoPtNodeArray(int parentPos,
                                            CodePointArrayView ptNodeCodePoints, const UnigramProperty *unigramProperty,
            int *forwardLinkFieldPos);

    bool setPtNodeProbability(const PtNodeParams *originalPtNodeParams,
            const UnigramProperty *unigramProperty, bool *outAddedNewUnigram);

    bool createChildrenPtNodeArrayAndAChildPtNode(const PtNodeParams *parentPtNodeParams,
            const UnigramProperty *unigramProperty,
                                                  CodePointArrayView remainingCodePoints);

    bool createNewPtNodeArrayWithAChildPtNode(int parentPos,
                                              CodePointArrayView ptNodeCodePoints,
            const UnigramProperty *unigramProperty);

    bool reallocatePtNodeAndAddNewPtNodes(const PtNodeParams *reallocatingPtNodeParams,
                                          size_t overlappingCodePointCount, const UnigramProperty *unigramProperty,
                                          CodePointArrayView newPtNodeCodePoints);

    const PtNodeParams getUpdatedPtNodeParams(const PtNodeParams *originalPtNodeParams,
                                              bool isNotAWord, bool isPossiblyOffensive, bool isTerminal,
                                              int parentPos, CodePointArrayView codePoints, int probability) const;

    const PtNodeParams getPtNodeParamsForNewPtNode(bool isNotAWord,
                                                   bool isPossiblyOffensive, bool isTerminal, int parentPos,
                                                   CodePointArrayView codePoints, int probability) const;
};
} // namespace latinime
#endif /* LATINIME_DYNAMIC_PATRICIA_TRIE_UPDATING_HELPER_H */
