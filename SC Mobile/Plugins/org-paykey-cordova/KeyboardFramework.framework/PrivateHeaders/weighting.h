/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_WEIGHTING_H
#define LATINIME_WEIGHTING_H

#include <dictionary_structure_with_buffer_policy.h>
#include <dic_node_utils.h>
#include "defines.h"
#include "error_type_utils.h"

namespace latinime {

class DicNode;
class DicTraverseSession;
struct DicNode_InputStateG;
class MultiBigramMap;

class Weighting {
 public:
    static void addCostAndForwardInputIndex(const Weighting *weighting,
                                            CorrectionType correctionType,
            const DicTraverseSession *traverseSession,
            const DicNode *parentDicNode, DicNode *dicNode,
            MultiBigramMap *multiBigramMap);
    
    virtual float getTerminalLanguageCost(Improbability improbability, bool hasMultipleWords) const = 0;

    virtual Improbability getBigramNodeImprobability(
            const DictionaryStructureWithBufferPolicy *const dictionaryStructurePolicy,
            const DicNode *const dicNode, MultiBigramMap *const multiBigramMap) const {
        return DicNodeUtils::getBigramNodeImprobability(
                dictionaryStructurePolicy, dicNode, multiBigramMap);
    }

protected:
    virtual float getTerminalSpatialCost(const DicTraverseSession *traverseSession,
                                         const DicNode *parentDicNode,
                                         const DicNode *dicNode) const = 0;

    virtual float getOmissionCost(const DicTraverseSession *parentDicNode, const DicNode *dicNode,
                                      const DicNode *pNode) const = 0;

    virtual float getMatchedCost(
            const DicTraverseSession *traverseSession, const DicNode *parentDicNode,
            const DicNode *dicNode,
            DicNode_InputStateG *inputStateG) const = 0;

    virtual bool isProximityDicNode(const DicTraverseSession *traverseSession,
            const DicNode *dicNode) const = 0;

    virtual float getTranspositionCost(
            const DicTraverseSession *traverseSession, const DicNode *parentDicNode,
            const DicNode *dicNode) const = 0;

    virtual float getInsertionCost(
            const DicTraverseSession *traverseSession,
            const DicNode *parentDicNode, const DicNode *dicNode) const = 0;

    virtual float getSpaceOmissionCost(const DicTraverseSession *traverseSession,
            const DicNode *dicNode, DicNode_InputStateG *inputStateG) const = 0;

    virtual float getNewWordBigramLanguageCost(
            const DicTraverseSession *traverseSession, const DicNode *dicNode,
            MultiBigramMap *multiBigramMap) const = 0;

    virtual float getCompletionCost(
            const DicTraverseSession *traverseSession,
            const DicNode *dicNode) const = 0;

    virtual float getTerminalInsertionCost(
            const DicTraverseSession *traverseSession,
            const DicNode *dicNode) const = 0;
    
    virtual bool needsToNormalizeCompoundDistance() const = 0;

    virtual float getAdditionalProximityCost() const = 0;

    virtual float getSubstitutionCost(const DicTraverseSession *pSession, const DicNode *pNode, const DicNode *pDicNode,
                                          DicNode_InputStateG *pG) const = 0;

    virtual float getSpaceSubstitutionCost(const DicTraverseSession *traverseSession,
            const DicNode *dicNode) const = 0;

    virtual ErrorTypeUtils::ErrorType getErrorType(CorrectionType correctionType,
            const DicTraverseSession *traverseSession,
            const DicNode *parentDicNode, const DicNode *dicNode) const = 0;

    virtual float getSkipCost(const DicTraverseSession *traverseSession, const DicNode *parentDicNode,
                      const DicNode *dicNode) const = 0;

    Weighting() {}
    virtual ~Weighting() {}

 private:
    DISALLOW_COPY_AND_ASSIGN(Weighting);

    static float getSpatialCost(const Weighting *weighting,
                                CorrectionType correctionType, const DicTraverseSession *traverseSession,
            const DicNode *parentDicNode, const DicNode *dicNode,
            DicNode_InputStateG *inputStateG);
    static float getLanguageCost(const Weighting *weighting,
                                 CorrectionType correctionType, const DicTraverseSession *traverseSession,
            const DicNode *parentDicNode, const DicNode *dicNode,
            MultiBigramMap *multiBigramMap);
    // TODO: Move to TypingWeighting and GestureWeighting?
    static int getForwardInputCount(CorrectionType correctionType);
    
};
} // namespace latinime
#endif // LATINIME_WEIGHTING_H
