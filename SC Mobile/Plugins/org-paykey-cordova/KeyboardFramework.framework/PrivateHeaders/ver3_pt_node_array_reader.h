/*
 * Copyright (C) 2014, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_VER3_PT_NODE_ARRAY_READER_H
#define LATINIME_VER3_PT_NODE_ARRAY_READER_H

#include <cstdint>

#include "defines.h"
#include "pt_node_array_reader.h"
#include "byte_array_view.h"

namespace latinime {

class Ver3PtNodeArrayReader : public PtNodeArrayReader {
 public:
    Ver3PtNodeArrayReader(const ReadOnlyByteArrayView buffer) : mBuffer(buffer) {};

    virtual bool readPtNodeArrayInfoAndReturnIfValid(int ptNodeArrayPos,
            int *const outPtNodeCount, int *outFirstPtNodePos) const;
    virtual bool readForwardLinkAndReturnIfValid(int forwardLinkPos,
            int *outNextPtNodeArrayPos) const;

 private:
    DISALLOW_COPY_AND_ASSIGN(Ver3PtNodeArrayReader);

    const ReadOnlyByteArrayView mBuffer;
};
} // namespace latinime
#endif /* LATINIME_VER2_PT_NODE_ARRAY_READER_H */
