/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_DIC_NODE_UTILS_H
#define LATINIME_DIC_NODE_UTILS_H

#include "defines.h"
#include "int_array_view.h"
#include <pt_node_pos_utils.h>
#include <improbability.h>

namespace latinime {

class DicNode;
class DicNodeVector;
class DictionaryStructureWithBufferPolicy;
class MultiBigramMap;

class DicNodeUtils {
 public:
    static void initAsRoot(
            const DictionaryStructureWithBufferPolicy *dictionaryStructurePolicy,
            WordIdArrayView prevWordIds, DicNode *newRootDicNode);
    static void initAsRootWithPreviousWord(
            const DictionaryStructureWithBufferPolicy *dictionaryStructurePolicy,
            const DicNode *prevWordLastDicNode, DicNode *newRootDicNode);
    static void initByCopy(const DicNode *srcDicNode, DicNode *destDicNode);
    static void getAllChildDicNodes(const DicNode *dicNode,
            const DictionaryStructureWithBufferPolicy *dictionaryStructurePolicy,
            DicNodeVector *childDicNodes);
    static Improbability getBigramNodeImprobability(
            const DictionaryStructureWithBufferPolicy *const dictionaryStructurePolicy,
            const DicNode *const dicNode, MultiBigramMap *const multiBigramMap);
    static Improbability getBigramNodeImprobability(
            const DictionaryStructureWithBufferPolicy *const dictionaryStructurePolicy,
            const DicNode *const dicNode, MultiBigramMap *const multiBigramMap, bool ignoreNotAWord);
    static bool isValidBigram(const DicNode *const dicNode,
            const DictionaryStructureWithBufferPolicy *const dictionaryStructurePolicy,
            std::vector<int> *stopWordIds);
    static float getImprobability(int probability);

 private:
    DISALLOW_IMPLICIT_CONSTRUCTORS(DicNodeUtils);
    // Max number of bigrams to look up
    static const int MAX_BIGRAMS_CONSIDERED_PER_CONTEXT = 500;
};
} // namespace latinime
#endif // LATINIME_DIC_NODE_UTILS_H
