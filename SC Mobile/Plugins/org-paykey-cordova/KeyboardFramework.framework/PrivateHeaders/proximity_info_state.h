/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_PROXIMITY_INFO_STATE_H
#define LATINIME_PROXIMITY_INFO_STATE_H

#include <cstring> // for memset()
#include <unordered_map>
#include <vector>
#include <char_utils.h>
#include <map>

#include "defines.h"
#include "proximity_info_params.h"
#include "proximity_info_state_utils.h"

namespace latinime {

    class ProximityInfo;

    class ProximityInfoState {
    public:
        /////////////////////////////////////////
        // Defined in proximity_info_state.cpp //
        /////////////////////////////////////////
        void initInputParams(const int pointerId, const float maxPointToKeyLength, const ProximityInfo *proximityInfo,
                             const int *inputCodes, const int inputSize, const int *xCoordinates, const int *yCoordinates,
                             const int *times, const int *pointerIds, const bool isGeometric,
                             const std::vector<int> *locale,
                             const std::map<int, std::vector<int>> * altProximitiesMap);

        void dump() const;

        void dumpSampleProbabilities() const;

        /////////////////////////////////////////
        // Defined here                        //
        /////////////////////////////////////////
        AK_FORCE_INLINE ProximityInfoState()
                : mProximityInfo(nullptr), mMaxPointToKeyLength(0.0f), mAverageSpeed(0.0f),
                  mHasTouchPositionCorrectionData(false), mMostCommonKeyWidthSquare(0),
                  mKeyCount(0), mCellHeight(0), mCellWidth(0), mGridHeight(0), mGridWidth(0),
                  mIsContinuousSuggestionPossible(false), mHasBeenUpdatedByGeometricInput(false),
                  mSampledInputXs(), mSampledInputYs(), mSampledTimes(), mSampledInputIndice(),
                  mSampledLengthCache(), mBeelineSpeedPercentiles(),
                  mSampledNormalizedSquaredLengthCache(), mSpeedRates(), mDirections(),
                  mCharProbabilities(), mSampledSearchKeySets(), mSampledSearchKeyVectors(),
                  mTouchPositionCorrectionEnabled(false), mSampledInputSize(0),
                  mMostProbableStringProbability(0.0f) {
            memset(mInputProximities, 0, sizeof(mInputProximities));
            memset(mPrimaryInputWord, 0, sizeof(mPrimaryInputWord));
            memset(mMostProbableString, 0, sizeof(mMostProbableString));
        }

        // Non virtual inline destructor -- never inherit this class
        AK_FORCE_INLINE ~ProximityInfoState() {}

        inline int getPrimaryCodePointAt(const int index) const {
            return getProximityCodePointsAt(index)[0];
        }

        inline int isRelativeIndex(int index) const {
            ASSERT(0 <= index && index < mFullSampledInputSize);
            for (int i = 0; i < mSampledInputSize; ++i) {
                if (mIndexToRelativeIndex[i] == index)
                    return true;
            }
            return false;
        }

        inline int getRelativeIndex(int index) const {
            ASSERT(0 <= index && index < mSampledInputSize);
            return mIndexToRelativeIndex.at(static_cast<unsigned long>(index));
        }

        inline int getFullSampleSize() const {
            return mFullSampledInputSize;
        }

        int getPrimaryOriginalCodePointAt(int index) const;

        inline bool sameAsTyped(const int *word, int length) const {
            if (length != mSampledInputSize) {
                return false;
            }
            const int *inputProximities = mInputProximities;
            while (length--) {
                if (*inputProximities != *word) {
                    return false;
                }
                inputProximities += MAX_PROXIMITY_CHARS_SIZE;
                word++;
            }
            return true;
        }

        inline bool sameAsTypedWithProb(const int *word, int wordLen) const {
            int mostProbLen = mMostProbableStringLength;
            int wordLenNoOmission = 0;

            const int *probString = mMostProbableString;
            bool isEqual = true;

            while (wordLen--) {
                if (!CharUtils::isIntentionalOmissionCodePoint(*word)) {
                    if(mostProbLen) {
                        if (*probString != *word) {
                            isEqual = false;
                        }
                        probString++;
                        mostProbLen--;
                    }
                    wordLenNoOmission++;
                }
                word++;
            }

            if(wordLenNoOmission != mMostProbableStringLength)
                return false;

            return isEqual;
        }

        AK_FORCE_INLINE bool existsCodePointInProximityAt(const int index, const int c) const {
            const int *codePoints = getProximityCodePointsAt(index);
            int i = 0;
            while (codePoints[i] > 0 && i < MAX_PROXIMITY_CHARS_SIZE) {
                if (codePoints[i++] == c) {
                    return true;
                }
            }
            return false;
        }

        AK_FORCE_INLINE bool existsAdjacentProximityChars(const int index) const {
            if (index < 0 || index >= mSampledInputSize) return false;
            const int currentCodePoint = getPrimaryCodePointAt(index);
            const int leftIndex = index - 1;
            if (leftIndex >= 0 && existsCodePointInProximityAt(leftIndex, currentCodePoint)) {
                return true;
            }
            const int rightIndex = index + 1;
            return rightIndex < mSampledInputSize
                   && existsCodePointInProximityAt(rightIndex, currentCodePoint);
        }

        inline bool touchPositionCorrectionEnabled() const {
            return mTouchPositionCorrectionEnabled;
        }

        bool isUsed() const {
            return mSampledInputSize > 0;
        }

        int size() const {
            return mSampledInputSize;
        }

        int getInputX(const int index) const {
            return mSampledInputXs[index];
        }

        int getInputY(const int index) const {
            return mSampledInputYs[index];
        }

        int getInputIndexOfSampledPoint(const int sampledIndex) const {
            return mSampledInputIndice[sampledIndex];
        }

        bool hasSpaceProximity(int index) const;

        int getLengthCache(const int index) const {
            return mSampledLengthCache[index];
        }

        bool isContinuousSuggestionPossible() const {
            return mIsContinuousSuggestionPossible;
        }

        int getImportantCharsCount() const {
            return mImportantCharsCount;
        }

        // TODO: Rename s/Length/NormalizedSquaredLength/
        float getPointToKeyByIdLength(int inputIndex, int keyId) const;

        // TODO: Rename s/Length/NormalizedSquaredLength/
        float getPointToKeyLength(int inputIndex, int codePoint) const;

        ProximityType getProximityType(int index, int codePoint,
                                       bool checkProximityChars, int *proximityIndex = 0) const;

        ProximityType getProximityTypeG(int index, int codePoint) const;

        float getSpeedRate(const int index) const {
            return mSpeedRates[index];
        }

        AK_FORCE_INLINE int getBeelineSpeedPercentile(const int id) const {
            return mBeelineSpeedPercentiles[id];
        }

        AK_FORCE_INLINE DoubleLetterLevel getDoubleLetterLevel(const int id) const {
            const int beelineSpeedRate = getBeelineSpeedPercentile(id);
            if (beelineSpeedRate == 0) {
                return A_STRONG_DOUBLE_LETTER;
            } else if (beelineSpeedRate
                       < ProximityInfoParams::MIN_DOUBLE_LETTER_BEELINE_SPEED_PERCENTILE) {
                return A_DOUBLE_LETTER;
            } else {
                return NOT_A_DOUBLE_LETTER;
            }
        }

        float getDirection(const int index) const {
            return mDirections[index];
        }

        // get xy direction
        float getDirection(int x, int y) const;

        float getMostProbableString(int *codePointBuf) const;

        void getMostProbableStringFromProb(int *codePointBuf) const;

        float getBestProbBetween(int startIndex, int keyIndex, bool isLogProb) const;

        float getBestProbBetweenRel(int startIndex, int endIndex, bool isLogProb, int keyIndex) const;

        float getAverageProbabilityAroundIndex(int topIndex, int codePointIndex,
                                               bool includeIndex) const;

        int getMostProbableStringLen() const;

        float getProbability(int index, int charIndex, bool logProb) const;

        int getMostProbableCharIndex(int index) const;

        bool isSkipIndex(int index) const;

        bool isKeyInSearchVectorAfterIndex(int index, int codepoint) const;

        int getMostProbableCodePoint(int index) const;

    private:
        DISALLOW_COPY_AND_ASSIGN(ProximityInfoState);

        inline const int *getProximityCodePointsAt(const int index) const {
            return ProximityInfoStateUtils::getProximityCodePointsAt(mInputProximities, index);
        }

        // const
        const ProximityInfo *mProximityInfo;
        float mMaxPointToKeyLength;
        float mAverageSpeed;
        bool mHasTouchPositionCorrectionData;
        int mMostCommonKeyWidthSquare;
        int mKeyCount;
        int mCellHeight;
        int mCellWidth;
        int mGridHeight;
        int mGridWidth;
        bool mIsContinuousSuggestionPossible;
        bool mHasBeenUpdatedByGeometricInput;

        std::vector<int> mSampledInputXs;
        std::vector<int> mSampledInputYs;
        std::vector<int> mSampledTimes;
        std::vector<int> mSampledInputIndice;
        std::vector<int> mSampledLengthCache;
        std::vector<int> mBeelineSpeedPercentiles;
        std::vector<float> mSampledNormalizedSquaredLengthCache;
        std::vector<float> mSpeedRates;
        std::vector<float> mDirections;
        // probabilities of skipping or mapping to a key for each point.
        std::vector<std::unordered_map<int, float>> mCharProbabilities;
        std::vector<std::unordered_map<int, float>> mOriginalCharProbabilities;

        //full data
        std::vector<std::unordered_map<int, float>> mFullCharProbabilities;
        std::vector<std::unordered_map<int, float>> mFullOriginalCharProbabilities;
        std::vector<int> mFullSampledInputXs;
        std::vector<int> mFullSampledInputYs;
        std::vector<ProximityInfoStateUtils::NearKeycodesSet> mFullSampledSearchKeySets;
        std::vector<std::vector<int>> mFullSampledSearchKeyVectors;
        int mFullSampledInputSize = 0;
        int mImportantCharsCount = 0;

        std::vector<int> mIndexToRelativeIndex;
        // The vector for the key code set which holds nearby keys of some trailing sampled input points
        // for each sampled input point. These nearby keys contain the next characters which can be in
        // the dictionary. Specifically, currently we are looking for keys nearby trailing sampled
        // inputs including the current input point.
        std::vector<ProximityInfoStateUtils::NearKeycodesSet> mSampledSearchKeySets;
        std::vector<std::vector<int>> mSampledSearchKeyVectors;
        bool mTouchPositionCorrectionEnabled;
        int mInputProximities[MAX_PROXIMITY_CHARS_SIZE * MAX_WORD_LENGTH];
        int mSampledInputSize = 0;
        int mPrimaryInputWord[MAX_WORD_LENGTH];
        float mMostProbableStringProbability;
        int mMostProbableString[MAX_WORD_LENGTH];
        int mMostProbableStringLength;

        int getImportantCharCount() const;
    };
} // namespace latinime
#endif // LATINIME_PROXIMITY_INFO_STATE_H
