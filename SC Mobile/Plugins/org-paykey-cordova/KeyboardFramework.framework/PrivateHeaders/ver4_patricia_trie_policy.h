/*
 * Copyright (C) 2013, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_VER4_PATRICIA_TRIE_POLICY_H
#define LATINIME_VER4_PATRICIA_TRIE_POLICY_H

#include <vector>

#include "defines.h"
#include "header_policy.h"
#include "dictionary_structure_with_buffer_policy.h"
#include "dynamic_pt_updating_helper.h"
#include "ver4_shortcut_list_policy.h"
#include "ver4_dict_buffers.h"
#include "ver4_patricia_trie_node_reader.h"
#include "ver4_patricia_trie_node_writer.h"
#include "ver4_patricia_trie_writing_helper.h"
#include "ver4_pt_node_array_reader.h"
#include "buffer_with_extendable_buffer.h"
#include "entry_counters.h"
#include "int_array_view.h"

namespace latinime {

class DicNode;
class DicNodeVector;

// Word id = Artificial id that is stored in the PtNode looked up by the word.
class Ver4PatriciaTriePolicy : public DictionaryStructureWithBufferPolicy {
 public:
    Ver4PatriciaTriePolicy(Ver4DictBuffers::Ver4DictBuffersPtr buffers)
            : mBuffers(std::move(buffers)), mHeaderPolicy(mBuffers->getHeaderPolicy()),
              mDictBuffer(mBuffers->getWritableTrieBuffer()),
              mShortcutPolicy(mBuffers->getMutableShortcutDictContent(),
                      mBuffers->getTerminalPositionLookupTable()),
              mNodeReader(mDictBuffer), mPtNodeArrayReader(mDictBuffer),
              mNodeWriter(mDictBuffer, mBuffers.get(), &mNodeReader, &mPtNodeArrayReader,
                      &mShortcutPolicy),
              mUpdatingHelper(mDictBuffer, &mNodeReader, &mNodeWriter),
              mWritingHelper(mBuffers.get()),
              mEntryCounters(mHeaderPolicy->getNgramCounts().getCountArray()),
              mTerminalPtNodePositionsForIteratingWords(), mIsCorrupted(false) {};

    AK_FORCE_INLINE int getRootPosition() const override {
        return 0;
    }

    void createAndGetAllChildDicNodes(const DicNode *dicNode,
            DicNodeVector *childDicNodes) const override;

    int getCodePointsAndReturnCodePointCount(int wordId, int maxCodePointCount,
            int *outCodePoints) const override;

    int getWordId(CodePointArrayView wordCodePoints, bool forceLowerCaseSearch) const override;

    const WordAttributes getWordAttributesInContext(WordIdArrayView prevWordIds,
                                                    int wordId, MultiBigramMap *multiBigramMap) const override;

    // TODO: Remove
    int getProbability(const int unigramProbability, const int bigramProbability) const override {
        // Not used.
        return NOT_A_PROBABILITY;
    }
    
    int getProbabilityOfWord(WordIdArrayView prevWordIds, int wordId, bool absolute) const override;

    void iterateNgramEntries(WordIdArrayView prevWordIds,
            NgramListener *listener) const override;

    BinaryDictionaryShortcutIterator getShortcutIterator(int wordId) const override;

    const DictionaryHeaderStructurePolicy *getHeaderStructurePolicy() const override {
        return mHeaderPolicy;
    }

    bool addUnigramEntry(CodePointArrayView wordCodePoints,
            const UnigramProperty *unigramProperty) override;

    bool removeUnigramEntry(CodePointArrayView wordCodePoints) override;

    bool addNgramEntry(const NgramProperty *ngramProperty) override;

    bool removeNgramEntry(const NgramContext *ngramContext,
                          CodePointArrayView wordCodePoints) override;

    bool updateEntriesForWordWithNgramContext(const NgramContext *ngramContext,
                                              CodePointArrayView wordCodePoints, bool isValidWord, bool isNotAWord,
                                              HistoricalInfo historicalInfo) override;

    bool updateShortcutsEntriesForWord(CodePointArrayView wordCodePoints,
                                       std::vector<UnigramProperty::ShortcutProperty> shortcuts) override;

    bool flush(const char *filePath) override;

    bool supportsShortcutsIndividualFrequency() const override;

    bool flushWithGC(const char *filePath) override;

    bool needsToRunGC(bool mindsBlockByGC) const override;

    void getProperty(const char *query, int queryLength, char *outResult,
                     int maxResultLength) override;

    bool isOffensive(CodePointArrayView wordCodePoints) const override;

    const WordProperty getWordProperty(CodePointArrayView wordCodePoints) const override;

    int getNextWordAndNextToken(int token, int *outCodePoints,
            int *outCodePointCount) override;

    bool isCorrupted() const override{
        return mIsCorrupted;
    }

 private:
    DISALLOW_IMPLICIT_CONSTRUCTORS(Ver4PatriciaTriePolicy);

    static const char *const UNIGRAM_COUNT_QUERY;
    static const char *const BIGRAM_COUNT_QUERY;
    static const char *const MAX_UNIGRAM_COUNT_QUERY;
    static const char *const MAX_BIGRAM_COUNT_QUERY;
    // When the dictionary size is near the maximum size, we have to refuse dynamic operations to
    // prevent the dictionary from overflowing.
    static const int MARGIN_TO_REFUSE_DYNAMIC_OPERATIONS;
    static const int MIN_DICT_SIZE_TO_REFUSE_DYNAMIC_OPERATIONS;

    const Ver4DictBuffers::Ver4DictBuffersPtr mBuffers;
    const HeaderPolicy *const mHeaderPolicy;
    BufferWithExtendableBuffer *const mDictBuffer;
    Ver4ShortcutListPolicy mShortcutPolicy;
    Ver4PatriciaTrieNodeReader mNodeReader;
    Ver4PtNodeArrayReader mPtNodeArrayReader;
    Ver4PatriciaTrieNodeWriter mNodeWriter;
    DynamicPtUpdatingHelper mUpdatingHelper;
    Ver4PatriciaTrieWritingHelper mWritingHelper;
    MutableEntryCounters mEntryCounters;
    std::vector<int> mTerminalPtNodePositionsForIteratingWords;
    mutable bool mIsCorrupted;

    int getShortcutPositionOfWord(int wordId) const;
};
} // namespace latinime
#endif // LATINIME_VER4_PATRICIA_TRIE_POLICY_H
