/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LATINIME_DICTIONARY_H
#define LATINIME_DICTIONARY_H

#include <memory>

#include "defines.h"
#include "dictionary_header_structure_policy.h"
#include "dictionary_structure_with_buffer_policy.h"
#include "ngram_listener.h"
#include "historical_info.h"
#include "word_property.h"
#include "suggest_interface.h"
#include "int_array_view.h"
#include "suggest.h"
#include "gesture_suggest_policy_factory.h"
#include "typing_suggest_policy_factory.h"

namespace latinime {

    class DictionaryStructureWithBufferPolicy;

    class DicTraverseSession;

    class NgramContext;

    class ProximityInfo;

    class SuggestionResults;

    class SuggestOptions;

    class Dictionary {
    public:
        // Taken from SuggestedWords.java
        static const int KIND_MASK_KIND = 0xFF; // Mask to get only the kind
        static const int KIND_TYPED = 0; // What user typed
        static const int KIND_CORRECTION = 1; // Simple correction/suggestion
        static const int KIND_COMPLETION = 2; // Completion (suggestion with appended chars)
        static const int KIND_WHITELIST = 3; // Whitelisted word
        static const int KIND_BLACKLIST = 4; // Blacklisted word
        static const int KIND_HARDCODED = 5; // Hardcoded suggestion, e.g. punctuation
        static const int KIND_APP_DEFINED = 6; // Suggested by the application
        static const int KIND_SHORTCUT = 7; // A shortcut
        static const int KIND_PREDICTION = 8; // A prediction (== a suggestion with no input)
        // KIND_RESUMED: A resumed suggestion (comes from a span, currently this type is used only
        // in java for re-correction)
        static const int KIND_RESUMED = 9;
        static const int KIND_OOV_CORRECTION = 10; // Most probable string correction

        static const int KIND_FLAG_POSSIBLY_OFFENSIVE = 0x80000000;
        static const int KIND_FLAG_EMOJI = 0x8000000;
        static const int KIND_FLAG_EXACT_MATCH = 0x40000000;
        static const int KIND_FLAG_EXACT_MATCH_WITH_INTENTIONAL_OMISSION = 0x20000000;
        static const int KIND_FLAG_APPROPRIATE_FOR_AUTOCORRECTION = 0x10000000;


        static Dictionary *createNewDictionary(DictionaryStructureWithBufferPolicy::StructurePolicyPtr
                                               dictionaryStructureWithBufferPolicy);

        void getSuggestions(ProximityInfo *proximityInfo, DicTraverseSession *traverseSession,
                            int *xcoordinates, int *ycoordinates, int *times, int *pointerIds, int *inputCodePoints,
                            int inputSize, const NgramContext *ngramContext,
                            const SuggestOptions *suggestOptions, float weightOfLangModelVsSpatialModel,
                            SuggestionResults *outSuggestionResults,
                            std::vector<int> *stopWordIds) const;

        void search(DicTraverseSession *traverseSession, const CodePointArrayView codePointArrayView,
                    const SuggestOptions *suggestOptions, SuggestionResults *outSuggestionResults) const;

        void getPredictions(const NgramContext *ngramContext,
                            SuggestionResults *outSuggestionResults, CodePointArrayView codePointArrayView) const;

        void getPredictions(const NgramContext *ngramContext, NgramListener *listener) const;

        int getProbability(CodePointArrayView codePoints) const;

        int getMaxProbabilityOfExactMatches(CodePointArrayView codePoints) const;

        bool addUnigramEntry(CodePointArrayView codePoints, const UnigramProperty *unigramProperty);

        int getNgramProbability(const NgramContext *ngramContext, CodePointArrayView codePoints) const;

        bool removeUnigramEntry(CodePointArrayView codePoints);

        bool addNgramEntry(const NgramProperty *ngramProperty);

        bool removeNgramEntry(const NgramContext *ngramContext,
                              CodePointArrayView codePoints);

        bool updateEntriesForWordWithNgramContext(const NgramContext *ngramContext,
                                                  CodePointArrayView codePoints, bool isValidWord, bool isNotAWord,
                                                  HistoricalInfo historicalInfo);

        bool updateShortcutsEntriesForWord(CodePointArrayView wordCodePoints,
                                           std::vector<UnigramProperty::ShortcutProperty> shortcuts);

        bool flush(const char *filePath);

        bool flushWithGC(const char *filePath);

        bool needsToRunGC(bool mindsBlockByGC);

        const bool isOffensive(CodePointArrayView codePoints);

        const WordProperty getWordProperty(CodePointArrayView codePoints);

        // Method to iterate all words in the dictionary.
        // The returned token has to be used to get the next word. If token is 0, this method newly
        // starts iterating the dictionary.
        int getNextWordAndNextToken(int token, int *outCodePoints,
                                    int *outCodePointCount);

        const DictionaryStructureWithBufferPolicy *getDictionaryStructurePolicy() const {
            return mDictionaryStructureWithBufferPolicy.get();
        }

    private:
        DISALLOW_IMPLICIT_CONSTRUCTORS(Dictionary);

        typedef std::unique_ptr<SuggestInterface> SuggestInterfacePtr;

        Dictionary(DictionaryStructureWithBufferPolicy::StructurePolicyPtr
                   dictionaryStructureWithBufferPolicy, const SuggestPolicy *typingSuggestPolicy,
                   const SuggestPolicy *gestureTypingPolicy);

        class NgramListenerForPrediction : public NgramListener {
        public:
            NgramListenerForPrediction(const NgramContext *ngramContext,
                                       WordIdArrayView prevWordIds, SuggestionResults *suggestionResults,
                                       const DictionaryStructureWithBufferPolicy *dictStructurePolicy,
                                       const int *prefixCodePoints,
                                       int prefixSize);

            void onVisitEntry(int ngramProbability, int targetWordId) override;


        private:
            DISALLOW_IMPLICIT_CONSTRUCTORS(NgramListenerForPrediction);

            const NgramContext *const mNgramContext;
            const WordIdArrayView mPrevWordIds;
            SuggestionResults *const mSuggestionResults;
            const DictionaryStructureWithBufferPolicy *const mDictStructurePolicy;
            const int *mPrefixCodePoints;
            int mPrefixSize;
        };


        class NgramListenerForSuggestion : public NgramListener {
        public:
            NgramListenerForSuggestion(const NgramContext *ngramContext,
                                       WordIdArrayView prevWordIds, SuggestionResults *suggestionResults,
                                       const DictionaryStructureWithBufferPolicy *dictStructurePolicy,
                                       int *prefixCodePoints,
                                       int prefixSize);

            void onVisitEntry(int ngramProbability, int targetWordId) override;


        private:
            DISALLOW_IMPLICIT_CONSTRUCTORS(NgramListenerForSuggestion);

            const NgramContext *const mNgramContext;
            const WordIdArrayView mPrevWordIds;
            SuggestionResults *const mSuggestionResults;
            const DictionaryStructureWithBufferPolicy *const mDictStructurePolicy;
            int *mPrefixCodePoints;
            int mPrefixSize;
        };

        static const int HEADER_ATTRIBUTE_BUFFER_SIZE;

        const DictionaryStructureWithBufferPolicy::StructurePolicyPtr
                mDictionaryStructureWithBufferPolicy;
        const SuggestInterfacePtr mGestureSuggest;
        const SuggestInterfacePtr mTypingSuggest;
        const SuggestInterfacePtr mSearchSuggest;

    public:
        char *logDictionaryInfo(/*JNIEnv *const env*/) const;
    };
} // namespace latinime
#endif // LATINIME_DICTIONARY_H
