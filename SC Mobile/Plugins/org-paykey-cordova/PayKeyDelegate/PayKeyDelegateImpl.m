//
//  HSBCPayKeyDelegateImpl.m
//  Generic_Example
//
//  Created by Eran Israel on 12/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import "PayKeyDelegateImpl.h"
#import "LoginKBFlow.h"
#import "Request.h"
#import <Contacts/Contacts.h>

@import PayKeyUI;

@interface APITemplate : PKCallbackMethodSignatureTemplate<SCTWDelegate>
@end

@implementation APITemplate

- (void)onFlowStart {}
- (void)onFlowStop {}
- (void)loginKBWithLanguage:(NSString*)language andwWithCompletion:(nonnull SCTWBalanceCompletionBlock)completion {}
- (void)logoutKBWithIsValidateFail:(BOOL)isValidateFail {}
- (void)getKBOutAcctBalance:(nonnull SCTWBalanceCompletionBlock)completion {}
- (void)pageUserTrackWithPageTitle:(PageOptions) pageTitle andWithPageUrl:(PageOptions) pageUrl {}
- (void)isAvailableWithCompletion:(nonnull SCTWIsAvailableCompletionBlock)completion {}
- (void)getTxFavorAccListWithCompletion:(nonnull SCTWFavAccountsInfoCompletionBlock) completion {}
- (void)getDefaultCoverWithDefaultCoverList:(nullable NSArray<__kindof SCTWDefaultCover*> *)coverList andWithCompletion:(nonnull SCTWDefaultCoverResponse)completion {}
- (void)getNotesWithFunctionId:(NSString*) functionID andWithCompletion:(nonnull SCTWNotesResponse)completion {}
- (void)getBanksCodesWithCompletion:(nonnull SCTWBankCodesResponse) completion {}
- (void)preSendSingleRedEnvWithRequest:(SCTWPreSendRequest*) request withCompletion:(nonnull SCTWPreSendResponseCompletionBlock) completion {}
- (void)confirmRedEnvDetailWithPageCheckId:(NSString*)pageCheckId AndWithValidationType:(NSString*)validationType andWithCompletion:(SCTWConfirmRedEnvResponseCompletionBlock)completion {}
- (void)validateIdentityWithPageCheckId:(NSString*)pageCheckId andWithValidationCode:(NSString*)validationCode andWithCompletion:(SCTWValidateIdentityDataCompletionBlock)completion {}
- (void)sendRedEnvResendSmsOtp:(SCTWResendSmsOTPDataCompletionBlock) completion {}
- (void)getRandomListWithTotalAmount:(SCTWRandomListRequest*)request WithCompletion:(SCTWRandomListCompletionBlock)completion {}
- (void)preSendGroupRedEnvWithRequest:(SCTWPreSendGroupRequest*)request WithCompletion:(SCTWPreSendGroupRersponseCompletionBlock) completion {}
- (void)redEnvelopeRecInqWithRequest:(SCTWRedEnvelopeRecInqRequest*)request AndWithCompletion:(SCTWRedEnvelopeRecInqDataCompletionBlock)completion {}
- (void)desendRedEnvWithRequest:(SCTWDesendRedEnvRequest*)request WithCompletion:(SCTWDesendRedEnvDataCompletionBlock) completion {}
- (void)desendRedEnvResendSmsOtpWithCpmpletion:(SCTWResendSmsOTPDataCompletionBlock)completion {}
- (void)desendValidateIdentityWithRequest:(SCTWDesendValidateIdentityRequest*)request WithCompletion:(SCTWDesendValidateResponseCompletionBlock)completion {}
- (void)getForeignExchangeWithCompletion:(SCTWForeignExchangeResponseCompletionBlock)completion {}
- (void)getWhatsNewWithCompletion:(SCTWWhatsNewResponse)completion {}
- (void)BiometricAuthenticationCompletionBlock:(nonnull BiometricAuthenticationCompletionBlock)completion {}
- (void)addContacts:(NSArray<__kindof PKContact *> *)contactList {}
- (void)getContactsWithCompletion:(nonnull SCTWContactsResponse)completion {}
- (void)resendPWDSmsWithID:(NSString *)redEnvId andResponse:(nonnull SCTWResendPWDSMSResponse)completion {}
- (void)addContacts:(NSArray<__kindof PKContact *> *)contactList WithCompletion:(nonnull SCTWAddContactResponse)completion {}

@end

@interface PayKeyDelegateImpl() <PKContactImageFetcher>
{
    LoginKBFlow *loginKBFlow;
}
@property (strong, nonatomic) UIImage * contactImage;
@property (assign, nonatomic) BOOL hasImage;

@property (strong,nonatomic) id implementor;
@end

@implementation PayKeyDelegateImpl

- (instancetype)init {
    if (self = [super init]) {
        self.dispatcher.signatureTemplate = [APITemplate new];
        self.localAuthenticator = [[PKLocalAuthenticator alloc] initWithReason:@"Enter your Pincode" fallback:@"Enter Pincode"];
        self.implementor = [PKDefaultImplementor new];
    }
    return self;
}

- (void)onFlowStart {
    NSLog(@"PayKeyDelegateImpl.onFlowStart");
}
- (void)onFlowStop {
    
    [Request apiFetcherWithCompletion:[Request getSignatureDic:nil BodyValue:nil JwtToken:[Request getPliststring:@"jwtToken"] AesKey:[Request getAeskeyString] GMElliptic:[Request cryptob]] Api:@"rest/keyboardBanking/logoutKeyboardBanking" Completion:^(NSDictionary *dict, NSError *error) {
        
    }];
}

-(void)loginKBWithLanguage:(NSString *)language andwWithCompletion:(SCTWBalanceCompletionBlock)completion {
    loginKBFlow = [[LoginKBFlow alloc]init];
    [loginKBFlow executeLoginAction];
    __block PayKeyDelegateImpl *weakSelf = self;
    loginKBFlow.loginResultBlock = ^(BOOL isSuccess,NSDictionary *jsonDict) {
        if (isSuccess) {
            // handle success event
            SCTWBalanceData * balance = [[SCTWBalanceData alloc]init];
            NSDictionary * bodyDic = [jsonDict dictionaryForKey:@"body"];
            
            balance.accountBalance = [Request aesGcm:[bodyDic stringForKey:@"acctBalance"] AesKey:[Request getAeskeyString]];
            balance.accountBalanceFormat = [Request aesGcm:[bodyDic stringForKey:@"acctBalanceFormat"] AesKey:[Request getAeskeyString]];
        
            completion(nil,balance);
        } else {
            
            // relogin
            // Herer is recursively method, use it carefully
            [weakSelf->loginKBFlow executeLoginAction];
            
            PKError * pkerror = [[PKError  alloc]init];
            pkerror.title = [jsonDict stringForKey:@"title"];
            pkerror.message = [jsonDict stringForKey:@"message"];
            pkerror.code = [jsonDict stringForKey:@"code"];
            pkerror.userInfo = [jsonDict dictionaryForKey:@"userInfo"];
            
            completion(pkerror,nil);
        }
    };
}

- (void)logoutKBWithIsValidateFail:(BOOL)isValidateFail{
    
    __block BOOL aisValidateFail;
    
    aisValidateFail = isValidateFail;
    
    [Request apiFetcherWithCompletion:[Request getSignatureDic:nil BodyValue:nil JwtToken:[Request getPliststring:@"jwtToken"] AesKey:[Request getAeskeyString] GMElliptic:[Request cryptob]] Api:@"rest/keyboardBanking/logoutKeyboardBanking" Completion:^(NSDictionary *dict, NSError *error) {
        NSDictionary * bodyDic = [dict dictionaryForKey:@"body"];
        aisValidateFail = [bodyDic boolForKey:@"success"];
        
    }];
}

- (void)getKBOutAcctBalance:(nonnull SCTWBalanceCompletionBlock)completion {
    
    [Request apiFetcherWithCompletion:[Request getSignatureDic:nil BodyValue:nil JwtToken:[Request getPliststring:@"jwtToken"] AesKey:[Request getAeskeyString] GMElliptic:[Request cryptob]] Api:@"rest/keyboardBanking/getKBOutAcctBalance" Completion:^(NSDictionary *dict, NSError *error) {
        
        if (dict) {
            NSDictionary * bodyDic = [dict dictionaryForKey:@"body"];
            
            SCTWBalanceData * balance = [[SCTWBalanceData alloc]init];
            balance.accountBalance = [Request aesGcm:[bodyDic stringForKey:@"accountBalance"] AesKey:[Request getAeskeyString]];
            balance.accountBalanceFormat = [Request aesGcm:[bodyDic stringForKey:@"accountBalanceFormat"] AesKey:[Request getAeskeyString]];
            completion(nil,balance);
        }else{
            PKError * pkerror = [[PKError  alloc]initWithTitle:@"Error" message:error.localizedDescription code:[NSString stringWithFormat:@"%ld",(long)error.code]];
            completion(pkerror,nil);
        }
    }];
}

- (void)pageUserTrackWithPageTitle:(PageOptions)pageTitle andWithPageUrl:(PageOptions)pageUrl{
    /*
     android是這樣 public void pageUserTrack(String pageTitle, String pageUrl, final CompletionCallback<Void> completion)-有爭議
     */
    
    
    NSArray * bodyKeys = @[@"fundId",@"pageTitle",@"pageUrl"];
    NSArray * bodyValue = @[@"",[NSString stringWithFormat:@"%ld",(long)pageTitle],[NSString stringWithFormat:@"%ld",(long)pageUrl]];
    
    [Request apiFetcherWithCompletion:[Request getSignatureDic:bodyKeys BodyValue:bodyValue JwtToken:[Request getPliststring:@"jwtToken"] AesKey:[Request getAeskeyString] GMElliptic:[Request cryptob]] Api:@"rest/common/pageUserTrack" Completion:^(NSDictionary *dict, NSError *error) {
        if (error) {
            NSLog(@"%@",error);
        }
        
    }];
}

- (void)isAvailableWithCompletion:(nonnull SCTWIsAvailableCompletionBlock)completion {
    
    [Request apiFetcherWithCompletion:[Request getSignatureDic:nil BodyValue:nil JwtToken:[Request getPliststring:@"jwtToken"] AesKey:[Request getAeskeyString] GMElliptic:[Request cryptob]] Api:@"rest/keyboardBanking/checkFunctionAvailable" Completion:^(NSDictionary *dict, NSError *error) {
        if (dict) {
            NSDictionary * bodyDic = [dict dictionaryForKey:@"body"];
            SCTWIsAvailableData * isAvailableData = [SCTWIsAvailableData new];
            NSDictionary * easyTransferAvailableRslt = [bodyDic dictionaryForKey:@"easyTransferAvailableRslt"];
            //isAvailableData.easyTransferAvailable = [easyTransferAvailableRslt boolForKey:@"available"];
            isAvailableData.easyTransferAvailable = YES;
            isAvailableData.message = [easyTransferAvailableRslt stringForKey:@"message"];
            completion(nil,isAvailableData);
        }else{
            PKError * pkerror = [[PKError  alloc]initWithTitle:@"Error" message:error.localizedDescription code:[NSString stringWithFormat:@"%ld",(long)error.code]];
            completion(pkerror,nil);
        }
    }];
    
}

- (void)getTxFavorAccListWithCompletion:(nonnull SCTWFavAccountsInfoCompletionBlock) completion {
    
    //enum (2:台幣非約定常用帳號, 3:常用信用卡繳款編號)
    NSArray * bodyKeys = @[@"txType"];
    NSArray * bodyValue = @[@"2"];
    
    [Request apiFetcherWithCompletion:[Request getSignatureDic:bodyKeys BodyValue:bodyValue JwtToken:[Request getPliststring:@"jwtToken"] AesKey:[Request getAeskeyString] GMElliptic:[Request cryptob]] Api:@"rest/common/common0008" Completion:^(NSDictionary *dict, NSError *error) {
        if (dict) {
            NSDictionary * bodyDic = [dict dictionaryForKey:@"body"];
            NSArray * acctInfos = [bodyDic arrayForKey:@"acctInfos"];
            NSMutableArray * addArray = [[NSMutableArray alloc]init];
            for (int i = 0; i < acctInfos.count; i++) {
                SCTWAccountInfo * accountInfo = [SCTWAccountInfo new];
                accountInfo.acctFmt = acctInfos[i][@"acctFmt"];
                accountInfo.accNo = acctInfos[i][@"acctNo"];
                accountInfo.bankCode = acctInfos[i][@"bankCode"];
                [addArray addObject:accountInfo];
            }
            
            NSArray * arrayInfos = [addArray mutableCopy];
            completion(nil,arrayInfos);
        }else{
            PKError * pkerror = [[PKError  alloc]initWithTitle:@"Error" message:error.localizedDescription code:[NSString stringWithFormat:@"%ld",(long)error.code]];
            completion(pkerror,nil);
        }
        
    }];
}

- (void)getDefaultCoverWithDefaultCoverList:(nullable NSArray<__kindof SCTWDefaultCover*> *)coverList andWithCompletion:(nonnull SCTWDefaultCoverResponse)completion {
    
    NSArray * bodyKeys = @[@"defaultCoverList"];
    NSArray * bodyValue = nil;
    
    if (coverList.count > 0) {
        NSMutableArray * defaultCoverArray = [[NSMutableArray alloc]init];
        for (SCTWDefaultCover * defaultCover in coverList) {
            NSMutableDictionary * dic = [[NSMutableDictionary alloc]init];
            [dic setValue:defaultCover.defaultCover forKey:@"defaultCover"];
            [dic setValue:defaultCover.coverID forKey:@"id"];
            [dic setValue:defaultCover.updateDateTime forKey:@"updateDateTime"];
            [defaultCoverArray addObject:dic];
        }
        NSArray * coverArray = [defaultCoverArray mutableCopy];
        bodyValue = @[coverArray];
    }
    
    [Request apiFetcherWithCompletion:[Request getSignatureDic:bodyKeys BodyValue:bodyValue JwtToken:[Request getPliststring:@"jwtToken"] AesKey:[Request getAeskeyString] GMElliptic:[Request cryptob]] Api:@"rest/keyboardBanking/getDefaultCover" Completion:^(NSDictionary *dict, NSError *error) {
        if (dict) {
            NSDictionary * bodyDic = [dict dictionaryForKey:@"body"];
            NSArray * defaulArray = [bodyDic arrayForKey:@"defaultCoverList"];
            NSMutableArray * addArray = [[NSMutableArray alloc]init];
            for (int i = 0; i < defaulArray.count; i++) {
                SCTWDefaultCover * defaultCover = [SCTWDefaultCover new];
                defaultCover.defaultCover = defaulArray[i][@"defaultCover"];
                NSNumber *conerid = [NSNumber numberWithInteger:[defaulArray[i][@"id"] integerValue]];
                defaultCover.coverID = conerid;
                defaultCover.updateDateTime = defaulArray[i][@"updateDateTime"];
                [addArray addObject:defaultCover];
            }
            
            NSArray * arraySc = [addArray mutableCopy];
            completion(nil,arraySc,[bodyDic boolForKey:@"renew"]);
        }else{
            PKError * pkerror = [[PKError  alloc]initWithTitle:@"Error" message:error.localizedDescription code:[NSString stringWithFormat:@"%ld",(long)error.code]];
            completion(pkerror,nil,nil);
        }
        
    }];
}

- (void)getNotesWithFunctionId:(NSString*) functionID andWithCompletion:(nonnull SCTWNotesResponse)completion {
    
    NSArray * bodyKeys = @[@"functionId"];
    NSArray * bodyValue = @[functionID];
    
    [Request apiFetcherWithCompletion:[Request getSignatureDic:bodyKeys BodyValue:bodyValue JwtToken:[Request getPliststring:@"jwtToken"] AesKey:[Request getAeskeyString] GMElliptic:[Request cryptob]] Api:@"rest/common/getNotes" Completion:^(NSDictionary *dict, NSError *error) {
        if (dict) {
            NSDictionary * bodyDic = [dict dictionaryForKey:@"body"];
            completion(nil,[bodyDic stringForKey:@"notes"]);
        }else{
            PKError * pkerror = [[PKError  alloc]initWithTitle:@"Error" message:error.localizedDescription code:[NSString stringWithFormat:@"%ld",(long)error.code]];
            completion(pkerror,nil);
        }
        
    }];
}

- (void)getBanksCodesWithCompletion:(nonnull SCTWBankCodesResponse) completion {
    [Request apiFetcherWithCompletion:[Request getSignatureDic:nil BodyValue:nil JwtToken:[Request getPliststring:@"jwtToken"] AesKey:[Request getAeskeyString] GMElliptic:[Request cryptob]] Api:@"rest/common/common0007" Completion:^(NSDictionary *dict, NSError *error) {
        
        if (dict) {
            
            NSDictionary * bodyDic = [dict dictionaryForKey:@"body"];
            NSArray * dictArray = [bodyDic arrayForKey:@"bankInfos"];
            NSMutableArray * addArray = [[NSMutableArray alloc]init];
            for (int i = 0; i < dictArray.count; i++) {
                SCTWBankInfo * bankInfo = [SCTWBankInfo new];
                bankInfo.bankCode = dictArray[i][@"bankCode"];
                bankInfo.bankName = dictArray[i][@"bankName"];
                
                [addArray addObject:bankInfo];
            }
            NSArray * bankArray = [addArray mutableCopy];
            completion(nil,bankArray);
        }else{
            PKError * pkerror = [[PKError  alloc]initWithTitle:@"Error" message:error.localizedDescription code:[NSString stringWithFormat:@"%ld",(long)error.code]];
            completion(pkerror,nil);
        }

    }];
}

- (void)preSendSingleRedEnvWithRequest:(SCTWPreSendRequest*)request withCompletion:(nonnull SCTWPreSendResponseCompletionBlock) completion {
    
    NSString * amount = request.amount == nil ? @"" : [request.amount stringValue];
    NSString * greetings = request.greetings == nil ? @"" : request.greetings;
    NSString * inAcctNo = request.inAcctNo == nil ? @"" : request.inAcctNo;;
    NSString * inBankCode = request.inBankCode == nil ? @"" : request.inBankCode;

    NSArray * bodyKeys = @[@"amount",@"greetings",@"inAcctNo",@"inBankCode"];
    NSArray * bodyValue =@[amount,greetings,inAcctNo,inBankCode];
    
    if (request.inAcctType == 3){
        
        NSMutableDictionary * dic = [[NSMutableDictionary alloc]init];
        NSMutableDictionary * reciverdic = [[NSMutableDictionary alloc]init];
        [reciverdic setObject:request.reciver.givenName forKey:@"inName"];
        [reciverdic setObject:request.reciver.phones[0].phoneNumber forKey:@"inPhoneNum"];
        [dic setObject:reciverdic forKey:@"KBReceiver"];
        
        bodyKeys = @[@"amount",@"greetings",@"inAcctNo",@"inBankCode",@"receiver"];
        bodyValue =@[amount,greetings,inAcctNo,inBankCode,dic];
    }
    
    [Request apiFetcherWithCompletion:[Request getSignatureDic:bodyKeys BodyValue:bodyValue JwtToken:[Request getPliststring:@"jwtToken"] AesKey:[Request getAeskeyString] GMElliptic:[Request cryptob]] Api:@"rest/keyboardBanking/preSendSingleRedEnv" Completion:^(NSDictionary *dict, NSError *error) {
        if (dict) {
            NSDictionary * bodyDic = [dict dictionaryForKey:@"body"];
            SCTWPreSendResponse * sctwpreSendResponse = [SCTWPreSendResponse new];
            sctwpreSendResponse.amount = [bodyDic stringForKey:@"amount"];
            sctwpreSendResponse.greetings = [bodyDic stringForKey:@"greetings"];
            sctwpreSendResponse.pageCheckID = [bodyDic stringForKey:@"pageCheckId"];
            completion(nil,sctwpreSendResponse);
        }else{
            PKError * pkerror = [[PKError  alloc]initWithTitle:@"Error" message:error.localizedDescription code:[NSString stringWithFormat:@"%ld",(long)error.code]];
            completion(pkerror,nil);
        }
        
    }];
   
}

- (void)confirmRedEnvDetailWithPageCheckId:(NSString*)pageCheckId AndWithValidationType:(NSString*)validationType andWithCompletion:(SCTWConfirmRedEnvResponseCompletionBlock)completion {
    
    NSArray * bodyKeys = @[@"pageCheckId",@"validationType"];
    NSArray * bodyValue = @[pageCheckId,validationType];
    
    [Request apiFetcherWithCompletion:[Request getSignatureDic:bodyKeys BodyValue:bodyValue JwtToken:[Request getPliststring:@"jwtToken"] AesKey:[Request getAeskeyString] GMElliptic:[Request cryptob]] Api:@"rest/keyboardBanking/confirmRedEnvDetail" Completion:^(NSDictionary *dict, NSError *error) {
        if (dict) {
            NSDictionary * bodyDic = [dict dictionaryForKey:@"body"];
            SCTWConfirmRedEnvResponse * sctwconfirmRedEnvResponse = [SCTWConfirmRedEnvResponse new];
            sctwconfirmRedEnvResponse.pageCheckId = [bodyDic stringForKey:@"pageCheckId"];
            sctwconfirmRedEnvResponse.phoneNum = [bodyDic stringForKey:@"phoneNum"];
            sctwconfirmRedEnvResponse.validationType = [bodyDic stringForKey:@"validationType"];
            sctwconfirmRedEnvResponse.webId = [bodyDic stringForKey:@"webId"];
            completion(nil,sctwconfirmRedEnvResponse);
        }else{
            PKError * pkerror = [[PKError  alloc]initWithTitle:@"Error" message:error.localizedDescription code:[NSString stringWithFormat:@"%ld",(long)error.code]];
            completion(pkerror,nil);
        }
        
    }];

}

- (void)validateIdentityWithPageCheckId:(NSString*)pageCheckId andWithValidationCode:(NSString*)validationCode andWithCompletion:(SCTWValidateIdentityDataCompletionBlock)completion {
    
    NSArray * bodyKeys = @[@"pageCheckId",@"validationType"];
    NSArray * bodyValue = @[pageCheckId,validationCode];
    
    [Request apiFetcherWithCompletion:[Request getSignatureDic:bodyKeys BodyValue:bodyValue JwtToken:[Request getPliststring:@"jwtToken"] AesKey:[Request getAeskeyString] GMElliptic:[Request cryptob]] Api:@"rest/keyboardBanking/validateIdentity" Completion:^(NSDictionary *dict, NSError *error) {
        
        if (dict) {
            NSDictionary * bodyDic = [dict dictionaryForKey:@"body"];
            SCTWValidateIdentityData * sctwValidateIdentityData = [SCTWValidateIdentityData new];
            sctwValidateIdentityData.message = [bodyDic stringForKey:@"message"];
            
            completion(nil,sctwValidateIdentityData);
        }else{
            PKError * pkerror = [[PKError  alloc]initWithTitle:@"Error" message:error.localizedDescription code:[NSString stringWithFormat:@"%ld",(long)error.code]];
            completion(pkerror,nil);
        }
    }];
    
}

- (void)sendRedEnvResendSmsOtp:(SCTWResendSmsOTPDataCompletionBlock) completion {
    
    [Request apiFetcherWithCompletion:[Request getSignatureDic:nil BodyValue:nil JwtToken:[Request getPliststring:@"jwtToken"] AesKey:[Request getAeskeyString] GMElliptic:[Request cryptob]] Api:@"rest/keyboardBanking/sendRedEnvResendSmsOtp" Completion:^(NSDictionary *dict, NSError *error) {
       
        if (dict) {
            NSDictionary * bodyDic  = [dict dictionaryForKey:@"body"];
            SCTWResendSmsOTPData * sctwResendSmsOTPData = [SCTWResendSmsOTPData new];
            sctwResendSmsOTPData.mobilePhone = [bodyDic stringForKey:@"mobilePhone"];
            sctwResendSmsOTPData.pageCheckId = [bodyDic stringForKey:@"pageCheckId"];
            sctwResendSmsOTPData.webId = [bodyDic stringForKey:@"webId"];
            
            completion(nil,sctwResendSmsOTPData);
        }else{
            PKError * pkerror = [[PKError  alloc]initWithTitle:@"Error" message:error.localizedDescription code:[NSString stringWithFormat:@"%ld",(long)error.code]];
            completion(pkerror,nil);
        }
        
    }];
    
}

//隨機輕鬆轉
- (void)getRandomListWithTotalAmount:(SCTWRandomListRequest*)request WithCompletion:(SCTWRandomListCompletionBlock)completion {
    
    NSMutableArray * moneyArray = [[NSMutableArray alloc]init];
    int totalNum = [request.totalNum intValue];
    int totalAmount = [request.totalAmount intValue];
    int restMoney = [request.totalAmount intValue];
    
    for (int i = 0; i < totalNum; i++) {
        //前提：人數>金額時要擋掉；金額=人數時，直接全部分一塊錢
        if (totalNum > totalAmount) {
            //toast.show();秀出alert提醒人數金額有誤
            break;
        } else if (totalNum == totalAmount) {
            [moneyArray addObject:@"1"];
            continue;
        }
        //如果已是最後一步，直接給值
        if (i == totalNum - 1) {//如果最後一個值，尾數剛好是4，必須抓回前一個數重新分配
            if (((restMoney % 10) == 4)) {
                restMoney = restMoney + [moneyArray[totalNum - 2]intValue];//取回上一個值加回去
                i = i - 2;//從倒數第二個值重抓
                continue;
            } else {
                //!發錢!
                [moneyArray replaceObjectAtIndex:i withObject:[NSString stringWithFormat:@"%d",restMoney]];
            }
        } else {
            int randomMoney = (arc4random() % restMoney);//取得隨機金額，並設定取值範圍
            //挑出以下情況重來，避免在中途金額已發完，導致有人取到0元或crash
            //1: 直接取到了最大值
            //2: 餘額只剩1塊
            //3: 餘額小於尚未領取的人數(ex:剩3塊，還有4人未領)
            if ((randomMoney == restMoney) ||(restMoney - randomMoney == 1 ||restMoney - randomMoney < totalNum - (i - 1))) {
                    i--;//重來
                } else {
                    if (randomMoney == 0) {
                        i--;//如果取到0，就重來
                        continue;
                    }
                    if (((randomMoney % 10)) == 4) {
                        i--;//尾數取到4的話就重來
                    } else {
                        [moneyArray replaceObjectAtIndex:i withObject:[NSString stringWithFormat:@"%d",randomMoney]];//!發錢!
                        restMoney = restMoney - randomMoney;//處理餘額，下次隨機金額的範圍調整
                    }
                }
        }
    }//for end

    if (moneyArray.count > 0) {
        NSArray *sortedArray = [[moneyArray mutableCopy] sortedArrayUsingSelector:@selector(compare:)];
        SCTWRandomListResponse * sctwRandomListResponse = [SCTWRandomListResponse new];
        sctwRandomListResponse.randomList = [moneyArray mutableCopy];
        sctwRandomListResponse.maxAmt = [sortedArray lastObject];
        sctwRandomListResponse.minAmt = sortedArray[0];
        completion(nil,sctwRandomListResponse);
    }
}


- (void)preSendGroupRedEnvWithRequest:(SCTWPreSendGroupRequest*)request WithCompletion:(SCTWPreSendGroupRersponseCompletionBlock) completion {
    
    NSArray * bodyKeys = @[@"amountType",@"amtList",@"coverType",@"fixAmt",@"greetings",@"maxAmt",@"minAmt",@"totalAmount",@"totalNum",@"uploadCover"];
    NSArray * bodyValue = @[request.amountType,@"amtList",@"coverType",request.fixAmt,request.greetings,request.maxAmt,request.minAmt,request.totalAmount,[NSString stringWithFormat:@"%ld",(long)request.totalNum],request.uploadCover];
    
    [Request apiFetcherWithCompletion:[Request getSignatureDic:bodyKeys BodyValue:bodyValue JwtToken:[Request getPliststring:@"jwtToken"] AesKey:[Request getAeskeyString] GMElliptic:[Request cryptob]] Api:@"rest/keyboardBanking/preSendGroupRedEnv" Completion:^(NSDictionary *dict, NSError *error) {
        
        if (dict) {
            NSDictionary * bodyDic  = [dict dictionaryForKey:@"body"];
            NSArray * dictArray = [bodyDic arrayForKey:@"amtList"];;
            NSMutableArray * addArray = [[NSMutableArray alloc]init];
            
            for (NSString * string in dictArray) {
                NSNumberFormatter *tempNum = [[NSNumberFormatter alloc] init];
                [tempNum setNumberStyle:NSNumberFormatterDecimalStyle];
                NSNumber *numTemp = [tempNum numberFromString:string];
                [addArray addObject:numTemp];
            }
            NSArray * amtList = [addArray mutableCopy];

            SCTWPreSendGroupResponse * sctwPreSendGroupResponse = [SCTWPreSendGroupResponse new];
            sctwPreSendGroupResponse.greetings = [bodyDic stringForKey:@"greetings"];
            sctwPreSendGroupResponse.pageCheckId = [bodyDic stringForKey:@"pageCheckId"];
            sctwPreSendGroupResponse.validationCode = [bodyDic stringForKey:@"validationCode"];
            sctwPreSendGroupResponse.amountType = [bodyDic stringForKey:@"amountType"];
            sctwPreSendGroupResponse.amtList = amtList;
            sctwPreSendGroupResponse.fixAmt = [bodyDic stringForKey:@"fixAmt"];
            sctwPreSendGroupResponse.minAmt = [bodyDic stringForKey:@"minAmt"];
            sctwPreSendGroupResponse.maxAmt = [bodyDic stringForKey:@"maxAmt"];
            sctwPreSendGroupResponse.totalAmount = [bodyDic stringForKey:@"totalAmount"];
            sctwPreSendGroupResponse.totalNum = [bodyDic integerForKey:@"totalNum"];
            
            completion(nil,sctwPreSendGroupResponse);
            
        }else{
            PKError * pkerror = [[PKError  alloc]initWithTitle:@"Error" message:error.localizedDescription code:[NSString stringWithFormat:@"%ld",(long)error.code]];
            completion(pkerror,nil);
        }
        
    }];
}


- (void)redEnvelopeRecInqWithRequest:(SCTWRedEnvelopeRecInqRequest *)request AndWithCompletion:(SCTWRedEnvelopeRecInqDataCompletionBlock)completion {
    
    NSArray * bodyKeys = @[@"pageIndex",@"type"];
    NSArray * bodyValue =@[[NSString stringWithFormat:@"%ld",(long)request.pageIndex],request.type];
    
    [Request apiFetcherWithCompletion:[Request getSignatureDic:bodyKeys BodyValue:bodyValue JwtToken:[Request getPliststring:@"jwtToken"] AesKey:[Request getAeskeyString] GMElliptic:[Request cryptob]] Api:@"rest/keyboardBanking/redEnvelopeRecInq" Completion:^(NSDictionary *dict, NSError *error) {
        if (dict) {
            NSDictionary * bodyDic  = [dict dictionaryForKey:@"body"];
            SCTWRedEnvelopeRecInqData * sctwRedEnvelopeRecInqData = [SCTWRedEnvelopeRecInqData new];
            sctwRedEnvelopeRecInqData.groupRedEnvRecList = [self getGroupRedEnvRecList:[bodyDic arrayForKey:@"groupRedEnvRecList"]];
            sctwRedEnvelopeRecInqData.singleRedenvRecList =[self getRedEnvelopeDetailList:[bodyDic arrayForKey:@"singleRedenvRecList"]];
            
            sctwRedEnvelopeRecInqData.totalPages = [bodyDic integerForKey:@"totalPages"];
            sctwRedEnvelopeRecInqData.type = [bodyDic stringForKey:@"type"];
            
            completion(nil,sctwRedEnvelopeRecInqData);
        }else{
            PKError * pkerror = [[PKError  alloc]initWithTitle:@"Error" message:error.localizedDescription code:[NSString stringWithFormat:@"%ld",(long)error.code]];
            completion(pkerror,nil);
        }
        
    }];
    
}

- (NSArray *)getGroupRedEnvRecList:(NSArray *)listArray{
    NSMutableArray * addArray = [[NSMutableArray alloc]init];
    for (int i = 0; i < listArray.count; i++) {
        SCTWGroupRedEnvRec * redenvRec = [SCTWGroupRedEnvRec new];
        redenvRec.amountType = listArray[i][@"amountType"];
        redenvRec.cover = listArray[i][@"cover"];
        redenvRec.recivedAmt = listArray[i][@"recivedAmt"];
        redenvRec.recivedNum = [listArray[i][@"recivedNum"]integerValue];
        redenvRec.totalNum = [listArray[i][@"totalNum"]integerValue];
        redenvRec.fixAmt = listArray[i][@"fixAmt"];
        redenvRec.maxAmt = listArray[i][@"maxAmt"];
        redenvRec.minAmt = listArray[i][@"minAmt"];
        redenvRec.redEnvelopeDetailList = [self getRedEnvelopeDetailList:listArray[i][@"redEnvelopeDetailList"]];
       
        
        [addArray addObject:redenvRec];
    }
    return [addArray mutableCopy];
}

- (NSArray *)getSingleRedenvRecList:(NSArray *)listArray{
    NSMutableArray * addArray = [[NSMutableArray alloc]init];
    for (int i = 0; i < listArray.count; i++) {
        SCTWRedEnvDetail * redenv = [SCTWRedEnvDetail new];
        redenv.amount = listArray[i][@"amount"];
        redenv.txDateTime = listArray[i][@"txDateTime"];
        redenv.status = listArray[i][@"status"];
        [addArray addObject:redenv];
    }
    return [addArray mutableCopy];
}

- (NSArray*)getRedEnvelopeDetailList:(NSArray *)listArray{
    NSMutableArray * addArray = [[NSMutableArray alloc]init];
    for (int i = 0; i < listArray.count; i++) {
        SCTWSingleRedEnvRec * redenvRec = [SCTWSingleRedEnvRec new];
        redenvRec.txDateTime = listArray[i][@"txDateTime"];
        [addArray addObject:redenvRec];
    }
    return [addArray mutableCopy];
}

- (void)desendRedEnvWithRequest:(SCTWDesendRedEnvRequest*)request WithCompletion:(SCTWDesendRedEnvDataCompletionBlock) completion {
    
    NSArray * bodyKeys = @[@"id",@"validationType"];
    NSArray * bodyValue = @[request.redEnvelopeID,request.validationType];
    
    [Request apiFetcherWithCompletion:[Request getSignatureDic:bodyKeys BodyValue:bodyValue JwtToken:[Request getPliststring:@"jwtToken"] AesKey:[Request getAeskeyString] GMElliptic:[Request cryptob]] Api:@"rest/keyboardBanking/desendRedEnv" Completion:^(NSDictionary *dict, NSError *error) {
        
        if (dict) {
            NSDictionary * bodyDic  = [dict dictionaryForKey:@"body"];
            SCTWDesendRedEnvData * sctwDesendRedEnvData = [SCTWDesendRedEnvData new];
            sctwDesendRedEnvData.pageCheckId = [bodyDic stringForKey:@"pageCheckId"];
            sctwDesendRedEnvData.phoneNum = [bodyDic stringForKey:@"phoneNum"];
            sctwDesendRedEnvData.validationType = [bodyDic stringForKey:@"validationType"];
            sctwDesendRedEnvData.webID = [bodyDic stringForKey:@"webId"];
            completion(nil,sctwDesendRedEnvData);
        }else{
            PKError * pkerror = [[PKError  alloc]initWithTitle:@"Error" message:error.localizedDescription code:[NSString stringWithFormat:@"%ld",(long)error.code]];
            completion(pkerror,nil);
        }
        
    }];
}

-(void)desendRedEnvResendSmsOtpWithCpmpletion:(SCTWResendSmsOTPDataCompletionBlock)completion {
    
    [Request apiFetcherWithCompletion:[Request getSignatureDic:nil BodyValue:nil JwtToken:[Request getPliststring:@"jwtToken"] AesKey:[Request getAeskeyString] GMElliptic:[Request cryptob]] Api:@"rest/keyboardBanking/desendRedEnvResendSmsOtp" Completion:^(NSDictionary *dict, NSError *error) {
        
        if (dict) {
            NSDictionary * bodyDic  = [dict dictionaryForKey:@"body"];
            SCTWResendSmsOTPData * sctwResendSmsOTPData = [SCTWResendSmsOTPData new];
            sctwResendSmsOTPData.mobilePhone = [bodyDic stringForKey:@"mobilePhone"];
            sctwResendSmsOTPData.pageCheckId = [bodyDic stringForKey:@"pageCheckId"];
            sctwResendSmsOTPData.webId = [bodyDic stringForKey:@"webId"];
            completion(nil,sctwResendSmsOTPData);
        }else{
            PKError * pkerror = [[PKError  alloc]initWithTitle:@"Error" message:error.localizedDescription code:[NSString stringWithFormat:@"%ld",(long)error.code]];
            completion(pkerror,nil);
        }
        
    }];
}

- (void)desendValidateIdentityWithRequest:(SCTWDesendValidateIdentityRequest*)request WithCompletion:(SCTWDesendValidateResponseCompletionBlock)completion {
    
    NSArray * bodyKeys = @[@"pageCheckId",@"validationCode"];
    NSArray * bodyValue = @[request.pageCheckID,request.validationCode];
    
    [Request apiFetcherWithCompletion:[Request getSignatureDic:bodyKeys BodyValue:bodyValue JwtToken:[Request getPliststring:@"jwtToken"] AesKey:[Request getAeskeyString] GMElliptic:[Request cryptob]] Api:@"rest/keyboardBanking/desendValidateIdentity" Completion:^(NSDictionary *dict, NSError *error) {
        
        if (dict) {
            NSDictionary * bodyDic  = [dict dictionaryForKey:@"body"];
            SCTWDesendValidateResponse * sctwDesendValidateResponse = [SCTWDesendValidateResponse new];

            SCTWGroupRedEnvRec * redenvRec = [SCTWGroupRedEnvRec new];
            NSDictionary * redenvRecDic = [bodyDic dictionaryForKey:@"groupRedEnvRec"];
            redenvRec.amountType = [redenvRecDic stringForKey:@"amountType"];
            redenvRec.cover = [redenvRecDic stringForKey:@"cover"];
            redenvRec.recivedAmt = [redenvRecDic stringForKey:@"recivedAmt"];
            redenvRec.recivedNum = [redenvRecDic integerForKey:@"recivedNum"];
            redenvRec.totalNum = [redenvRecDic integerForKey:@"totalNum"];
            redenvRec.fixAmt = [redenvRecDic stringForKey:@"fixAmt"];
            redenvRec.maxAmt = [redenvRecDic stringForKey:@"maxAmt"];
            redenvRec.minAmt = [redenvRecDic stringForKey:@"minAmt"];
            redenvRec.redEnvelopeDetailList = [self getRedEnvelopeDetailList:[dict arrayForKey:@"redEnvelopeDetailList"]];
        
            SCTWSingleRedEnvRec * singleRedenvRec = [SCTWSingleRedEnvRec new];
            NSDictionary * singlerDic = [bodyDic dictionaryForKey:@"groupRedEnvRec"];
            singleRedenvRec.txDateTime = [singlerDic stringForKey:@"txDateTime"];
            
            sctwDesendValidateResponse.redEnvType = [bodyDic stringForKey:@"redEnvType"];
            sctwDesendValidateResponse.success = [bodyDic boolForKey:@"success"];
            sctwDesendValidateResponse.groupRedEnvRec = redenvRec;
            sctwDesendValidateResponse.singelRedEnvRec = singleRedenvRec;
            
            completion(nil,sctwDesendValidateResponse);
            
        }else{
            PKError * pkerror = [[PKError  alloc]initWithTitle:@"Error" message:error.localizedDescription code:[NSString stringWithFormat:@"%ld",(long)error.code]];
            completion(pkerror,nil);
        }
        
    }];
}

- (void)getForeignExchangeWithCompletion:(SCTWForeignExchangeResponseCompletionBlock)completion {
    
    [Request apiFetcherWithCompletion:[Request getSignatureDic:nil BodyValue:nil JwtToken:[Request getPliststring:@"jwtToken"] AesKey:[Request getAeskeyString] GMElliptic:[Request cryptob]] Api:@"rest/keyboardBanking/getForeignExchange" Completion:^(NSDictionary *dict, NSError *error) {
        
        if (dict) {
            NSDictionary * bodyDic = [dict dictionaryForKey:@"body"];
            
            SCTWForeignExchangeResponse * sctwForeignExchangeResponse = [[SCTWForeignExchangeResponse alloc]init];

            sctwForeignExchangeResponse.foreignExchangeInfosOfBuy = [self getforeignExchangeInfos:[bodyDic arrayForKey:@"foreignExchangeInfosOfBuy"]];
            sctwForeignExchangeResponse.foreignExchangeInfosOfSell = [self getforeignExchangeInfos:[bodyDic arrayForKey:@"foreignExchangeInfosOfSell"]];
            sctwForeignExchangeResponse.lastUpdateTime = [bodyDic stringForKey:@"lastUpdateTime"];
            completion(nil,sctwForeignExchangeResponse);
    
        }else{
            PKError * pkerror = [[PKError  alloc]initWithTitle:@"Error" message:error.localizedDescription code:[NSString stringWithFormat:@"%ld",(long)error.code]];
            completion(pkerror,nil);
        }
        
    }];
}

- (NSArray*)getforeignExchangeInfos:(NSArray *)listArray{
    NSMutableArray * addArray = [[NSMutableArray alloc]init];
    for (int i = 0; i < listArray.count; i++) {
        SCTWForeignExchange * object = [SCTWForeignExchange new];
        object.cash = listArray[i][@"cash"];
        object.telegraphic = listArray[i][@"telegraphic"];
        object.currencyName = listArray[i][@"currencyName"];
        [addArray addObject:object];
    }
    return [addArray mutableCopy];
}

- (void)getWhatsNewWithCompletion:(SCTWWhatsNewResponse)completion {
    
    [Request apiFetcherWithCompletion:[Request getSignatureDic:nil BodyValue:nil JwtToken:[Request getPliststring:@"jwtToken"] AesKey:[Request getAeskeyString] GMElliptic:[Request cryptob]] Api:@"rest/keyboardBanking/getWhatsNew" Completion:^(NSDictionary *dict, NSError *error) {
        
        if (dict) {
            NSDictionary * bodyDic = [dict dictionaryForKey:@"body"];
            completion(nil,[bodyDic stringForKey:@"url"]);
            
        }else{
            PKError * pkerror = [[PKError  alloc]initWithTitle:@"Error" message:error.localizedDescription code:[NSString stringWithFormat:@"%ld",(long)error.code]];
            completion(pkerror,nil);
        }
        
    }];
}

- (void)BiometricAuthenticationCompletionBlock:(BiometricAuthenticationCompletionBlock)completion{
    [self.localAuthenticator authenticateBiometricsWithCompletion:completion];
}

//增加聯絡人資訊
- (void)addContacts:(NSArray<__kindof PKContact *> *)contactList WithCompletion:(nonnull SCTWAddContactResponse)completion {
    
    CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
    if (status == CNAuthorizationStatusDenied || status == CNAuthorizationStatusRestricted) {
        return;
    }
    
    CNContactStore *store = [[CNContactStore alloc] init];
    
    [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (!granted) {
            dispatch_async(dispatch_get_main_queue(), ^{
                // user didn't grant access;
                // so, again, tell user here why app needs permissions in order  to do it's job;
                // this is dispatched to the main queue because this request could be running on background thread
            });
            return;
        }
        
        for (PKContact * pkcontact in contactList) {
            // create contact
            CNMutableContact *contact = [[CNMutableContact alloc] init];
            
            contact.givenName = pkcontact.givenName;
            contact.middleName = pkcontact.middleName;
            contact.familyName = pkcontact.familyName;
            contact.organizationName = pkcontact.organization;
            contact.nickname = pkcontact.nickname;
           
            /*
            pkcontact.imageFetcher = self;
            
            if (self.hasImage) {
                NSData *imageData = UIImagePNGRepresentation(self.contactImage);
                contact.imageData = imageData;
            }
            */
            
            for (PKPhoneNumber *nubmer in pkcontact.phones) {
                CNLabeledValue *homePhone = [CNLabeledValue labeledValueWithLabel:CNLabelHome value:[CNPhoneNumber phoneNumberWithStringValue:nubmer.phoneNumber]];
                contact.phoneNumbers = @[homePhone];
            }
    
            CNSaveRequest *request = [[CNSaveRequest alloc] init];
            [request addContact:contact toContainerWithIdentifier:nil];
            
            // save it
            NSError *saveError;
            if (![store executeSaveRequest:request error:&saveError]) {
                NSLog(@"error = %@", saveError);
                PKError * pkerror = [[PKError  alloc]initWithTitle:@"Error" message:error.localizedDescription code:[NSString stringWithFormat:@"%ld",(long)error.code]];
                completion(pkerror);
            }else{
                completion(nil);
            }
        }
    }];
    
}

//取得聯絡人資料
- (void)getContactsWithCompletion:(nonnull SCTWContactsResponse)completion {
    
    // Request authorization to Contacts
    CNContactStore *store = [[CNContactStore alloc] init];
    [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (granted == YES)
        {

            NSArray *keys = @[CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPhoneNumbersKey, CNContactImageDataKey,CNContactMiddleNameKey,CNContactOrganizationNameKey,CNContactNicknameKey];
            NSString *containerId = store.defaultContainerIdentifier;
            NSPredicate *predicate = [CNContact predicateForContactsInContainerWithIdentifier:containerId];
            NSError *error;
            NSArray *cnContacts = [store unifiedContactsMatchingPredicate:predicate keysToFetch:keys error:&error];
            if (error)
            {
                PKError * pkerror = [[PKError  alloc]init];
                pkerror.userInfo = error.userInfo;
                completion(pkerror,nil);
            }
            else
            {
                NSString *phone;
                NSMutableArray *contactNumbersArray = [[NSMutableArray alloc]init];
                NSMutableArray *contactPKContactArray = [[NSMutableArray alloc]init];
                for (CNContact *contact in cnContacts) {
                    
                    PKContact * pkcontact = [PKContact new];
                    pkcontact.givenName = contact.givenName;
                    pkcontact.middleName = contact.middleName;
                    pkcontact.familyName = contact.familyName;
                    pkcontact.organization = contact.organizationName;
                    pkcontact.nickname = contact.nickname;
                    
                    /*
                    UIImage *image = [UIImage imageWithData:contact.imageData];
                    self.contactImage = nil;
                    self.hasImage = NO;
                    if (image != nil) {
                        self.hasImage = YES;
                        self.contactImage = image;
                    }
                    pkcontact.imageFetcher = self;
                    */
                    
                    for (CNLabeledValue *label in contact.phoneNumbers) {
                        phone = [label.value stringValue];
                        if ([phone length] > 0) {
                            PKPhoneNumber * pkphoneNumber = [[PKPhoneNumber alloc]initWithPhoneNumber:phone];
                            [contactNumbersArray addObject:pkphoneNumber];
                        }
                    }
                    
                    pkcontact.phones = [contactNumbersArray mutableCopy];
                    
                    [contactPKContactArray addObject:pkcontact];
                }
                completion(nil,[contactPKContactArray mutableCopy]);
            }
        }else{
            PKError * pkerror = [[PKError  alloc]initWithTitle:@"Error" message:error.localizedDescription code:[NSString stringWithFormat:@"%ld",(long)error.code]];
            completion(pkerror,nil);
        }
    }];
    
}

- (UIImage *)fetchImage{
    return self.contactImage;
}

- (BOOL)hasImage{
    return self.hasImage;
}


- (void)resendPWDSmsWithID:(NSString *)redEnvId andResponse:(nonnull SCTWResendPWDSMSResponse)completion {
    
    NSArray * bodyKeys = @[@"id"];
    NSArray * bodyValue = @[redEnvId];
    
     [Request apiFetcherWithCompletion:[Request getSignatureDic:bodyKeys BodyValue:bodyValue JwtToken:[Request getPliststring:@"jwtToken"] AesKey:[Request getAeskeyString] GMElliptic:[Request cryptob]] Api:@"rest/keyboardBanking/resendPwdSms" Completion:^(NSDictionary *dict, NSError *error) {
        
        if (dict) {
            NSDictionary * bodyDic = [dict dictionaryForKey:@"body"];
            completion(nil,[bodyDic stringForKey:@"message"]);
            
        }else{
            PKError * pkerror = [[PKError  alloc]initWithTitle:@"Error" message:error.localizedDescription code:[NSString stringWithFormat:@"%ld",(long)error.code]];
            completion(pkerror,nil);
        }
        
    }];

}

//- (void)validateIdentityWithPageCheckId:(NSString *)pageCheckId andWithValidationCode:(NSString *)validationCode andWithCompletion:(SCTWValidateIdentityDataCompletionBlock)completion{
//
//    if([validationCode isEqualToString:@"123456"]){
//        SCTWValidateIdentityData* validationData = [SCTWValidateIdentityData new];
//        validationData.message = @"msg";
//        completion(nil,validationData);
//    }
//    else{
//        PKError* validateIdentityError = [PKError new];
//        validateIdentityError.title = @"identity not validate";
//        validateIdentityError.message = @"validate denied";
//        completion(validateIdentityError,nil);
//    }
//
//}
//
- (void)forwardInvocation:(NSInvocation *)anInvocation {
    if([self useStubIfNeeded:NSStringFromSelector([anInvocation selector])]){
        [super forwardInvocation:anInvocation];
    }else{
        [anInvocation invokeWithTarget:self.implementor];
    }
}

-(BOOL)useStubIfNeeded:(NSString*)func{
    NSDictionary* dict  = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults appGroupUserDefaults] objectForKey:@"callback stubs"]];
//    if([((NSArray*)[dict objectForKey:func]) count]<1]{
//
//    }
    NSArray* item = [dict objectForKey:func];
    return [item count] > 0 ;
}

@end

@implementation PKDefaultImplementor

- (void)validateIdentityWithPageCheckId:(NSString *)pageCheckId andWithValidationCode:(NSString *)validationCode andWithCompletion:(SCTWValidateIdentityDataCompletionBlock)completion{
    
    if([validationCode isEqualToString:@"123456"]){
        SCTWValidateIdentityData* validationData = [SCTWValidateIdentityData new];
        validationData.message = @"msg";
        completion(nil,validationData);
    }
    else{
        PKError* validateIdentityError = [PKError new];
        validateIdentityError.title = @"identity not validate";
        validateIdentityError.message = @"validate denied";
        completion(validateIdentityError,nil);
    }
    
}
- (void)preSendSingleRedEnvWithRequest:(SCTWPreSendRequest *)request withCompletion:(SCTWPreSendResponseCompletionBlock)completion{
    SCTWPreSendResponse* response = [[SCTWPreSendResponse alloc] init];
    response.amount = [request.amount description];
    response.greetings = request.greetings ?: @"greetings default";
    response.pageCheckID = @"13";
    completion(nil,response);
}
- (void)preSendGroupRedEnvWithRequest:(SCTWPreSendGroupRequest *)request WithCompletion:(SCTWPreSendGroupRersponseCompletionBlock)completion{
    SCTWPreSendGroupResponse* response = [SCTWPreSendGroupResponse new];
    response.greetings = request.greetings ?: @"greetings default";
    response.amountType = request.amountType;
    response.fixAmt =  request.fixAmt;
    response.minAmt = request.minAmt;
    response.maxAmt = request.maxAmt;
    response.totalNum = request.totalNum;
    response.totalAmount = request.totalAmount;
    completion(nil,response);
}

@end
