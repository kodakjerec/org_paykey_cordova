//
//  BaseDelegateImpl.m
//  Generic_Example
//
//  Created by Eran Israel on 18/12/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import "BaseDelegateImpl.h"
@import KeyboardFramework;

@implementation BaseDelegateImpl {
    dispatch_queue_t backgroundQueue;
}

- (instancetype)init {
    if (self = [super init]) {
        self.dispatcher = [PKCallbackDispatcher new];
        self.localAuthenticator = [[PKLocalAuthenticator alloc] initWithReason:@"because"];
        backgroundQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    }
    return self;
}

- (void)forwardInvocation:(NSInvocation *)anInvocation {
    [anInvocation retainArguments];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)([self delay] * NSEC_PER_SEC)), backgroundQueue, ^{
        self.dispatcher.stubs =
        [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults appGroupUserDefaults] objectForKey:@"callback stubs"]];
        if (self.dispatcher.stubs != nil) {
            [anInvocation invokeWithTarget:self.dispatcher];
        }
    });
}


- (CGFloat)delay {
    return [[NSKeyedUnarchiver unarchiveObjectWithData:
             [[NSUserDefaults appGroupUserDefaults] objectForKey:@"callback stubs"]]
            [@"delay"] floatValue];
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector {
    return [self.dispatcher methodSignatureForSelector:aSelector];
}

@end
