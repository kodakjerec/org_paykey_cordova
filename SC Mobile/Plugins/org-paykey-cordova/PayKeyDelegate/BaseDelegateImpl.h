//
//  BaseDelegateImpl.h
//  Generic_Example
//
//  Created by Eran Israel on 18/12/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
@import PayKeyUI;

@import UIFlowHelper;

@interface BaseDelegateImpl : NSObject

@property PKCallbackDispatcher *dispatcher;
@property PKLocalAuthenticator* localAuthenticator;

@end
