//
//  HSBCPayKeyDelegateImpl.h
//  Generic_Example
//
//  Created by Eran Israel on 12/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseDelegateImpl.h"

@import PKSCTWClient;
@interface PKDefaultImplementor : NSObject<SCTWDelegate>

@end

@interface PayKeyDelegateImpl : BaseDelegateImpl<SCTWDelegate>

@end
