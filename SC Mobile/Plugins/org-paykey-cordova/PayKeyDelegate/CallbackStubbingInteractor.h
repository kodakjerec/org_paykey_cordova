//
//  HSBCCallbackStubbingInteractor.h
//  Generic_Example
//
//  Created by Alex Kogan on 17/10/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIFlowHelper/PKCallbackStubbingInteractor.h>

@import UIFlowHelper;

@interface CallbackStubbingInteractor : PKCallbackStubbingInteractor
@end
