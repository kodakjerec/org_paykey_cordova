//
//  HSBCCallbackStubbingInteractor.m
//  Generic_Example
//
//  Created by Alex Kogan on 17/10/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import "CallbackStubbingInteractor.h"

@import PKSCTWClient;

@implementation CallbackStubbingInteractor


- (void)createOptions {
    [self createDelayJuncture];
    [self createLoginJuncture];
    [self createIsAvailableJuncture];
    [self createGetTxFavorAccListJuncture];
    [self createBankCodesJuncture];
    [self createGetDefaultCoverWithDefaultCoverListJuncture];
    [self createGetNotesJuncture];
    [self createPreSendGroupEnvJuncture];
    [self createConfirmRedEnvDetailWithPageCheckIdJucture];
    [self createValidateIdentityJuncture];
    [self createPreSendSingleEnvJuncture];
    [self createGetRandomList];
    [self createGetKBOutAcctBalance];
    [self createSendRedEnvResendSmsOtp];
    [self createGetRandomList];
    [self desendRedEnvWithRequestJuncture];
    [self desendRedEnvResendSmsOtpWithCpmpletionJuncture];
    [self desendValidateIdentityWithRequestJuncture];
    [self getForeignExchangeWithCompletionJuncture];
    [self getWhatsNewWithCompletionJuncture];
    [self stubAddContact];
    [self stubGetContacts];
    [self stubResendPWDSmsWithID];
}

- (void)createDelayJuncture {
    [self.flow setStub:@{@"delay": @(0)}
             forBranch:@"None"
            atJuncture:@"Delay"];
    
    [self.flow setStub:@{@"delay": @(3)}
             forBranch:@"3 Seconds"
            atJuncture:@"Delay"];
    [self.flow setStub:@{@"delay": @(0.5)}
             forBranch:@"0.5 Seconds"
            atJuncture:@"Delay"];
}

-(void)createConfirmRedEnvDetailWithPageCheckIdJucture{
    NSString* const juctureName = @"createConfirmRedEnvDetailWithPageCheckIdJuctureName";
    SCTWConfirmRedEnvResponse* otpResponse = [[SCTWConfirmRedEnvResponse alloc]init];
    otpResponse.pageCheckId = @"p";
    otpResponse.phoneNum = @"05261645733";
    otpResponse.validationType = @"otp";
    otpResponse.webId = @"1234";
    [self.flow setStub:Stub(confirmRedEnvDetailWithPageCheckId:AndWithValidationType:andWithCompletion:,[PKNilBox new],[otpResponse box]) forBranch:@"OTP" atJuncture:juctureName];
    SCTWConfirmRedEnvResponse* fingerPrintResponse = [[SCTWConfirmRedEnvResponse alloc]init];
    fingerPrintResponse.pageCheckId = @"a";
    fingerPrintResponse.validationType = @"fingerprint";
    fingerPrintResponse.webId = @"2345";
    [self.flow setStub:Stub(confirmRedEnvDetailWithPageCheckId:AndWithValidationType:andWithCompletion:,[PKNilBox new],[fingerPrintResponse box]) forBranch:@"FINGERPRINT" atJuncture:juctureName];
    SCTWConfirmRedEnvResponse* patternResponse = [[SCTWConfirmRedEnvResponse alloc]init];
    patternResponse.pageCheckId = @"s";
    patternResponse.validationType = @"pattern";
    patternResponse.webId = @"3345";
    [self.flow setStub:Stub(confirmRedEnvDetailWithPageCheckId:AndWithValidationType:andWithCompletion:,[PKNilBox new],[patternResponse box]) forBranch:@"PATTERN" atJuncture:juctureName];
    SCTWConfirmRedEnvResponse* faceIDresp = [[SCTWConfirmRedEnvResponse alloc]init];
    faceIDresp.pageCheckId = @"d";
    faceIDresp.validationType = @"faceid";
    faceIDresp.webId = @"3347";
    [self.flow setStub:Stub(confirmRedEnvDetailWithPageCheckId:AndWithValidationType:andWithCompletion:,[PKNilBox new],[faceIDresp box]) forBranch:@"FaceID" atJuncture:juctureName];
    PKError* error = [[PKError alloc] init];
    error.title = @"ConfirmRedEnvDetails";
    error.message = @"Reason: Unable to connect to the server, please check your Internet connection.";
    [self.flow setStub:Stub(confirmRedEnvDetailWithPageCheckId:AndWithValidationType:andWithCompletion:,[error box], [PKNilBox new]) forBranch:@"failure" atJuncture:juctureName];
    
}

-(void)createValidateIdentityJuncture{
     [self.flow setStub:Stub(validateIdentityWithPageCheckId:andWithValidationCode:andWithCompletion:) forBranch:@"implementation" atJuncture:@"createValidateIdentityJuncture"];
    SCTWValidateIdentityData* vlidationData = [[SCTWValidateIdentityData alloc]init];
    vlidationData.message = @"msg";
   
    [self.flow setStub:Stub(validateIdentityWithPageCheckId:andWithValidationCode:andWithCompletion:,[PKNilBox new],[vlidationData box]) forBranch:@"default" atJuncture:@"createValidateIdentityJuncture"];
    PKError* validateIdentityError = [PKError new];
    validateIdentityError.title = @"identity not validate";
    validateIdentityError.message = @"validate denied";
}

-(void)createPreSendGroupEnvJuncture{
    SCTWPreSendGroupResponse * response = [[SCTWPreSendGroupResponse alloc] init];
    [self.flow setStub:Stub(preSendGroupRedEnvWithRequest:WithCompletion:) forBranch:@"implementation" atJuncture:@"PreSendGroupEnv"];
    response.greetings = @"greetings is good for you it gives your life a meaning.\n laurem ipsum dulur sit amet isn what you expect";
    response.pageCheckId = @"13";
    response.validationCode = @"32";
    response.amountType = @"F";
    response.fixAmt = @(2);
    response.totalNum = 5;
    [self.flow setStub:Stub(preSendGroupRedEnvWithRequest:WithCompletion:,[PKNilBox new],[response box])
             forBranch:@"success" atJuncture:@"PreSendGroupEnv"];
    PKError* error = [[PKError alloc] init];
    error.title = @"PreSendGroupEnv";
    error.message = @"Reason: Unable to connect to the server, please check your Internet connection.";
    [self.flow setStub:Stub(preSendGroupRedEnvWithRequest:WithCompletion:,[error box],[PKNilBox new])
             forBranch:@"failure" atJuncture:@"PreSendGroupEnv"];
}

-(void)createPreSendSingleEnvJuncture{
    [self.flow setStub:Stub(preSendSingleRedEnvWithRequest:withCompletion:) forBranch:@"implementation" atJuncture:@"PreSendSingleEnv"];
    SCTWPreSendResponse* response = [[SCTWPreSendResponse alloc] init];
    response.amount =@"1000";
    response.greetings = nil;
    response.pageCheckID = @"13";

    [self.flow setStub:Stub(preSendSingleRedEnvWithRequest:withCompletion:,[PKNilBox new],[response box]) forBranch:@"success" atJuncture:@"PreSendSingleEnv"];

    PKError* error = [[PKError alloc] init];
    error.title = @"PreSendSingleEnv";
    error.message = @"Reason: Unable to connect to the server, please check your Internet connection.";

    [self.flow setStub:Stub(preSendSingleRedEnvWithRequest:withCompletion:,[error box],[PKNilBox new]) forBranch:@"failure" atJuncture:@"PreSendSingleEnv"];
}

- (SCTWDefaultCover *)createCoverWithID:(NSInteger)coverID {
    SCTWDefaultCover *cover = [SCTWDefaultCover new];
    NSString* coverName;
    switch (coverID) {
        case 0:
        case 1:
        case 2:
            coverName = [NSString stringWithFormat:@"card%@",@(coverID)];
            break;
        default:
            coverName = @"card1";
            break;
    }
    
    UIImage* imageToEncode = [UIImage imageNamed:coverName
                            inBundle:[NSBundle bundleForClass:[self class]]
       compatibleWithTraitCollection:nil];
    cover.defaultCover = [ImageBase64Convertor encodeToBase64String:imageToEncode];
    
    cover.coverID = @(coverID);
    cover.updateDateTime = @"123";
    return cover;
}

- (void)createGetDefaultCoverWithDefaultCoverListJuncture{
    NSArray *array = @[[self createCoverWithID:0],[self createCoverWithID:1],[self createCoverWithID:2]];
    [self.flow setStub:Stub(getDefaultCoverWithDefaultCoverList:andWithCompletion:, [PKNilBox new], [array box],[@NO integerBox])
             forBranch:@"login success"
            atJuncture:@"Get Default Cover"];
    PKError* error = [[PKError alloc] init];
    error.title = @"GetDefaultCover";
    error.message = @"Reason: Unable to connect to the server, please check your Internet connection.";
    [self.flow setStub:Stub(getDefaultCoverWithDefaultCoverList:andWithCompletion:, [error box], [PKNilBox new],[@NO integerBox])
             forBranch:@"failure"
            atJuncture:@"Get Default Cover"];
}

-(void)createLoginJuncture{
    SCTWBalanceData* balanceData = [[SCTWBalanceData alloc] init];
    balanceData.accountBalance = @"20";
    balanceData.accountBalanceFormat = @"TWD 20";
    [self.flow setStub:Stub(loginKBWithLanguage:andwWithCompletion:, [PKNilBox new], [balanceData box])
             forBranch:@"login success"
            atJuncture:@"login"];
    PKError* error = [[PKError alloc] init];
    error.title = @"LoginKB";
    error.message = @"Reason: Unable to connect to the server, please check your Internet connection.";
    [self.flow setStub:Stub(loginKBWithLanguage:andwWithCompletion:, [error box], [PKNilBox new])
             forBranch:@"failure"
            atJuncture:@"login"];
}
-(SCTWIsAvailableData* )isAvaialbleWithBooleanValue:(BOOL) value
{
    SCTWIsAvailableData* isAvailable = [[SCTWIsAvailableData alloc] init];
    
    isAvailable.easyTransferAvailable = value;
    return isAvailable;
}

-(void)createIsAvailableJuncture{
    NSString* const JunctureName = @"isAvailableEasyTransfer";
    SCTWIsAvailableData* isAvailable = [self isAvaialbleWithBooleanValue:YES];
    [self.flow setStub:Stub(isAvailableWithCompletion:,[PKNilBox new],[isAvailable box])
             forBranch:@"easyTransferAvailable"
            atJuncture:JunctureName];
     SCTWIsAvailableData* isNotAvailable = [self isAvaialbleWithBooleanValue:NO];
     [self.flow setStub:Stub(isAvailableWithCompletion:,[PKNilBox new],[isNotAvailable box])
              forBranch:@"easyTransferIsNotAvailable"
             atJuncture:JunctureName];
    PKError* error = [[PKError alloc] init];
    error.title = @"easyTransferIsNotAvailable";
    error.message = @"Reason: Unable to connect to the server, please check your Internet connection.";
    [self.flow setStub:Stub(isAvailableWithCompletion:,[error box], [PKNilBox new])
             forBranch:@"error occured"
            atJuncture:JunctureName];
}

-(void)createGetTxFavorAccListJuncture{
    NSArray* const arryOfAccounts = @[[self generateAccountInfo:@"IDK" :@"123" :@"RTS"],[self generateAccountInfo:@"IDK" :@"176890" :@"YTRF"],[self generateAccountInfo:@"IDK" :@"367845" :@"TWTW"]];
    NSString* const junctureName = @"GetTxFavorAccList";
    [self.flow setStub:Stub(getTxFavorAccListWithCompletion:,[PKNilBox new],[arryOfAccounts box]) forBranch:@"success" atJuncture:junctureName];
    PKError* error = [[PKError alloc] init];
    error.title = @"GetTxFavorAccList";
    error.message = @"Reason: Unable to connect to the server, please check your Internet connection.";
    [self.flow setStub:Stub(getTxFavorAccListWithCompletion:,[error box], [PKNilBox new]) forBranch:@"failed" atJuncture:junctureName];
}

-(SCTWAccountInfo*) generateAccountInfo:(NSString*)acctFmt :(NSString*)accNo :(NSString*)bankCode{
    SCTWAccountInfo* accountInfo =  [[SCTWAccountInfo alloc] init];
    accountInfo.acctFmt = acctFmt;
    accountInfo.accNo = accNo;
    accountInfo.bankCode = bankCode;
    return accountInfo;
}

-(void) createBankCodesJuncture{
    NSString* const banksCodesJunctureName = @"banksCodesJuncutre";
    NSArray* const banks = @[[self generateBankInfo:@"TW" :@"10"],[self generateBankInfo:@"TW2" :@"11"]];
    [self.flow setStub:Stub(getBanksCodesWithCompletion:,[PKNilBox new],[banks box]) forBranch:@"succsess" atJuncture:banksCodesJunctureName];
    PKError* error = [[PKError alloc] init];
    error.title = @"GetBankCodes";
    error.message = @"Reason: Unable to connect to the server, please check your Internet connection.";
    [self.flow setStub:Stub(getBanksCodesWithCompletion:,[error box], [PKNilBox new]) forBranch:@"failure" atJuncture:banksCodesJunctureName];
}

-(void) createGetNotesJuncture {
    NSString* const getNotesJunctureName = @"getNotes";
    NSString *notes = @"PCFET0NUWVBFIGh0bWw+DQo8aHRtbD4NCjxib2R5Pg0KDQo8aDE+TXkgRmlyc3QgSGVhZGluZzwvaDE+DQoNCjxwPk15IGZpcnN0IHBhcmFncmFwaC48L3A+DQoNCjwvYm9keT4NCjwvaHRtbD4=";
    [self.flow setStub:Stub(getNotesWithFunctionId:andWithCompletion:,[PKNilBox new],[notes box])
             forBranch:@"succsess"
            atJuncture:getNotesJunctureName];
    PKError* error = [[PKError alloc] init];
    error.title = @"GetNotes";
    error.message = @"Reason: Unable to connect to the server, please check your Internet connection.";
    [self.flow setStub:Stub(getNotesWithFunctionId:andWithCompletion:,[error box], [PKNilBox new])
             forBranch:@"failure"
            atJuncture:getNotesJunctureName];
}

-(SCTWBankInfo*) generateBankInfo:(NSString*) bankName :(NSString*)bankCode{
    SCTWBankInfo* bankInfo = [[SCTWBankInfo alloc]init];
    bankInfo.bankCode = bankCode;
    bankInfo.bankName = bankName;
    return bankInfo;
}


-(void) createGetKBOutAcctBalance{
    SCTWBalanceData* balanceData = [[SCTWBalanceData alloc] init];
    balanceData.accountBalance = @"123.43";
    balanceData.accountBalanceFormat = @"format";
    [self.flow setStub:Stub(getKBOutAcctBalance:,[PKNilBox new],[balanceData box]) forBranch:@"default" atJuncture:@"createGetKBOutAcctBalance"];
    PKError* kbouterror = [PKError new];
    [self.flow setStub:Stub(getKBOutAcctBalance:,[kbouterror box],[PKNilBox new]) forBranch:@"error" atJuncture:@"createGetKBOutAcctBalance"];
}

-(void)createSendRedEnvResendSmsOtp{
    NSString* const junctureName = @"createSendRedEnvResendSmsOtp";
    SCTWResendSmsOTPData* data = [[SCTWResendSmsOTPData alloc] init];
    data.mobilePhone = @"0528512085";
    data.pageCheckId = @"3";
    data.webId = @"3";
    
    [self.flow setStub:Stub(sendRedEnvResendSmsOtp:,[PKNilBox new],[data box]) forBranch:@"default" atJuncture:junctureName];
    PKError* resendSmsError = [PKError new];
    resendSmsError.title = @"resend sms";
    resendSmsError.message = @"resend sms otp error";
    [self.flow setStub:Stub(sendRedEnvResendSmsOtp:,[resendSmsError box],[PKNilBox new]) forBranch:@"error" atJuncture:junctureName];
}

-(void)createGetRandomList{
    NSString* const junctureName = @"createGetRandomListJuncture";
    NSArray* randomList = @[@"10",@"13"];
    SCTWRandomListResponse* resp = [[SCTWRandomListResponse alloc] init];
    resp.randomList = randomList;
    resp.minAmt = @"1.2";
    resp.maxAmt = @"20.45";
    [self.flow setStub:Stub(getRandomListWithTotalAmount:WithCompletion:,[PKNilBox new],[resp box]) forBranch:@"default" atJuncture:junctureName];
    PKError* error = [PKError new];
    error.title = @"random list error";
    error.message = @"random list message";
    [self.flow setStub:Stub(getRandomListWithTotalAmount:WithCompletion:,[error box],[PKNilBox new]) forBranch:@"error" atJuncture:junctureName];
}

-(void)desendRedEnvWithRequestJuncture{
    SCTWDesendRedEnvData* data = [[SCTWDesendRedEnvData alloc] init];
    data.pageCheckId = @"126";
    data.phoneNum = @"05268231232";
    data.validationType = @"otp";
    data.webID = @"www.paykey.com";
    
    [self.flow setStub:Stub(desendRedEnvWithRequest:WithCompletion:,[PKNilBox new],[data box]) forBranch:@"default" atJuncture:@"desendRedEnvWithRequestJuncture"];
    
    PKError* error = [PKError new];
    error.title = @"desend red env falied";
    error.message = @"desend red env failed message";
    [self.flow setStub:Stub(desendRedEnvWithRequest:WithCompletion:,[error box],[PKNilBox new]) forBranch:@"error" atJuncture:@"desendRedEnvWithRequestJuncture"];
}

-(void)desendRedEnvResendSmsOtpWithCpmpletionJuncture{
    SCTWResendSmsOTPData* data = [[SCTWResendSmsOTPData alloc] init];
    data.mobilePhone = @"0525312321";
    data.pageCheckId = @"313";
    data.webId  = @"www.paykey.com";
    [self.flow setStub:Stub(desendRedEnvResendSmsOtpWithCpmpletion:,[PKNilBox new],[data box]) forBranch:@"default" atJuncture:@"desendRedEnvResendSmsOtpWithCpmpletionJuncture"];
    PKError* error = [PKError new];
    error.title = @"desend red env sms falied";
    error.message = @"desend red env sms failed message";
    [self.flow setStub:Stub(desendRedEnvWithRequest:WithCompletion:,[error box],[PKNilBox new]) forBranch:@"error" atJuncture:@"desendRedEnvResendSmsOtpWithCpmpletionJuncture"];
}

-(void)desendValidateIdentityWithRequestJuncture{
    SCTWDesendValidateResponse* resp = [SCTWDesendValidateResponse new];
    SCTWGroupRedEnvRec* groupRedEnvRec = [SCTWGroupRedEnvRec new];
    groupRedEnvRec.amountType = @"R";
    groupRedEnvRec.cancelMark = YES;
    groupRedEnvRec.cover = @"";
    groupRedEnvRec.expireDateTime = @"2018/11/13";
    groupRedEnvRec.greetings = @"greeting you";
    groupRedEnvRec.envelopeID = @"envelope1";
    groupRedEnvRec.link = @"www.paykey.com";
    groupRedEnvRec.recivedAmt = @"10";
    groupRedEnvRec.recivedNum = 1;
    groupRedEnvRec.sendDateTime = @"2018/09/01 12:00:00";
    groupRedEnvRec.status = @"Process";
    groupRedEnvRec.totalAmount = @"1000";
    groupRedEnvRec.totalNum = 13;
    groupRedEnvRec.validationCode = @"23";
    groupRedEnvRec.message = @"I have used SCB Easy-Transfer to transfer 315 to you. The last 5 digits of my account is 01234. Please go to www.google.com (SCB website) to withdraw the money before tomorrow.";
    groupRedEnvRec.status = @"Cancel";
    resp.redEnvType = @"G";
    resp.success = @YES;
    resp.groupRedEnvRec = groupRedEnvRec;
    
    [self.flow setStub:Stub(desendValidateIdentityWithRequest:WithCompletion:,[PKNilBox new],[resp box]) forBranch:@"branch" atJuncture:@"desendValidateIdentityWithRequestJuncture"];
    PKError* error = [PKError new];
    error.title = @"desend validate identity falied";
    error.message = @"desend validate identity falied message";
    [self.flow setStub:Stub(desendRedEnvWithRequest:WithCompletion:,[error box],[PKNilBox new]) forBranch:@"error" atJuncture:@"desendValidateIdentityWithRequestJuncture"];
}

-(void)getForeignExchangeWithCompletionJuncture{
    NSString* const junctureName = @"getForeignExchangeWithCompletionJunctureName";
    SCTWForeignExchangeResponse* usdResp = [[SCTWForeignExchangeResponse alloc] init];
    SCTWForeignExchange* excUSD = [[SCTWForeignExchange alloc]init];
    excUSD.cash = @"12.3";
    excUSD.telegraphic = @"12.4";
    excUSD.currencyName = @"USD";
    usdResp.lastUpdateTime = @"12:00:23 JANU 2018";
    SCTWForeignExchange* excCAD = [[SCTWForeignExchange alloc]init];
    excCAD.cash = @"132.3";
    excCAD.telegraphic = @"122.4";
    excCAD.currencyName = @"CAD";
    usdResp.lastUpdateTime = @"12:00:23 JANU 2018";
    usdResp.foreignExchangeInfosOfBuy = @[excUSD,excCAD];
    usdResp.foreignExchangeInfosOfSell = @[excUSD,excCAD];
    [self.flow setStub:Stub(getForeignExchangeWithCompletion:,[PKNilBox new],[usdResp box]) forBranch:@"Def" atJuncture:junctureName];
    PKError* foreignExchangeError = [PKError new];
    foreignExchangeError.title = @"Foreign Exchange failure";
    foreignExchangeError.message = @"no foreigns data currently available, try to refresh";
    [self.flow setStub:Stub(getForeignExchangeWithCompletion:,[foreignExchangeError box],[PKNilBox new]) forBranch:@"error" atJuncture:junctureName];
    
}

-(void)getWhatsNewWithCompletionJuncture{
    NSString* const juctureName = @"getWhatsNewWithCompletionJuncture";
    [self.flow setStub:Stub(getWhatsNewWithCompletion:,[PKNilBox new],[@"http://www.paykey.com" box]) forBranch:@"def" atJuncture:juctureName];
    PKError* whatsNewError = [PKError new];
    whatsNewError.title = @"whats new error";
    whatsNewError.message = @"we had a problem regarding system network, maybe we should replace the wifi";
    [self.flow setStub:Stub(getWhatsNewWithCompletion:,[whatsNewError box],[PKNilBox new]) forBranch:@"error" atJuncture:juctureName];
}

- (void)stubGetContacts {
    NSString* getContactsJunctureName = @"getContacts";
    NSMutableArray* contacts = [NSMutableArray array];
    for (NSInteger i = 1; i<10;i++){
        PKContact* contact = [[PKContact alloc] init];
        contact.givenName = [NSString stringWithFormat:@"contact%@",[@(i) description]];
        PKPhoneNumber* phoneNumber = [[PKPhoneNumber alloc] initWithPhoneNumber:[NSString stringWithFormat:@"052134234%@",[@(i) description]]];
        contact.phones = @[phoneNumber];
        [contacts addObject:contact];
    }
    PKContact* cont = [[PKContact alloc] init];
    cont.givenName = @"d";
     PKPhoneNumber* phoneNumber = [[PKPhoneNumber alloc] initWithPhoneNumber:@"2"];
    cont.phones = @[phoneNumber];
    [contacts addObject:cont];
    [self.flow setStub:Stub(getContactsWithCompletion:,[PKNilBox new],[contacts box]) forBranch:@"9 contacts" atJuncture:getContactsJunctureName];
    PKError* error = [PKError new];
    error.title = @"error can't get contacts, try to restart internet network";
    error.message = @"error can't get contacts, try to restart internet network but in message";
    [self.flow setStub:Stub(getContactsWithCompletion:,[error box],[PKNilBox new]) forBranch:@"Error" atJuncture:getContactsJunctureName];
}

- (void)stubAddContact {
    NSString* stubAddContactJunctureName = @"addContact";
    [self.flow setStub:Stub(addContacts:WithCompletion:,[PKNilBox new]) forBranch:@"ok" atJuncture:stubAddContactJunctureName];
    PKError* addContactError = [PKError new];
    addContactError.title = @"no sorry";
    addContactError.message = @"see unfortunatey we can't seem to add a friend to you";
    [self.flow setStub:Stub(addContacts:WithCompletion:,[addContactError box]) forBranch:@"error" atJuncture:stubAddContactJunctureName];
}

- (void)stubResendPWDSmsWithID {
    NSString* const junctureName = @"resendPWDSmsWithID";
    [self.flow setStub:Stub(resendPWDSmsWithID:andResponse:,[PKNilBox new],[@"message is good" box]) forBranch:@"good sms" atJuncture:junctureName];
    PKError* error = [PKError new];
    error.title = @"error";
    error.message = @"error";
    [self.flow setStub:Stub(resendPWDSmsWithID:andResponse:,[error box],[PKNilBox box]) forBranch:@"error" atJuncture:junctureName];
}

@end
