//
//  SCTWRedEnvelopeRecInqData.h
//  PayKey-Clients
//
//  Created by Daniel M. on 04/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCTWGroupRedEnvRec.h"
#import "SCTWSingleRedEnvRec.h"

@interface SCTWRedEnvelopeRecInqData : NSObject<NSCoding>

@property (strong,nonatomic,nullable) NSArray<__kindof SCTWGroupRedEnvRec*>* groupRedEnvRecList;
@property (strong,nonatomic,nullable) NSArray<__kindof SCTWSingleRedEnvRec*>* singleRedenvRecList;

@property (assign,nonatomic)NSInteger totalPages;
@property (nonatomic,strong)NSString* type; // S: individual red envelope G: group red envelope
@end
