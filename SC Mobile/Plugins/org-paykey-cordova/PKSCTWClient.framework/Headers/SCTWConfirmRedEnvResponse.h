//
//  SCTWConfirmRedEnvResponse.h
//  PayKey-Clients
//
//  Created by Daniel M. on 03/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCTWConfirmRedEnvResponse : NSObject<NSCoding>
@property (copy,nonatomic) NSString* pageCheckId; // ID that used to avoid resending
@property (copy,nonatomic,nullable) NSString* phoneNum; // (optional) User’s phone number, only with value when validation type is otp
@property (copy,nonatomic) NSString* validationType; //Validation Type of users: “fingerprint”: Biometric; “pattern”: pattern; “faceid”: Face Id (Iphone X); “otp”: SMS otp validation.
@property (copy,nonatomic,nullable) NSString* webId; // (optional) Web page identification code
@end
