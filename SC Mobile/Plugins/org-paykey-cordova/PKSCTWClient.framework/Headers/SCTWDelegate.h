//
//  SCTADelegate.h
//  PayKey-Clients
//
//  Created by Daniel M. on 02/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#ifndef SCTADelegate_h
#define SCTADelegate_h
@import PayKeyUI;

#import "SCTWBalanceData.h"
#import "SCTWPageUserTrack.h"
#import "SCTWIsAvailableData.h"
#import "SCTWAccountInfo.h"
#import "SCTWDefaultCover.h"
#import "SCTWBankInfo.h"
#import "SCTWPreSendRequest.h"
#import "SCTWPreSendResponse.h"
#import "SCTWConfirmRedEnvResponse.h"
#import "SCTWValidateIdentityData.h"
#import "SCTWResendSmsOTPData.h"
#import "SCTWPreSendGroupResponse.h"
#import "SCTWPreSendGroupRequest.h"
#import "SCTWRedEnvelopeRecInqData.h"
#import "SCTWDesendRedEnvData.h"
#import "SCTWResendSmsOTPData.h"
#import "SCTWDesendValidateResponse.h"
#import "SCTWForeignExchangeResponse.h"
#import "SCTWRedEnvelopeRecInqRequest.h"
#import "SCTWDesendRedEnvRequest.h"
#import "SCTWDesendValidateIdentityRequest.h"
#import "SCTWRandomListRequest.h"
#import "SCTWRandomListResponse.h"


typedef void (^SCTWBalanceCompletionBlock)(__kindof PKError *_Nullable error, SCTWBalanceData * _Nullable balanceData);

typedef void (^SCTWIsAvailableCompletionBlock)(__kindof PKError *_Nullable error, SCTWIsAvailableData * _Nullable isAvailableData);

typedef void (^SCTWFavAccountsInfoCompletionBlock)(__kindof PKError *_Nullable error,NSArray<__kindof SCTWAccountInfo*>* _Nullable accounts);

typedef void (^SCTWDefaultCoverResponse)(__kindof PKError *_Nullable error,NSArray<__kindof SCTWDefaultCover*>* _Nullable covers,BOOL renew);

typedef void (^SCTWNotesResponse)(__kindof PKError* _Nullable error, NSString* _Nullable notes);

typedef void (^SCTWBankCodesResponse)(__kindof PKError* _Nullable error, NSArray<__kindof SCTWBankInfo*>* _Nullable banksCodesInfo);

typedef void (^SCTWPreSendResponseCompletionBlock)(__kindof PKError* _Nullable error, SCTWPreSendResponse* _Nullable response);

typedef void (^SCTWConfirmRedEnvResponseCompletionBlock) (__kindof PKError* _Nullable error,SCTWConfirmRedEnvResponse* _Nullable response);

typedef void (^SCTWValidateIdentityDataCompletionBlock)(__kindof PKError* _Nullable error,SCTWValidateIdentityData* _Nullable validateData);

typedef void (^SCTWResendSmsOTPDataCompletionBlock)(__kindof PKError* _Nullable error,SCTWResendSmsOTPData* _Nullable resendSmsOptData);

typedef void (^SCTWRandomListCompletionBlock)(__kindof PKError* _Nullable error, SCTWRandomListResponse* _Nullable response );

typedef void (^SCTWPreSendGroupRersponseCompletionBlock)(__kindof PKError* _Nullable error, SCTWPreSendGroupResponse* _Nullable response);

typedef void (^SCTWRedEnvelopeRecInqDataCompletionBlock) (__kindof PKError* _Nullable error,SCTWRedEnvelopeRecInqData* _Nullable data);

typedef void (^SCTWDesendRedEnvDataCompletionBlock) (__kindof PKError* _Nullable error, SCTWDesendRedEnvData* _Nullable data);

typedef void (^SCTWDesendValidateResponseCompletionBlock) ( __kindof PKError* _Nullable error, SCTWDesendValidateResponse* _Nullable data);

typedef void (^SCTWForeignExchangeResponseCompletionBlock) ( __kindof PKError* _Nullable error, SCTWForeignExchangeResponse* _Nullable data);

typedef void (^SCTWWhatsNewResponse) (__kindof PKError* _Nullable error, NSString* urlWhatsNewResponse);

typedef void (^SCTWContactsResponse) (__kindof PKError* _Nullable error, NSArray<__kindof PKContact*>* contacts);

typedef void (^SCTWResendPWDSMSResponse) (__kindof PKError* _Nullable error, NSString* message);

typedef void (^SCTWAddContactResponse) (__kindof PKError* _Nullable error);

@protocol SCTWDelegate

-(void)onFlowStart;

-(void)onFlowStop;

-(void)loginKBWithLanguage:(NSString*)language andwWithCompletion:(nonnull SCTWBalanceCompletionBlock)completion;

-(void)logoutKBWithIsValidateFail:(BOOL)isValidateFail;

-(void)getKBOutAcctBalance:(nonnull SCTWBalanceCompletionBlock)completion;

-(void)pageUserTrackWithPageTitle:(PageOptions) pageTitle andWithPageUrl:(PageOptions) pageUrl;

-(void)isAvailableWithCompletion:(nonnull SCTWIsAvailableCompletionBlock)completion;

-(void)getTxFavorAccListWithCompletion:(nonnull SCTWFavAccountsInfoCompletionBlock) completion;

-(void)getDefaultCoverWithDefaultCoverList:(nullable NSArray<__kindof SCTWDefaultCover*> *)coverList andWithCompletion:(nonnull SCTWDefaultCoverResponse)completion;

-(void)getNotesWithFunctionId:(NSString*) functionID andWithCompletion:(nonnull SCTWNotesResponse)completion;

-(void)getBanksCodesWithCompletion:(nonnull SCTWBankCodesResponse) completion;

-(void)preSendSingleRedEnvWithRequest:(SCTWPreSendRequest*) request withCompletion:(nonnull SCTWPreSendResponseCompletionBlock) completion;

/**
 * pageCheckId - Id that prevent from resending
 * validationType - Validation Type of users: “fingerprint”: Biometric; “pattern”: pattern; “faceid”: Face Id(Iphone X) ; “otp”: SMS otp validation
 PS: only otp can use when transfer type is GROUP
 */
-(void)confirmRedEnvDetailWithPageCheckId:(NSString*)pageCheckId AndWithValidationType:(NSString*)validationType andWithCompletion:(SCTWConfirmRedEnvResponseCompletionBlock)completion;

/**
 * pageCheckId - *Get from PreSendSingleRedEnv
 * validationCode - numbers in string if validationType from ConfirmRedEnvDetail is otp or
 pattern
 */
-(void)validateIdentityWithPageCheckId:(NSString*)pageCheckId andWithValidationCode:(NSString*)validationCode andWithCompletion:(SCTWValidateIdentityDataCompletionBlock)completion;
    
-(void)sendRedEnvResendSmsOtp:(SCTWResendSmsOTPDataCompletionBlock) completion;

/**

 Response -
 
 Min of range:1
 Max of range :choose the smaller of
 • Transfer limit per transaction
 • User input
 • Remainder from last generating round(EX: 1~10 get 7,
 next round will be 1~(10-7))
 Avoid “4” on units digit
 Total amount equals user input

 @param completion completion with random list
 */
-(void)getRandomListWithTotalAmount:(SCTWRandomListRequest*)request WithCompletion:(SCTWRandomListCompletionBlock)completion;

-(void)preSendGroupRedEnvWithRequest:(SCTWPreSendGroupRequest*)request WithCompletion:(SCTWPreSendGroupRersponseCompletionBlock) completion;

-(void)redEnvelopeRecInqWithRequest:(SCTWRedEnvelopeRecInqRequest*)request AndWithCompletion:(SCTWRedEnvelopeRecInqDataCompletionBlock)completion;

-(void)desendRedEnvWithRequest:(SCTWDesendRedEnvRequest*)request WithCompletion:(SCTWDesendRedEnvDataCompletionBlock) completion;

-(void)desendRedEnvResendSmsOtpWithCpmpletion:(SCTWResendSmsOTPDataCompletionBlock)completion;

-(void)desendValidateIdentityWithRequest:(SCTWDesendValidateIdentityRequest*)request WithCompletion:(SCTWDesendValidateResponseCompletionBlock)completion;

-(void)getForeignExchangeWithCompletion:(SCTWForeignExchangeResponseCompletionBlock)completion;

-(void)getWhatsNewWithCompletion:(SCTWWhatsNewResponse)completion;

- (void)BiometricAuthenticationCompletionBlock:(nonnull BiometricAuthenticationCompletionBlock)completion;

-(void)getContactsWithCompletion:(nonnull SCTWContactsResponse)completion;

-(void)addContacts:(NSArray<__kindof PKContact*>*)contactList WithCompletion:(nonnull SCTWAddContactResponse)completion;

-(void)resendPWDSmsWithID:(NSString*)redEnvId andResponse:(nonnull SCTWResendPWDSMSResponse)completion;

@end

#endif /* SCTADelegate_h */

