//
//  DesendRedEnvData.h
//  PayKey-Clients
//
//  Created by Daniel M. on 04/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCTWDesendRedEnvData : NSObject<NSCoding>

@property (copy,nonatomic) NSString*  pageCheckId; //The code that prevent to re-send
@property (copy,nonatomic,nullable) NSString* phoneNum; // (optional) User’s phone number
@property (copy,nonatomic) NSString* validationType; // Validation Type of users: “fingerprint”: Biometric; “pattern”: pattern; “faceid”: Face Id (Iphone X); “otp”: SMS otp validation.
@property (copy,nonatomic,nullable) NSString* webID;  //(optional) Web page identification code

@end
