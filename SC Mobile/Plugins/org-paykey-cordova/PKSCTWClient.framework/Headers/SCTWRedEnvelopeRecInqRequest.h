//
//  SCTWRedEnvelopeRecInqRequest.h
//  PayKey-Clients
//
//  Created by Daniel M. on 05/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCTWRedEnvelopeRecInqRequest : NSObject<NSCoding>
@property (assign,nonatomic) NSString* type;
@property (assign,nonatomic) NSInteger pageIndex;
@end
