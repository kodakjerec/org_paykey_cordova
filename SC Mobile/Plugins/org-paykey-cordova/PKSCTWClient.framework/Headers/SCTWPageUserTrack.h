//
//  SCTWPageUserTrack.h
//  PayKey-Clients
//
//  Created by Daniel M. on 02/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Page options enum for analytics


 */
typedef NS_ENUM(NSInteger, PageOptions){
    KB_EasyTransfer,
    KB_History,
    KB_ForeignExchange,
    KB_WhatsNew
};

