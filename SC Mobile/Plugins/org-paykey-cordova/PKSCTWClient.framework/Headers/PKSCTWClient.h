//
//  PKSCTWClient.h
//  PKSCTWClient
//
//  Created by ishay weinstock on 12/07/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for SCUAEClient.
FOUNDATION_EXPORT double SCTWClientVersionNumber;

//! Project version string for SCUAEClient.
FOUNDATION_EXPORT const unsigned char SCTWClientVersionString[];


//#import "SCUAEDelegate.h"
#import "SCTWPageUserTrack.h"
#import "SCTWAccountInfo.h"
#import "SCTWBalanceData.h"
#import "SCTWBankInfo.h"
#import "SCTWIsAvailableData.h"
#import "SCTWDefaultCover.h"
#import "SCTWPreSendRequest.h"
#import "SCTWPreSendResponse.h"
#import "SCTWDelegate.h"
