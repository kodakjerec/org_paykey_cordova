//
//  SCTWPreSendRequest.h
//  PayKey-Clients
//
//  Created by Daniel M. on 02/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

@import PayKeyUI;
#import "SCTWPreSendCoveredRequest.h"

@interface SCTWPreSendRequest : SCTWPreSendCoveredRequest
@property (strong,nonatomic,nonnull)NSNumber* amount;
@property (copy,nonatomic,nullable)NSString* greetings;
@property (copy,nonatomic,nullable)NSString* inAcctNo;
@property (copy,nonatomic,nullable)NSString* inBankCode;
@property (assign,nonatomic)NSInteger inAcctType;

/**
 Contact information. Required if inAcctType is 3.
 */
@property (strong,nonatomic,nullable)PKContact* reciver;

@end
