//
//  SCTWDesendRedValdiationTypeOptions.h
//  PayKey-Clients
//
//  Created by Daniel M. on 05/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#ifndef SCTWValdiationTypeOptions_h
#define SCTWValdiationTypeOptions_h

typedef NS_ENUM(NSInteger, SCTWValdiationTypeOptionsEnum){
    SCTWValdiationTypeOptionsEnumFirst,
    SCTWValdiationTypeOptionsEnumTOUCH_ID,
    SCTWValdiationTypeOptionsEnumPATTERN,
    SCTWValdiationTypeOptionsEnumSMS_OTP,
    SCTWValdiationTypeOptionsEnumFACE_ID
};

#endif /* SCTWDesendRedValdiationTypeOptions_h */
