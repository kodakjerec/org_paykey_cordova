//
//  SCTWPreSendGroupResponse.h
//  BaseDelegate
//
//  Created by Daniel M. on 04/09/2018.
//

#import <Foundation/Foundation.h>

@interface SCTWPreSendGroupResponse : NSObject<NSCoding>


/**
 (optional) The greeting that user chose or edit by himself
 */
@property ( nonatomic,copy,nullable) NSString* greetings;

/**
 The code that prevent to re-send
 */
@property (nonatomic, copy) NSString* pageCheckId;


/**
(optional) Will exist if “type” = 2(encode required)
 */
@property (nonatomic,copy,nullable) NSString* validationCode;

/**
 Type of send red envelope
 */
@property (copy,nonatomic,nullable) NSString* amountType;


/**
 Random red envelope amount list needs to be passed when amountType = 2.(optional)
 */
@property (nonatomic, strong,nullable) NSArray<__kindof NSNumber*>* amtList;


/**
 The average of fix amount needs to be passed when amountType = 1.
 */
@property (nonatomic,strong,nullable ) NSString* fixAmt;



/**
 The lowest random amount needs to be passed when amountType = 2.(optional)
 */
@property (nonatomic,strong,nullable) NSString* minAmt;


/**
 The maximum random amount needs to be passed when amountType = 2.(optional)
 */
@property (nonatomic,strong,nullable) NSString* maxAmt;


/**
 The total amount of red envelope sent.
 */
@property (nonatomic,strong,nullable) NSString* totalAmount;


/**
 The total number of red envelopes sent (maximum:100).
 */
@property (nonatomic,assign)NSInteger totalNum;

@end
