//
//  SCTWRedEnvDetail.h
//  PayKey-Clients
//
//  Created by Daniel M. on 04/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCTWRedEnvDetail : NSObject<NSCoding>
@property (copy,nonatomic) NSString* amount;  //Amount of the envelope
@property (copy,nonatomic) NSString* txDateTime; // Trasaction date of the red envelope
@property (copy,nonatomic) NSString* status; // Status of the envelope Finish／ Process／Expired／Fail／Cancel
@end
