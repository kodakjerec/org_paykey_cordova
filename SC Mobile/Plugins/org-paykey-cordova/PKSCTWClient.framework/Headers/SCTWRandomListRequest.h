//
//  SCTWRandomListRequest.h
//  PayKey-Clients
//
//  Created by Daniel M. on 13/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCTWRandomListRequest : NSObject<NSCoding>
@property (strong,nonatomic)NSNumber* totalAmount;
@property (strong,nonatomic)NSNumber* totalNum;
@end
