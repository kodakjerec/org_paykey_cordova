//
//  SCTWGroupRedEnvRec.h
//  PayKey-Clients
//
//  Created by Daniel M. on 04/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import "SCTWRedEnvDetail.h"
#import "SCTWRedEnvRec.h"

#ifndef SCTWGroupRedEnvRec_H
#define SCTWGroupRedEnvRec_H

@interface SCTWGroupRedEnvRec : SCTWRedEnvRec

@property (copy,nonatomic)NSString* amountType; // Type of sending envelope
@property (copy,nonatomic)NSString* cover; // Cover in base 64
@property (copy,nonatomic)NSString* recivedAmt; // Amount of envelope that already received
@property (assign,nonatomic)NSInteger recivedNum; // Quantity of envelopes that already received
@property (assign,nonatomic)NSInteger totalNum; // Total quantity of the envelopes

// (optional)
@property (copy,nonatomic)NSString* fixAmt;
@property (copy,nonatomic)NSString* maxAmt;
@property (copy,nonatomic)NSString* minAmt;
@property (strong,nonatomic,nullable) NSArray<__kindof SCTWRedEnvDetail*>* redEnvelopeDetailList;
@end
#endif
