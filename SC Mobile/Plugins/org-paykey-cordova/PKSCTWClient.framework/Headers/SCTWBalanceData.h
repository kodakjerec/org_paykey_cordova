//
//  SCTABankBalance.h
//  PayKey-Clients
//
//  Created by Daniel M. on 02/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 a class representing bank balance data
 */
@interface SCTWBalanceData : NSObject<NSCoding>

/**
 the user's account balance
 */
@property (copy,nonatomic) NSString* accountBalance;


/**
 the format of the account balance property
 */
@property (copy,nonatomic) NSString* accountBalanceFormat;


@end
