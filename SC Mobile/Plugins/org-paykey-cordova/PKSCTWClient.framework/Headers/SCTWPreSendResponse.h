//
//  PreSendResponse.h
//  PayKey-Clients
//
//  Created by Daniel M. on 02/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCTWPreSendResponse : NSObject<NSCoding>

@property (copy,nonatomic,nonnull) NSString* amount;
@property (copy,nonatomic,nullable) NSString* greetings;
@property (copy,nonatomic,nonnull) NSString* pageCheckID;

@end
