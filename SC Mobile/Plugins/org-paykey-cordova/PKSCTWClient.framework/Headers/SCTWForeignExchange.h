//
//  ForeignExchange.h
//  PayKey-Clients
//
//  Created by Daniel M. on 04/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCTWForeignExchange : NSObject<NSCoding>

/**
  Cash exchange rate
 */
@property (copy,nonatomic)NSString* cash;

/**
  spot exchange rate
 */
@property (copy,nonatomic)NSString* telegraphic;

/**
 Currency name: American(USD) Hong Kong(HKD) British(GBP) Australia(AUD) Canada(CAD) Singapore(SGD) Switzerland(CHF) Japan(JPY)
 Thailand(THB) EU(EUR)
 New Zealand(NZD) South Africa(ZAR) Sweden (SEK) China(CNY)
 */
@property (copy,nonatomic)NSString* currencyName;

@end
