//
//  SCTWSingleRedEnvRec.h
//  PayKey-Clients
//
//  Created by Daniel M. on 04/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import "SCTWRedEnvRec.h"
@interface SCTWSingleRedEnvRec : SCTWRedEnvRec

@property (copy,nonatomic) NSString* txDateTime; // Transaction date of the envelope (Chinese date: yyyy/mm/dd HH:mm:ss、English date: HH:mm:ss Jan 14 2018)

@end
