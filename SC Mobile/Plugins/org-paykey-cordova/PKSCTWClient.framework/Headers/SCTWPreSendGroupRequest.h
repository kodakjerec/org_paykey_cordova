

//
//  SCTWPreSendGroupRequest.h
//  PayKey-Clients
//
//  Created by Daniel M. on 04/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

@import PayKeyUI;
#import "SCTWPreSendCoveredRequest.h"

@interface SCTWPreSendGroupRequest :SCTWPreSendCoveredRequest

/**
 The way of sending red envelope amount. (F: fix amount, R: random amount)
 */
@property( nonatomic, copy ) NSString *amountType;

/**
 (optional) The greeting that user chose or edit by himself
 */
@property( nonatomic, copy, nullable ) NSString *greetings;

/**
 Random red envelope amount list needs to be passed when amountType =
 2.(optional)
 */
@property( nonatomic, strong, nullable ) NSArray<__kindof NSNumber *> *amtList;

/**
 The average of fix amount needs to be passed when amountType = 1.
 */
@property( nonatomic, strong, nullable ) NSString *fixAmt;

/**
 The lowest random amount needs to be passed when amountType = 2.(optional)
 */
@property( nonatomic, strong, nullable ) NSString *minAmt;

/**
 The maximum random amount needs to be passed when amountType = 2.(optional)
 */
@property( nonatomic, strong, nullable ) NSString *maxAmt;

/**
The total amount of red envelope sent.
 */
@property( nonatomic, strong, nullable ) NSString *totalAmount;

/**
The total number of red envelopes sent (maximum:100).
 */
@property( nonatomic, assign ) NSInteger totalNum;

@property (nonatomic,strong)NSArray<__kindof PKContact*>* receiverList;

@end

