//
//  SCTWPreSendCoveredRequest.h
//  SCTWClient
//
//  Created by Daniel M. on 27/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCTWPreSendCoveredRequest : NSObject<NSCoding>
/**
 (optional) Image to base64
 */
@property (nonatomic,copy,nullable) NSString* uploadCover;


/**
 The id of greeting card’s type(id is 99 when user upload picture in base64)
 */
@property (nonatomic,assign)NSInteger coverType;





@end
