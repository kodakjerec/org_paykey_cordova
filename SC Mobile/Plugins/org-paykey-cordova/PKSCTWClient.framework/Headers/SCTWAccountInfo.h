//
//  SCTWAccountInfo.h
//  PayKey-Clients
//
//  Created by Daniel M. on 02/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCTWAccountInfo : NSObject<NSCoding>
/**
 String storing  Information of user’s binding account with format
 */
@property (copy,nonatomic) NSString* acctFmt;


/**
 String storing information of user’s binding account
 */
@property (copy,nonatomic) NSString* accNo;


/**
 string storing Bank code list in TW.
 */
@property (copy,nonatomic) NSString* bankCode;
@end
