//
//  SCTWDesendValidateResponse.h
//  PayKey-Clients
//
//  Created by Daniel M. on 04/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCTWGroupRedEnvRec.h"
#import "SCTWSingleRedEnvRec.h"

@interface SCTWDesendValidateResponse : NSObject<NSCoding>

@property (copy,nonatomic)NSString* redEnvType; //  The type of red Envelope: S: individual red envelope G: group red envelope

@property (assign,nonatomic)BOOL success;

@property (strong,nonatomic,nullable) SCTWGroupRedEnvRec* groupRedEnvRec; // Update group red envelope record when redEnvType = G

@property (strong,nonatomic,nullable) SCTWSingleRedEnvRec* singelRedEnvRec; //  Update individual red envelope record when redEnvType = S

@end
