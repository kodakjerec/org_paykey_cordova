//
//  ValidateIdentityData.h
//  PayKey-Clients
//
//  Created by Daniel M. on 03/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCTWValidateIdentityData : NSObject<NSCoding>
@property (copy,nonatomic) NSString* message;

@end
