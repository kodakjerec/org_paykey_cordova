//
//  SCTWRedEnvelopeRecInqTypeOptions.h
//  SCTWClient
//
//  Created by Daniel M. on 05/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#ifndef SCTWRedEnvelopeRecInqTypeOptions_h
#define SCTWRedEnvelopeRecInqTypeOptions_h

// Page options enum for analytics
typedef NS_ENUM(NSInteger, SCTWRedEnvelopeRecInqTypeOptions){
    SCTWRedEnvelopeRecInqTypeOptionsGroup,
    SCTWRedEnvelopeRecInqTypeOptionsSingle
};

#endif /* SCTWRedEnvelopeRecInqTypeOptions_h */
