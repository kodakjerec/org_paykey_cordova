//
//  SCTWResendSmsOTPData.h
//  PayKey-Clients
//
//  Created by Daniel M. on 03/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//
#ifndef SCTWResendSmsOTPData_HFILE
#define SCTWResendSmsOTPData_HFILE
#import <Foundation/Foundation.h>

@interface SCTWResendSmsOTPData : NSObject<NSCoding>
@property (copy,nonatomic) NSString* mobilePhone;
@property (copy,nonatomic) NSString* pageCheckId;
@property (copy,nonatomic) NSString* webId;
@end
#endif
