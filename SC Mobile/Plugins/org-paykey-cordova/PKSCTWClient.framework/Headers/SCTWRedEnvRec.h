//
//  SCTWRedEnvRec.h
//  SCTWClient
//
//  Created by Eran Israel on 09/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
             
@interface SCTWRedEnvRec : NSObject<NSCoding>

@property (copy,nonatomic) NSString* cancelDateTime; // Cancel date of the envelope(Chinese date: yyyy/mm/dd HH:mm:ss、 English date: HH:mm:ss Jan 14 2018)
@property (copy,nonatomic) NSString* expireDateTime; // Expire date of the envelope (Chinese date: yyyy/mm/dd HH:mm:ss、 English date: HH:mm:ss Jan 14 2018)
@property (copy,nonatomic) NSString* greetings; // (optional) The greeting that user chose or edit by himself
@property (copy,nonatomic) NSString* envelopeID; // Id of the envelope
@property (copy,nonatomic) NSString* link; // Link of the envelope
@property (copy,nonatomic) NSString* message; // The 1st share message
@property (copy,nonatomic) NSString* pwdMessage; // (Optional) The 2nd share message with password (If hasWithdrawalPwd is true)
@property (copy,nonatomic) NSString* sendDateTime; // Date of sending envelope (Chinese date: yyyy/mm/dd HH:mm:ss、 English date: HH:mm:ss Jan 14 2018)
@property (copy,nonatomic) NSString* status; // Status of the envelope: Finish/Process/Expired/Fail/Cancel
@property (copy,nonatomic) NSString* totalAmount; // Total amount of the envelope
@property (copy,nonatomic) NSString* validationCode; // Withdraw password

@property (assign,nonatomic) BOOL cancelMark; // If user can cancel or not
@property (assign,nonatomic) BOOL hasWithdrawalPwd; // If the withdrawal password comes from user


/**
 If this transaction allows to resend withdrawal password via SMS OTP
 true: show the clickable resend button
 false: hide the resend button
 */
@property (assign,nonatomic) BOOL resendPwdSmsMark;


/**
 1: select option1, select a favor account for individual transfer
 2: select option2, input an account manually to transfer 3: select option3, without any account.
 */
@property (assign,nonatomic) NSInteger inAcctType;


@end
