//
//  SCTWIsAvailableData.h
//  PayKey-Clients
//
//  Created by Daniel M. on 02/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCTWIsAvailableData : NSObject<NSCoding>
@property (assign,nonatomic) BOOL easyTransferAvailable;
@property (copy,nonatomic) NSString* message;
@end
