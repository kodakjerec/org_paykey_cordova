//
//  DesendValidateIdentityRequest.h
//  PayKey-Clients
//
//  Created by Daniel M. on 05/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCTWDesendValidateIdentityRequest : NSObject<NSCoding>
@property (copy,nonatomic) NSString* pageCheckID;
@property (copy,nonatomic) NSString* validationCode;
@end
