//
//  SCTWRandomListResponse.h
//  PayKey-Clients
//
//  Created by Daniel M. on 13/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCTWRandomListResponse : NSObject<NSCoding>

@property (strong,nonatomic) NSArray<__kindof NSString*>* _Nullable randomList;
@property (strong, nonatomic) NSString* maxAmt;
@property (strong,nonatomic) NSString* minAmt;

@end
