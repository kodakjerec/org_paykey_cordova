//
//  SCTWBankInfo.h
//  PayKey-Clients
//
//  Created by Daniel M. on 02/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCTWBankInfo : NSObject<NSCoding>
@property (copy,nonatomic) NSString* bankCode;
@property (copy,nonatomic) NSString* bankName;
@end
