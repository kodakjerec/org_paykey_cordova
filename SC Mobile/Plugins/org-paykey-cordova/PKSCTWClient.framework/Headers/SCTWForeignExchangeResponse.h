//
//  ForeignExchangeResponse.h
//  PayKey-Clients
//
//  Created by Daniel M. on 04/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCTWForeignExchange.h"
@interface SCTWForeignExchangeResponse : NSObject<NSCoding>

/**
 Information of foreign exchange buy
 */
@property (nonatomic,strong) NSArray<__kindof SCTWForeignExchange*>* foreignExchangeInfosOfBuy;


/**
 Information of foreign exchange sell
 */
@property (nonatomic,strong) NSArray<__kindof SCTWForeignExchange*>* foreignExchangeInfosOfSell;


/**
 the last of update time(HH:mm:ss MMMM dd yyyy)
 */
@property (nonatomic,copy) NSString* lastUpdateTime;
@end
