//
//  SCTWDefaultCover.h
//  PayKey-Clients
//
//  Created by Daniel M. on 02/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import <UIKit/UIKit.h>
 #import <Foundation/Foundation.h>
@interface SCTWDefaultCover : NSObject<NSCoding>


/**
 default cover 
 */
@property (copy,nonatomic) NSString* defaultCover;



/**
 cover identifier
 */
@property (copy,nonatomic)NSNumber* coverID;


/**
 time of update in format (yyyy/MM/dd HH:mm:ss)
 */
@property (copy,nonatomic) NSString* updateDateTime;
@end
