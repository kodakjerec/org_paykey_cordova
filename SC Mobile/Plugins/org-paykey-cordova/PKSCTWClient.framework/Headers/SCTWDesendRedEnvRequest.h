//
//  SCTWDesendRedEnvRequest.h
//  PayKey-Clients
//
//  Created by Daniel M. on 05/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SCTWDesendRedEnvRequest : NSObject<NSCoding>
@property (copy,nonatomic) NSString* redEnvelopeID;
@property (copy,nonatomic) NSString* validationType; // Validation Type of users: “fingerprint”: Biometric; “pattern”: pattern; “faceid”: Face Id (Iphone X); “otp”: SMS otp validation.
@end
