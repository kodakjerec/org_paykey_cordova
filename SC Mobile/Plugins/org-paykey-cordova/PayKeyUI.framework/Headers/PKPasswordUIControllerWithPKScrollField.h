//
//  PKPasswordUIControllerWithPKScrollField.h
//  KeyboardApp
//
//  Created by Alex Kogan on 14/01/2017.
//  Copyright © 2017 Marat . All rights reserved.
//

#import "PKScrollTextFieldUIController.h"
#import "PKPasswordUIOutputWithPKScrollField.h"
#import "PasswordViewDelegate.h"


@interface PKPasswordUIControllerWithPKScrollField : PKScrollTextFieldUIController <PasswordViewDelegate>
@property (nonatomic, weak) id <PKPasswordUIOutputWithPKScrollField> passwordOutput;
@end
