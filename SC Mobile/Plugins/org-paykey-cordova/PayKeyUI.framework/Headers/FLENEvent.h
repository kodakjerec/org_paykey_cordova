//
//  PKEventInteractor.h
//  PayKeyUI
//
//  Created by German Velibekov on 23/12/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FLENEventListener.h"

@class PKViewInteractorAggregator;
@class PKUIController;

static NSString * _Nonnull const kEventResult = @"kEventResult";

NS_ASSUME_NONNULL_BEGIN

@protocol FLENEvent

@property (nonatomic, weak)id<FLENEventListener> delegate;

-(void)subscribeWithTarget:(id<FLENEventListener>)target
                aggregator:(PKViewInteractorAggregator*)model;

NS_ASSUME_NONNULL_END

@end
