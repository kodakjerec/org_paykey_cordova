//
// Created by Alex Kogan on 02/04/2018.
// Copyright (c) 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PKViewModel;

@protocol PKViewModel <NSObject>

-(void)applyToView:(UIView*)view;

@end
