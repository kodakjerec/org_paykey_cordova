//
// Created by Alex Kogan on 02/04/2018.
// Copyright (c) 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKViewModel.h"
#import "PKViewObserver.h"

@interface PKControlViewModel : PKViewObserver <PKViewModel>

@property (nonatomic) BOOL enabled;
@property (nonatomic) BOOL selected;

@end
