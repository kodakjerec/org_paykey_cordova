//
//  PKContactsProvider.h
//  PayKeyUI
//
//  Created by Eden Landau on 15/03/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKContact.h"
#import "PKSendToPhoneProtocolWrapperArray.h"

typedef BOOL (^PKFiltersContacts)(PKContact *, NSDictionary *);

@protocol PKContactFetcher <NSObject>
- (NSArray<PKContact *> *)getContacts;
- (NSArray<PKContact *> *)getContacts:(NSError **)error;

@end

@protocol PKContactProviderStrategy <NSObject>

- (NSArray<PKContact *> *)executeStrategy:(NSArray<PKContact *> *)contacts;

@end

@interface PKContactsProvider : NSObject

@property (nonatomic, strong) id <PKContactFetcher> contactFetcher;
@property (nonatomic, strong) NSString *filter;

- (NSArray *)getContacts;
- (NSArray *)getContacts:(NSError **)error;
- (void)addStrategy:(id <PKContactProviderStrategy>)strategy;
- (NSArray<id <PKContactProviderStrategy>> *)getStrategies;

@end
