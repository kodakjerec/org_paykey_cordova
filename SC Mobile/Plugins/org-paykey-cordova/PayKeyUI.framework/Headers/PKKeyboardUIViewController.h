//
//  PKKeyboardUIViewController.h
//  KeyboardApp
//
//  Created by Eden Landau on 08/01/2017.
//  Copyright © 2017 Marat . All rights reserved.
//

#import "PKUIKeyboardConfig.h"
#import "PKNetworkAccessible.h"
#import "PKPaymentInputDecorator.h"

@import KeyboardFramework;

@interface PKKeyboardUIViewController : KeyboardController <PKNetworkAccessible>

@property (nonatomic, strong) PKUIKeyboardConfig * keyboardConfig;
@property (nonatomic,strong) __kindof PKPaymentInputDecorator *inputDecorator;

@end
