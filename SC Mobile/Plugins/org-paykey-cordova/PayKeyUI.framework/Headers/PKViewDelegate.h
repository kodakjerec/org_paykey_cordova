//
//  PKViewDelegate.h
//  PayKeyUI
//
//  Created by Eden Landau on 11/07/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

@protocol PKViewDelegate <NSObject>

@required
- (void)continueRun:(id)result;

@optional
- (void)abortTapped __attribute__((deprecated));
- (void)backTapped __attribute__((deprecated));
- (void)navigateLeft;
- (void)navigateRight;
- (void)inputUpdated:(NSString *)newInput;
- (void)reportEvent:(NSString*)eventName eventParams:(NSDictionary*)eventParams;
- (void)reportEvent:(NSString*)eventName;

@end
