//
//  PKResponderHandler.h
//  PayKeyUI
//
//  Created by Daniel M. on 26/05/2019.
//  Copyright © 2019 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PKResponderHandler : NSObject

+(BOOL) shouldRespond;

@end

NS_ASSUME_NONNULL_END
