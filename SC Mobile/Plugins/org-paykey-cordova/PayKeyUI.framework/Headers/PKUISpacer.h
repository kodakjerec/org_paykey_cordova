//
//  PKUISpacer.h
//  MakePayment
//
//  Created by Jonathan Beyrak-Lev on 18/01/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

#define spacer(aSize, aPriority) [[PKSpacer alloc] initWithSize:aSize priority:aPriority]

@protocol PKUISpacer <NSObject>

@property (readonly) CGFloat size;
@property (readonly) NSInteger priority;

- (CGFloat)relativeOn:(UILayoutConstraintAxis)axis;
- (CGFloat)relativeHeight;
- (CGFloat)relativeWidth;

@end

@interface PKConcreteSpacer : NSObject <PKUISpacer>

- (instancetype)initWithSize:(CGFloat)size priority:(CGFloat)priority;

@end
