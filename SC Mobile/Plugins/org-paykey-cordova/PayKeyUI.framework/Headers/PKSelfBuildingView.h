//
//  PKSelfBuildingView.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 07/02/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PKSelfBuildingView : UIView

- (instancetype)build;

@end
