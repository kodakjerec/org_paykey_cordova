//
//  NSTimer+FireTools.h
//  PayKeyUI
//
//  Created by Daniel M. on 13/03/2019.
//  Copyright © 2019 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSTimer (FireTools)

+ (NSTimer*)setTimerWithFireDate:(NSDate *)fireDate fireNow:(BOOL)fireNow andBlock:(void (^)(void))fireBlock;

@end

NS_ASSUME_NONNULL_END
