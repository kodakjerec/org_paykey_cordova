//
//  NSNumber+PKTools.h
//  PayKeyUI
//
//  Created by Eran Israel on 05/03/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSNumber (PKTools)

- (UIImage*)imageFromNumberWithFont:(UIFont*)font andBackgroundColor:(UIColor*)color;

@end
