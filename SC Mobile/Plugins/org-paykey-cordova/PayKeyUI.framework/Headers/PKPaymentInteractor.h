//
//  PKPaymentInteractor.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 24/10/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import "PKPaymentData.h"
#import "PKPaymentActions.h"
#import "PKDataStoringStrategy.h"

extern NSString * const PaymentFlowAmount;
extern NSString * const PaymentFlowPayee;
extern NSString * const PaymentFlowShouldRestore;

@interface PKPaymentInteractor : NSObject <PKPaymentData, PKPaymentActions>

@property (strong, nonatomic) id <PKDataStoringStrategy> store;

- (instancetype)initWithStore:(id <PKDataStoringStrategy>)store;
- (void)flushHeavyData;

@end
