//
// Created by Alex Kogan on 03/04/2018.
// Copyright (c) 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PKTrackEventListener <NSObject>
- (void)reportEvent:(NSString*)eventName eventParams:(NSDictionary*)eventParams;
@end