//
//  PKConstraintBaseModel.h
//  PayKeyUI
//
//  Created by Daniel M. on 23/12/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKConstraintModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PKConstraintBaseModel : NSObject <PKConstraintModel>

@property (assign,nonatomic) NSUInteger priority;

@property (assign, nonatomic)NSUInteger constant;

@property (weak,nonatomic)NSLayoutConstraint* constraint;

- (void)applyToConstraint:(NSLayoutConstraint*)constraint;

@end

NS_ASSUME_NONNULL_END
