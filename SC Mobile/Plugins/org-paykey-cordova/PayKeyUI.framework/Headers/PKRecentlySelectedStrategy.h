//
//  PKRecentlySelectedStrategy.h
//  PayKeyUI
//
//  Created by Marat  on 7/9/17.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import "PKContactsProvider.h"

@interface PKRecentlySelectedStrategy : NSObject <PKContactProviderStrategy>

- (instancetype)initWithCount:(NSUInteger)size;

@end
