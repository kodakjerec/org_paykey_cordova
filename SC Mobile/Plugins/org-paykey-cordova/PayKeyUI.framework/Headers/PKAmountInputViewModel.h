//
//  PKAmountInputViewModel.h
//  PayKeyUI
//
//  Created by German Velibekov on 29/10/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKViewModel.h"
#import "PKCurrencyLabel.h"
#import "PKViewObserver.h"

NS_ASSUME_NONNULL_BEGIN

@interface PKAmountInputViewModel : PKViewObserver <PKViewModel>

@property (nonatomic) BOOL showCurrencyInPlaceHolder;
@property (nonatomic) PKCurrencyGravity currencyGravity;
@property (nonatomic) CurrencySymbolPosition currencyPosition;
@property (nonatomic) BOOL isPlaceholderSymbolNormalSize;
@property (nonatomic) CGFloat symbolToTextMargin;
@property (nonatomic) NSString* overrideCurrencySymbol;
@property (assign,nonatomic) CGFloat capHeightDevider;
@property (assign, nonatomic) CGFloat currencySymbolCoefficient;
@property (strong,nonatomic) PKCurrencyInputHandlerSettings* settings;

@end

NS_ASSUME_NONNULL_END
