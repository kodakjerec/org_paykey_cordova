//
//  UIColor+EasyGradient.h
//  MakePayment
//
//  Created by Jonathan Beyrak-Lev on 09/01/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (EasyGradient)

- (CGGradientRef)newGradientToColor:(UIColor *)color;

@end

