//
//  PKUserDefaultsSessionStoring.h
//  PayKeyUI
//
//  Created by Alex Kogan on 25/09/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PKUserDefaultsSessionStoring : NSObject

- (instancetype)initNSUserDefaults:(NSUserDefaults *)userDefaults storeKey:(NSString *)key validTime:(NSUInteger)seconds;
- (instancetype)initNSUserDefaults:(NSUserDefaults *)userDefaults storeKey:(NSString *)key;
- (void)dumpedData:(NSData *)data;
- (NSData *)provideRestoreData;

@end
