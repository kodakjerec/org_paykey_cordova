//
//  PKControllerNavigationListener.h
//  PayKeyUI
//
//  Created by German Velibekov on 18/11/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol PKControllerNavigationListener <NSObject>

@optional

- (void)navigateRight;
- (void)navigateLeft;

@end

NS_ASSUME_NONNULL_END
