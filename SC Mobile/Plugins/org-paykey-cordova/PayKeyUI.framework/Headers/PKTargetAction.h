//
//  PKTargetAction.h
//  MakePayment
//
//  Created by Jonathan Beyrak-Lev on 16/01/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

#define targetAction(anAction) [[PKTargetAction alloc] initWithTarget:self action:@selector(anAction)]

@interface PKTargetAction : NSObject

@property (nonatomic, strong) NSObject *target;
@property (nonatomic) SEL action;

- (instancetype)initWithTarget:(NSObject *)target action:(SEL)action;

@end
