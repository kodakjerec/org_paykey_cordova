//
//  PKLegacyViewWithCursor.h
//  PayKeyUI
//
//  Created by ishay weinstock on 15/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKEditTextProtocol.h"
typedef NS_ENUM(NSInteger, PKLegacyViewCursorMode) {
    PKLegacyViewCursorModeNever,
    PKLegacyViewCursorModeWhileEditing,
    PKLegacyViewCursorModeAlways
};

typedef NS_ENUM(NSInteger, PKLegacyCursorCalculationType) {
    PKLegacyCursorCalculationTypeRect,
    PKLegacyCursorCalculationTypeChar
};


@interface PKLegacyViewWithCursor : UIView <UIKeyInput,UITextInputTraits,PKEditTextProtocol>

@property (nonatomic, strong) UIColor *cursorColor;
@property (nonatomic) PKLegacyViewCursorMode cursorMode;
@property (nonatomic) PKLegacyCursorCalculationType cursorCalculationType;
@property (nonatomic) UITextAutocorrectionType autocorrectionType;
@property (nonatomic, assign) BOOL disabled;
- (CGFloat)calcCursorXPosition;
- (CGRect)calcCursorRect;
- (CGFloat)cursorHeight;
- (void)updateCursorPosition;
- (void)setupCursor;

- (void)beginEditing;
- (BOOL)endEditing;

@end
