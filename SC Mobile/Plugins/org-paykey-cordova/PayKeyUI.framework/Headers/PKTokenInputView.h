//
//  PKTokenInputView.h
//  PKTokenInputView
//
//  Created by Rizwan Sattar on 2/24/14.
//  Copyright (c) 2014 Cluster Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PKToken.h"
#import "PKTextField.h"

@import KeyboardFramework;

#if __has_feature(objc_generics)
#define PK_GENERIC_ARRAY(type) NSArray<type>
#define PK_GENERIC_MUTABLE_ARRAY(type) NSMutableArray<type>
#define PK_GENERIC_SET(type) NSSet<type>
#define PK_GENERIC_MUTABLE_SET(type) NSMutableSet<type>
#else
#define PK_GENERIC_ARRAY(type) NSArray
#define PK_GENERIC_MUTABLE_ARRAY(type) NSMutableArray
#define PK_GENERIC_SET(type) NSSet
#define PK_GENERIC_MUTABLE_SET(type) NSMutableSet
#endif

NS_ASSUME_NONNULL_BEGIN

@class PKTokenViewConfig;
@class PKTokenInputView;

@protocol PKTokenInputTextDelegate <NSObject>

@required

- (PKTokenViewConfig*)tokenViewConfig;

@optional

- (BOOL)tokenInputView:(PKTokenInputView *)tokenInputView shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
- (BOOL) tokenInputViewShouldAddTextWhenNoSpace:(PKTokenInputView *)tokenInputView;
- (KeyModelUI*)tokenInputTextReturnKeyModel;
- (void)tokenInputViewDidEndEditing:(PKTokenInputView *)view;
- (void)tokenInputViewDidBeginEditing:(PKTokenInputView *)view;
- (BOOL)tokenInputViewShouldReturn:(PKTokenInputView *)view;
- (void)tokenInputView:(PKTokenInputView *)view didChangeText:(nullable NSString *)text;
@end

@protocol PKTokenInputViewDelegate <NSObject,PKTokenInputTextDelegate>

@optional
/**
 * Called when a token has been added. You should use this opportunity to update your local list of selected items.
 */
- (void)tokenInputView:(PKTokenInputView *)view didAddToken:(PKToken *)token;
/**
 * Called when a token has been removed. You should use this opportunity to update your local list of selected items.
 */
- (void)tokenInputView:(PKTokenInputView *)view didRemoveToken:(PKToken *)token;
/** 
 * Called when the user attempts to press the Return key with text partially typed.
 * @return A PKToken for a match (typically the first item in the matching results),
 * or nil if the text shouldn't be accepted.
 */
- (nullable PKToken *)tokenInputView:(PKTokenInputView *)view tokenForText:(NSString *)text;
/**
 * Called when the view has updated its own height. If you are
 * not using Autolayout, you should use this method to update the
 * frames to make sure the token view still fits.
 */
- (void)tokenInputView:(PKTokenInputView *)view didChangeHeightTo:(CGFloat)height;

-(CGFloat)defualtMinSizeForTokenInputView:(PKTokenInputView *)view;

-(void)tokenInputView:(PKTokenInputView *)view userDidTapOnView:(UIView*)view;

-(CGFloat)topPaddingForTokenInputView:(PKTokenInputView*)view;

-(CGFloat)bottomPaddingForTokenInputView:(PKTokenInputView*)view;

@end

@interface PKTokenInputView : UIView<PKTextDocumentProxy, PKTextFieldDelegate>

@property (weak, nonatomic, nullable) IBOutlet NSObject <PKTokenInputViewDelegate> *delegate;

/** An optional view that shows up presumably on the first line */
@property (strong, nonatomic, nullable) UIView *fieldView;
/** Option text which can be displayed before the first line (e.g. "To:") */
@property (copy, nonatomic, nullable) IBInspectable NSString *fieldName;
/** Color of optional */
@property (strong, nonatomic, nullable) IBInspectable UIColor *fieldColor;
@property (copy, nonatomic, nullable) IBInspectable NSString *placeholderText;
@property (strong,nonatomic) UIColor* placeHolderColor;
@property (strong,nonatomic) UIFont* placeHolderFont;
@property (strong, nonatomic, nullable) UIView *accessoryView;
@property (assign, nonatomic) IBInspectable UIKeyboardType keyboardType;
@property (assign, nonatomic) IBInspectable UITextAutocapitalizationType autocapitalizationType;
@property (assign, nonatomic) IBInspectable UITextAutocorrectionType autocorrectionType;
@property (assign, nonatomic) IBInspectable UIKeyboardAppearance keyboardAppearance;

/** 
 * Optional additional characters to trigger the tokenization process (and call the delegate
 * with `tokenInputView:tokenForText:`
 * @discussion By default this array is empty, as only the Return key will trigger tokenization
 * however, if you would like to trigger tokenization with additional characters (such as a comma,
 * or as a space), you can supply the list here.
 */
@property (copy, nonatomic) PK_GENERIC_SET(NSString *) *tokenizationCharacters;
@property (assign, nonatomic) IBInspectable BOOL drawBottomBorder;

@property (readonly, nonatomic) PK_GENERIC_ARRAY(PKToken *) *allTokens;
@property (readonly, nonatomic, getter = isEditing) BOOL editing;
@property (readonly, nonatomic) CGFloat textFieldDisplayOffset;
@property (copy, nonatomic, nullable) NSString *text;


- (void)addToken:(PKToken *)token;
- (void)removeToken:(PKToken *)token;
- (nullable PKToken *)tokenizeTextfieldText;
- (void)beginEditing;
- (void)endEditing;
- (void)clearTextField;
- (CGRect)frameForTextField;
- (BOOL)isSecureTextEntry;
@end

NS_ASSUME_NONNULL_END
