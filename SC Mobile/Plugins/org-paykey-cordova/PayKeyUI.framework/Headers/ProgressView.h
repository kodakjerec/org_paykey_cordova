//
//  ProgressView.h
//  PayKeyUI
//
//  Created by Alex Kogan on 09/02/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import "PKScreen.h"
#import "PKNavigationBar.h"

@interface ProgressView : PKScreen

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
