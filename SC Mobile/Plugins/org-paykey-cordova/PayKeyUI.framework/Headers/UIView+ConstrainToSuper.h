//
//  UIView+ConstrainToSuper.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 14/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView(ConstrainToSuper)

- (__kindof UIView *)fillWithSubview:(UIView *)subview;
- (instancetype)filledWithSubview:(UIView *)subview;
- (void)constrainToSuperview;
- (void)addConstraintArrays:(NSArray *)constraintArrays;
- (void)makeForemost;
- (instancetype)withCompressionResistance:(NSInteger)resistance forAxis:(UILayoutConstraintAxis)axis;
- (void)pinToSuperviewTop;
- (void)pinToSuperviewBottom;
- (void)pinToSuperviewTrailing;
- (void)pinToSuperviewLeading;
- (void)centerOnSuperviewLeading;
- (void)centerOnSuperviewTrailing;

@end
