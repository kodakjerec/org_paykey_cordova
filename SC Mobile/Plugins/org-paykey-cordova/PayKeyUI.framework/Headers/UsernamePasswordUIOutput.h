//
//  UsernamePasswordUIOutput.h
//  PayKeyUI
//
//  Created by Eran Israel on 26/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#ifndef UsernamePasswordUIOutput_h
#define UsernamePasswordUIOutput_h

#import <Foundation/Foundation.h>

#endif /* UsernamePasswordUIOutput_h */

@class PKTextField;

@protocol UsernamePasswordUIOutput <NSObject>

@property (nonatomic, weak) PKTextField *passwordTextField;
@property (nonatomic, weak) PKTextField *usernameTextField;

-(void)resetError;

@end
