//
//  PKPincodeInputEventBuilder.h
//  PayKeyUI
//
//  Created by German Velibekov on 01/01/2019.
//  Copyright © 2019 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKViewEventBuilder.h"

NS_ASSUME_NONNULL_BEGIN

@interface PKPincodeInputEventBuilder : PKViewEventBuilder

-(PKInteractorBuilder *)onInputChange;

@end

NS_ASSUME_NONNULL_END
