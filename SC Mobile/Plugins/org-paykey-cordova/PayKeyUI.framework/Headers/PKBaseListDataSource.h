//
//  PKBaseListDataSource.h
//  PayKeyUI
//
//  Created by Eran Israel on 21/03/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CustomModel.h"
#import "PKItemCell.h"

@protocol PKListDataSourceOutput <NSObject>
- (void)itemSelected:(UITableView *)tableView atIndexPath:(NSIndexPath*)indexPath;
- (void)scrollViewWillBeginDragging;
- (void)fetchNextItems;

@optional
- (void)itemSelected:(id)selectedItem;

@end

@interface PKBaseListDataSource : NSObject <UITableViewDelegate, UITableViewDataSource>

- (instancetype)initWithListDataSourceOutput:(id<PKListDataSourceOutput>)output;

- (void)configureCell:(PKItemCell*)cell atIndexPath:(NSIndexPath*)indexPath;
- (void)initData:(CustomModel*)model;
- (void)itemSelected:(UITableView *)tableView atIndexPath:(NSIndexPath*)indexPath;
- (NSString*)cellNibName;
- (NSString*)loadingCellClassName;
- (BOOL)isLoadingIndexPath:(NSIndexPath *)indexPath;
- (void)addItemsFromArrayIfNeeded:(NSArray*)arr;

@property (nonatomic, weak) id<PKListDataSourceOutput> listDataSourceOutput;
@property (nonatomic, strong) NSArray* items;
@property (nonatomic, assign) BOOL shouldShowLoadingCell;
@property (nonatomic, strong) CustomModel* model;
@property (nonatomic, assign) BOOL initialize;
@property (nonatomic, strong) NSIndexPath* selectedIndexPath;

@end
