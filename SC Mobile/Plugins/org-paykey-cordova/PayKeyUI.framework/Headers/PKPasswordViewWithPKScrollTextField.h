//
//  PKPasswordViewWithPKScrollTextField.h
//  PayKeyUI
//
//  Created by Eran Israel on 17/10/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import "PasswordView.h"
#import "PKPasswordUIOutputWithPKScrollField.h"

@class PKScrollTextField;

@interface PKPasswordViewWithPKScrollTextField : PKScreen <PKPasswordUIOutputWithPKScrollField>
@property (weak, nonatomic) IBOutlet PKScrollTextField *passwordTextField;
@property (weak, nonatomic) id<PasswordViewDelegate> passwordViewDelegate;

@end


