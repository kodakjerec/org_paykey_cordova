//
//  PKContactsFetcher.h
//  PayKeyUI
//
//  Created by Eden Landau on 16/03/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Contacts/Contacts.h>
#import "PKContact.h"
#import "PKContactsProvider.h"

@interface PKContactsFetcher : NSObject <PKContactFetcher>
@end
