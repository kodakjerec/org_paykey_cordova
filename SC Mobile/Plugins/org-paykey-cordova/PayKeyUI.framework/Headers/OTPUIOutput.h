//
//  OTPUIOutput.h
//  PayKeyUI
//
//  Created by ishay weinstock on 29/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#ifndef OTPUIOutput_h
#define OTPUIOutput_h

@class PKInlineError;
@protocol OTPViewDelegate;

@protocol OTPUIOutput <UIKeyInput>

@property (weak, nonatomic) PKInlineError *error;
@property (weak, nonatomic) id<OTPViewDelegate> OTPDelegate;
@property (assign, nonatomic) NSInteger pincodeLength;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;

// Set empty and full digit symbols. Specify nil fullSymbol to show user input instead.
@property (strong, nonatomic) NSString *emptySymbol;
@property (strong, nonatomic) NSString *fullSymbol;

@end

#endif /* OTPUIOutput_h */
