//
//  UILabel+BlankSlate.h
//  MakePayment
//
//  Created by Jonathan Beyrak-Lev on 14/01/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (BlankSlate)

+ (instancetype)centeredBlankSlate;
- (instancetype)centered;
- (void)assignText:(id)text;
- (instancetype)withText:(id)text;
- (instancetype)withLines:(NSInteger)lines;
- (instancetype)stretchable;

@end
