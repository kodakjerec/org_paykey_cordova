//
//  PKFramesAnimator.h
//  PayKey-Clients
//
//  Created by Daniel M. on 16/01/2019.
//  Copyright © 2019 PayKey. All rights reserved.
//

@import UIKit;

NS_ASSUME_NONNULL_BEGIN
@protocol PKFramesAnimatorDataSource,PKFramesAnimatorDelegate;

@interface PKFramesAnimator : NSObject

@property (strong,nonatomic)id<PKFramesAnimatorDataSource> dataSource;

@property (strong,nonatomic)id<PKFramesAnimatorDelegate> delegate;

-(void)startAnimating;

-(void)suggestEndAnimating;

@end


@protocol PKFramesAnimatorDataSource <NSObject>

-(NSUInteger)numberOfFramesForAnimator:(PKFramesAnimator*)animator;

-(void)applyFrameForAnimator:(PKFramesAnimator*)animator withIndex:(NSUInteger)index ;

@end


@protocol PKFramesAnimatorDelegate <NSObject>

@optional

-(NSTimeInterval)delayBetweenAnimationsForAnimator:(PKFramesAnimator*)animator;

/**
 @return replay count . 0 for infinity, default is 1.
 */
-(NSUInteger)replayCountForAnimator:(PKFramesAnimator*)animator;

-(void)animationFinishedForAnimator:(PKFramesAnimator*)animator;

@end
NS_ASSUME_NONNULL_END
