//
//  PKActionCollectionCell.h
//  PayKeyUI
//
//  Created by Eden Landau on 22/05/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKFlowAction.h"

@interface PKActionCollectionCell : UICollectionViewCell

@property (readonly) NSString *title;

- (void)configureWithAction:(PKFlowAction *)action;

@end
