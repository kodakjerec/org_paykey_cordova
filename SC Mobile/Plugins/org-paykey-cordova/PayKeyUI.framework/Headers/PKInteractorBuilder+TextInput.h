//
//  PKInteractorBuilder+TextInput.h
//  PayKeyUI
//
//  Created by German Velibekov on 02/01/2019.
//  Copyright © 2019 PayKey. All rights reserved.
//

#import "PKInteractorBuilder.h"

@interface PKInteractorBuilder (TextInput)

-(PKInteractorBuilder *)validateText:(PKValidationBlock)validationBlock;
-(id<PKViewInteractor>)ep_becomeFirstResponder;

@end

