//
//  PKFlowActions.h
//  KeyboardApp
//
//  Created by Alex Kogan on 14/01/2017.
//  Copyright © 2017 Marat . All rights reserved.
//

#import "PKControl.h"

@class PKFlowItem;

@protocol PKFlowActions <NSObject>

- (void)addFlowItem:(PKFlowItem *)flowItem;
- (void)addFlowItem:(PKFlowItem *)flowItem withTag:(NSString *)tag;
- (void)addOperationalFlowItem:(OperationalBlock)delegate withTag:(NSString *)tag;
- (void)addOperationalFlowItem:(OperationalBlock)delegate;
- (void)goTo:(NSString *)tag;
- (void)cancel:(NSString *)tag;
- (void)update:(NSString*)tag;
- (void)updateOrGoTo:(NSString*)tag;
- (void)updateCurrentScreen;
- (BOOL)isCurrentController:(NSString *)tag;
- (PKModel*)getModel:(NSString *)tag;
- (void)continueRun;

@end
