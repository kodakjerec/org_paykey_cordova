//
//  PKEmptyAction.h
//  PayKeyUI
//
//  Created by German Velibekov on 18/11/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#ifndef PKEmptyAction_h
#define PKEmptyAction_h

typedef void (^PKEmptyAction)(void);

#endif /* PKEmptyAction_h */
