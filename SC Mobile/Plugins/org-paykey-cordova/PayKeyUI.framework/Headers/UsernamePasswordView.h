//
//  UsernamePasswordView.h
//  PayKeyUI
//
//  Created by Eden Landau on 01/02/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//


#import "PKScreen.h"
#import "UsernamePasswordUIOutput.h"

@protocol UsernamePasswordViewDelegate <PKViewDelegate>

@required
- (void)continueRunWithUsername:(NSString *)username password:(NSString *)password;

@optional
- (void)showError;

@end

@interface UsernamePasswordView : PKScreen <UsernamePasswordUIOutput>

@property (weak, nonatomic) id<UsernamePasswordViewDelegate> userNamePasswordViewDelegate;

@property (weak, nonatomic) IBOutlet PKTextField *usernameTextField;
@property (weak, nonatomic) IBOutlet PKTextField *passwordTextField;

@end
