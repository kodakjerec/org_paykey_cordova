//
//  PKPhone.h
//  PayKeyUI
//
//  Created by Alex Kogan on 13/06/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKContact.h"

@interface PKPhone : NSObject

- (instancetype)initWithPhoneNumber:(NSString *)number;

@property (nonatomic, readonly, strong) NSString *phoneNumber;

@end
