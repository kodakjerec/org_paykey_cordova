//
//  PKSendToCell.h
//  PayKeyUI
//
//  Created by Alex Kogan on 13/06/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKTableViewCellPresenterSetterProtocol.h"

@interface PKPhoneCell : UITableViewCell<PKTableViewCellPresenterSetterProtocol>

@property (weak, nonatomic) IBOutlet UILabel *title;

@end
