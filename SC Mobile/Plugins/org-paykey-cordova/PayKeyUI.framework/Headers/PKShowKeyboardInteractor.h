//
//  PKShowKeyboardInteractor.h
//  PayKeyUI
//
//  Created by Eran Israel on 20/02/2019.
//  Copyright © 2019 PayKey. All rights reserved.
//

#import "FLENEventInteractor.h"

NS_ASSUME_NONNULL_BEGIN

@interface PKShowKeyboardInteractor : FLENEventInteractor

@end

NS_ASSUME_NONNULL_END
