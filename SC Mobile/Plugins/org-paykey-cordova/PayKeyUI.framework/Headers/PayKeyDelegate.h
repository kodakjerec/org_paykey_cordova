//
//  PayKeyDelegate.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 05/07/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#ifndef PayKeyDelegate_h
#define PayKeyDelegate_h

#import "PKContact.h"
#import "PKError.h"

typedef void (^FetchContactsCompletionBlock)(__kindof PKError *_Nullable error, NSArray<__kindof PKContact *> *_Nullable contacts);

@protocol PayKeyDelegate <NSObject>

@optional

- (void)fetchContacts:(nullable NSString *)filter completion:(nonnull FetchContactsCompletionBlock)completion;

@end

#endif /* PayKeyDelegate_h */
