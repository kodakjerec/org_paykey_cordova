//
//  PKCurrencyInputHandler.h
//  PayKeyUI
//
//  Created by Alex Kogan on 20/04/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PKCurrencyInputHandlerSettings.h"

@protocol PKCurrencyInputHandlerDelegate <NSObject>

@optional

- (BOOL)shouldAcceptInput:(NSString *)input;
- (void)amountChanged:(NSNumber *)amount;
- (void)amountStringChanged:(NSString*)amountString amount:(NSNumber *)amount;

@end

@interface PKCurrencyInputHandler : NSObject <UIKeyInput>

@property (readonly, strong) NSNumber *amount;
@property (readonly, strong) NSString *amountString;
@property (readonly) NSRange currencySymbolRange;
@property (nonatomic, strong) PKCurrencyInputHandlerSettings *settings;
@property (nonatomic, weak) id <PKCurrencyInputHandlerDelegate> delegate;

- (instancetype)initWithSettings:(PKCurrencyInputHandlerSettings *)settings;
- (void)clear;
- (void)insertInitialValue:(NSNumber *)initialValue;

@end

