//
//  PKUIImageViewAnimationSettings.h
//  PayKeyUI
//
//  Created by Daniel M. on 07/01/2019.
//  Copyright © 2019 PayKey. All rights reserved.
//

@import UIKit;

NS_ASSUME_NONNULL_BEGIN

@interface PKImageViewAnimationSettings : NSObject

/**
 please note the images can't sit in a assets folder but in root folder, you can use a group without folder if you want 
 */
@property (strong,nonatomic)NSArray<__kindof NSString*>* imageNames;

@property (strong,nonatomic)NSBundle *bundle;

@property (strong, nonatomic)NSNumber* duration;

@property (strong, nonatomic)NSNumber* repeatCount;

@end

NS_ASSUME_NONNULL_END
