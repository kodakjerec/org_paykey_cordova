//
//  PKSearchFieldListUIController.h
//  PayKeyUI
//
//  Created by Eran Israel on 02/08/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import "PKListUIController.h"
#import "PKSearchNavigationBarOutput.h"
#import "PKScrollTextField.h"
#import "PKSearchFilterBaseListDataSource.h"

__attribute__ ((deprecated))
@interface PKSearchFieldListUIController : PKListUIController <PKScrollTextFieldDelegate, PKTextFieldDelegate>
@property (nonatomic, weak) id <PKScrollSearchOutput> scrollSearchFieldOutput;
@property (nonatomic, weak) id <PKSearchNavigationBarOutput> searchFieldOutput;
@property (strong, nonatomic) PKSearchFilterBaseListDataSource* dataSource;
@property (assign, nonatomic) BOOL filterLocalItemsOnly; // if set, filer items locally.. Default is: YES

- (void)updateWithItems:(NSArray*)items andFilter:(NSString*)filter;

@end
