//
//  PKBaseFlowManager.h
//  KeyboardApp
//
//  Created by Alex Kogan on 14/01/2017.
//  Copyright © 2017 Marat . All rights reserved.
//

#import "PKFlowActions.h"
#import "PKFlowAction.h"
#import "PKFlowManagerDelegate.h"
#import "PKDataCollector.h"
#import "PKFlowHandler.h"
#import "PKScreenFactory.h"
#import "PKUIController.h"
#import "PKAnimationProtocol.h"


// These methods have different names for argument labels and parameter names
// because macros are silly and inject their argument into both places.
#define Get(tag, propName) ((id)[self getIdInModel:tag prop:propName])
#define Set(tag, propName, value) [self setIdInModel:tag prop:propName val:value]

@interface PKBaseFlowManager : NSObject <PKFlowActions, PKControllerDelegate, PKAnimationProtocol>

@property(nonatomic, weak) id<PKFlowManagerDelegate> flowManagerDelegate;
@property(nonatomic, weak) id<PKFlowHandler> flowHandler;
@property (nonatomic) PKDataCollector *dataCollector;

- (instancetype)initWithControllerFactory:(PKScreenFactory *)factory;

- (void)onDestroy;
- (void)start;
- (void)destroy;
- (void)reset;
- (void)resetCurrentFlow;
- (void)didChangeResult:(PKModel *)model;
- (void)notifyClosePayment;
- (id <PKControl>)configuredController:(id <PKControl>)controller;
- (void)setLastScreenTag:(NSString*)tag;
- (void)addOperation:(OperationalBlock)operation;
- (void)addOperation:(OperationalBlock)operation withTag:(NSString *)tag;
- (void)addScreen:(NSInteger)itemType withTag:(NSString *)tag;
- (void)addScreen:(NSInteger)itemType withTag:(NSString *)tag leftNavigation:(PKFlowAction *)left;
- (void)addScreen:(NSInteger)itemType withTag:(NSString *)tag rightNavigation:(PKFlowAction *)right;
- (void)addScreen:(NSInteger)itemType withTag:(NSString *)tag result:(PKControllerResultHandler)resultHandler;
- (void)addScreen:(NSInteger)itemType withTag:(NSString *)tag result:(PKControllerResultHandler)resultHandler rightNavigation:(PKFlowAction *)right;
- (void)addScreen:(NSInteger)itemType withTag:(NSString *)tag result:(PKControllerResultHandler)resultHandler leftNavigation:(PKFlowAction *)left;
- (void)addScreen:(NSInteger)itemType withTag:(NSString *)tag result:(PKControllerResultHandler)resultHandler leftNavigation:(PKFlowAction *)left rightNavigation:(PKFlowAction *)right;


- (void)show:(NSInteger)itemType withTag:(NSString *)tag result:(PKControllerResultHandler)resultHandler;

- (OperationalBlock)goesToNext;
- (OperationalBlock)operationGoTo:(NSString*)screenKey;
- (PKFlowAction *)abortAction;
- (PKFlowAction *)backAction;

- (void)setIdInModel:(NSString *)tag prop:(NSString *)propName val:(id)value;
- (id)getIdInModel:(NSString *)tag prop:(NSString *)propName;

-(void)buildFlow;


@end
