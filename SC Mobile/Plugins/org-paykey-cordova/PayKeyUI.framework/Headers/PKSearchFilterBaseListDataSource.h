//
//  PKSearchFilterBaseListDataSource.h
//  PayKeyUI
//
//  Created by Eran Israel on 29/08/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import "PKBaseListDataSource.h"
//@class PKSearchFieldListUIController;

@interface PKSearchFilterBaseListDataSource : PKBaseListDataSource
@property (nonatomic, strong) NSArray* originalItems;

- (BOOL)filterMatchForObject:(id)object filter:(NSString*)filter;
- (void)localUpdateItems;
- (void)remoteUpdateItems;
- (BOOL)shouldFilterItems;
- (NSPredicate*)predicateFilter;
- (NSAttributedString *)formatText:(NSMutableAttributedString*)text
                        withFilter:(NSString*)filter
                             color:(UIColor*)color
                              font:(UIFont*)font;
@end
