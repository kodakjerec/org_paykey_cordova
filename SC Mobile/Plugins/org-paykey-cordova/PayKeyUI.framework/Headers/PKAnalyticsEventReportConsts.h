//
//  PKAnalyticsEventReportConsts.h
//  PayKeyUI
//
//  Created by Marat  on 9/28/17.
//  Copyright © 2017 PayKey. All rights reserved.
//

//#define PK_EVENT_SWITCHED_TO_PAYKEY           @"switched_to_paykey"
#define PK_EVENT_PAYKEY_UI_OPENED             @"paykey_ui_opened"
#define PK_EVENT_PAYKEY_UI_CLOSED             @"paykey_ui_closed"
#define PK_EVENT_SCREEN_SHOWN                 @"screen_shown"
#define PK_EVENT_BUTTON_PRESS                 @"button_press"
#define PK_EVENT_RECIPIENT_SELECTED           @"recipient_selected"
#define PK_EVENT_TEXT_INPUT                   @"text_input"
#define PK_EVENT_BUTTON_ENABLED               @"button_enabled"

#define PK_EVENT_KEY_SCREEN_NAME             @"screen_name"
#define PK_EVENT_KEY_VIEW_NAME               @"view_name"
#define PK_EVENT_KEY_CONTACT_NAME            @"name"
#define PK_EVENT_KEY_IS_RECENTLY             @"latest_contact"


#define PK_EVENT_VALUE_SEARCH_INPUT          @"search_input"
#define PK_EVENT_VALUE_EXIT_BUTTON           @"exit_button"
#define PK_EVENT_VALUE_BACK_BUTTON           @"toolbar_back"
#define PK_EVENT_VALUE_AMOUNT_INPUT          @"amount_input"
#define PK_EVENT_VALUE_AMOUNT_CONFIRM        @"amount_confirm"
#define PK_EVENT_VALUE_PASSWORD_CONTINUE     @"continue_button"
#define PK_EVENT_VALUE_COLLAPSE              @"collapse"
#define PK_EVENT_VALUE_PASSWORD              @"password"
#define PK_EVENT_VALUE_NEXT                  @"next"

