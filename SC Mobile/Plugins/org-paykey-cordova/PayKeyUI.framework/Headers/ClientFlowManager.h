//
//  CustomFlowManager.h
//  KeyboardApp
//
//  Created by Alex Kogan on 14/01/2017.
//  Copyright © 2017 Marat . All rights reserved.
//

#import "PKBaseFlowManager.h"
#import "PayKeyDelegate.h"
#import "PKLocalAuthenticatorOutcomes.h"
#import "PKBaseFlowManager.h"
@import KeyboardFramework;

@protocol PKControllerFactory;
@protocol PKPaymentActions;
@protocol PKPaymentData;

@class PKFlowAction;
@class PKInMemoryDataStoringStrategy;

@interface ClientFlowManager : PKBaseFlowManager <PKFlowHandler>

@property (nonatomic, weak) id <PKPaymentData> paymentData;
@property (nonatomic, weak) id <PKPaymentActions> paymentActions;
@property (nonatomic, weak) PKInMemoryDataStoringStrategy* dataStoringStrategy;
@property (nonatomic, weak) id <PKKeyboardEventsProtocol> eventsDelegate;

@property (nonatomic, strong, readonly) id<NSObject> client;

- (instancetype)initWithClient:(id<PayKeyDelegate>)client
                       factory:(PKScreenFactory *)factory;


- (PKFlowAction *)actionGoTo:(NSString *)destination;
- (PKFlowAction *)actionGoTo:(NSString *)destination analyticsValue:(NSString*)analyticsValue;

#pragma warning - Move to image manager
- (UIImage *)imageNamed:(NSString *)imageName;

@end
