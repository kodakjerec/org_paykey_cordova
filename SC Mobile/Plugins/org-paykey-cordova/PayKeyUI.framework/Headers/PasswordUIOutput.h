//
//  ReferenceUIOutput.h
//  PayKeyUI
//
//  Created by Eran Israel on 26/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#ifndef PasswordUIOutput_h
#define PasswordUIOutput_h

#import <Foundation/Foundation.h>

#endif /* PasswordUIOutput_h */

@class PKTextField;

@protocol PasswordUIOutput <NSObject>

@property (weak, nonatomic) PKTextField *passwordTextField;

-(void)resetError;

@end
