//
//  PKListViewDelegate.h
//  PayKeyUI
//
//  Created by German Velibekov on 17/10/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PKBaseListDataSource;

@protocol PKListViewDelegate <NSObject>
@required
- (PKBaseListDataSource*)dataSource;
- (NSString*)cellNibName;
- (NSString*)loadingCellClassName;

@optional
- (void)onNext;
- (NSInteger)currentItemsCount;
- (void)updateDataSource;
@end
