//
//  PKScreenFactory.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 04/07/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKNavigationBarSettings.h"
#import "PKControllerFactory.h"

#define C(className) [className class]
#define MapClassToMethod(className, method) @{NSStringFromClass([className class]): NSStringFromSelector(@selector(method))}

@class PKScreen;
@class PKNavigationBar;
@class CustomUIController;
@class PKScreenBlueprint;
@class ScreenFactoryStrategy;

@interface PKScreenFactory : NSObject <PKControllerFactory>

- (instancetype)initWithScreenFactoryStrategy:(ScreenFactoryStrategy *)strategy;

@end

@interface PKHidesLeftButtonNavigationBarSettings : PKNavigationBarSettings @end
@interface PKHidesRightButtonNavigationBarSettings : PKNavigationBarSettings @end
@interface PKHidesBothButtonsNavigationBarSettings : PKNavigationBarSettings @end
@interface PKShowsBothButtonsNavigationBarSettings : PKNavigationBarSettings @end
