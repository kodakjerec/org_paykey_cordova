//
//  PKContactsArray.h
//  PayKeyUI
//
//  Created by Alex Kogan on 13/06/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKContact.h"
#import "PKPhone.h"

@protocol PKSendToPhoneProtocol;

@interface PKSendToPhoneProtocolWrapperArray : NSArray <PKContact *>

- (instancetype)initWithArray:(NSArray<PKContact *> *)contacts;
- (BOOL)phoneEntryValid:(NSString *)phoneNumber;

@property (nonatomic, weak) id <PKSendToPhoneProtocol> sendToPhoneDelegate;

@end
