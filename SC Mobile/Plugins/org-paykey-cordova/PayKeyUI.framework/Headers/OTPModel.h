//
//  OTPModel.h
//  KeyboardApp
//
//  Created by Alex Kogan on 14/01/2017.
//  Copyright © 2017 Marat . All rights reserved.
//

#import "CustomModel.h"

@interface OTPModel : CustomModel

@property (nonatomic, strong) NSString *pincode;
@property (nonatomic, assign) BOOL shouldResendOtp;

@end
