//
//  UIImageView+Init.h
//  MakePayment
//
//  Created by Jonathan Beyrak-Lev on 11/01/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Init)

+ (instancetype)blankSlateWithImage:(NSString *)image;
- (instancetype)withImage:(NSString *)image;
- (instancetype)aspectFit;

@end
