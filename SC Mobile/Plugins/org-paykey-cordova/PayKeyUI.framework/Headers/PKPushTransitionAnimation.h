//
//  PKPushTransitionAnimation.h
//  PayKeyUI
//
//  Created by ishay weinstock on 25/04/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import "PKTransitionAnimation.h"

@interface PKPushTransitionAnimation : NSObject <PKTransitionAnimation>

@end
