//
//  PasswordModel.h
//  KeyboardApp
//
//  Created by Alex Kogan on 14/01/2017.
//  Copyright © 2017 Marat . All rights reserved.
//

#import "CustomModel.h"

@interface PasswordModel : CustomModel

@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSNumber *minChars;
@property (nonatomic, strong) NSNumber *maxChars;


@end
