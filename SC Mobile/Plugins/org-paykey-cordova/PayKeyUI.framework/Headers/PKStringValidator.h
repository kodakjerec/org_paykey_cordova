//
//  PKStringValidator.h
//  PayKeyUI
//
//  Created by German Velibekov on 21/11/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKValidator.h"

typedef BOOL (^PKStringValidationBlock)(NSString *_Nullable);

NS_ASSUME_NONNULL_BEGIN

@interface PKStringValidator : NSObject <PKValidator>

+(instancetype)validatorWithBlock:(PKStringValidationBlock)block;
+(instancetype)validatorWithCharacterSet:(NSCharacterSet *)set;

@end

NS_ASSUME_NONNULL_END
