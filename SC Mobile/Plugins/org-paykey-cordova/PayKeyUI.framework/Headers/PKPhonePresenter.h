//
//  PKPhonePresenter.h
//  PayKeyUI
//
//  Created by Alex Kogan on 13/06/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKContactPresenter.h"
#import "PKTableViewCellPresenter.h"

@class PKPhone;

@interface PKPhonePresenter : PKTableViewCellPresenter

@property (nonatomic, readonly) PKPhone *phone;

- (instancetype)initWithNibName:(NSString *)nibName bundle:(NSBundle *)bundle phone:(PKPhone *)phone;
- (NSString *)title;

@end
