//
//  UIButton+Defaults.h
//  MakePayment
//
//  Created by Jonathan Beyrak-Lev on 11/01/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKTargetAction.h"

@interface UIButton (Defaults)

+ (instancetype)verticallyNoncompressingBlankSlate;
+ (instancetype)buttonWithTarget:(id)target action:(SEL)action;
+ (instancetype)buttonWithTargetAction:(PKTargetAction *)targetAction;
- (instancetype)withTarget:(id)target action:(SEL)action;
- (instancetype)withTargetAction:(PKTargetAction *)targetAction;
- (instancetype)withAttributedTitle:(NSAttributedString *)title;
- (instancetype)enabled;
- (instancetype)disabled;
- (void)setDefaultAttributedTitle:(NSAttributedString *)title;
- (void)setTargetAction:(PKTargetAction *)targetAction;

@end
