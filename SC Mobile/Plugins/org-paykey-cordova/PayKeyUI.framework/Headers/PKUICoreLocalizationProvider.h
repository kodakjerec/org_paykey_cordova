//
//  PKUICoreLocalizationProvider.h
//  PayKeyUICore
//
//  Created by Marat  on 3/21/17.
//  Copyright © 2017 alon muroch. All rights reserved.
//

@import KeyboardFramework;

@protocol PKUICoreLocalizationProvider <PKKeyboardLocalizationProvider>

- (NSString * _Nullable)localizedUIStringForKey:(NSString * _Nonnull)key;
- (NSString * _Nullable)uiLocalizationForKey:(NSString * _Nonnull)key;
- (NSBundle * _Nullable)loadUILocalizationBundle;
- (NSString * _Nonnull)paymentLocalizationTableName;
- (PKLanguage * _Nonnull)paymentLanguage;

@end
