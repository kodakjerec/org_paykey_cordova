//
//  PKFlowAction.h
//  PayKeyUI
//
//  Created by Eden Landau on 22/05/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void (^PKPerformsAction)(NSInteger tag);

@interface PKFlowAction : NSObject

@property (nonatomic) NSString *title;
@property (nonatomic) UIImage *image;
@property (nonatomic) NSUInteger tag;
@property (nonatomic) NSString *analyticsValue;

- (instancetype)initWithAction:(PKPerformsAction)action;
- (instancetype)initWithAction:(PKPerformsAction)action title:(NSString *)title image:(UIImage *)image tag:(NSUInteger)tag;
- (void)execute;

@end
