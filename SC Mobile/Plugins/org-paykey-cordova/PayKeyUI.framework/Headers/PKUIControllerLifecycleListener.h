//
// Created by Alex Kogan on 02/04/2018.
// Copyright (c) 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

@protocol PKUIControllerLifecycleListener <NSObject>

@optional
- (void)onBindView:(UIView *)view;
- (void)viewDidLoad;
- (void)viewWillAppear:(BOOL)animated;    // Called when the view is about to made visible. Default does nothing
- (void)viewDidAppear:(BOOL)animated;     // Called when the view has been fully transitioned onto the screen. Default does nothing
- (void)viewWillDisappear:(BOOL)animated; // Called when the view is dismissed, covered or otherwise hidden. Default does nothing
- (void)viewDidDisappear:(BOOL)animated;  // Called after the view was dismissed, covered or otherwise hidden. Default does nothing

@end
