//
//  PKPincodeOutput.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 22/10/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#ifndef PKPincodeOutput_h
#define PKPincodeOutput_h

@import KeyboardFramework;


@protocol PKPincodeOutput <PKTextDocumentProxy>

@property (nonatomic, assign) NSInteger codeLength;

- (void)beginEditing;
- (BOOL)endEditing;

@end

#endif /* PKPincodeOutput_h */
