//
//  ScreenFactoryStrategy.h
//  PayKeyUI
//
//  Created by ishay weinstock on 05/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKNavigationBarSettings.h"
#import "PKUICoreLocalizationProvider.h"

@class PKScreen;
@class PKNavigationBar;
@class CustomUIController;
@class PKScreenBlueprint;
@class PKPaymentInteractor;
@class PKLocalizedStringProvider;
@class PKPaymentInteractor;
@class PKUIKeyboardConfig;

@protocol PKKeyboardEventsProtocol;
@protocol PKPaymentData;
@protocol PKPaymentActions;

@interface ScreenFactoryStrategy : NSObject

@property (nonatomic) NSLocale *locale;
@property (nonatomic) PKScreenBlueprint *blueprint;
@property NSInteger tag;
@property (nonatomic, weak) id <PKKeyboardEventsProtocol> eventsDelegate;
@property (nonatomic,strong) id<PKUICoreLocalizationProvider> localizationProvider;
@property (nonatomic, weak) id <PKPaymentData> paymentData;
@property (nonatomic, weak) id <PKPaymentActions> paymentActions;
@property (nonatomic, strong, readonly) PKUIKeyboardConfig *keyboardConfig;

- (CustomUIController *)create:(NSInteger)tag;
- (PKScreenBlueprint *)makeBlueprint;
- (void)configureBlueprint;
- (instancetype)initWithLocalizationProvider:(PKUIKeyboardConfig*)keyboardConfig
                                      locale:(NSLocale *)locale
                                  interactor:(PKPaymentInteractor *)interactor;
- (id)viewFromNib:(NSString *)nib;
- (id)viewFromNib:(NSString *)nib bundle:(NSBundle*)bundle;
- (NSArray *)extendedLinkMap;

-(PKLocalizedStringProvider *)localizedProviderForKey:(NSString*)key;
- (NSString *)localizedString:(NSString *)key;

/**
 sets the screen attirubted title using localised strings file given the key, uses
 - (NSAttributedString*)attributedTitleForTitle:(NSString*)title to get attributed string
 override the latter method to customize title  attributed style.
 
 @param key key of title to set
 */
- (void)setScreenAttributedTitle:(NSString*)key;


/**
 attributed title for title,
 setScreenAttributedTitle  uses this , override this to make it a custom styled title
 default behaviour returns NSAttributedString with no attributes.
 
 @param title the title to attribute
 @return attributed title
 */
- (NSAttributedString*)attributedTitleForTitle:(NSString*)title;


@end
