//
//  PasswordUIController.h
//  KeyboardApp
//
//  Created by Alex Kogan on 14/01/2017.
//  Copyright © 2017 Marat . All rights reserved.
//

#import "PasswordModel.h"
#import "PasswordView.h"
#import "PKTextFieldUIController.h"
#import "PasswordUIOutput.h"

@interface PasswordUIController : PKTextFieldUIController <PasswordViewDelegate>

@property (nonatomic, weak) id <PasswordUIOutput> passwordOutput;


@end
