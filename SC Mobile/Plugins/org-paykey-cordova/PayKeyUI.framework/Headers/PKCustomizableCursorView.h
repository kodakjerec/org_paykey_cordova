//
//  PKCustomizableCursorView.h
//  PayKeyUI
//
//  Created by German Velibekov on 11/06/2018.
//

@import KeyboardFramework;

/**
 This class is used to control cursor from outside
 */
@interface PKCustomizableCursorView : PKViewWithCursor

- (void) updateCursorXPosition:(CGFloat)xPosition;
- (void) setupCursorWithHeight:(CGFloat)cursorHeight;

@end
