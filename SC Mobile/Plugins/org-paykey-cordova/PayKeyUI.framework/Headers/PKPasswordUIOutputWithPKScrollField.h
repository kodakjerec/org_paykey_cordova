//
//  PKPasswordUIOutputWithScrollFiled.h
//  PayKeyUI
//
//  Created by Eran Israel on 26/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//


#import <Foundation/Foundation.h>

@class PKScrollTextField;

@protocol PKPasswordUIOutputWithPKScrollField <NSObject>
@property (weak, nonatomic) PKScrollTextField *passwordTextField;
@end
