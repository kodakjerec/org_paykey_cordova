//
//  PKInputViewInteractions.h
//  PayKeyUI
//
//  Created by German Velibekov on 28/10/2018.
//

@import Foundation;
#import "PKViewInteractorsBuilder.h"
#import "PKValidator.h"
#import "PKEmptyAction.h"

@class FLENEventInteractor;
@class KeyModelUI;

NS_ASSUME_NONNULL_BEGIN

@interface PKInputViewInteractorsBuilder: PKViewInteractorsBuilder

-(id<PKViewInteractor>)focusOnStart;
-(id<PKViewInteractor>)returnInputOnKeyboardReturnKey;
-(id<PKViewInteractor>)restrictMaxLengthOf:(NSInteger)maxLength;
-(id<PKViewInteractor>)enableKeyboardReturnKeyOnMinLengthOf:(NSInteger)minLength;
-(id<PKViewInteractor>)applyKeyboardReturnKeyModel:(KeyModelUI *)keyModel;
-(id<PKViewInteractor>)runCommandsOnFirstInputChange:(PKEmptyAction)action;
-(id<PKViewInteractor>)runCommandsOnFirstInputChangeWithDelay:(NSTimeInterval)delay action:(PKEmptyAction)action;
-(id<PKViewInteractor>)trackOnReturnKeyTapped;
-(id<PKViewInteractor>)trackOnFirstInputChanged:(NSString *)viewName;
-(id<PKViewInteractor>)restrictText:(id<PKValidator>)validator;
-(id<PKViewInteractor>)restrictText:(id<PKValidator>)validator action:(PKValidateAction)action;
-(id<PKViewInteractor>)focusOnTap;
-(id<PKViewInteractor>)saveTextToValueWithSelector:(SEL)valueSelector;

-(FLENEventInteractor *)endEditing;

@end

NS_ASSUME_NONNULL_END
