//
//  PKGoodSearchFieldListUIController.h
//  PayKeyUI
//
//  Created by Eran Israel on 02/08/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import "PKListUIController.h"
#import "PKSearchNavigationBarOutput.h"
#import "PKSearchFilterBaseListDataSource.h"

@interface PKGoodSearchFieldListUIController : PKListUIController <PKGoodTextFieldDelegate>
@property (nonatomic, weak) id <PKGoodSearchOutput> searchFieldOutput;
@property (strong, nonatomic) PKSearchFilterBaseListDataSource* dataSource;
@property (assign, nonatomic) BOOL filterLocalItemsOnly; // if set, filer items locally.. Default is: YES

- (void)updateWithItems:(NSArray*)items andFilter:(NSString*)filter;

@end
