//
//  PKCurrencyBaseLabel.h
//  PayKeyUI
//
//  Created by ishay weinstock on 15/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PKCurrencyBaseLabel : UILabel

- (CGRect)textRect;

@end
