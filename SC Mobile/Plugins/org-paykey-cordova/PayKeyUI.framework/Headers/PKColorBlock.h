//
//  PKColorBlock.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 07/02/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PKColorBlock : UIView

@property (nonatomic) CGSize size;

- (instancetype)initWithColor:(UIColor *)color width:(CGFloat)width;
- (instancetype)initWithColor:(UIColor *)color height:(CGFloat)height;
- (instancetype)initWithColor:(UIColor *)color size:(CGSize)size;
- (instancetype)noncompressible;

@end
