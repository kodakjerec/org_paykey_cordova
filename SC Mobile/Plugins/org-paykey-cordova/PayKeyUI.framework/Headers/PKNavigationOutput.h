//
//  PKNavigationOutput.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 18/10/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#ifndef PKNavigationOutput_h
#define PKNavigationOutput_h

#import "CustomModel.h"

@protocol PKNavigationBarDelegate<NSObject>

@optional

- (void)leftNavigationButtonTapped;
- (void)rightNavigationButtonTapped;
- (void)inputUpdated:(NSString *)newInput;

@end

@protocol PKNavigationOutput

@property (nonatomic) NSAttributedString *attributedTitle;
@property (nonatomic, weak) id<PKNavigationBarDelegate> delegate;

- (void)configureWithModel:(CustomModel *)model;
- (void)setRightButtonEnabled:(BOOL)enabled;

@end

#endif /* PKNavigationOutput_h */
