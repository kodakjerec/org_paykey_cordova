//
//  PKViewEventBuilder.h
//  PayKeyUI
//
//  Created by German Velibekov on 26/12/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKInteractorBuilder.h"

NS_ASSUME_NONNULL_BEGIN

@interface PKViewEventBuilder : NSObject

@property (nonatomic, assign, readonly) NSInteger viewTag;

+ (instancetype)builderForView:(NSInteger)viewTag;

-(PKInteractorBuilder *)onTap;

@end

NS_ASSUME_NONNULL_END
