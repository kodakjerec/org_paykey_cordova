//
//  PKScrollTextField.h
//  PayKeyUI
//
//  Created by Eden Landau on 21/02/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

@import KeyboardFramework;
#import "PKLegacyViewWithCursor.h"
@class PKScrollTextField;

__attribute__ ((deprecated))
@protocol PKScrollTextFieldDelegate <NSObject>

@optional
- (BOOL)textFieldShouldBeginEditing:(PKScrollTextField *)textField;
- (void)textFieldDidBeginEditing:(PKScrollTextField *)textField;
- (BOOL)textFieldShouldEndEditing:(PKScrollTextField *)textField;
- (void)textFieldDidEndEditing:(PKScrollTextField *)textField;
- (BOOL)textField:(PKScrollTextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
- (BOOL)textFieldShouldClear:(PKScrollTextField *)textField;
- (BOOL)textFieldShouldReturn:(PKScrollTextField *)textField;
- (void)textDidChange:(PKScrollTextField *)textField;
- (KeyModelUI*)returnKeyModel;

@end

__attribute__ ((deprecated))
@interface PKScrollTextField :  PKLegacyViewWithCursor<PKTextDocumentProxy, UIReturnKeyProxy>

//////// XIB
@property (nonatomic) IBInspectable UIColor *cursorColor;
@property (nonatomic) IBInspectable UIColor *textColor;
@property (nonatomic) IBInspectable UIColor *placeholderTextColor;
@property (nonatomic) IBInspectable NSString *placeholderText;
@property (nonatomic) IBInspectable UIImage *icon;
@property (nonatomic) IBInspectable UIImage *clearIcon;
@property (nonatomic) IBInspectable CGFloat letterSpacing;
@property (nonatomic) IBInspectable CGFloat textHorizontalMargin;
@property (nonatomic) IBInspectable NSString *initialValue;
@property (nonatomic) IBInspectable BOOL hidePlaceHolderWhenEditing;
@property (nonatomic) IBInspectable CGFloat iconWidth;
@property (nonatomic, getter=isSecureTextEntry) IBInspectable BOOL secureTextEntry;



////////////////////////////////////////////////////////////
@property (nonatomic) UIEdgeInsets clearButtonEdgeInest;
@property (nonatomic) CGFloat clearButtonRightPadding;
@property (nonatomic) UITextFieldViewMode clearButtonMode;
@property (nonatomic) NSTextAlignment textAlignment;
@property (nonatomic, strong) NSNumber *overrideCursorLabelAlignment;
@property (nonatomic) UIFont *font;
@property (nonatomic) UIFont *placeHolderFont;

@property (readonly) BOOL inputActive;
@property (readonly) NSString *text;
@property (nonatomic, weak) id<PKScrollTextFieldDelegate>delegate;
@property (nonatomic, weak) id <PKInputTextStateObserver> inputTextStateObserver;

- (void)beginEditing;
- (BOOL)endEditing;

- (void)clear;

@end
