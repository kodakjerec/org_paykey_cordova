//
// Created by Alex Kogan on 03/04/2018.
// Copyright (c) 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKViewInteractor.h"
#import "PKEmptyAction.h"

@interface PKViewInteractorsBuilder : NSObject

@property (nonatomic, assign, readonly) NSInteger tag;

+ (instancetype)builderForView:(NSInteger)viewTag;

- (id <PKViewInteractor>)resultOnTap:(id)result;

@end
