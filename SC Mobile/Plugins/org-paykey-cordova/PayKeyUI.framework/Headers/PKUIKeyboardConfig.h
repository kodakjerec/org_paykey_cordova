//
//  PKUIKeyboardConfig.h
//  KeyboardApp
//
//  Created by Alex Kogan on 13/01/2017.
//  Copyright © 2017 Marat . All rights reserved.
//

#import "PayKeyDelegate.h"
#import "PKUICoreLocalizationProvider.h"

@import KeyboardFramework;

typedef NS_ENUM(NSInteger, PKExtensionType) {
    PKExtensionTypeKeyboard,
    PKExtensionTypeIMessage
};

@protocol PaymentButtonAttributesDelegate <NSObject>
- (PKPaymentButtonAttributes* _Nullable)  overridePaymentButtonAttributes;
@end

@interface PKUIKeyboardConfig : PKKeyboardConfig <PKUICoreLocalizationProvider>

@property (assign, nonatomic)PKExtensionType extensionType;
@property (weak, nonatomic, nullable) id<PaymentButtonAttributesDelegate> delegate;
@property (strong, nonatomic, nonnull)PKLanguage* paymentLanguage;

- (id<PayKeyDelegate>_Nullable)payKeyDelegate;
- (NSLocale *_Nullable)locale;
- (BOOL)landscapeFlowEnabled;

@end
