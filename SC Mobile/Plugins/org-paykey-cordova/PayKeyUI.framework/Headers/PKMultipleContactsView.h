//
//  PKMultipleContactsView.h
//  PayKeyUI
//
//  Created by Daniel M. on 09/12/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import "PKContactsViewSearchField.h"

NS_ASSUME_NONNULL_BEGIN

@interface PKMultipleContactsView : PKContactsViewSearchField

@end

NS_ASSUME_NONNULL_END
