//
//  PKPhoneFormatStrategy.h
//  PayKeyUI
//
//  Created by Marat  on 5/21/17.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKContactsProvider.h"

@interface PKPhoneFormatStrategy : NSObject<PKContactProviderStrategy>

@property (nonatomic,strong)NSLocale *locale;
@property (nonatomic,assign)BOOL displayCountryCode;
@property (nonatomic,assign)BOOL displayPlusSign;

@end
