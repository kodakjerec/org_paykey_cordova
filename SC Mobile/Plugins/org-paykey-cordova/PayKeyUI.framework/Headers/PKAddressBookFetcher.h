//
//  PKAddressBookFetcher.h
//  PayKeyUI
//
//  Created by Eden Landau on 18/03/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKContact.h"
#import "PKContactsProvider.h"

@interface PKAddressBookFetcher : NSObject <PKContactFetcher>
@end
