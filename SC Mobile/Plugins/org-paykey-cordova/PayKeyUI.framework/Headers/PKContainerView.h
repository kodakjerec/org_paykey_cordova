//
//  PKContainerView.h
//
//  Created by ishay weinstock on 12/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PKScreen;
@interface PKContainerView : UIView

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *navigationBarContainer;

- (void)installView:(PKScreen*)view;
- (void)installNavigationBar:(UIView*)navigationBar;

@end
