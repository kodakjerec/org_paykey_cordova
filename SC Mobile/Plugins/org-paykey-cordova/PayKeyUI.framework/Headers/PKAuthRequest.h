//
//  PKAuthRequest.h
//  PayKeyUI
//
//  Created by Alex Kogan on 02/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, PKAuthType) {
    PKAuthTypeNone,
    PKAuthTypeBiometric,
    PKAuthTypeTouchId __deprecated_enum_msg("Use PKAuthTypeBiometric instead.") = PKAuthTypeBiometric,
    PKAuthTypePinCode,
    PKAuthTypePassword,
    PKAuthTypeUsernamePassword,
    PKAuthTypeOTP,
    PKAuthTypeLast
};


@interface PKAuthRequest : NSObject

@property (nonatomic) PKAuthType type;
@property (nonatomic) NSString* pincode;
@property (nonatomic) NSString* username;
@property (nonatomic) NSString* password;
@property (nonatomic) NSString* otp;

@end
