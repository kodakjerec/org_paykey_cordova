//
//  PKImageLoader.h
//  PayKeyUI
//
//  Created by Eden Landau on 23/02/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PKImageLoader : NSObject

+ (UIImage *)loadImageNamed:(NSString *)name;
+ (UIImage *)loadImageNamed:(NSString *)name inBundle:(NSBundle *)bundle;

@end
