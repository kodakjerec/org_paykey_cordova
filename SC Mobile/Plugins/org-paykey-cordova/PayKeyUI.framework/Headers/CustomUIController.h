//
//  CustomUIController.h
//  KeyboardApp
//
//  Created by Alex Kogan on 14/01/2017.
//  Copyright © 2017 Marat . All rights reserved.
//

#import "CustomModel.h"
#import "PKFlowAction.h"
#import "PKNavigationOutput.h"
#import "PKOutput.h"
#import "PKUIController.h"
#import "PKViewDelegate.h"
#import "PKControllerNavigationListener.h"

@interface CustomUIController : PKUIController <PKViewDelegate>

@property (nonatomic) __kindof CustomModel *model;
@property PKFlowAction *leftNavigationAction;
@property PKFlowAction *rightNavigationAction;
@property (nonatomic) id<PKNavigationOutput> navigationOutput;
@property (nonatomic) id<PKOutput> output;
@property (nonatomic, weak) id<PKControllerNavigationListener> navigationListener;

@property(nonatomic) NSDictionary *screenReportData;
@property(nonatomic, weak) id <PKKeyboardEventsProtocol> eventsDelegate;

- (void)reportAnalyticsEvent:(PKAnalyticsEvent*)event;

@end
