//
//  PKTextDocumentProxyView.h
//  PayKeyUI
//
//  Created by ishay weinstock on 30/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import "PKScreen.h"
#import "PKTextDocumentProxyOutput.h"

@interface PKTextDocumentProxyView : PKScreen <PKTextDocumentProxyOutput>

@end
