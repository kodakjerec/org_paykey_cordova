//
//  PKNavigationBar.h
//  PayKeyUI
//
//  Created by Eden Landau on 13/02/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKNavigationOutput.h"


@interface PKNavigationBar : UIView <PKNavigationOutput>

@property (strong, nonatomic) NSString *leftButtonTitle;
@property (strong, nonatomic) NSString *rightButtonTitle;
@property (strong, nonatomic) NSString *rightButtonAccessibilityTitle;
@property (strong, nonatomic) NSString *leftButtonAccessibilityTitle;
@property (strong, nonatomic) NSString *titleAccessibilityTitle;
@property (strong, nonatomic) CustomModel* model;

- (IBAction)rightButtonTapped:(id)sender;
- (IBAction)leftButtonTapped:(id)sender;

- (void)configureWithSettings:(PKNavigationBarSettings *)settings;
- (void)fillContainerView:(UIView *)view;
- (void)setLeftButtonContentMode:(UIViewContentMode)mode;
- (void)setRightButtonContentMode:(UIViewContentMode)mode;
- (void)setTitleLetterSpacing:(CGFloat)spacing;
- (void)setRightButtonEnabled:(BOOL)enabled;
- (void)setLeftButtonEnabled:(BOOL)enabled;

@end
