//
//  PKSetupData.h
//  PayKeyUI
//
//  Created by Eran Israel on 30/01/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKAmountValues.h"

@interface PKSetupData : NSObject <NSCoding>

@property (nonatomic, readonly) PKAmountValues* makePaymentValues;

@property (nonatomic) NSNumber* minValue  __deprecated;
@property (nonatomic) NSNumber* maxValue __deprecated;

+ (instancetype)setupDataWithAmountValues:(PKAmountValues*)amountValues;

@end
