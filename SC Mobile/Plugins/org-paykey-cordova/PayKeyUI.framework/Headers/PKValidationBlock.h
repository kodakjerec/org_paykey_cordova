//
//  PKValidationBlock.h
//  PayKeyUI
//
//  Created by German Velibekov on 02/01/2019.
//  Copyright © 2019 PayKey. All rights reserved.
//

#ifndef PKValidationBlock_h
#define PKValidationBlock_h

typedef BOOL (^PKValidationBlock)(NSString *text);

#endif /* PKValidationBlock_h */
