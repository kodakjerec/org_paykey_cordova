//
//  ReferenceUIOutput.h
//  PayKeyUI
//
//  Created by Eran Israel on 26/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#ifndef ReferenceUIOutput_h
#define ReferenceUIOutput_h

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PKScrollTextField.h"

#endif /* ReferenceUIOutput_h */

@protocol ReferenceUIOutput <NSObject>

@property (nonatomic, weak) PKScrollTextField *referenceTextField;
@property (nonatomic, weak) UIButton *nextButton;


@end
