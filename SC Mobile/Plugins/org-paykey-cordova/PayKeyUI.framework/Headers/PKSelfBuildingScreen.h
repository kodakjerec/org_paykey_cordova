//
//  PKSelfBuildingScreen.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 11/02/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import "PKScreen.h"

@interface PKSelfBuildingScreen : PKScreen

- (instancetype)build;

@end
