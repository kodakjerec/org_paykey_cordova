//
// Created by Alex Kogan on 03/04/2018.
// Copyright (c) 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKViewInteractor.h"
#import "FLENEvent.h"
#import "PKEmptyAction.h"
#import "PKResultAction.h"
#import "PKValidationBlock.h"

@class FLENEventInteractor;

@interface PKInteractorBuilder : NSObject

+ (instancetype)builderForEvent:(id<FLENEvent>)event;

-(id<PKViewInteractor>)customAction:(PKResultAction)action;
-(id<PKViewInteractor>)customInteractor:(FLENEventInteractor *)interactor;
-(id<PKViewInteractor>)customInteractors:(NSArray<FLENEventInteractor*> *)interactors;
-(PKInteractorBuilder *)delaySubscription:(NSTimeInterval)delayInSeconds;
-(PKInteractorBuilder *)filter:(PKValidationBlock)block;


@end
