//
//  PKAmountOutput.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 22/10/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#ifndef PKAmountOutput_h
#define PKAmountOutput_h

#import "PKCurrencyInputHandler.h"

typedef NS_ENUM(NSUInteger, AmountValidation) {
    AmountValidationEmpty,
    AmountValidationValid,
    AmountValidationLessMin,
    AmountValidationAboveMax,
    AmountValidationAboveBalance,
    AmountValidationWrongMultiplier,
    AmountValidationAboveMaxWrongMultiplier,
    AmountValidationLessMinWrongMultiplier
};

@class PKCurrencyTextField;

@protocol PKAmountOutput

@property (weak, nonatomic)  PKCurrencyTextField *amountLabel;

- (void)setInputHandler:(PKCurrencyInputHandler *)inputHandler;
- (void)validateAmount:(AmountValidation)amountValidType;
- (void)setInitialAmount;
- (void)updateAccessibilityLabel;

@end

#endif /* PKAmountOutput_h */
