//
//  PKPhoneNumber.h
//  PayKeyUI
//
//  Created by Alex Kogan on 20/03/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PKPhoneNumber : NSObject <NSCoding>

- (instancetype)initWithPhoneNumber:(NSString *)number;
- (instancetype)initWithPhoneNumber:(NSString *)number withTag:(NSString*)tag;

@property (nonatomic, readonly, strong) NSString *phoneNumber;
@property (nonatomic, readonly, strong) NSString *phoneNumberTag;

@end
