//
//  PKRedirectableErrorController.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 07/03/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import "ActionableMessageUIController.h"

@interface PKRedirectableErrorUIController : ActionableMessageUIController

@end
