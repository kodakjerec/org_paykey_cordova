//
//  PKTextPresentable.h
//  PayKeyUI
//
//  Created by German Velibekov on 28/10/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

NS_ASSUME_NONNULL_BEGIN

@protocol PKTextPresentable <NSObject>

- (void)setText:(NSString *)text;

@optional
- (void)setTextColor:(UIColor *)textColor;
- (void)setFont:(UIFont *)font;
- (void)setAttributedText:(NSAttributedString *)attributedText;
- (void)setTextAlignment:(NSTextAlignment)textAlignment;

@end

NS_ASSUME_NONNULL_END
