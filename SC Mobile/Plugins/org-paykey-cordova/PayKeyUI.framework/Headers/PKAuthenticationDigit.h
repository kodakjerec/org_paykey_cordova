//
//  PKAuthenticationDigit.h
//  PayKeyUI
//
//  Created by Eden Landau on 16/02/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PKAuthenticationDigit : UIView
@property (nonatomic, strong) NSString *emptySymbol;
@property (nonatomic, strong) NSString *fullSymbol;
@property (nonatomic, assign, getter=isFull) BOOL full;
@property (weak, nonatomic) IBOutlet UILabel *digitLabel;
@end
