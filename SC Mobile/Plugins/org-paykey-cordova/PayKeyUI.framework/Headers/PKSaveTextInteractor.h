//
//  PKSaveTextInteractor.h
//  PayKeyUI
//
//  Created by German Velibekov on 13/11/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKViewInteractor.h"

NS_ASSUME_NONNULL_BEGIN

@interface PKSaveTextInteractor : NSObject <PKViewInteractor>

+ (instancetype)forView:(NSInteger)viewTag
               selector:(SEL)selector;

@end

NS_ASSUME_NONNULL_END
