//
//  PKScreen.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 13/06/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKNavigationBar.h"
#import "CustomModel.h"
#import "PKViewDelegate.h"
#import "PKUICoreLocalizationProvider.h"
#import "PKUICreation.h"
#import "PKOutput.h"

@interface PKScreen : UIView <PKNavigationBarDelegate, PKOutput>

@property (weak, nonatomic) id<PKViewDelegate> delegate;
@property (strong, nonatomic)__kindof PKNavigationBar *navigationBar;
@property (strong, nonatomic) IBOutlet UIView *navigationBarContainer DEPRECATED_MSG_ATTRIBUTE("Use PKContainerView instead");
@property (weak, nonatomic) __kindof CustomModel* model;
@property (nonatomic) CGFloat navigationBarHeight;

- (void)updateWithModel:(CustomModel *)model;
- (void)installNavigationBar:(PKNavigationBar *)navigationBar;
- (void)_setUpNavigationBar;
- (void)reportEvent:(NSString*)eventName;
- (void)reportEvent:(NSString*)eventName eventParams:(NSDictionary*)eventParams;

@end
