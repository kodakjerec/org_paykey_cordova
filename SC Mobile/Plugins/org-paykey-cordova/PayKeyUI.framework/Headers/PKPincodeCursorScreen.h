//
//  PKPincodeCursorScreen.h
//  PayKeyUI
//
//  Created by German Velibekov on 11/06/2018.
//

#import "PKPincodeScreen.h"

@interface PKPincodeCursorScreen : PKPincodeScreen

@property (weak, nonatomic) IBOutlet UIView *pinCodeContainer;

// -------------- Overridable --------------
- (CGFloat)cursorHeight;

@end
