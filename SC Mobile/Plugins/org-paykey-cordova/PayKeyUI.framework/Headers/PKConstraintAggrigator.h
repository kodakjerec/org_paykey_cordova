//
//  PKConstraintAggrigator.h
//  PayKeyUI
//
//  Created by Daniel M. on 23/12/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKConstraintModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface PKConstraintAggrigator : NSObject

- (id<PKConstraintModel>)modelByID:(NSString*)constraintId;
- (void)applyConstraintsToView:(UIView*)view;

@end

NS_ASSUME_NONNULL_END
