//
//  UsernamePasswordModel.h
//  KeyboardApp
//
//  Created by Alex Kogan on 14/01/2017.
//  Copyright © 2017 Marat . All rights reserved.
//

#import "CustomModel.h"

@interface UsernamePasswordModel : CustomModel

@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *password;

@end
