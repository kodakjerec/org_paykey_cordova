//
//  PKContact+Name.h
//  PayKeyUI
//
//  Created by ishay weinstock on 10/04/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import "PKContact.h"

@interface PKContact (Name)

- (NSString*)fullName;
- (NSComparisonResult)compareWithFullName:(PKContact *)otherContact;
- (NSString*)phoneNumberSafe;

@end
