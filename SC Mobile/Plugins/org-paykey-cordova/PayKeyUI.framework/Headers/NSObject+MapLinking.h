//
//  NSObject+MapLinking.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 14/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject(MapLinking)

- (void)linkTo:(id)dependency usingArrayOfPairs:(NSArray *)array;
- (void)linkTo:(id)dependency class:(NSString *)classString selector:(NSString*)selector;

@end

