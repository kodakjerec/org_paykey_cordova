//
//  PKTextInputEventBuilder.h
//  PayKeyUI
//
//  Created by German Velibekov on 26/12/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import "PKViewEventBuilder.h"

NS_ASSUME_NONNULL_BEGIN

@interface PKTextInputEventBuilder : PKViewEventBuilder

-(PKInteractorBuilder *)onShouldReturn;
-(PKInteractorBuilder *)onTextDidChange;
-(PKInteractorBuilder *)onTextDidBeginEditing;
-(PKInteractorBuilder *)onTextDidEndEditing;

@end

NS_ASSUME_NONNULL_END
