//
//  NSArray+ObjectCreation.h
//  MakePayment
//
//  Created by Jonathan Beyrak-Lev on 15/01/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+RGB.h"

@interface NSArray (ObjectCreation) <PKRGB>

- (UIColor *)rgb;
- (UIColor *)rgbWithAlpha:(CGFloat)alpha;

@end
