//
//  UIView+image.h
//  PayKeyUI
//
//  Created by Eran Israel on 14/01/2019.
//  Copyright © 2019 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (image)
- (UIImage *)snapshotImage;
@end
