//
//  OTPUIController.h
//  KeyboardApp
//
//  Created by Alex Kogan on 14/01/2017.
//  Copyright © 2017 Marat . All rights reserved.
//

#import "CustomUIController.h"
#import "OTPModel.h"
#import "OTPUIOutput.h"

@interface OTPUIController : CustomUIController

@property (nonatomic, weak) id<OTPUIOutput> otpOutput;

@end
