//
//  PKBacktrackableErrorController.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 08/03/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import "ActionableMessageUIController.h"

@interface PKBacktrackableErrorUIController : ActionableMessageUIController

@end
