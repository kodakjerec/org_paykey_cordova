//
//  PKListView.h
//  PayKeyUI


#import "PKScreen.h"
#import "PKListOutput.h"
#import "PKListViewDelegate.h"

@class PKItemCell;
@class PKBaseListDataSource;

@interface PKListView: PKScreen <PKListOutput>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) id<PKListViewDelegate> listDelegate;

@end
