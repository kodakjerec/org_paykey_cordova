//
//  PaymentInputDecorator.h
//  PayKeyUICore
//
//  Created by Marat  on 12/28/16.
//  Copyright © 2016 alon muroch. All rights reserved.
//

@import KeyboardFramework;

#import "PKFlowManagerDelegate.h"
#import "PKUIKeyboardConfig.h"
#import "PKBaseFlowManager.h"
#import "PKEditTextProtocol.h"
@class PKUIKeyboardConfig;
@class PKDataCollectorFactory;
@class PKDataCollector;
@class PKFlowManagerFactory;
@class PKLandscapeBlockerView;

@interface PKPaymentInputDecorator : UIViewController <
                                        PKFlowManagerDelegate,
                                        PaymentButtonAttributesDelegate,
                                        PKInputTextStateObserver,
                                        PKKeyboardOutputProtocol>

@property (nonatomic, strong) PKUIKeyboardConfig *keyboardConfig;
@property (nonatomic, weak) id <PKKeyboardEventsProtocol> eventsDelegate;
@property (nonatomic,strong,readonly)PKBaseFlowManager* flowManager;
@property (nonatomic, assign) id <PKKeyboardDelegate> PKKeyboardDelegate;
@property (nonatomic, weak) id<PKEditTextProtocol> nextEditField;

- (instancetype)initWithKeyboardConfig:(PKUIKeyboardConfig *)config;

- (void)assignContainerView:(UIView *)container;
- (void)closePaymentUI;
- (void)openPaymentUI;
- (__kindof PKLandscapeBlockerView *)inflateLandscapeBlocker;
- (void)reshowLandscapeBlockerMessageIfNeeded;
- (BOOL)isPaymentMode;
- (void)setFlowManagerFactory:(PKFlowManagerFactory *)flowManagerFactory;
- (void)updateTopBarType;
- (TopBarViewType)suggestionBarType;

//iMessage
- (void)setBottomPortraitPadding:(CGFloat)padding;
- (void)showCompactModeBlockerIfNeeded:(BOOL)isCompact;

@end
