//
// Created by Alex Kogan on 02/04/2018.
// Copyright (c) 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKBaseViewModel.h"
#import "PKTextViewModel.h"
#import "PKInputViewModel.h"
#import "PKAmountInputViewModel.h"
#import "PKButtonViewModel.h"
#import "PKControlViewModel.h"
#import "PKPincodeInputViewModel.h"
#import "PKConstraintModel.h"
#import "PKViewWithCursorModel.h"
#import "PKInputViewInteractorsBuilder.h"
#import "PKAmountViewInteractorsBuilder.h"
#import "PKControllerInteractorsBuilder.h"
#import "PKPincodeViewInteractorsBuilder.h"
#import "PKCurrencyTextViewModel.h"
#import "PKGradientViewModel.h"
#import "PKViewEventBuilder.h"

#ifndef PK_VIEW_CONSTANTS_H
#define PK_VIEW_CONSTANTS_H

//ViewModel
#define PK_DEFAULT(view,viewmodel) ((viewmodel *)[wSelf.blueprint.mAggregator modelByView:view ofType:[viewmodel class]])
#define PK_VIEW(view) PK_DEFAULT(view,PKBaseViewModel)
#define PK_IMAGE(view) PK_DEFAULT(view,PKImageViewModel)
#define PK_TEXT(view) PK_DEFAULT(view,PKTextViewModel)
#define PK_INPUT(view) PK_DEFAULT(view,PKInputViewModel)
#define PK_CONTROL(view) PK_DEFAULT(view,PKControlViewModel)
#define PK_BUTTON(view) PK_DEFAULT(view,PKButtonViewModel)
#define PK_AMOUNT_INPUT(view) PK_DEFAULT(view,PKAmountInputViewModel)
#define PK_PINCODE_INPUT(view) PK_DEFAULT(view,PKPincodeInputViewModel)
#define PK_COLOR(view) PK_DEFAULT(view,FLENColorViewModel)
#define PK_CURRENCY_TEXT(view) PK_DEFAULT(view,PKCurrencyTextViewModel)
#define PK_CURSOR_VIEW(view) PK_DEFAULT(view,PKViewWithCursorModel)
#define PK_GRADIENT_VIEW(view) PK_DEFAULT(view,PKGradientViewModel)

// Constraints Model
#define PK_CONSTRAINT(CONSTRAINT_ID) ((id<PKConstraintModel>)[wSelf.blueprint.constraintAggregator modelByID:CONSTRAINT_ID])

//Interactor builders
#define PK_VIEW_ACTIONS(view) [PKViewInteractorsBuilder builderForView:view]
#define PK_INPUT_ACTIONS(view) [PKInputViewInteractorsBuilder builderForView:view]
#define PK_PINCODE_ACTIONS(view) [PKPincodeViewInteractorsBuilder builderForView:view]
#define PK_AMOUNT_ACTIONS(view) [PKAmountViewInteractorsBuilder builderForView:view]
#define PK_IMAGE_INTERACTORS(view) [PKImageInteractorsBuilder builderForView:view]
#define PK_ACTIVITY_INDICATOR_INTERACTORS(view) [PKActivityIndicatorInteractorsBuilder builderForView:view]
#define PK_CONTROLLER_ACTIONS [PKControllerInteractorsBuilder new]

// Events & Actions
#define PK_CONTROLLER_EVENTS [PKControllerEventBuilder new]
#define PK_VIEW_EVENTS(view) [PKViewEventBuilder builderForView:view]
#define PK_INPUT_EVENTS(view) [PKTextInputEventBuilder builderForView:view]
#define PK_PINCODE_INPUT_EVENTS(view) [PKPincodeInputEventBuilder builderForView:view]

#endif
