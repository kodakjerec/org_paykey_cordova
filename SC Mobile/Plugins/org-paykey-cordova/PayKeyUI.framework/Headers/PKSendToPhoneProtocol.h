//
//  PKSendToPhoneProtocol.h
//  PayKeyUI
//
//  Created by Alex Kogan on 13/06/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PKSendToPhoneProtocol <NSObject>

- (BOOL)showSendToPhone:(NSString *)filter;

@end
