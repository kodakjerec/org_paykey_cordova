//
//  PKCellItemPresenter.h
//  PayKeyUICore
//
//  Created by Alex Kogan on 20/06/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKFlowLocalizationProvider.h"
#import "CustomModel.h"

@interface PKTableViewCellPresenter : NSObject

- (instancetype)initWithNibName:(NSString *)nibName bundle:(NSBundle *)bundle dataModel:(id)dataModel;



- (void)registerNib:(UITableView *)tableView;

- (UITableViewCell *)dequeueReusableCellFromTableView:(UITableView *)tableView forIndex:(NSIndexPath *)indexPath;

@property (nonatomic) BOOL isSelected;
@property (nonatomic, weak) id <PKFlowLocalizationProvider> localizationProvider;
@property (nonatomic, readonly) NSString *nibName;
@property (nonatomic, readonly) id dataModel;


@end
