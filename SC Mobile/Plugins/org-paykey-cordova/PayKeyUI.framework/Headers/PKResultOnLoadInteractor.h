//
//  PKResultOnLoadInteractor.h
//  PayKeyUI
//
//  Created by Eran Israel on 26/11/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKViewInteractor.h"

@interface PKResultOnLoadInteractor : NSObject <PKViewInteractor>

+ (instancetype)interactorWithResult:(id)result;

@end

