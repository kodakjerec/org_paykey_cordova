//
//  PKContactsOutput.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 22/10/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#ifndef PKContactsOutput_h
#define PKContactsOutput_h

#import "PKPhone.h"
@import KeyboardFramework;

@protocol PKContactsOutput

- (void)setContactsSections:(NSArray *)contactsSections phone:(PKPhone *)phone;

@end

#endif /* PKContactsOutput_h */
