//
//  PKCurrencyTextViewModel.h
//  PayKeyUI
//
//  Created by Daniel M. on 24/12/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKViewModel.h"
#import "PKViewObserver.h"
#import "PKCurrencyLabel.h"
NS_ASSUME_NONNULL_BEGIN

@interface PKCurrencyTextViewModel : PKViewObserver <PKViewModel>

@property (nonatomic) CurrencySymbolPosition currencyPosition;
@property (nonatomic) PKCurrencyGravity currencyGravity;
@property (nonatomic) NSString *currencySymbol;
@property (nonatomic) CGFloat   symbolToTextMargin;
@property (nonatomic) CGFloat   capHeightDevider;
@property (nonatomic) BOOL      isSymbolNormalSize;
@property (nonatomic) CGFloat   currencySymbolCoefficient;
@property (nonatomic) UIColor  *currencySymbolTextColor;
@property (nonatomic) UIFont   *currencySymbolFont;

@end

NS_ASSUME_NONNULL_END
