//
//  ActionableMessageModel.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 07/03/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import "CustomModel.h"

@interface ActionableMessageModel : CustomModel

@property (copy) NSString *messageTitle;
@property (copy) NSString *messageText;
@property (copy) NSString *messageImageName;
@property (copy) NSString *actionText;

@property (nonatomic,strong) UIColor* actionButtonBorderColor;
@property (nonatomic,assign) CGFloat actionButtonBorderWidth;
@property (nonatomic,assign) UIEdgeInsets actionEdgeInsets;
@property (copy) NSNumber *autocloseDelay;

@end
