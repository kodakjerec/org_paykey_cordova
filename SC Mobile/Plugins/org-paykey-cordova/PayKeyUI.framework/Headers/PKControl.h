//
//  PKControl.h
//  PayKeyUICore
//
//  Created by ishay weinstock on 26/10/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#ifndef PKControl_h
#define PKControl_h

#import "PKModel.h"
#import <UIKit/UIKit.h>


@class PKUIController;
@protocol PKTextDocumentProxy;
@protocol PKEditTextProtocol;
@protocol PKControllerDelegate <NSObject>

- (void)controllerDidChangeTo:(PKUIController *)nextController;
- (void)setDocumentTextProxy:(id<PKTextDocumentProxy>)textProxy;
- (BOOL)isExpanded;
- (void)hideKeyboard;
- (void)showKeyboard;
- (void)openURL:(NSString *)url;
- (void)notifyAbort;
- (void)setOutputText:(NSString*)text;
- (void)updateReturnKey;
- (void)advanceToNextInputMode;
- (void)setNextEditField:(id<PKEditTextProtocol>)nextEditField;

@end

@protocol PKControl <NSObject>

@property (nonatomic) NSString *tag;
@property (strong, nonatomic) PKModel *model;
@property (nonatomic,weak) id<PKControllerDelegate> controllerDelegate;

- (void)appear;
- (void)remove;
@end

#endif /* PKControl_h */
