//
//  PKKeyboardSafeResponder.h
//  PayKeyUI
//
//  Created by Daniel M. on 02/06/2019.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PKKeyboardSafeResponder : NSObject

+(BOOL)canSafelyRespond;

@end

NS_ASSUME_NONNULL_END
