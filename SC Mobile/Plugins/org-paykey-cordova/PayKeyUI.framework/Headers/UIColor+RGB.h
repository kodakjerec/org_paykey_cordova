//
//  UIColor+RGB.h
//  MakePayment
//
//  Created by Jonathan Beyrak-Lev on 15/01/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]


@protocol PKRGB

- (UIColor *)rgb;
- (UIColor *)rgbWithAlpha:(CGFloat)alpha;

@end

@interface UIColor (RGB) <PKRGB>

+ (instancetype)rgbColorWithWhite:(NSInteger)white;
+ (instancetype)rgbColorWithWhite:(NSInteger)white alpha:(CGFloat)alpha;
+ (instancetype)rgbColorWithRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue;
+ (instancetype)rgbColorWithRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue alpha:(CGFloat)alpha;
- (instancetype)rgb;

@end
