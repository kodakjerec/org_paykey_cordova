//
//  PKEventBasedInteractor.h
//  PayKeyUI
//
//  Created by German Velibekov on 02/01/2019.
//  Copyright © 2019 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKViewInteractor.h"
#import "FLENEvent.h"

NS_ASSUME_NONNULL_BEGIN

@interface FLENEventInteractor : NSObject <PKViewInteractor, FLENEventListener>

- (instancetype)initWithEvent:(id<FLENEvent>)event;

@property (nonatomic, strong) id<FLENEvent>event;

@end

NS_ASSUME_NONNULL_END
