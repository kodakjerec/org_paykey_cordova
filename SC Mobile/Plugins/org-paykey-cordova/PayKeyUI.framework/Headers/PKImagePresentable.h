//
//  PKCurrencyTextPresentable.h
//  PayKeyUI
//
//  Created by Daniel M. on 24/12/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol PKImagePresentable <NSObject>

-(void)setImage:(UIImage*)image;

@optional

-(void)setAnimationRepeatCount:(NSInteger)animationRepeatCount;

-(void)setAnimationImages:(NSArray*)animationImages;

-(void)setAnimationDuration:(NSTimeInterval)animationDuration;

@end

NS_ASSUME_NONNULL_END
