//
//  PKTextFieldUIController.h
//  PayKeyUI
//
//  Created by Eran Israel on 26/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import "CustomUIController.h"
#import "PKTextField.h"

__attribute__ ((deprecated))
@interface PKTextFieldUIController : CustomUIController <PKTextFieldDelegate>

@end
