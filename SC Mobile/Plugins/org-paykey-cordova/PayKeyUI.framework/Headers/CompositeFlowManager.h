//
//  PKCompositeFlowManager.h
//  PayKeyUI
//
//  Created by Alex Kogan on 17/06/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import "PKBaseFlowManager.h"
#import "PayKeyDelegate.h"
#import "ClientFlowManager.h"
#import "PKFlowLocalizationProvider.h"
#import "PKFlowAction.h"

@protocol PKControllerFactory;

@interface CompositeFlowManager : ClientFlowManager

@property (nonatomic, strong, readonly) NSDictionary* flowManagerDictionary;
@property (nonatomic, strong) PKBaseFlowManager* currentFlowManager;

- (instancetype)initWithClient:(id<PayKeyDelegate>)client
                       factory:(PKScreenFactory *)factory
          localizationProvider:(id<PKFlowLocalizationProvider>)localizationProvider
                  flowManagers:(NSDictionary*)flowManagers;

- (NSInteger)menuItemType;
- (void)createMenuActions;
- (PKPerformsAction)flowAction:(PKBaseFlowManager*)manager;
- (void)buildPreMenuFlow;
- (NSString*)initialScreenTag;
- (void)onFlowStart:(NSInteger)tag;
- (void)restoreLatestOperation;
- (void)setRestoreState;
- (void)onRestore;
- (void)handleShouldRestoreState;
- (void)handleSessionNotRestored;
- (void)goToFlowManager:(NSInteger)index;

@end
