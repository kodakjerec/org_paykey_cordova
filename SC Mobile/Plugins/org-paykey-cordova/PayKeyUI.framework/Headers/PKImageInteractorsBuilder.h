//
//  PKImageInteractorsBuilder.h
//  PayKeyUI
//
//  Created by Daniel M. on 06/01/2019.
//  Copyright © 2019 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKViewInteractorsBuilder.h"
#import "PKImageViewAnimationSettings.h"
#import "FLENEventInteractor.h"
NS_ASSUME_NONNULL_BEGIN

@interface PKImageInteractorsBuilder : PKViewInteractorsBuilder

/**
 animate image with given settings
 
 @note
 The images can't sit in a assets folder but in root folder. You can use a group without folder if you want.
 
 Also images format must be PNG.

 @param imageViewAnimationSettings the animation settings
 @return event based interactor of animate
 */
- (FLENEventInteractor *)animateImageWithSettings:(PKImageViewAnimationSettings*)imageViewAnimationSettings;

@end

NS_ASSUME_NONNULL_END
