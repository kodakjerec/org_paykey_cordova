//
// Created by Alex Kogan on 03/04/2018.
// Copyright (c) 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKViewInteractor.h"
#import "PKEmptyAction.h"

@interface PKRunActionOnPincodeInputChangeInteractor : NSObject <PKViewInteractor>

+ (instancetype)forView:(NSInteger)inputViewTag action:(PKEmptyAction)action;

@end
