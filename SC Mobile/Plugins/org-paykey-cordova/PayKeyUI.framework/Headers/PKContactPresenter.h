//
//  PKContactPresenter.h
//  PayKeyUI
//
//  Created by Eden Landau on 20/04/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import "PKContact.h"
#import "PKTableViewCellPresenter.h"

@interface PKContactPresenter : PKTableViewCellPresenter <PKContact>

@property (nonatomic, strong) PKContact *contact;
@property (nonatomic, weak)CustomModel* appModel;

- (instancetype)initWithNibName:(NSString *)nibName bundle:(NSBundle *)bundle contact:(PKContact *)contact;

- (instancetype)initWithNibName:(NSString *)nibName bundle:(NSBundle *)bundle contact:(PKContact*)dataModel withAppModel:(CustomModel *)model;

- (NSString *)fullName;
- (NSString *)firstPhoneNumber;
- (NSString *)initials;
- (UIImage *)getImage;
@end
