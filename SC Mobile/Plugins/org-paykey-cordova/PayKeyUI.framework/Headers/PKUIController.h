//
//  PKUIController.h
//  KeyboardApp
//
//  Created by Alex Kogan on 14/01/2017.
//  Copyright © 2017 Marat . All rights reserved.
//
// PKUIController wrapper to support the PKFlowDelegate capability
#import "PKEditTextProtocol.h"
#import "PKControl.h"
#import "PKUIControllerLifecycleListener.h"
#import "PKTrackEventListener.h"

typedef void (^PKControllerResultHandler)(id result);

@class PKLanguage;
@class PKDataCollector;
@class PKModel;

@interface PKUIController : UIViewController <PKControl, PKTrackEventListener>

@property (nonatomic, weak) id<PKUIControllerLifecycleListener> lifecycleListener;

@property (nonatomic,weak) id<PKControllerDelegate> controllerDelegate;
@property (nonatomic) BOOL shouldShowKeyboard;
@property (nonatomic) BOOL showExtraActionButtonInPortrait;
@property (nonatomic) BOOL requireExpandedMode; // iMessage
@property (nonatomic, strong) NSNumber *height;
@property (nonatomic, copy) PKControllerResultHandler resultHandler;

- (void)setNextEditField:(id<PKEditTextProtocol>)field;
- (void)notifyAbort;
- (void)notifyResult:(id)data;
- (void)notifyResult:(id)data afterDelay:(CGFloat)seconds;
- (void)showKeyboard;
- (void)hideKeyboard;
- (void)advanceToNextInputMode;
- (UIView*)getView;
- (void)bindView;
- (void)zeroDocumentProxy;
- (void)onBindView:(UIView *)view model:(__kindof PKModel *)model;
//imessage
- (void)shouldRespositionKeyboard;
- (BOOL)isExpanded;


@end
