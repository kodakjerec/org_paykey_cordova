//
//  PKContactCellProtocol.h
//  PayKeyUICore
//
//  Created by Alex Kogan on 20/06/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import <Foundation/Foundation.h>
@class PKTableViewCellPresenter;

@protocol PKTableViewCellPresenterSetterProtocol <NSObject>

@required

- (void)setTableViewCellPresenter:(PKTableViewCellPresenter *)presenter;

@optional

- (void)selectCell:(BOOL)selected;

@end
