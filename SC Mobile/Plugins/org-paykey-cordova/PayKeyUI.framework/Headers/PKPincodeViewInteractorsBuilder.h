//
//  PKInputViewInteractions.h
//  PayKeyUI
//
//  Created by German Velibekov on 28/10/2018.
//

@import Foundation;
#import "PKViewInteractorsBuilder.h"
#import "PKEmptyAction.h"

NS_ASSUME_NONNULL_BEGIN

@interface PKPincodeViewInteractorsBuilder: PKViewInteractorsBuilder

- (id<PKViewInteractor>)resultOnMaximumLengthReached:(__nullable id)result;
- (id<PKViewInteractor>)onInputChange:(PKEmptyAction)action;
- (id<PKViewInteractor>)savePincode;

@end

NS_ASSUME_NONNULL_END
