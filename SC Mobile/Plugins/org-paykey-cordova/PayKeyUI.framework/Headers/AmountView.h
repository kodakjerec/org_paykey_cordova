//
//  AmountView.h
//  PayKeyUI
//
//  Created by Eden Landau on 01/02/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import "PKScreen.h"
#import "PKNavigationBar.h"
#import "PKCurrencyTextField.h"
#import "PKAmountOutput.h"

@protocol AmountViewDelegate <NSObject>

@required
- (void)continueRunWithAmount:(NSNumber *)amount;

@end

@interface AmountView : PKScreen <PKAmountOutput>

@property (weak, nonatomic) id<AmountViewDelegate> amountViewDelegate;
@property (weak, nonatomic) IBOutlet PKCurrencyTextField *amountLabel;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;

@property (nonatomic, strong) UIColor *initialAmountColor;
@property (nonatomic, strong) UIFont *initialAmountFont;

- (void)setInputHandler:(PKCurrencyInputHandler *)inputHandler;
- (IBAction)tappedButton:(id)sender;
- (void)shouldShowError:(BOOL)showError;
- (NSNumber *)amount;
- (void)updateAmountLabel;

@end

