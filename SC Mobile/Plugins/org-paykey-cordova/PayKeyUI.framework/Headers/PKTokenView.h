//
//  PKTokenView.h
//  PKTokenInputView
//
//  Created by Rizwan Sattar on 2/24/14.
//  Copyright (c) 2014 Cluster Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class PKTokenView;
@class PKTokenViewConfig;
@protocol PKTokenViewDelegate <NSObject>

@required
- (void)tokenViewDidRequestSelection:(PKTokenView *)tokenView;

@end


@interface PKTokenView : UIView

@property (assign, nonatomic) BOOL selected;
@property (assign, nonatomic) BOOL hideUnselectedComma;
@property (weak, nonatomic, nullable) NSObject <PKTokenViewDelegate> *delegate;

- (void)setSelected:(BOOL)selected animated:(BOOL)animated;
- (void)updateWithConfig:(PKTokenViewConfig*)config displayText:(NSString*)displayText;
@end

NS_ASSUME_NONNULL_END
