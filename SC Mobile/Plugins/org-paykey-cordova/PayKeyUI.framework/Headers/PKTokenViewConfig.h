//
//  PKTokenViewConfig.h
//  PayKeyUI
//
//  Created by Eran Israel on 13/12/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

@import UIKit;

NS_ASSUME_NONNULL_BEGIN

@interface PKTokenViewConfig : NSObject

@property(nonatomic, strong) UIColor* labelTextColor;
@property(nonatomic, strong) UIColor* selectedLabelTextColor;

@property(nonatomic, strong) UIFont* labelFont;

@property(nonatomic, strong) UIColor* backgroundViewColor;
@property(nonatomic, strong) UIColor* selectedBackgroundViewColor;

@property(nonatomic, assign) CGFloat backgroundViewRadius;
@property(nonatomic, strong) UIColor* backgroundViewBorderColor;
@property(nonatomic, assign) CGFloat backgroundViewBorderWidth;
@property(nonatomic, assign)CGFloat height;

@end

NS_ASSUME_NONNULL_END
