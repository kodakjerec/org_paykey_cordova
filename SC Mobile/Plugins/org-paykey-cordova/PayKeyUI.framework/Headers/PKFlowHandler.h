//
//  PKFlowHandler.h
//  KeyboardApp
//
//  Created by Alex Kogan on 14/01/2017.
//  Copyright © 2017 Marat . All rights reserved.

@protocol PKFlowHandler <NSObject>

- (void)build;
- (BOOL)back;
- (void)goToFlowManager:(NSInteger)index;

@end
