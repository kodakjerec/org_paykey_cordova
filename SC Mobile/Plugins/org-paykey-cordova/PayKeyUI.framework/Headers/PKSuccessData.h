//
//  PKSuccessData.h
//  PayKeyUI
//
//  Created by Marat  on 7/4/17.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PKSuccessData : NSObject <NSCoding>

@property(nonatomic)NSString *outputText;

@end
