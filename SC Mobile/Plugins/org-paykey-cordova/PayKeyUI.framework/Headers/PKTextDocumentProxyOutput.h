//
//  PKTextDocumentProxyOutput.h
//  PayKeyUI
//
//  Created by ishay weinstock on 30/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#ifndef PKTextDocumentProxyOutput_h
#define PKTextDocumentProxyOutput_h

@protocol PKTextDocumentProxyOutput <UITextDocumentProxy>

@end
#endif /* PKTextDocumentProxyOutput_h */
