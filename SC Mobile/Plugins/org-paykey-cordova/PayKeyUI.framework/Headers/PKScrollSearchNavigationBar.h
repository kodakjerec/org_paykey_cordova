//
//  PKScrollSearchNavigationBar.h
//  PayKeyUI
//
//  Created by Eran Israel on 02/08/2018.
//

#import "PKNavigationBar.h"
#import "PKSearchNavigationBarOutput.h"

__attribute__ ((deprecated))
@interface PKScrollSearchNavigationBar : PKNavigationBar <PKScrollSearchOutput>
@property (nonatomic, weak) IBOutlet PKScrollTextField *scrollSearchField;
@end
