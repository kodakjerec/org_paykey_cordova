//
//  PKLinkLabel.h
//  PayKeyUI
//
//  Created by Marat  on 7/15/17.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PKLinkeLabelDelegate <NSObject>

- (void)didClickOnLinkAtRange:(NSRange)rangeOfClick;

@end

@interface PKLinkLabel : UILabel

@property(nonatomic,weak)id<PKLinkeLabelDelegate>delegate;

- (void)addLinkWithAttributes:(NSDictionary*)dictAttrs range:(NSRange)range;
- (void)resetLinks;

@end
