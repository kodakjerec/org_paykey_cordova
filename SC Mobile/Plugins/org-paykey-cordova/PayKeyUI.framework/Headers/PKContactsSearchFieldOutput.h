//
//  PKContactsSearchFieldOutput.h
//  PayKeyUI
//
//  Created by Eran Israel on 14/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKContactsOutput.h"
#import "PKSearchNavigationBarOutput.h"

__attribute__ ((deprecated))
@protocol PKContactsSearchFieldOutput <PKContactsOutput, PKSearchNavigationBarOutput>

@end

