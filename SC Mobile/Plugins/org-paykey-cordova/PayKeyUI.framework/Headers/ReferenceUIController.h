//
//  PasswordUIController.h
//  KeyboardApp
//
//  Created by Alex Kogan on 14/01/2017.
//  Copyright © 2017 Marat . All rights reserved.
//

#import "PKScrollTextField.h"
#import "CustomUIController.h"
#import "ReferenceUIOutput.h"
#import "ReferenceView.h"

@interface ReferenceUIController : CustomUIController <PKScrollTextFieldDelegate, ReferenceViewDelegate>
@property (nonatomic, weak) id <ReferenceUIOutput> referenceOutput;
@end
