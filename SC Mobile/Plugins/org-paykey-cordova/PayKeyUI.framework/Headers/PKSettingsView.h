//
//  PKSettingsView.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 19/06/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import "PKScreen.h"
@import KeyboardFramework;

@interface PKSettingsView : PKScreen

@property (weak, nonatomic) IBOutlet PKSettingsTableView *settingsTableView;

@end
