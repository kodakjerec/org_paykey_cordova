//
//  UIImage+PKContact.h
//  PayKeyUI
//
//  Created by Eden Landau on 25/04/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (PKContact)

+ (instancetype)contactImageWithInitials:(NSString *)fullName;

@end
