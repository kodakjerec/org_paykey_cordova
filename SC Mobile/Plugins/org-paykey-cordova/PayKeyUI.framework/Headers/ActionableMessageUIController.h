//
//  ActionableMessageUIController.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 07/03/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import "CustomUIController.h"
#import "ActionableMessageModel.h"
#import "ActionableMessageView.h"

@interface ActionableMessageUIController : CustomUIController <ActionableMessageViewDelegate>

- (NSNumber*)defualtResult;

@end
