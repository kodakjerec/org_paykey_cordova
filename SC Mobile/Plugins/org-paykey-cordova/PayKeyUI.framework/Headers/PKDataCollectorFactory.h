//
//  PKDataCollectorFactory.h
//  PayKeyUICore
//
//  Created by Alex Kogan on 07/06/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import <Foundation/Foundation.h>
@class PKDataCollector;
@interface PKDataCollectorFactory : NSObject

- (PKDataCollector *)create;

@end
