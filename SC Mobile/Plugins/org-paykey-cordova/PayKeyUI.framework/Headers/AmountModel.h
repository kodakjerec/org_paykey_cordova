//
//  AmountModel.h
//  KeyboardApp
//
//  Created by Alex Kogan on 14/01/2017.
//  Copyright © 2017 Marat . All rights reserved.
//

#import "CustomModel.h"

@interface AmountModel : CustomModel

@property (nonatomic,strong) NSString *nextButtonTitle;

@property (nonatomic) NSNumber *minLimit;
@property (nonatomic) NSNumber *maxLimit;

@end
