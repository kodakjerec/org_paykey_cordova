//
//  UIFont+LTStd.h
//  PayKeyUI
//
//  Created by Marat  on 5/14/17.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomFonts : NSObject

- (void)loadFonts:(NSArray *)fontFileNames bundle:(NSBundle *)bundle;
- (void)unloadAllFonts;

@end
