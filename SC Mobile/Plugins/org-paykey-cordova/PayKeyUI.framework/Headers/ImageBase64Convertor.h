//
//  ImageBase64Convertor.h
//  PayKey-Clients
//
//  Created by Daniel M. on 06/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageBase64Convertor : NSObject
+ (NSString *)encodeToBase64String:(UIImage *)image;
+ (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData;
@end
