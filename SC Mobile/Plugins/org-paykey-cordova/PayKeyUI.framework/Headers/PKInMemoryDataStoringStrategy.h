//
//  PKInMemoryDataStoringStrategy.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 12/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKDataStoringStrategy.h"

@interface PKInMemoryDataStoringStrategy : NSObject <PKDataStoringStrategy, NSCoding>

+ (instancetype)restoreWithID:(NSString *)persistenceID validTime:(NSNumber*)timeInSeconds;
- (instancetype)initWithID:(NSString *)persistenceID validTime:(NSNumber*)timeInSeconds;
- (void)persist;

@end
