//
//  PKCurrencyTextViewModel.h
//  PayKeyUI
//
//  Created by Daniel M. on 24/12/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKViewModel.h"
#import "PKViewObserver.h"

NS_ASSUME_NONNULL_BEGIN

@interface PKImageViewModel : PKViewObserver <PKViewModel>

@property (nonatomic, strong) UIImage *image;

@property (nonatomic, strong) NSArray* animationImages;

@property (assign,nonatomic) NSNumber* animationDuration;

@property (assign,nonatomic) NSNumber* animationRepeatCount;

@end

NS_ASSUME_NONNULL_END
