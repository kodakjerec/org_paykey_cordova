//
//  PKPaymentActions.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 25/10/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#ifndef PKPaymentActions_h
#define PKPaymentActions_h

@class PKSetupData;
@class PKError;
@class PKSuccessData;
@class PKContact;
@class PKFatalError;

@protocol PKPaymentActions

- (void)setSetup:(PKSetupData*)data;
- (void)setAmount:(NSNumber *)amount;
- (void)setPayee:(__kindof PKContact *)payee;
- (void)setShouldRestoreState:(BOOL)shouldRestore;
- (void)setPassword:(NSString *)password;
- (void)setUserName:(NSString *)username;
- (void)setPincode:(NSString*)pincode;
- (void)setOtp:(NSString*)otp;
- (void)setTouchIDResult:(NSNumber *)result  __attribute__((deprecated("Replaced by -setBiometricResult:")));
- (void)setBiometricResult:(NSNumber*)result;
- (void)setTouchIDResultError:(PKFatalError*)error  __attribute__((deprecated("Replaced by -setBiometricResultError:")));
- (void)setBiometricResultError:(__kindof PKError*)error;
- (void)setAvailablePayees:(NSArray *)availablePayees;
- (void)clear;
- (void)setReference:(NSString*)reference;
- (void)setFlowManagerIndex:(NSNumber*)index;
- (void)setCurrentError:(PKError*)error;
- (void)setCurrentFlowType:(NSNumber*)type;
- (void)setSuccessData:(PKSuccessData *)successData;
- (void)setSelectedPayees:(NSArray*)selectedPayees;
- (void)addSelectedPayee:(__kindof PKContact*)payee;
- (void)setLastScreenTag:(NSString *)tag;


// List items
- (void)setListItemsFilter:(NSString*)filter;
- (void)setNextListItemsClassifier:(id)classifier;

@end

#endif /* PKPaymentActions_h */
