//
//  PKCurrencyLabel.h
//  PayKeyUI
//
//  Created by Alex Kogan on 20/04/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKCurrencyInputHandler.h"
#import "PKCurrencyBaseLabel.h"

typedef NS_ENUM(NSUInteger, CurrencySymbolPosition) {
    CurrencySymbolPositionTop,
    CurrencySymbolPositionButtom,
    CurrencySymbolPositionCenter
};

@interface PKCurrencyLabel : PKCurrencyBaseLabel

@property (nonatomic) CurrencySymbolPosition currencyPosition;
@property (nonatomic) PKCurrencyGravity currencyGravity;
@property (nonatomic) NSString *currencySymbol;
@property (nonatomic) CGFloat   symbolToTextMargin;
@property (nonatomic) CGFloat   capHeightDevider;
@property (nonatomic) BOOL      isSymbolNormalSize;
@property (nonatomic) CGFloat   currencySymbolCoefficient;
@property (nonatomic) UIColor  *currencySymbolTextColor;
@property (nonatomic) UIFont   *currencySymbolFont;

- (void)resizeTextToFit;

@end
