//
//  PKScreenBlueprint.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 23/10/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import "CustomModel.h"
#import "PKScreenConfiguration.h"
#import "PKUICoreLocalizationProvider.h"

@class CustomUIController;
@class PKScreen;
@class PKNavigationBar;
@class PKViewModelAggregator;
@class PKViewInteractorAggregator;
@class PKConstraintAggrigator;
@class PKContainerView;

@interface PKScreenBlueprint : NSObject

@property (nonatomic) CustomUIController *controller;
@property (nonatomic) PKScreen *view;
@property (nonatomic) PKNavigationBar *navigationBar;
@property (nonatomic) CustomModel *model;
@property (nonatomic) PKNavigationBarSettings *navigationSettings;
@property (nonatomic) PKViewSettings *viewSettings;
@property (nonatomic) NSLocale *locale;
@property (nonatomic) BOOL shouldShowKeyboard;
@property (nonatomic) BOOL showExtraActionButtonInPortrait;
@property (nonatomic) BOOL requireExpandedMode; // iMessage
@property (nonatomic) NSArray* linkMap;
@property (nonatomic) NSArray* extendedLinkMap;
@property (nonatomic, weak) id <PKPaymentData> paymentData;
@property (nonatomic, weak) id <PKPaymentActions> paymentActions;
@property (weak, nonatomic) id<PKUICoreLocalizationProvider> localizationProvider;

@property (nonatomic) NSArray* viewInteractors;

@property (nonatomic, strong) PKViewModelAggregator *mAggregator;
@property (nonatomic, strong) PKViewInteractorAggregator *iAggregator;
@property (nonatomic, strong) PKConstraintAggrigator * constraintAggregator;

- (void)installView:(PKScreen *)view;
- (void)installNavigationBar;
- (void)onDelegatesLink;
- (CustomUIController *)build;
- (NSString*)containerViewXibName;
- (__kindof PKContainerView*)containerView;
- (void)setConfiguration:(PKScreenConfiguration *)configuration;

@end

