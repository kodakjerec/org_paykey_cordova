//
//  PKRedirectableError.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 08/03/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import "PKFatalError.h"

@interface PKRedirectableError : PKFatalError

@property (nonatomic, strong) NSString *redirectUrl;
@property (nonatomic, strong) NSString *actionText;

@end
