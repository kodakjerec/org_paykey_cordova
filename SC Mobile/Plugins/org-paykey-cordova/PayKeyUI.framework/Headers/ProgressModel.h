//
//  ProgressModel.h
//  PayKeyUI
//
//  Created by Alex Kogan on 09/02/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import "CustomModel.h"

@interface ProgressModel : CustomModel

@property (nonatomic, strong) NSString* progressMessage;

@end

