//
//  NSString+PKAdditions.h
//  PayKeyUI
//
//  Created by Eran Israel on 16/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (PKAdditions)

- (BOOL)isEmptyStringIncludeWhitespace;
- (BOOL)isEmptyString;
- (BOOL)stringMathcesRegex:(NSString*)regex;
- (NSString *)mask:(NSString*)maskChar withShowingLastDigits:(NSInteger)digitsToShow;

@end
