//
//  PKResultAction.h
//  PayKeyUI
//
//  Created by German Velibekov on 18/11/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#ifndef PKResultAction_h
#define PKResultAction_h

typedef void (^PKResultAction)(id result);

#endif /* PKResultAction_h */
