//
//  PKFatalError.h
//  PayKeyUI
//
//  Created by Eden Landau on 08/02/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKError.h"

__attribute__ ((deprecated))
@interface PKFatalError : PKError

@property (nonatomic, strong) NSString * _Nullable subtitle;

- (instancetype _Nonnull)initWithTitle:(NSString* _Nonnull)title subtitle:(NSString * _Nullable)subtitle;

@end
