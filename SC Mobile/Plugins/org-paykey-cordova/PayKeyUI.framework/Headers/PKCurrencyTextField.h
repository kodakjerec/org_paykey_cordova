//
//  PKCurrencyTextField.h
//  PayKeyUI
//
//  Created by ishay weinstock on 15/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import "PKCurrencyLabel.h"
#import "PKCurrencyViewWithCursor.h"
@import KeyboardFramework;


@class PKCurrencyTextField;

@protocol PKCurrencyTextFieldDelegate <NSObject>

@optional
- (void)amountChanged:(NSNumber*)amount;
- (BOOL)currencyTextFieldShouldBeginEditing:(PKCurrencyTextField *)currencyTextField;
- (void)insertText:(NSString *)text;
- (NSString*)formateInputLabel:(NSString*)stringAmount;

@end

@interface PKCurrencyTextField : PKCurrencyViewWithCursor <PKTextDocumentProxy>

// UI
@property (nonatomic) IBInspectable UIColor *placeHolderCurrencySymbolTextColor;
@property (nonatomic) IBInspectable UIColor *cursorColor;
@property (nonatomic) IBInspectable UIColor *textColor;
@property (nonatomic) IBInspectable UIColor *placeHolderTextColor;
@property (nonatomic) IBInspectable BOOL showCurrencyInPlaceHolder;
@property (nonatomic) IBInspectable CGFloat   symbolToTextMargin;
@property (nonatomic) IBInspectable CGFloat   capHeightDevider;
@property (nonatomic) IBInspectable BOOL      isPlaceholderSymbolNormalSize;
@property (nonatomic) IBInspectable BOOL      isSymbolNormalSize;
@property (nonatomic) IBInspectable NSString *overrideCurrencySymbol;


@property (nonatomic) NSString *placeholderText;
@property (nonatomic) NSNumber* initialValue;
@property (nonatomic) PKCurrencyGravity currencyGravity;
@property (nonatomic) CurrencySymbolPosition currencyPosition;
@property (nonatomic) NSTextAlignment textAlignment;
@property (nonatomic) UIFont *font;
@property (nonatomic) UIFont *placeHolderFont;
@property (nonatomic, strong) PKCurrencyInputHandler *inputHandler;
@property (nonatomic, readonly) BOOL isEmpty;
@property (nonatomic, weak) id<PKCurrencyTextFieldDelegate> delegate;
@property (nonatomic, readonly) NSNumber *amount;
@property (nonatomic, assign) CGFloat currencySymbolCoefficient;

@property (nonatomic, strong) NSNumber *overrideCursorLabelAlignment;
@property (nonatomic, weak) id <PKInputTextStateObserver> inputTextStateObserver;

@end
