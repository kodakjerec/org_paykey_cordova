//
//  FLENCursorViewEditable
//  PayKeyUI
//
//  Created by Daniel M. on 26/05/2019.
//  Copyright © 2019 PayKey. All rights reserved.
//

@import UIKit;
#import "PKLegacyViewWithCursor.h"
NS_ASSUME_NONNULL_BEGIN

@protocol FLENCursorViewEditable <NSObject>

@required

-(void)setCursorMode:(PKLegacyViewCursorMode)cursorMode;

@optional

-(void)setAutocorrectionType:(UITextAutocorrectionType)autocorrectionType;

@end

NS_ASSUME_NONNULL_END
