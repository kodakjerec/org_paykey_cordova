//
//  UILabel+Spacing.h
//  PayKeyUI
//
//  Created by Eran Israel on 14/01/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Spacing)

- (void)addCharactersSpacing:(CGFloat)spacing text:(NSString*)text;
- (void)addCharactersSpacing:(CGFloat)spacing;

@end
