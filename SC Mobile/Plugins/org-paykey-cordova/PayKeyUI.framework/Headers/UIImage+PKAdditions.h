//
//  UIImage+PKAdditions.h
//  PayKeyUI
//
//  Created by Eran Israel on 05/03/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (PKAdditions)

- (UIImage*) drawImage:(UIImage*) fgImage atPoint:(CGPoint) point;

@end
