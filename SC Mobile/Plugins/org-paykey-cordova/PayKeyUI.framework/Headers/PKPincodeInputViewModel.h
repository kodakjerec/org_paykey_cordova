//
// Created by Alex Kogan on 02/04/2018.
// Copyright (c) 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKViewModel.h"
#import "PKViewObserver.h"

@interface PKPincodeInputViewModel : PKViewObserver <PKViewModel>

@property (nonatomic, assign) NSInteger length;
@property (nonatomic, strong) NSString* digitXibName;
@property (nonatomic, assign) CGSize digitSize;
@property (nonatomic, strong) NSBundle* bundle;
@property (nonatomic, strong) NSString* pincode;

@end
