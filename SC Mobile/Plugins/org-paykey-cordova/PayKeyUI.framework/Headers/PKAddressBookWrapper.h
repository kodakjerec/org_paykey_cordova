//
//  PKAddressBookWrapper.h
//  PayKeyUI
//
//  Created by Eden Landau on 19/03/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#pragma clang diagnostic ignored "-Wdeprecated-declarations"

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>
@class UIImage;

typedef void (^requestAccessCompletion)(bool granted, CFErrorRef error);

typedef NS_ENUM(NSInteger, PKAddressBookPersonPropertyTag) {
    kPKAddressBookGivenName,
    kPKAddressBookFamilyName
};

@interface PKAddressBookPhone : NSObject

@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) NSString *tag;

@end

@interface PKAddressBookWrapper : NSObject

- (ABAddressBookRef)create:(NSError **)error;
- (NSArray *)copyArrayOfAllPeople:(ABAddressBookRef)addressBook;
- (void)requestAccessWithError:(NSError **)error;
- (ABAuthorizationStatus)getAuthorizationStatus;
- (NSString *)getStringFromRef:(ABRecordRef)ref withKey:(PKAddressBookPersonPropertyTag)key;
- (NSArray *)getPhonePropertyFromRef:(ABRecordRef)ref;
- (UIImage *)copyImage:(ABRecordRef)ref;

@end
