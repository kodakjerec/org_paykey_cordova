//
//  PKTransitionAnimation.h
//  PayKeyUI
//
//  Created by ishay weinstock on 25/04/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PKTransitionAnimation <NSObject>

- (void)animateWithPrevView:(UIView *)prevView
                   nextView:(UIView *)nextView
              containerView:(UIView *)continerView
                 completion:(void (^)(void))completion;

@property (nonatomic, assign) NSTimeInterval duration;

@end
