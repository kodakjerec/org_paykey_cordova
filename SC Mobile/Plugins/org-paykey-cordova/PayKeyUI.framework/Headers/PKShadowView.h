//
//  SCShadowView.h
//  SCUAEClient
//
//  Created by Eran Israel on 14/08/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PKShadowView : UIView

@property (strong, nonatomic) UIColor* shadowColor;
@property (assign, nonatomic) CGSize shadowOffset;
@property (assign, nonatomic) CGFloat shadowOpacity;
@property (assign, nonatomic) CGFloat shadowRadius;

@end
