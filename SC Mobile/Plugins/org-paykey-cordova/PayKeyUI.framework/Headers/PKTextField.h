//
//  PKTextField.h
//  PayKeyUI
//
//  Created by Eden Landau on 21/02/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import "PKLegacyViewWithCursor.h"

@import KeyboardFramework;

@class PKTextField;

@protocol PKTextFieldDelegate <NSObject>

@optional
- (BOOL)textFieldShouldBeginEditing:(PKTextField *)textField;
- (void)textFieldDidBeginEditing:(PKTextField *)textField;
- (BOOL)textFieldShouldEndEditing:(PKTextField *)textField;
- (void)textFieldDidEndEditing:(PKTextField *)textField;
- (BOOL)textField:(PKTextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
- (BOOL)textFieldShouldClear:(PKTextField *)textField;
- (BOOL)textFieldShouldReturn:(PKTextField *)textField;
- (void)textDidChange:(PKTextField *)textField;
- (BOOL)textFieldShouldAddTextWhenNoSpace:(PKTextField *)textField;
- (KeyModelUI*)returnKeyModel;

@end

@interface PKTextField : PKLegacyViewWithCursor <PKTextDocumentProxy, UIReturnKeyProxy,PKEditTextProtocol>

//////// XIB
@property (nonatomic) IBInspectable UIColor *cursorColor;
@property (nonatomic) IBInspectable UIColor *textColor;
@property (nonatomic) IBInspectable UIColor *placeholderTextColor;
@property (nonatomic) IBInspectable CGFloat placeholderMinimumFontScale;
@property (nonatomic) IBInspectable NSString *placeholderText;
@property (nonatomic) IBInspectable UIImage *icon;
@property (nonatomic) IBInspectable UIImage *clearIcon;
@property (nonatomic) IBInspectable CGFloat letterSpacing;
@property (nonatomic) IBInspectable CGFloat textHorizontalMargin;
@property (nonatomic) IBInspectable NSString *initialValue;
@property (nonatomic) IBInspectable BOOL hidePlaceHolderWhenEditing;
@property (nonatomic) IBInspectable CGFloat iconWidth;
@property (nonatomic, getter=isSecureTextEntry) IBInspectable BOOL secureTextEntry;



////////////////////////////////////////////////////////////
@property (nonatomic) UIEdgeInsets clearButtonEdgeInest;
@property (nonatomic) CGFloat clearButtonRightPadding;
@property (nonatomic) UITextFieldViewMode clearButtonMode;
@property (nonatomic) NSTextAlignment textAlignment;
@property (nonatomic, strong) NSNumber *overrideCursorLabelAlignment;
@property (nonatomic) UIFont *font;
@property (nonatomic) UIFont *placeHolderFont;
@property (nonatomic) BOOL disabled;

@property (readonly) BOOL inputActive;
@property (readonly) NSString *text;
@property (nonatomic, weak) id<PKTextFieldDelegate>delegate;
@property (nonatomic, weak) id <PKInputTextStateObserver> inputTextStateObserver;

- (void)beginEditing;
- (BOOL)endEditing;
- (void)clear;
- (CGRect)labelContainerRect;
- (CGSize)textSize:(NSString*)text;
- (BOOL)hasMoreSpaceFor:(NSString*)text;
@end
