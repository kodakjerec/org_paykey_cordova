//
//  DataCollector.h
//  KeyboardApp
//
//  Created by Alex Kogan on 14/01/2017.
//  Copyright © 2017 Marat . All rights reserved.
//

#import "PKModel.h"
@class PKUICoreSettings;

@interface PKDataCollector : NSObject

@property (nonatomic, strong) id<PKModelDumpping> dumpDelegate;
@property (nonatomic, strong) id<PKModelRestoring> restoreDelegate;

- (void)registerModel:(PKModel *)model withTag:(NSString *)tag;
- (id)getModel:(NSString *)tag;
- (void)reset;
- (void)saveState;
- (void)restoreState;

@end
