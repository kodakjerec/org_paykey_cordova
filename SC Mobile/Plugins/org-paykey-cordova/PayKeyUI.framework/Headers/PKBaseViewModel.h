//
// Created by Alex Kogan on 02/04/2018.
// Copyright (c) 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKViewModel.h"
#import "PKViewObserver.h"
#import "PKViewPresentable.h"

@interface PKBaseViewModel : PKViewObserver <PKViewModel>

@property(nonatomic, assign) BOOL hidden;
@property(nonatomic, assign) CGFloat cornerRadius;
@property(nonatomic, assign) UIRectCorner corners;
@property(nonatomic, strong) UIColor *borderColor;
@property(nonatomic, assign) CGFloat borderWidth;
@property(nonatomic) CGFloat alpha;
@property(nonatomic, strong) UIColor *backgroundColor;
@property(nonatomic, assign) CGFloat width;
@property(nonatomic, assign) CGFloat height;


@end
