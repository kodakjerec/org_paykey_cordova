//
//  PincodeModel.h
//  KeyboardApp
//
//  Created by Alex Kogan on 14/01/2017.
//  Copyright © 2017 Marat . All rights reserved.
//

#import "CustomModel.h"

@interface PincodeModel : CustomModel

@property (nonatomic,strong)  NSString *pincodeInstructionsText;
@property (nonatomic,strong)  NSString *pincodeErrorText;
@property (nonatomic, strong) NSString *pincode;

@end
