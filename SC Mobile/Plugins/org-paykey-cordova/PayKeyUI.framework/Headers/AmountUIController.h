//
//  AmountUIController.h
//  KeyboardApp
//
//  Created by Alex Kogan on 14/01/2017.
//  Copyright © 2017 Marat . All rights reserved.
//

#import "CustomUIController.h"
#import "AmountModel.h"
#import "PKAmountOutput.h"
#import "PKCurrencyTextField.h"
#import "AmountView.h"


@interface AmountUIController : CustomUIController <AmountViewDelegate,PKCurrencyTextFieldDelegate>

@property (nonatomic, weak) id <PKAmountOutput> amountOutput;

- (PKCurrencyInputHandlerSettings*)inputHandlerSettings;
- (NSInteger)maxDigitCount;
- (AmountValidation)isValidAmount;
- (void)resetSavedAmount;

@property (readonly, nonatomic) double minAmount;
@property (readonly, nonatomic) double maxAmount;

-(id)nextResult;

@end
