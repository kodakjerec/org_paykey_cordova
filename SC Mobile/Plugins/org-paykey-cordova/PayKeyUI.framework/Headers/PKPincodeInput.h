//
//  PKPincodeInput.h
//  PayKeyUI
//
//  Created by Eran Israel on 27/11/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol PKPincodeInput <NSObject>

-(void)setLength:(NSInteger)length;
-(void)setDigitXibName:(NSString*) digitXibName;
-(void)setDigitSize:(CGSize) digitSize;
-(void)setBundle:(NSBundle*)bundle;
-(void)setPincode:(NSString *)password;
- (NSString *)pincode;

@end

NS_ASSUME_NONNULL_END
