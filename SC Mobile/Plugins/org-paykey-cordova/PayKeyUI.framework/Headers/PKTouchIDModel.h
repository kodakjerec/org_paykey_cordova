//
//  PKTouchIDModel.h
//  PayKeyUI
//
//  Created by Eran Israel on 25/10/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import "PKModel.h"

@interface PKTouchIDModel : PKModel

@property (nonatomic, strong) NSNumber* result;

- (instancetype)initWithResultStatus:(NSInteger)result;
- (BOOL)isValid;

@end
