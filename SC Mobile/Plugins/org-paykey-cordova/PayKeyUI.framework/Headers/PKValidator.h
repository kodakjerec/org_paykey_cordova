//
//  PKValidator.h
//  PayKeyUI
//
//  Created by German Velibekov on 20/11/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^PKValidateAction)(id validationResult);

@protocol PKValidator <NSObject>

- (id)validate:(id)object;

@end

NS_ASSUME_NONNULL_END
