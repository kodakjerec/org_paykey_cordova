//
//  PKConvinienceMacros.h
//  PayKeyUI
//
//  Created by Daniel M. on 11/10/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#ifndef PKConvinienceMacros_h
#define PKConvinienceMacros_h
#define WEAK_SELF(wSelfName) __weak typeof(self) wSelfName = self;
#define WEAK_SELF_DEFAULT WEAK_SELF(wSelf)

#endif /* PKConvinienceMacros_h */
