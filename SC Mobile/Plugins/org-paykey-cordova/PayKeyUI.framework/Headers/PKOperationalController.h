//
//  PKOperationalController.h
//  KeyboardApp
//
//  Created by Alex Kogan on 14/01/2017.
//  Copyright © 2017 Marat . All rights reserved.
//

#import "PKControl.h"

@interface PKOperationalController : NSObject <PKControl>

@property (nonatomic) BOOL skip;
@property (nonatomic, copy) OperationalBlock operationalBlock;

@end
