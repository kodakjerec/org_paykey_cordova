//
//  NSArray+NSNumber.h
//  SCTWClient
//
//  Created by German Velibekov on 06/09/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (NSNumber)

- (NSNumber *)max;
- (NSNumber *)min;

@end
