//
//  NSObject+Dictionary.h
//  PayKeyUI
//
//  Created by Alex Kogan on 25/09/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Dictionary)

- (NSDictionary *)dictionary;
- (NSDictionary *)dictionary:(NSArray*)excludeClasses;

- (id)fillWithDictionary:(NSDictionary*)dictionary;
- (id)fillWithDictionary:(NSDictionary*)dictionary excludeClasses:(NSArray*)excludeClasses;
@end
