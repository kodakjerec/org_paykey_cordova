//
//  PKScreenConfiguration.h
//  PayKeyUI
//
//  Created by German Velibekov on 27/02/2019.
//  Copyright © 2019 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKViewModelProvider.h"
#import "PKPaymentData.h"
#import "PKPaymentActions.h"

#define PK_DEFAULT_(view,viewmodel) ((viewmodel *)[self.vmProvider modelByView:view ofType:[viewmodel class]])
#define PK_VIEW_(view) PK_DEFAULT_(view,PKBaseViewModel)
#define PK_IMAGE_(view) PK_DEFAULT_(view,PKImageViewModel)
#define PK_TEXT_(view) PK_DEFAULT_(view,PKTextViewModel)
#define PK_INPUT_(view) PK_DEFAULT_(view,PKInputViewModel)
#define PK_CONTROL_(view) PK_DEFAULT_(view,PKControlViewModel)
#define PK_BUTTON_(view) PK_DEFAULT_(view,PKButtonViewModel)
#define PK_COLOR_(view) PK_DEFAULT_(view,FLENColorViewModel)
#define PK_AMOUNT_INPUT_(view) PK_DEFAULT_(view,PKAmountInputViewModel)
#define PK_PINCODE_INPUT_(view) PK_DEFAULT_(view,PKPincodeInputViewModel)
#define PK_CURRENCY_TEXT_(view) PK_DEFAULT_(view,PKCurrencyTextViewModel)
#define PK_CURSOR_VIEW_(view) PK_DEFAULT_(view,PKViewWithCursorModel)
#define PK_GRADIENT_VIEW_(view) PK_DEFAULT_(view,PKGradientViewModel)


NS_ASSUME_NONNULL_BEGIN

@interface PKScreenConfiguration : NSObject

@property (nonatomic, weak) id<PKViewModelProvider> vmProvider;

- (void)configureScreen;
- (NSString *)localizedString:(NSString *)key;

- (id<PKPaymentData>) paymentData;
- (id<PKPaymentActions>) paymentActions;

@end

NS_ASSUME_NONNULL_END
