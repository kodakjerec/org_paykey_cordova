//
//  PKLocalAuthenticatorOutcomes.h
//  PayKeyUI
//
//  Created by Eden Landau on 21/05/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#ifndef PKLocalAuthenticatorOutcomes_h
#define PKLocalAuthenticatorOutcomes_h

typedef NS_ENUM(NSInteger, PKLocalAuthenticatorOutcome) {
    PKLocalAuthenticatorOutcomeStart,
    PKLocalAuthenticatorOutcomeSuccess,
    PKLocalAuthenticatorOutcomeFallback,
    PKLocalAuthenticatorOutcomeCancel,
    PKLocalAuthenticatorOutcomeError
};

@class PKError;
typedef void (^AuthenticateTouchIDCompletionBlock)(__kindof PKError* _Nullable error, PKLocalAuthenticatorOutcome outcome); // OLD
typedef void (^BiometricAuthenticationCompletionBlock)(__kindof PKError* _Nullable error, PKLocalAuthenticatorOutcome outcome);

#endif /* PKLocalAuthenticatorOutcomes_h */
