//
//  PKContactsSortStrategy.h
//  PayKeyUI
//
//  Created by Marat  on 5/17/17.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKContactsProvider.h"

@interface PKSortStrategy : NSObject <PKContactProviderStrategy>

@property(nonatomic,strong) NSArray <NSSortDescriptor *> *sortDescriptors;

@end
