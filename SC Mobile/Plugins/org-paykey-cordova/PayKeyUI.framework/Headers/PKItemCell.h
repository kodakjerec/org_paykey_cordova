//
//  PKItemCell.h
//  PayKeyUI
//
//  Created by Eran Israel on 19/03/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PKItemCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) id<NSObject> delegate;
@property (assign, nonatomic) BOOL isSelected;

@end
