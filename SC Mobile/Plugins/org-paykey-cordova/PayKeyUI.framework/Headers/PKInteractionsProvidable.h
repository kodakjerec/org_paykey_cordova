//
//  PKInteractionsProvidable.h
//  PayKeyUI
//
//  Created by German Velibekov on 27/02/2019.
//  Copyright © 2019 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol PKInteractionsProvidable <NSObject>

- (NSArray *)interactions;

@end

NS_ASSUME_NONNULL_END
