//
//  PKTextStyle.h
//  MakePayment
//
//  Created by Jonathan Beyrak-Lev on 17/01/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIColor+RGB.h"

#define textStyle(aColor, aFont) [[PKTextStyle alloc] initWithColor:aColor font:aFont]

@interface PKTextStyle : NSObject

@property (strong, nonatomic) id <PKRGB> color;
@property (strong, nonatomic) UIFont *font;

- (instancetype)initWithColor:(id <PKRGB>)color font:(UIFont *)font;

@end
