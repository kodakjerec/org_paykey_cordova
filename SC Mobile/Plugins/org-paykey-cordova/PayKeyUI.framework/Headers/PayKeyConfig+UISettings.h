//
//  PayKeyConfig+UISettings.h
//  PayKeyUI
//
//  Created by Alex Kogan on 02/08/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

@import KeyboardFramework;

@interface PayKeyConfig (UISettings)

+ (PKLanguage *)paymentLanguage;
+ (NSString *)analyticsCustomerName;
+ (NSDictionary *)classesDictionary;
+ (BOOL)enableAnalytics;

@end
