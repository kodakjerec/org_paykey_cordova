//
//  PKNoContactsView.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 13/03/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PKNoContactsView : UIView

@property (strong, nonatomic) NSString *message;
@property (nonatomic) NSTextAlignment textAliment;

@end
