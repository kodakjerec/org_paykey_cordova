//
//  ContactCell.h
//  PayKeyUI
//
//  Created by Eden Landau on 29/01/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKTableViewCellPresenterSetterProtocol.h"


@interface ContactCell : UITableViewCell<PKTableViewCellPresenterSetterProtocol>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *contactPhoto;

- (void)prepareForDisplay;

@end
