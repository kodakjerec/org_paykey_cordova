//
//  PKViewObserver.h
//  PayKeyUI
//
//  Created by German Velibekov on 19/11/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

@import Foundation;
@import UIKit;

@interface PKViewObserver : NSObject

@property (nonatomic, weak) UIView* view;

+ (NSArray *)propertyList;

@end
