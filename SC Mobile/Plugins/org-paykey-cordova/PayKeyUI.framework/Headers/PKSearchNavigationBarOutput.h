//
//  PKSearchNavigationBarOutput.h
//  PayKeyUI
//
//  Created by Eran Israel on 05/12/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#ifndef PKSearchNavigationBarOutput_h
#define PKSearchNavigationBarOutput_h

#endif /* PKSearchNavigationBarOutput_h */

#import "PKScrollTextField.h"
#import "PKTextField.h"

__attribute__ ((deprecated))
@protocol PKSearchNavigationBarOutput <NSObject>
@property (nonatomic, weak) PKTextField *searchField;
@end

__attribute__ ((deprecated))
@protocol PKScrollSearchOutput <NSObject>
@property (nonatomic, weak) PKScrollTextField *scrollSearchField;
@end

@protocol PKGoodSearchOutput <NSObject>
@property (nonatomic, weak) PKGoodTextField *searchField;
@end
