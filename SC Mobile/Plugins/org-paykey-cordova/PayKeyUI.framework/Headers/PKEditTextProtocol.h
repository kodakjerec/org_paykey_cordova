//
//  PKEditTextProtocol.h
//  PayKeyUI
//
//  Created by Daniel M. on 02/06/2019.
//  Copyright © 2019 PayKey. All rights reserved.
//

#ifndef PKiMessageEditTextProtocol_h
#define PKiMessageEditTextProtocol_h
@import Foundation;
@protocol PKEditTextProtocol <NSObject>

@optional

-(void)beginEditing;
-(void)endEditing:(BOOL)force;
-(void)endEditing;

@end

#endif /* PKiMessageEditTextProtocol_h */
