//
//  PKInteractorBuilder+Controller.h
//  PayKeyUI
//
//  Created by German Velibekov on 26/12/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import "PKInteractorBuilder.h"
#import "PKProvider.h"

@interface PKInteractorBuilder (Controller)

-(id<PKViewInteractor>)notifyResult:(id)result;
-(id<PKViewInteractor>)notifyReturnResult;
-(id<PKViewInteractor>)setOutputText:(NSString*)text;
-(id<PKViewInteractor>)notifyAbort;
-(id<PKViewInteractor>)notifyResultWithProvider:(PKProvider)provider;
-(id<PKViewInteractor>)hideKeyboard;
-(id<PKViewInteractor>)showKeyboard;
-(id<PKViewInteractor>)reportEvent:(NSString*)eventName eventParams:(NSDictionary*)eventParams;

@end
