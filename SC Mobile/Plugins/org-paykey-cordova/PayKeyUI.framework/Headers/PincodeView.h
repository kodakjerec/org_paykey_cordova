//
//  PincodeView.h
//  PayKeyUI
//
//  Created by Eden Landau on 12/07/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import "PKScreen.h"
#import "PincodeViewDelegate.h"
#import "PKInlineError.h"
#import "PKPincodeOutput.h"
#import "PKTextDocumentProxyView.h"

@interface PincodeView : PKTextDocumentProxyView <PKPincodeOutput>

@property (weak, nonatomic) PKInlineError *error;
@property (weak, nonatomic) id<PincodeViewDelegate> delegate;
@property (assign, nonatomic) NSInteger pincodeLength;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;

// Set empty and full digit symbols. Specify nil fullSymbol to show user input instead.
@property (strong, nonatomic) NSString *emptySymbol;
@property (strong, nonatomic) NSString *fullSymbol;
@property (strong, nonatomic) NSString *pincodeInstructions;

// Internal
@property (nonatomic, assign) NSInteger digitMargin;
@property (nonatomic, strong) NSString *digitNibName;


- (void)configureSelf;

@end
