//
//  PKFlowManagerDelegate.h
//  PayKeyboard
//
//  Created by Eden Landau on 15/02/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PKUIController;
@protocol PKTextDocumentProxy;
@protocol PKEditTextProtocol;
@protocol PKOutputTextProtocol <NSObject>

- (void)setOutputText:(NSString *)text;
- (void)clearOutputText;

@end

@protocol PKFlowManagerDelegate <PKOutputTextProtocol>

- (void)controllerDidChangeTo:(PKUIController *)nextController;
- (void)returnToNormalKeyboard;
- (void)hideKeyboard;
- (void)showKeyboard;
- (void)advanceToNextInputMode;
- (void)openURL:(NSString *)url;
- (void)setDocumentTextProxy:(id<PKTextDocumentProxy>)textProxy;
- (BOOL)isExpanded;
- (void)updateReturnKey;
- (void)backClick;
- (PKUIController*)getCurrentController;
- (void)setNextEditField:(id<PKEditTextProtocol>)field;


@end
