//
//  PKContactsUIController.h
//  PayKeyUI
//
//  Created by Eden Landau on 29/01/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import "CustomUIController.h"
#import "ContactsModel.h"
#import "PKContactsOutput.h"
#import "PKContactsView.h"
#import "PKTextFieldUIController.h"

__attribute__ ((deprecated))
@interface PKContactsUIController : PKTextFieldUIController <ContactsViewDelegate>

@property (nonatomic, weak) id <PKContactsOutput> contactsOutput;
@property (nonatomic, weak) id <PKSearchNavigationBarOutput> searchFieldOutput;
@property (nonatomic, strong, readonly) NSString *filter;
@property (nonatomic, strong) NSArray *filteredContacts;
@property (nonatomic, strong) NSArray *contacts;

- (BOOL)filterMatchForContact:(PKContact*)contact filter:(NSString*)filter;
- (void)setSendToPhonePresenterIfNeeded:(NSString*)candidate;
- (BOOL)handleShouldChangeCharactersInRange:(NSString*)input;
- (void)setContentToView:(NSArray<PKContact *>*)contacts phone:(PKPhone*)phone;
- (void)setContanstSections:(NSArray *)contactsSections phone:(PKPhone *)phone;
- (NSArray*)getContacts;
- (NSArray<PKContact *>*)sortContactsByName:(NSArray<PKContact *>*)contacts;
- (void)reportTappedOnContact:(NSString*)contactName isRecentlyUsed:(BOOL)isRecentlyUsed;
- (NSArray*)filterResults:(NSString *)newFilter;

@end
