//
//  PKTypedError.h
//  PayKeyUICore
//
//  Created by Alex Kogan on 08/06/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKError.h"

__attribute__ ((deprecated))
@interface PKTypeError : PKError

@property (nonatomic, strong) NSString *type;

- (instancetype)initWithType:(NSString *)type title:(NSString *)title;

@end
