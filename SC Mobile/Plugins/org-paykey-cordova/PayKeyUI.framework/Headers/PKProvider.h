//
//  PKProvider.h
//  PayKeyUI
//
//  Created by German Velibekov on 28/01/2019.
//  Copyright © 2019 PayKey. All rights reserved.
//

#ifndef PKProvider_h
#define PKProvider_h

typedef id (^PKProvider)(void);

#endif /* PKProvider_h */
