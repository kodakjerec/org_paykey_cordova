//
//  PKContact.h
//  PayKeyUI
//
//  Created by Eden Landau on 02/02/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import <Foundation/Foundation.h>
@class UIImage;
@class PKPhoneNumber;

@protocol PKContactImageFetcher <NSObject>

- (UIImage *)fetchImage;
- (BOOL)hasImage;

@end

@protocol PKContact <NSObject>

@property (nonatomic, strong) NSString *givenName;
@property (nonatomic, strong) NSString *middleName;
@property (nonatomic, strong) NSString *familyName;
@property (nonatomic, strong) NSString *organization;
@property (nonatomic, strong) NSString *nickname;
@property (nonatomic, strong) NSString *phoneNumber __deprecated;
@property (nonatomic, strong) NSString *phoneNumberTag __deprecated;
@property (nonatomic, strong) NSArray<PKPhoneNumber*>* phones;
@property (nonatomic, strong) id <PKContactImageFetcher> imageFetcher;

@end

@interface PKContact : NSObject <PKContact, NSCoding>

@end
