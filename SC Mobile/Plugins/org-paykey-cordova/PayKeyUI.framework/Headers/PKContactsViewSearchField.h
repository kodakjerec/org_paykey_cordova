//
//  PKContactsViewSearchField.h
//  PayKeyUI
//
//  Created by Eran Israel on 15/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import "PKContactsView.h"
#import "PKContactsSearchFieldOutput.h"

__attribute__ ((deprecated))
@interface PKContactsViewSearchField : PKContactsView <PKContactsSearchFieldOutput>

@property (weak, nonatomic) IBOutlet PKTextField *searchField;

- (void)setupSearchTextField;

@end
