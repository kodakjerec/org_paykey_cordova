//
// Created by Alex Kogan on 02/04/2018.
// Copyright (c) 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKViewModel.h"
#import "PKViewObserver.h"
@import KeyboardFramework;

@interface PKInputViewModel : PKViewObserver <PKViewModel>

@property(nonatomic, strong) NSString *placeholder;
@property(nonatomic, strong) UIColor *placeholderColor;
@property(nonatomic, strong) UIFont *placeholderFont;
@property(nonatomic) UIReturnKeyType returnKeyType;
@property(nonatomic) UIKeyboardType keyboardType;
@property(nonatomic) UITextFieldViewMode clearButtonMode;
@property(nonatomic) UITextAutocapitalizationType autocapitalizationType;
@property (nonatomic) NSTextAlignment textAlignment;
@property (nonatomic) PKViewCursorMode cursorMode;
@property (nonatomic) BOOL secureTextEntry;
@property (nonatomic, weak) UIViewController *rootVC;
@property (nonatomic, weak) id<PKKeyboardLocalizationProvider> localizationProvider;



@end
