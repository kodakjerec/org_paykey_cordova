//
//  ActionableMessageView.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 07/03/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import "PKScreen.h"

@class ActionableMessageView;

@protocol ActionableMessageViewDelegate <NSObject>

- (void)didTapActionOnView:(ActionableMessageView *)view;

@end

@interface ActionableMessageView : PKScreen

@property (weak, nonatomic) IBOutlet UIImageView *messageImageView;
@property (weak, nonatomic) IBOutlet UILabel *messageTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageTextLabel;
@property (weak, nonatomic) id<ActionableMessageViewDelegate> actionDelegate;
@property (strong, nonatomic) NSString *actionTitle;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;

@end
