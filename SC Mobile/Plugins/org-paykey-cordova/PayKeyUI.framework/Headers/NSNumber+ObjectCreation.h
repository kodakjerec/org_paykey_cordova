//
//  NSNumber+ObjectCreation.h
//  MakePayment
//
//  Created by Jonathan Beyrak-Lev on 15/01/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+RGB.h"
#import "PKUISpacer.h"

@interface NSNumber (ObjectCreation) <PKRGB, PKUISpacer>

- (UIColor *)rgb;
- (UIColor *)rgbWithAlpha:(CGFloat)alpha;
- (UIFont *)relativeSizedFontWithName:(NSString *)name;
- (NSString *)moneyWithCurrency:(NSString *)currencySymbol;
- (id <PKUISpacer>)priority:(NSInteger)priority;
- (id <PKUISpacer>)mediumPriority;

@end
