//
//  PKModel.h
//  KeyboardApp
//
//  Created by Alex Kogan on 14/01/2017.
//  Copyright © 2017 Marat . All rights reserved.
//

#import "PKError.h"
@class PKModel;

typedef void (^OperationalBlock)(__kindof PKModel *);

@protocol PKModelRestoring <NSObject>

- (void)restore:(NSArray<PKModel *> *)models;

@end

@protocol PKModelDumpping <NSObject>

- (void)dump:(NSArray<PKModel *> *)models;

@end

@interface PKModel : NSObject

@property (nonatomic, strong) NSString *tag;
@property (nonatomic, strong) __kindof PKError *error;
@property (nonatomic, copy) NSAttributedString *attributeTitle;
@property (nonatomic, copy) NSString *title;

@end
