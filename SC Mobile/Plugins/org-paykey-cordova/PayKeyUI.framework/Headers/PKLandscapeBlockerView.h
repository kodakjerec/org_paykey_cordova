//
//  PKLandscapeBlockerView.h
//  PayKeyUICore
//
//  Created by Jonathan Beyrak-Lev on 30/04/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PKLandscapeBlockerView;

@protocol PKLandscapeBlockerViewDelegate <NSObject>

- (void)didTapAbortLandscapeBlockerView:(PKLandscapeBlockerView *)view;

@end

@interface PKLandscapeBlockerView : UIView

@property (strong, nonatomic) NSString *message;
@property (weak, nonatomic) id <PKLandscapeBlockerViewDelegate> delegate;
@property (nonatomic, assign) CGFloat extraHeight;
@property (nonatomic, strong) UIColor* abortBtnTintColor;

- (void)showMessage;
- (void)hideMessage;

@end
