//
//  CustomModel+localizeTransactionDescription.h
//  PayKeyUI
//
//  Created by ishay weinstock on 06/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CustomModel.h"

@interface NSString (Hyphenation)

- (NSString *)hyphenatedAsNeeded;

@end

@interface CustomModel (LocalizeTransactionDescription)

- (NSString *)payeeText;
- (NSString *)payeeTextOrNumber;
- (NSString *)amountText;
- (NSString *)currencyStyleAmount:(NSNumber*)amount;
- (NSNumber*)roundUpAmount:(NSNumber*)amount;
- (NSString *)spellOutStyleAmountForVoiceOver:(NSNumber*)amount;
- (NSString *)payeeFirstPhoneNumber;

@end
