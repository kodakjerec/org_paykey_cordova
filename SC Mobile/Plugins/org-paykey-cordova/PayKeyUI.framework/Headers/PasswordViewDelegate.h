//
//  PasswordViewDelegate.h
//  PayKeyUI
//
//  Created by Eran Israel on 17/10/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#ifndef PasswordViewDelegate_h
#define PasswordViewDelegate_h

#import "PKViewDelegate.h"

@protocol PasswordViewDelegate <PKViewDelegate>

@required
- (void)continueRunWithPassword:(NSString *)password;

@optional
- (void)showError;

@end

#endif /* PasswordViewDelegate_h */
