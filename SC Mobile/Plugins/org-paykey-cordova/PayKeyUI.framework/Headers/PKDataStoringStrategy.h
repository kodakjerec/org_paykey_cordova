//
//  PKDataStoringStrategy.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 25/10/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#ifndef PKDataStore_h
#define PKDataStore_h

@protocol PKDataStoringStrategy

- (id)objectForKey:(NSString *)key;
- (void)setObject:(id)obj forKey:(NSString *)key;

@end

#endif /* PKDataStore_h */
