//
//  PKListUIController.h
//  PayKeyUI
//
//  Created by Eran Israel on 19/03/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import "CustomUIController.h"
#import "PKListOutput.h"
#import "PKListViewDelegate.h"
#import "PKBaseListDataSource.h"

typedef NS_ENUM(NSInteger, PKFetchNextItems){
    PKFetchNextItemsResult = 000000001
};

@interface PKListUIController : CustomUIController <PKListViewDelegate, PKListDataSourceOutput>
@property (weak, nonatomic) id<PKListOutput> listOutput;
@property (strong, nonatomic) PKBaseListDataSource* dataSource;


- (void)scrollViewWillBeginDragging;

@end
