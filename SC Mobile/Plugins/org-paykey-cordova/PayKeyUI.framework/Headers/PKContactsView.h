//
//  PKContactsView.h
//  PayKeyUI
//
//  Created by Eden Landau on 30/01/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import "PKScreen.h"
#import "PKSearchNavigationBar.h"
#import "PKContactsOutput.h"
#import "PKContactPresenter.h"

@class PKTableViewCellPresenter;

@protocol ContactsViewDelegate <NSObject>

@optional
- (void)scrollViewWillBeginDragging;

@required
- (void)itemSelected:(id)item isRecentlyUsed:(BOOL)isRecentlyUsed;
- (void)gotNewFilter:(NSString *)filter;
@end

@interface PKContactsView : PKScreen <PKContactsOutput,  UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *contactsList;
@property (weak, nonatomic) id<ContactsViewDelegate> contactsDelegate;
@property (strong, nonatomic) NSString *noContactsMessage;
@property (nonatomic) NSTextAlignment noMessageTextAligment;
@property (nonatomic, strong) NSString *savedContactsHeaderTitle;
@property (nonatomic, strong) NSString *contactsHeaderTitle;
@property (nonatomic, strong) NSArray *headerTitles;
@property (nonatomic, strong) NSMutableArray<NSArray<PKTableViewCellPresenter *> *> *contactsCellPresentersSections;

- (Class)contactPresenterClass;
- (PKTableViewCellPresenter *)createPresenter:(NSBundle *)bundle item:(id)item;

@end
