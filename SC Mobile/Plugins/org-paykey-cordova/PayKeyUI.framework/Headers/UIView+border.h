//
//  UIView+border.h
//  SCHClient
//
//  Created by Eran Israel on 13/06/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (border)

- (CALayer *)addBottomBorderWithColor:(UIColor*)color andWidth:(CGFloat)borderWidth;
- (CALayer *)addTopBorderWithColor:(UIColor*)color andWidth:(CGFloat)borderWidth;
- (CALayer *)addLeftBorderWithColor:(UIColor*)color andWidth:(CGFloat)borderWidth;
- (CALayer *)addRightBorderWithColor:(UIColor *)color andWidth:(CGFloat)borderWidth;
@end
