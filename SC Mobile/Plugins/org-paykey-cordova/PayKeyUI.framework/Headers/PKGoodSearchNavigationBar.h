//
//  PKGoodSearchNavigationBar.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 12/03/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import "PKNavigationBar.h"
#import "PKSearchNavigationBarOutput.h"

@interface PKGoodSearchNavigationBar : PKNavigationBar <PKGoodSearchOutput>
@property (weak, nonatomic) IBOutlet PKGoodTextField *searchField;
@end
