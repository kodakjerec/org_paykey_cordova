//
//  ContactsModel.h
//  KeyboardApp
//
//  Created by Eden Landau on 14/01/2017.
//  Copyright © 2017 Marat . All rights reserved.
//

#import "CustomModel.h"

@class PKContactsArray;

@interface ContactsModel : CustomModel

@property (nonatomic, strong) NSString *alertMessage;//should be removed and move to view

@end
