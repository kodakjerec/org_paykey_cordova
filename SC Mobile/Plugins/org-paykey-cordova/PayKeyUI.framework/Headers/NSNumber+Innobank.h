//
//  NSNumber+Innobank.h
//  MakePayment
//
//  Created by Jonathan Beyrak-Lev on 15/01/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSNumber (Innobank)

- (UIFont *)regularFont;
- (UIFont *)mediumFont;

@end

