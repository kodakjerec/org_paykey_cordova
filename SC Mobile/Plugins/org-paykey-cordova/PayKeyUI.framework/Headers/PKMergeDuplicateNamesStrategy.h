//
//  PKMergeDuplicateNamesStrategy.h
//  PayKeyUI
//
//  Created by Eden Landau on 15/03/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKContactsProvider.h"

@interface PKMergeDuplicateNamesStrategy : NSObject <PKContactProviderStrategy>
@end
