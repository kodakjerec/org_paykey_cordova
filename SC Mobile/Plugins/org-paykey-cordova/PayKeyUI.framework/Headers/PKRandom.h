//
//  PKRandom.h
//  PayKeyUI
//
//  Created by German Velibekov on 13/02/2019.
//  Copyright © 2019 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PKRandom : NSObject

+ (NSInteger)randomFrom:(NSInteger)min to:(NSInteger)max;

@end

NS_ASSUME_NONNULL_END
