//
//  PKTextView.h
//  PayKeyUI
//
//  Created by Eden Landau on 21/02/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

@import KeyboardFramework;

@class PKTextView;

@protocol PKTextViewDelegate <NSObject>

@optional

- (void)textViewDidBeginEditing:(PKTextView *)textView;
- (void)textViewDidEndEditing:(PKTextView *)textView;
- (BOOL)textView:(PKTextView *)textView shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
- (void)textDidChange:(PKTextView *)textView;
- (KeyModelUI*)returnKeyModel;

@end

@interface PKTextView : PKViewWithCursor <PKTextDocumentProxy, UIReturnKeyProxy>

//////// XIB
@property (nonatomic) IBInspectable UIColor *cursorColor;
@property (nonatomic) IBInspectable UIColor *textColor;
@property (nonatomic) IBInspectable UIColor *placeholderTextColor;
@property (nonatomic) IBInspectable NSString *placeholderText;
@property (nonatomic) IBInspectable CGFloat letterSpacing;
@property (nonatomic) IBInspectable NSString *initialValue;
@property (nonatomic) IBInspectable BOOL hidePlaceHolderWhenEditing;

////////////////////////////////////////////////////////////
@property (nonatomic) NSTextAlignment textAlignment;
@property (nonatomic, strong) NSNumber *overrideCursorLabelAlignment;
@property (nonatomic) UIFont *font;
@property (nonatomic) UIFont *placeHolderFont;
@property (nonatomic) NSInteger numberOfLines;
@property (nonatomic) NSInteger maxLength;

@property (readonly) BOOL inputActive;
@property (readonly) NSString *text;
@property (nonatomic, weak) id<PKTextViewDelegate>delegate;
@property (nonatomic, weak) id <PKInputTextStateObserver> inputTextStateObserver;

- (void)beginEditing;
- (BOOL)endEditing;
- (void)clear;

@end
