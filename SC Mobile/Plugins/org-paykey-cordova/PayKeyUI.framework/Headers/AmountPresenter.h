//
//  AmountPresenter.h
//  PayKeyUI
//
//  Created by Marat  on 5/17/17.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface AmountPresenter : NSObject
-(NSAttributedString*)amountAttrStringWith:(UIFont*)font  sizeInPercentage:(CGFloat)sizePercent  text:(NSString*)text symbolRange:(NSRange)symbolRange top:(BOOL)top;
@end
