//
//  PKArrangedView.h
//  MakePayment
//
//  Created by Jonathan Beyrak-Lev on 10/01/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKUISpacer.h"

@interface PKArrangedView : UIView

@property UIStackView *stack;

+ (instancetype)horizontalBlankSlate;
- (instancetype)initWithAxis:(UILayoutConstraintAxis)axis;
- (void)setup;
- (__kindof UIView *)arrangeSubview:(UIView *)view;
- (__kindof UIView *)arrangeSubview:(UIView *)view paddedBefore:(id <PKUISpacer>)before after:(id <PKUISpacer>)after;
- (__kindof UIView *)arrangeCenteredSubview:(UIView *)view;
- (__kindof UIView *)containing:(UIView *)view;
- (__kindof UIView *)containing:(UIView *)view paddedBefore:(id <PKUISpacer>)before after:(id <PKUISpacer>)after;
- (__kindof UIView *)containingCentered:(UIView *)view;
- (instancetype)topLeadingAligned;
- (instancetype)bottomTrailingAligned;

@end
