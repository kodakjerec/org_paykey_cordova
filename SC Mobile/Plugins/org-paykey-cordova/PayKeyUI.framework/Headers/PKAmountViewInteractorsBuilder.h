//
//  PKAmountViewInteractions.h
//  PayKeyUI
//
//  Created by German Velibekov on 04/11/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import "PKViewInteractorsBuilder.h"
#import "PKValidateAmountInteractor.h"
#import "PKValidator.h"

NS_ASSUME_NONNULL_BEGIN

@interface PKAmountViewInteractorsBuilder: PKViewInteractorsBuilder

-(id<PKViewInteractor>)validateAmount:(id<PKValidator>)validator
                               action:(PKValidateAction)action;

-(id<PKViewInteractor>)saveAmount;

@end

NS_ASSUME_NONNULL_END
