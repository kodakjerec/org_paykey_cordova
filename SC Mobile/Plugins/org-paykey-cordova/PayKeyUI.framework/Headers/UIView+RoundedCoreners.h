//
//  UIView+RoundedCoreners.h
//  PayKeyUI
//
//  Created by Daniel M. on 04/02/2019.
//  Copyright © 2019 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (RoundedCoreners)

/**
 round corners when only one(or not all) side is to round

 @param corners coreners to round use '|' on cases you want to include
 @param radius radius to round in pts.
 @warning please note this method should be used only in layout subviews
 */
-(void)roundSpecificCorners:(UIRectCorner)corners withRadius:(CGFloat)radius;

@end

NS_ASSUME_NONNULL_END
