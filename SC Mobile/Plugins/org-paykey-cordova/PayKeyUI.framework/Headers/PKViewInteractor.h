//
// Created by Alex Kogan on 02/04/2018.
// Copyright (c) 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PKViewInteractorAggregator;
@class PKUIController;
@protocol PKPaymentActions;
@protocol PKPaymentData;

@protocol PKViewInteractor <NSObject>

-(void)interact:(PKViewInteractorAggregator*)model with:(PKUIController*)controller;

@optional

-(void)interact:(PKViewInteractorAggregator*)model withPaymentData:(id<PKPaymentData>)paymentData
                                                    paymentActions:(id<PKPaymentActions>)paymentActions;

@end
