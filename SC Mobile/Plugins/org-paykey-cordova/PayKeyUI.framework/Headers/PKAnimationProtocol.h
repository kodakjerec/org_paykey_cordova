//
//  PKAnimationProtocol.h
//  PayKeyUI
//
//  Created by ishay weinstock on 25/04/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#ifndef PKAnimationProtocol_h
#define PKAnimationProtocol_h

@protocol PKTransitionAnimation;

@protocol PKAnimationProtocol

- (id<PKTransitionAnimation>)animateWithPrevController:(PKUIController *)prevController nextController:(PKUIController *)nextController containerView:(UIView *)continerView;
@end

#endif /* PKAnimationProtocol_h */
