//
//  FLENColorViewModel.h
//  PayKeyUI
//
//  Created by Eran Israel on 19/03/2019.
//  Copyright © 2019 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FLENColorViewModel : NSObject <PKViewModel>

@property (nonatomic, strong) UIColor* color;

@end

NS_ASSUME_NONNULL_END
