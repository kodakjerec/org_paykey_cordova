//
//  ReferenceView.h
//  PayKeyUI
//
//  Created by Eden Landau on 01/02/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//


#import "ReferenceUIOutput.h"
#import "PKScreen.h"


@protocol ReferenceViewDelegate <NSObject>

@required
- (void)continueRunWithReference:(NSString *)reference;

@optional
@property (nonatomic, weak) UIButton *nextButton;

@end

@interface ReferenceView : PKScreen <ReferenceUIOutput>

@property (weak, nonatomic) IBOutlet PKScrollTextField *referenceTextField;
@property (nonatomic, weak) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) id<ReferenceViewDelegate> referenceViewDelegate;

- (void)setUpTextField;

@end
