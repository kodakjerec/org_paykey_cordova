//
//  PincodeViewDelegate.h
//  PayKeyUI
//
//  Created by Eden Landau on 11/07/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import "PKViewDelegate.h"

@protocol PincodeViewDelegate <PKViewDelegate>

@required
- (void)continueRunWithPincode:(NSString *)pincode;

@end
