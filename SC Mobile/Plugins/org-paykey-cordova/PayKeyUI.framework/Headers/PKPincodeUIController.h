//
//  PKPincodeUIController.h
//  KeyboardApp
//
//  Created by Alex Kogan on 14/01/2017.
//  Copyright © 2017 Marat . All rights reserved.
//

#import "CustomUIController.h"
#import "PKPincodeOutput.h"
#import "PKPincodeScreen.h"

@interface PKPincodeUIController : CustomUIController <PKPincodeScreenDelegate>

@property (nonatomic, weak) id <PKPincodeOutput> pincodeOutput;
@property (nonatomic ,readonly) NSInteger codeLength;

- (void)onFinish;

@end
