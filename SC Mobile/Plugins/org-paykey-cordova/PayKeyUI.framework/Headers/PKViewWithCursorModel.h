//
//  PKViewWithCursorModel.h
//  PayKeyUI
//
//  Created by Daniel M. on 17/01/2019.
//  Copyright © 2019 PayKey. All rights reserved.
//

@import KeyboardFramework;
#import "PKViewModel.h"
#import "PKViewObserver.h"
NS_ASSUME_NONNULL_BEGIN

@interface PKViewWithCursorModel : PKViewObserver <PKViewModel>

@property (strong,nonatomic)NSNumber* cursorMode;

@property (assign,nonatomic) UITextAutocorrectionType  autocorrectionType;

@end

NS_ASSUME_NONNULL_END
