//
//  PKCurrencyCodePresenter.h
//  PayKeyUI
//
//  Created by Marat  on 6/6/17.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKCurrencyPresenting.h"

@interface PKCurrencyCodePresenter : NSObject <PKCurrencyPresenting>

@end
