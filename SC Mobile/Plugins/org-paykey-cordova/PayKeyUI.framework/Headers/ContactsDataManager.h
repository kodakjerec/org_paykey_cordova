//
//  ContactsDataManager.h
//  PayKeyUI
//
//  Created by Marat  on 7/9/17.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContactsDataManager : NSObject

@property(nonatomic)NSUserDefaults *userDefaults;
@property(nonatomic)NSInteger limit;

+ (instancetype)sharedInstance;
- (void)addContact:(NSString *)phone;
- (void)clear;
- (BOOL)contains:(NSString *)phone;
- (NSUInteger)indexOfPhone:(NSString *)phone;
- (NSUInteger)numberOfRenectlySaved;

@end
