//
//  PKAmountValues.h
//  PayKeyUI
//
//  Created by Eran Israel on 18/03/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PKAmountValues : NSObject<NSCoding>

@property (nonatomic, readonly) NSNumber* minValue;
@property (nonatomic, readonly) NSNumber* maxValue;

+(instancetype)amountWithMinValue:(NSNumber *)minValue
                         maxValue:(NSNumber *)maxValue;

@end
