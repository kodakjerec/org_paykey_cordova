//
//  PKGradientView.h
//  PayKeyUI
//
//  Created by Daniel M. on 05/02/2019.
//  Copyright © 2019 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PKGradientView : UIView

@property (strong, nonatomic)CALayer* gradientLayer;

@end

NS_ASSUME_NONNULL_END
