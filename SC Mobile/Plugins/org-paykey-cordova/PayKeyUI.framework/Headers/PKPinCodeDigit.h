//
//  PKPinCodeDigit.h
//  PayKeyUI
//
//  Created by Eden Landau on 16/02/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PKPinCodeDigit : UIView

@property (nonatomic, assign, getter=isFull) BOOL full;
@property (nonatomic, assign) BOOL error;

@end
