//
//  NSObject+IgnoreNil.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 18/06/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^NonNilBlock)(id nonNil);

@interface NSObject(IgnoreNil)

- (void)ifNonNil:(NonNilBlock)nonNil;

@end
