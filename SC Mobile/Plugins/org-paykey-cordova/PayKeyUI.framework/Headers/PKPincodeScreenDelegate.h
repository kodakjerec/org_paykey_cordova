//
//  PKPincodeScreenDelegate.h
//  PayKeyUI
//
//  Created by Eran Israel on 27/11/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol PKPincodeScreenDelegate <NSObject>
@optional
- (void)inputChanged:(NSArray*)keys;
- (void)reachedToMaximumLength;
- (void)tappedInput;
@end

NS_ASSUME_NONNULL_END
