//
//  PKViewPresentable.h
//  PayKeyUI
//
//  Created by German Velibekov on 20/11/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

NS_ASSUME_NONNULL_BEGIN

@protocol PKViewPresentable <NSObject>

-(void)setHidden:(BOOL)hidden;
-(void)setCornerRadius:(CGFloat)radius;
-(void)setCorners:(UIRectCorner)corners withRadius:(CGFloat)radius;
-(void)setBorderColor:(UIColor *)borderColor;
-(void)setBorderWidth:(CGFloat)borderWidth;
-(void)setAlpha:(CGFloat)alpha;
-(void)setBackgroundColor:(UIColor *)backgroundColor;

@end

NS_ASSUME_NONNULL_END
