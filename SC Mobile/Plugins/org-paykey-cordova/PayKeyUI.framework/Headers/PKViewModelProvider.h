//
//  PKViewModelProvider.h
//  PayKeyUI
//
//  Created by German Velibekov on 26/02/2019.
//  Copyright © 2019 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol PKViewModelProvider <NSObject>

-(id<PKViewModel>)modelByView:(NSInteger)tag
                       ofType:(Class)modelType;
@end

NS_ASSUME_NONNULL_END
