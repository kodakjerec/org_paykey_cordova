//
// Created by Alex Kogan on 02/04/2018.
// Copyright (c) 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKViewModel.h"

@interface PKButtonViewModel : NSObject <PKViewModel>

-(void)setTitleColor:(UIColor *)color
            forState:(UIControlState)state;

-(void)setTitle:(NSString *)title
       forState:(UIControlState)state;

-(void)setAttributeTitle:(NSAttributedString *)attributeTitle
       forState:(UIControlState)state;

-(void)setBackgroundColor:(UIColor *)color
                 forState:(UIControlState)state;

-(void)setImage:(UIImage *)image
       forState:(UIControlState)state;

-(void)setImageEdgeInsets:(UIEdgeInsets)insets;
-(void)setTitleEdgeInsets:(UIEdgeInsets)insets;

@property (strong,nonatomic) NSNumber* selected;
@end
