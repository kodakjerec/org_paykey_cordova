//
//  PKFlowManagerFactory.h
//  PayKeyUICore
//
//  Created by Alex Kogan on 07/06/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import <Foundation/Foundation.h>
@class PKBaseFlowManager;
@protocol PKKeyboardEventsProtocol;

@interface PKFlowManagerFactory : NSObject

@property (nonatomic, weak) id <PKKeyboardEventsProtocol> eventsDelegate;

- (PKBaseFlowManager *)create;

@end
