//
//  PKNumberValidator.h
//  PayKeyUI
//
//  Created by German Velibekov on 04/11/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKValidator.h"
#import "PKAmountValues.h"

typedef NS_ENUM(NSUInteger, PKAmountValidation) {
    PKAmountValidationEmpty,
    PKAmountValidationValid,
    PKAmountValidationLessMin,
    PKAmountValidationAboveMax
};

typedef PKAmountValidation (^PKValidateAmountBlock)(NSNumber * _Nonnull);

NS_ASSUME_NONNULL_BEGIN

@interface PKAmountValidator : NSObject <PKValidator>

- (instancetype)initWithBlock:(PKValidateAmountBlock)block;

+ (instancetype)defaultValidator:(PKAmountValues*)amountValues;

@end

NS_ASSUME_NONNULL_END
