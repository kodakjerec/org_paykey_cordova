//
//  PKCurrencyInputHandlerSettings.h
//  PayKeyUI
//
//  Created by Eden Landau on 08/05/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKCurrencyPresenting.h"

typedef NS_ENUM(NSInteger, PKCurrencyGravity) {
    PKCurrencyGravityMostLeft,
    PKCurrencyGravityMostRight,
    PKCurrencyGravityLeft,
    PKCurrencyGravityRight,
};

typedef NS_ENUM(NSInteger, PKInputMode) {
    PKInputModeImplicitSeparator,
    PKInputModeManualSeparator
};

@interface PKCurrencyInputHandlerSettings : NSObject

@property (nonatomic) NSLocale *locale;
@property (nonatomic) PKInputMode inputMode;
@property (nonatomic) NSInteger maxDigits;
@property (nonatomic) BOOL enableDecimalPoint;
@property (nonatomic) BOOL emptyShouldHaveNoFractalDigits;
@property (nonatomic) NSInteger fractionDigits;
@property (nonatomic, assign) NSUInteger initialValueFractionDigits;

+(PKCurrencyInputHandlerSettings *)defaultInputHandlerSettings;


@end
