//
//  NSNumberFormatter+PKCurrency.h
//  PayKeyUI
//
//  Created by Marat  on 4/13/17.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumberFormatter (PKCurrency)
+(instancetype)formatterWithLocale:(NSLocale *)locale;
@end
