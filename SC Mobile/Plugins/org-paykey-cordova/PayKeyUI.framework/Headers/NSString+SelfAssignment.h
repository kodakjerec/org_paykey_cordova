//
//  NSString+SelfAssignment.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 20/06/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString(SelfAssignment)

- (void)assignTo:(NSString * __strong *)assignee;
- (void)assignToButton:(UIButton *)button;
- (void)assignToLabel:(UILabel *)label;

@end
