//
//  NSLayoutConstraint+DefaultOptions.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 14/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSLayoutConstraint(DefaultOptions)

+ (NSArray *)constraintsWithVisualFormat:(NSString *)format views:(NSDictionary *)views;

@end
