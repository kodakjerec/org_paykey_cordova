//
//  PKPincodeView.h
//  PayKeyUI
//
//  Created by Eran Israel on 27/11/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKPincodeOutput.h"
#import "PKPincodeScreenDelegate.h"

@class PKPinCodeDigit;

@interface PKFlowPincodeView : UIStackView <PKPincodeOutput>

@property (nonatomic, assign) NSInteger length;
@property (nonatomic, strong) NSString* digitXibName;
@property (nonatomic, assign) CGSize digitSize;
@property (nonatomic, strong) NSBundle* bundle;
@property (nonatomic, strong) NSString* pincode;

@property (nonatomic, strong) PKPinCodeDigit *currentDigitView;
@property (nonatomic, weak) id<PKPincodeScreenDelegate> pincodeScreenDelegate;

@end

