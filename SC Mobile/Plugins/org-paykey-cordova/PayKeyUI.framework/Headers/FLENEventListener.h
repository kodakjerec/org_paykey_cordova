//
//  PKEventListener.h
//  PayKeyUI
//
//  Created by German Velibekov on 23/12/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FLENEventListener


-(void)onEvent:(NSString *)eventName
          info:(NSDictionary * _Nullable)info;

@end

NS_ASSUME_NONNULL_END
