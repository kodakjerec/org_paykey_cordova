//
//  PKOperationItem.h
//  PayKeyUI
//
//  Created by ishay weinstock on 22/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import "PKFlowItem.h"

@interface PKOperationItem : PKFlowItem

- (instancetype)initWithFlowItemTag:(NSString *)tag operationalBlock:(OperationalBlock)operationalBlock;

@end
