//
//  PasswordView.h
//  PayKeyUI
//
//  Created by Eden Landau on 01/02/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//


#import "PasswordUIOutput.h"
#import "PKScreen.h"
#import "PasswordViewDelegate.h"

@class PKTextField;
@interface PasswordView : PKScreen <PasswordUIOutput>

@property (weak, nonatomic) IBOutlet PKTextField *passwordTextField;
@property (weak, nonatomic) id<PasswordViewDelegate> passwordViewDelegate;
@property (strong, nonatomic)NSString *nextButtonText;

- (void)setUpTextField;

@end
