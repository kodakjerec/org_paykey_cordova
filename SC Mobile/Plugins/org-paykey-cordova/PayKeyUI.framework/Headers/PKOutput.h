//
//  PKOutput.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 18/10/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#ifndef PKOutput_h
#define PKOutput_h

#import "CustomModel.h"

@protocol PKOutput

- (void)updateWithModel:(CustomModel *)model;

@end

#endif /* PKOutput_h */
