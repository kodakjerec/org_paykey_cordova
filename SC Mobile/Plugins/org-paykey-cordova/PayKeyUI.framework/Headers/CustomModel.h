//
//  CustomModel.h
//  KeyboardApp
//
//  Created by Alex Kogan on 14/01/2017.
//  Copyright © 2017 Marat . All rights reserved.
//

#import "PKViewSettings.h"
#import "PKPaymentData.h"
#import "PKPaymentActions.h"
#import "PKModel.h"
#import "PKUICoreLocalizationProvider.h"
#import "PKFlowLocalizationProvider.h"

@interface CustomModel : PKModel <PKFlowLocalizationProvider>

@property (weak, nonatomic) id<PKUICoreLocalizationProvider> localizationProvider;
@property (nonatomic, weak) id <PKPaymentData> paymentData;
@property (nonatomic, weak) id <PKPaymentActions> paymentActions;
@property (nonatomic, strong) PKViewSettings *viewSettings;
@property (nonatomic, strong) NSString *screenName;

@end
