//
//  PKContactsHeader.h
//  PayKeyUI
//
//  Created by Marat  on 7/9/17.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>


extern NSString * const PKContactsHeaderId;

@interface PKContactsHeader : UITableViewHeaderFooterView

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

@end
