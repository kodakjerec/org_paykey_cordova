//
//  PKValidateAmountInteractor.h
//  PayKeyUI
//
//  Created by German Velibekov on 04/11/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKViewInteractor.h"
#import "PKValidator.h"

NS_ASSUME_NONNULL_BEGIN

@interface PKValidateAmountInteractor : NSObject <PKViewInteractor>

+ (instancetype)forView:(NSInteger)viewTag
              validator:(id<PKValidator>)validator
                 action:(PKValidateAction)action;

@end

NS_ASSUME_NONNULL_END
