//
//  PKRoundedLabel.h
//  PayKeyUI
//
//  Created by Eran Israel on 14/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PKLabel : UILabel

@property (nonatomic) IBInspectable NSInteger radius;
@property (nonatomic) IBInspectable CGFloat topInset;
@property (nonatomic) IBInspectable CGFloat leftInset;
@property (nonatomic) IBInspectable CGFloat bottomInset;
@property (nonatomic) IBInspectable CGFloat rightInset;

@end
