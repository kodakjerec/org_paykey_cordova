//
//  PKMultipleContactsUIControlle.h
//  PayKeyUI
//
//  Created by Daniel M. on 09/12/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import "PKContactsUIController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PKMultipleContactsUIController : PKContactsUIController

- (BOOL)shouldAddSelectedPayee;
- (void)userAlreadySelectedContact:(PKContact*)contact;

@end

NS_ASSUME_NONNULL_END
