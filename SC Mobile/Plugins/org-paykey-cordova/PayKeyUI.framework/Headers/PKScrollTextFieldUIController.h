//
//  PKScrollTextFieldUIController.h
//  PayKeyUI
//
//  Created by Eran Israel on 26/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import "CustomUIController.h"
#import "PKScrollTextField.h"

@interface PKScrollTextFieldUIController : CustomUIController <PKScrollTextFieldDelegate>

@end
