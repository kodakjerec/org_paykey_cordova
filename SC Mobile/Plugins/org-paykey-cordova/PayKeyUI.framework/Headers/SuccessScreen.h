//
//  SuccessScreen.h
//  PayKeyUI
//
//  Created by Eran Israel on 20/03/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import "PKScreen.h"

@protocol SuccessViewDelegate <NSObject>
- (void)onClose;
@end

@interface SuccessScreen : PKScreen

@property (weak, nonatomic) id<SuccessViewDelegate> successViewDelegate;

@end
