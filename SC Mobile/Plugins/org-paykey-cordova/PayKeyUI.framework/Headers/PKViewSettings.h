//
//  PKViewSettings.h
//  PayKeyUI
//
//  Created by Eden Landau on 06/04/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIColor.h>
#import "PKNavigationBarSettings.h"

@interface PKViewSettings : NSObject

@property (nonatomic, strong) PKNavigationBarSettings *navigationBarSettings;
@property (nonatomic, strong) UIColor *backgroundColor;
@property (nonatomic, strong) NSLocale *locale;
@property (nonatomic, strong) NSNumber* height;

- (instancetype)initWithHeight:(NSNumber*)height;

@end
