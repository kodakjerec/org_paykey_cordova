//
//  UsernamePasswordUIController.h
//  KeyboardApp
//
//  Created by Alex Kogan on 14/01/2017.
//  Copyright © 2017 Marat . All rights reserved.
//

#import "CustomUIController.h"
#import "UsernamePasswordModel.h"
#import "PKTextFieldUIController.h"
#import "UsernamePasswordUIOutput.h"
#import "UsernamePasswordView.h"

@interface UsernamePasswordUIController : PKTextFieldUIController

@property (nonatomic, weak) id <UsernamePasswordUIOutput> usernamePasswordUIOutput;


@end
