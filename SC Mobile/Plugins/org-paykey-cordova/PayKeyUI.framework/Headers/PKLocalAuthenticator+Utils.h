//
//  PKLocalAuthenticator+Utils.h
//  PayKeyUI
//
//  Created by German Velibekov on 23/07/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import "PKLocalAuthenticator.h"

typedef NS_ENUM(NSInteger, PKBiometricType) {
    PKBiometricTypeNone,
    PKBiometricTypeTouchID,
    PKBiometricTypeFaceID,
};


@interface PKLocalAuthenticator (Utils)

+ (PKBiometricType)biometryType;

@end
