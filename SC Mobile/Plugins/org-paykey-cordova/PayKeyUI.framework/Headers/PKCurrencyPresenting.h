//
//  PKCurrencyPresenter.h
//  PayKeyUI
//
//  Created by Marat  on 6/6/17.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PKCurrencyPresenting <NSObject>

@property (nonatomic) NSLocale *locale;

- (instancetype)initWithLocale:(NSLocale *)locale;
- (NSString *)currencyPresentation;

@end
