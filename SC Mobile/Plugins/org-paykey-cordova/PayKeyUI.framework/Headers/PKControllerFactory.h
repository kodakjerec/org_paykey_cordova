//
//  PKControllerFactory.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 01/01/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#ifndef PKControllerFactory_h
#define PKControllerFactory_h

#import "PKControl.h"

@protocol PKControllerFactory

- (id <PKControl>)create:(NSInteger)tag;

@end

#endif /* PKControllerFactory_h */
