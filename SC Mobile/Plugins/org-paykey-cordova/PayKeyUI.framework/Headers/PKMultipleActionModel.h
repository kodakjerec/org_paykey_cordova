//
//  PKMultipleActionModel.h
//  PayKeyUI
//
//  Created by Eden Landau on 22/05/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import "CustomModel.h"

@interface PKMultipleActionModel : CustomModel

@property NSArray *actions;
@property NSNumber *actionsPerRow;
@property NSNumber *interitemSpacing;
@property NSNumber *rowsPerScreen;
@property NSNumber *marginRatio;
@property NSString *cellNibName;

@end
