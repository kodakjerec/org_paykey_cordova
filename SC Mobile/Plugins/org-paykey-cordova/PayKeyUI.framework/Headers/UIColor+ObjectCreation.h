//
//  UIColor+ObjectCreation.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 08/02/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKColorBlock.h"

@interface UIColor (ObjectCreation)

- (PKColorBlock *)blockOfWidth:(CGFloat)width;
- (PKColorBlock *)blockOfHeight:(CGFloat)height;
- (PKColorBlock *)blockOfSize:(CGSize)size;

@end
