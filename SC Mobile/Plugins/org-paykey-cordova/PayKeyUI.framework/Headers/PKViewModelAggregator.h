//
// Created by Alex Kogan on 02/04/2018.
// Copyright (c) 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKViewModel.h"
#import "PKViewModelProvider.h"

@interface PKViewModelAggregator : NSObject <PKViewModel, PKViewModelProvider>

-(id<PKViewModel>)modelByView:(NSInteger)tag ofType:(Class)modelType;

@end
