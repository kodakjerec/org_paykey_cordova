//
//  SuccessUIController.h
//  PayKeyUI
//
//  Created by Eran Israel on 08/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import "CustomUIController.h"
#import "SuccessScreen.h"

@interface SuccessUIController : CustomUIController <SuccessViewDelegate>

@end
