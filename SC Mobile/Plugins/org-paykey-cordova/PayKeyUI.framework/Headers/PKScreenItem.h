//
//  PKScreenItem.h
//  PayKeyUI
//
//  Created by ishay weinstock on 22/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import "PKFlowItem.h"
#import "PKUIController.h"

@class PKFlowAction;

@interface PKScreenItem : PKFlowItem

@property (nonatomic, assign) NSInteger flowItemType;
@property (nonatomic, copy) PKControllerResultHandler resultHandler;
@property (nonatomic, strong) PKFlowAction *left;
@property (nonatomic, strong) PKFlowAction *right;

- (instancetype)initWithFlowItemType:(NSInteger)itemType 
                             withTag:(NSString *)tag 
                              result:(PKControllerResultHandler)resultHandler 
                      leftNavigation:(PKFlowAction *)left 
                     rightNavigation:(PKFlowAction *)right;

@end
