//
//  PKNavigationBarSettings.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 09/03/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PKNavigationBarSettings : NSObject

@property (nonatomic) BOOL hidesLeftButton;
@property (nonatomic) BOOL hidesRightButton;
@property (nonatomic) BOOL showTitleIcon;

@property (strong, nonatomic) UIColor *backgroundColor;
@property (strong, nonatomic) UIColor *navigationButtonsColor;
@property (strong, nonatomic) NSString *leftButtonTitle;
@property (strong, nonatomic) UIImage *leftButtonImage;
@property (strong, nonatomic) NSString *rightButtonTitle;
@property (strong, nonatomic) UIImage *rightButtonImage;

- (instancetype)initWithHidesLeftButton:(BOOL)hidesLeftButton hidesRightButton:(BOOL)hidesRightButton;

@end
