//
//  PKCustomBehaviorStrategy.h
//  PayKeyUI
//
//  Created by Eden Landau on 16/03/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKContactsProvider.h"

@interface PKCustomBehaviorStrategy : NSObject <PKContactProviderStrategy>

@property (nonatomic, strong) PKFiltersContacts customCallback;

@end
