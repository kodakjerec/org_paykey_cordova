//
//  PKControllerInteractions.h
//  PayKeyUI
//
//  Created by German Velibekov on 18/11/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKViewInteractor.h"
#import "PKEmptyAction.h"

@interface PKControllerInteractorsBuilder : NSObject

-(id<PKViewInteractor>)onLeft:(PKEmptyAction)action;
-(id<PKViewInteractor>)onRight:(PKEmptyAction)action;
-(id<PKViewInteractor>)resultOnLoad:(id)result;

@end
