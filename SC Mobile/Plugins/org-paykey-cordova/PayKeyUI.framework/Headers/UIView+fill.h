//
//  UIView+container.h
//  PayKeyUI
//
//  Created by Eran Israel on 30/09/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (fill)
- (void)fillWithView:(UIView*)view andInsets:(UIEdgeInsets)insets;
@end

NS_ASSUME_NONNULL_END
