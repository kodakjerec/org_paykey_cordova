//
//  PKPincode.h
//  PayKeyUI
//
//  Created by Eran Israel on 07/03/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import "PKScreen.h"
#import "PKPincodeOutput.h"
#import "PKPincodeScreenDelegate.h"

@class PKPinCodeDigit;

@interface PKPincodeScreen : PKScreen <PKPincodeOutput>

@property (nonatomic, weak) id<PKPincodeScreenDelegate> pincodeScreenDelegate;
@property (nonatomic, strong) PKPinCodeDigit *currentDigitView;
@property (nonatomic, assign) NSInteger length;

- (void)pressBackword;
- (void)pressKey:(NSString*)key;
- (NSString*)digitXibName;
- (CGSize)digitSize;
- (CGSize)digitSizeFull;
-(void)clear;

@end
