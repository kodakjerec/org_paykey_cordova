//
//  PKConstraintModel.h
//  PayKeyUI
//
//  Created by Daniel M. on 23/12/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#ifndef PKConstraintModel_h
#define PKConstraintModel_h

@protocol PKConstraintModel <NSObject>

@property (assign,nonatomic) NSUInteger priority;

@property (assign, nonatomic)NSUInteger constant;

@end

#endif /* PKConstraintModel_h */
