//
//  PKSearchNavigationBar.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 12/03/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import "PKNavigationBar.h"
#import "PKSearchNavigationBarOutput.h"

__attribute__ ((deprecated))
@interface PKSearchNavigationBar : PKNavigationBar <PKSearchNavigationBarOutput>
@property (weak, nonatomic) IBOutlet PKTextField *searchField;
@end
