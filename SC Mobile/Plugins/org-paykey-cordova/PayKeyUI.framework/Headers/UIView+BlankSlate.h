//
//  UIView+BlankSlate.h
//  MakePayment
//
//  Created by Jonathan Beyrak-Lev on 09/01/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKUISpacer.h"
#import "PKArrangedView.h"

@interface UIView (BlankSlate)

+ (instancetype)blankSlate;
+ (instancetype)blankSlateFilledWith:(UIView *)subview;
+ (instancetype)filledWith:(UIView *)subview;
- (PKArrangedView *)above:(UIView *)footer;
- (PKArrangedView *)aboveSpace:(id <PKUISpacer>)space;
- (PKArrangedView *)aboveSpace:(id <PKUISpacer>)space and:(UIView *)footer;
- (PKArrangedView *)belowSpace:(id <PKUISpacer>)space;
- (PKArrangedView *)leading:(UIView *)rightward;
- (PKArrangedView *)leadingSpace:(id <PKUISpacer>)space;
- (PKArrangedView *)leadingSpace:(id <PKUISpacer>)space and:(UIView *)rightward;
- (PKArrangedView *)trailingSpace:(id <PKUISpacer>)space;
- (PKArrangedView *)withBackground:(UIColor *)background;

@end
