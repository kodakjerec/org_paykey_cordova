//
//  UIViewController+container.h
//  PayKeyUI
//
//  Created by Eran Israel on 15/05/2019.
//  Copyright © 2019 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (container)
- (void)layoutAnchorsViewControllerWithViewTo:(UIView*)view;
@end

NS_ASSUME_NONNULL_END
