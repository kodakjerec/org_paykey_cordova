//
//  PKPaymentData.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 25/10/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#ifndef PKPaymentData_h
#define PKPaymentData_h
#import <AvailabilityMacros.h>
#import <UIKit/UIKit.h>

@class PKContact;
@class PKSetupData;
@class PKFatalError;
@class PKError;
@class PKSuccessData;

@protocol PKPaymentData

- (PKSetupData*)setup;
- (NSNumber *)amount;
/**
 Identifies different kinds of availabilty parameters for list itmes (ex. cursor/pageNumber/offset)
 after making a request to get additional items inside PKListView;
 */

- (__kindof PKContact *)payee;
- (BOOL)shouldRestoreState;
- (BOOL)isPaymentInProgress;
- (NSString *)password;
- (NSString *)userName;
- (NSString *)pincode;
- (NSString *)otp;
- (NSNumber *)touchIDResult  __attribute__((deprecated("Replaced by -biometricResult:")));
- (NSNumber *)biometricResult;
- (__kindof PKError*)touchIdResultError  __attribute__((deprecated("Replaced by -biometricResultError:")));
- (__kindof PKError*)biometricResultError;
- (NSArray *)availablePayees;
- (NSString*)reference;
- (NSNumber*)flowManagerIndex;
- (PKError*)currentError;
- (NSNumber*)currentFlowType;
- (PKSuccessData *)successData;
- (NSArray*)selectedPayees;
- (NSString *)lastScreenTag;
// List Filter
- (NSString*)listItemsFilter;
- (id)nextListItemsClassifier;


@end

#endif /* PKPaymentData_h */
