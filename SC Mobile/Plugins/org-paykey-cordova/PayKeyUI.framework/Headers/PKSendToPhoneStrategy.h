//
//  PKSentToPhoneStrategy.h
//  PayKeyUI
//
//  Created by Alex Kogan on 13/06/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKContactsProvider.h"

@interface PKSendToPhoneStrategy : NSObject<PKContactProviderStrategy>

@property (nonatomic, weak) id <PKSendToPhoneProtocol> sendToPhoneDelegate;

@end
