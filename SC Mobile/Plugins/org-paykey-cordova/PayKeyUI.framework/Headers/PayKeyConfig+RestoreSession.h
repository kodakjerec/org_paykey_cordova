//
//  PayKeyConfig+RestoreSession.h
//  BTPNClient
//
//  Created by Eran Israel on 29/10/2018.
//  Copyright © 2018 ishay weinstock. All rights reserved.
//

#import <KeyboardFramework/KeyboardFramework.h>

NS_ASSUME_NONNULL_BEGIN

@interface PayKeyConfig (RestoreSession)
+ (NSNumber*)restoreSessionValidTime;

@end

NS_ASSUME_NONNULL_END
