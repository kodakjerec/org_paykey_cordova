//
//  NSString+ViewCreation.h
//  MakePayment
//
//  Created by Jonathan Beyrak-Lev on 11/01/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSAttributedString (ViewCreation)

- (UIButton *)button;
- (UILabel *)label;
- (NSAttributedString *)underlined;

@end

@interface NSString (ViewCreation)

- (UIImage *)image;
- (UIImageView *)imageView;
- (UIButton *)button;
- (UIButton *)imageButton;
- (UILabel *)label;
- (UIFont *)fontWithSize:(CGFloat)size;

@end
