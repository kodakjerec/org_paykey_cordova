//
// Created by Alex Kogan on 02/04/2018.
// Copyright (c) 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKViewModel.h"
#import "PKTextPresentable.h"
#import "PKViewObserver.h"

@interface PKTextViewModel : PKViewObserver <PKViewModel>

@property (nonatomic, strong) NSString *text;
@property (nonatomic, copy)   NSAttributedString *attributedText;
@property (nonatomic, assign) NSTextAlignment textAlignment;
@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, strong) UIFont *font;

@end
