//
//  PKListOutput.h
//  PayKeyUI
//
//  Created by German Velibekov on 17/10/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PKListOutput <NSObject>
- (void)didInitialize;
@optional
- (void)updateWithItems:(NSArray*)items andFilter:(NSString*)filter;
- (void)scrollToTop;
@end
