//
//  PKMultipleActionView.h
//  PayKeyUI
//
//  Created by Eden Landau on 22/05/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import "PKScreen.h"
#import "PKFlowAction.h"

@interface PKMultipleActionView : PKScreen <UICollectionViewDelegateFlowLayout, UICollectionViewDataSource>

@property (nonatomic) NSArray *actions;
@property (nonatomic) IBInspectable NSInteger actionsPerRow;
@property (nonatomic) IBInspectable CGFloat interitemSpacing;
@property (nonatomic) IBInspectable NSInteger rowsPerScreen;
@property (nonatomic) IBInspectable CGFloat marginRatio;
@property (nonatomic) IBInspectable NSString *cellNibName;

- (void)_initializeToDefaultDimensions;
- (PKFlowAction *)actionAtIndexPath:(NSIndexPath *)path;
@end

