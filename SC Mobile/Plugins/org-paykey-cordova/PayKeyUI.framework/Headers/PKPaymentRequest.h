//
//  PKPaymentRequest.h
//  PayKeyUI
//
//  Created by Eden Landau on 07/02/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKContact.h"

@interface PKPaymentRequest : NSObject

@property (nonatomic, strong) PKContact *contact;
@property (nonatomic, strong) NSNumber *amount;

-(instancetype)initWithContact:(PKContact *)contact amount:(NSNumber *)amount;

@end
