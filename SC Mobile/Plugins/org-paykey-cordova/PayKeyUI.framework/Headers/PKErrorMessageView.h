//
//  PKErrorMessageView.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 22/10/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import "ActionableMessageView.h"

@interface PKErrorMessageView : ActionableMessageView

@end
