//
//  PKError.h
//  PayKeyUI
//
//  Created by Eden Landau on 08/02/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PKError : NSObject <NSCoding>

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSString* code;
@property (nonatomic, strong) NSDictionary* userInfo;

- (instancetype)initWithTitle:(NSString *)title;
- (instancetype)initWithTitle:(NSString *)title message:(NSString*)message;
- (instancetype)initWithTitle:(NSString *)title message:(NSString*)message code:(NSString*)code;
- (instancetype)initWithTitle:(NSString *)title
                      message:(NSString*)message
                         code:(NSString*)code
                     userInfo:(NSDictionary*)userInfo;

@end
