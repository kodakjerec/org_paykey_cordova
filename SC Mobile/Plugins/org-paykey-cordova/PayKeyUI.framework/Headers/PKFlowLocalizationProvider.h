//
//  PKFlowLocalizationProvider.h
//  PayKeyUICore
//
//  Created by Alex Kogan on 13/06/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Foundation/Foundation.h>

@protocol PKFlowLocalizationProvider <NSObject>

- (NSString *)localizedString:(NSString *)key;

@end
