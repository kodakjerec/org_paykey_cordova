//
//  OTPView.h
//  PayKeyUI
//
//  Created by Eden Landau on 01/02/2017.
//  Copyright © 2017 alon muroch. All rights reserved.
//

#import "PKNavigationBar.h"
#import "OTPUIOutput.h"
#import "PKTextDocumentProxyView.h"
@class PKInlineError;

@protocol OTPViewDelegate <NSObject>

@required

- (void)setShouldResendOtp:(BOOL)shouldResendOtp;
- (void)continueRunWithOTP:(NSString *)pincode;

@end

@interface OTPView : PKTextDocumentProxyView <OTPUIOutput>

@property (weak, nonatomic) PKInlineError *error;

@property (weak, nonatomic) id<OTPViewDelegate> OTPDelegate;
@property (assign, nonatomic) NSInteger pincodeLength;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;

// Set empty and full digit symbols. Specify nil fullSymbol to show user input instead.
@property (strong, nonatomic) NSString *emptySymbol;
@property (strong, nonatomic) NSString *fullSymbol;

@end
