//
//  NSString+Attributes.h
//  MakePayment
//
//  Created by Jonathan Beyrak-Lev on 10/01/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+RGB.h"
#import "PKTextStyle.h"

@interface NSString (Attributes)

- (NSAttributedString *)attributed;
- (NSAttributedString *)withAttributes:(NSDictionary *)attributes;
- (NSAttributedString *)withColor:(id <PKRGB>)color;
- (NSAttributedString *)withFont:(UIFont *)font;
- (NSAttributedString *)withColor:(id <PKRGB>)color font:(UIFont *)font;
- (NSAttributedString *)styled:(PKTextStyle *)style;
- (NSAttributedString *)withStyle:(PKTextStyle *)style attributes:(NSDictionary *)attributes;;
- (NSAttributedString *)withColor:(id <PKRGB>)color font:(UIFont *)font attributes:(NSDictionary *)attributes;

@end
