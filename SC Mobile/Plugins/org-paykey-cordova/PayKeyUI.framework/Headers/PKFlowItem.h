//
//  PKFlowItem.h
//  PayKeyUI
//
//  Created by ishay weinstock on 22/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKControllerFactory.h"

@interface PKFlowItem : NSObject

@property (nonatomic,readonly) NSString *tag;
@property (nonatomic, strong) id <PKControllerFactory> factory;
@property (nonatomic, assign) BOOL cancelItem;

- (instancetype)initWithFlowItemTag:(NSString *)tag;
- (id <PKControl>)instantiate;

@end
