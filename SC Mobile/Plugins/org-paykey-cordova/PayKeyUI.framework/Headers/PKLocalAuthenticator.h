//
//  PKLocalAuthenticator.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 14/03/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <LocalAuthentication/LocalAuthentication.h>
#import "PKLocalAuthenticatorOutcomes.h"

@protocol PKOutputTextProtocol;

@interface PKLocalAuthenticator : NSObject

- (instancetype)initWithReason:(NSString*)reasonMessage;
- (instancetype)initWithReason:(NSString*)reasonMessage fallback:(NSString*)fallbackMessage;


- (void)authenticateTouchIDWithCompletion:(AuthenticateTouchIDCompletionBlock)completion __attribute__((deprecated("Replaced by -authenticateBiometricWithCompletion:")));

- (void)authenticateBiometricsWithCompletion:(BiometricAuthenticationCompletionBlock)completion;

- (void)authenticateDefaultDeviceWithCompletion:(BiometricAuthenticationCompletionBlock)completion;

@property (nonatomic, weak) id<PKOutputTextProtocol> ouputTextDelegate;

@end
