//
//  PKGradientViewModel.h
//  PayKeyUI
//
//  Created by Daniel M. on 05/02/2019.
//  Copyright © 2019 PayKey. All rights reserved.
//

#import "PKViewObserver.h"
#import "PKViewModel.h"


@interface PKGradientViewModel : PKViewObserver<PKViewModel>
@property (strong,nonatomic)CALayer* gradientLayer;
@end


