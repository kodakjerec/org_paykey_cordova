//
//  PKNetworkAccessible.h
//  PayKeyUI
//
//  Created by Alex Kogan on 17/10/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PKNetworkAccessible <NSObject>

-(BOOL)networkAccessible;

@end
