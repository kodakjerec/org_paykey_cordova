//
//  CustomFlowManagerFactory.h
//  PayKeyUI
//
//  Created by Alex Kogan on 07/06/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKDataCollector.h"
#import "PKFlowManagerFactory.h"
#import "PKFlowLocalizationProvider.h"

typedef NS_ENUM(NSInteger, PKFlowManagerType) {
    PKFlowManagerTypeSingle,
    PKFlowManagerTypeComposite
};

@class PKUIKeyboardConfig;
@class PKPaymentInteractor;
@class PKInMemoryDataStoringStrategy;
@protocol PayKeyDelegate;

@interface CustomFlowManagerFactory : PKFlowManagerFactory <PKFlowLocalizationProvider>

- (instancetype)initWithKeyboardConfig:(PKUIKeyboardConfig *)config
                  paykeyDelegate:(id <PayKeyDelegate>)paykeyDelegate
                      interactor:(PKPaymentInteractor *)interactor
                   dataCollector:(PKDataCollector *)dataCollector
                   dataStoringStrategy:(PKInMemoryDataStoringStrategy*)dataStoring;

@end
