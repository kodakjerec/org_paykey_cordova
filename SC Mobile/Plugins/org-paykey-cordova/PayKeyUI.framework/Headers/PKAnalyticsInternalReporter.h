//
//  PKAnalyticsReporter.h
//  PayKeyUI
//
//  Created by Marat  on 9/27/17.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKNetworkAccessible.h"
@import KeyboardFramework;

@protocol PKFilterEventStrategyProtocol <NSObject>

- (PKAnalyticsEvent*)executeStrategy:(PKAnalyticsEvent*)event;

@end


@interface PKAnalyticsInternalReporter : NSObject <PKKeyboardEventsProtocol>

-(instancetype)initWithCustomerName:(NSString*)customerName;

@property(nonatomic,weak)id<PKKeyboardEventsProtocol>clientDelegate;
@property(nonatomic,weak)id<PKNetworkAccessible>networkAccessibleDelegate;
@property(nonatomic,strong) id<PKFilterEventStrategyProtocol> filterStrategy;

- (void)reportEvent:(PKAnalyticsEvent *)event;

@end
