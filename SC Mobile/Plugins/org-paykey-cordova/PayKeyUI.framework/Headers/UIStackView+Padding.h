//
//  UIStackView+Padding.h
//  MakePayment
//
//  Created by Jonathan Beyrak-Lev on 09/01/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKUISpacer.h"

@interface UIStackView (Padding)

- (void)addEquallyPaddedArrangedSubview:(UIView *)view;
- (void)addArrangedSubview:(UIView *)view paddedBefore:(id <PKUISpacer>)before after:(id <PKUISpacer>)after;

@end
