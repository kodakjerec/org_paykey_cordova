//
//  PKCurrencyViewWithCursor.h
//  PayKeyUI
//
//  Created by ishay weinstock on 15/11/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKEditTextProtocol.h"
typedef NS_ENUM(NSInteger, PKCurrencyViewCursorMode) {
    PKCurrencyViewCursorModeNever,
    PKCurrencyViewCursorModeWhileEditing,
    PKCurrencyViewCursorModeAlways
};

typedef NS_ENUM(NSInteger, PKCurrencyCursorCalculationType) {
    PKCurrencyCursorCalculationTypeRect,
    PKCurrencyCursorCalculationTypeChar
};

@interface PKCurrencyViewWithCursor : UIView <UIKeyInput,PKEditTextProtocol>

@property (nonatomic, strong) UIColor *cursorColor;
@property (nonatomic) PKCurrencyViewCursorMode cursorMode;
@property (nonatomic) PKCurrencyCursorCalculationType cursorCalculationType;
@property (nonatomic, assign) BOOL disabled;
- (CGFloat)calcCursorXPosition;
- (CGRect)calcCursorRect;
- (CGFloat)cursorHeight;
- (void)updateCursorPosition;
- (void)setupCursor;

- (void)beginEditing;
- (BOOL)endEditing;

@end
