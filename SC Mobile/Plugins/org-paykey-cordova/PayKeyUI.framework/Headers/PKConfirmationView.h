//
//  PKConfirmationView.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 16/03/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import "ActionableMessageView.h"

@interface PKConfirmationView : ActionableMessageView

@end
