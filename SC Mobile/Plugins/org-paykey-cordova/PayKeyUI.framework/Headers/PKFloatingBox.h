//
//  PKFloatingBox.h
//  MakePayment
//
//  Created by Jonathan Beyrak-Lev on 09/01/2018.
//  Copyright © 2018 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, FloatingBoxShadowDirection) {
    FloatingBoxShadowDirectionUp,
    FloatingBoxShadowDirectionDown
};

@interface PKFloatingBox : UIView

@property (nonatomic) IBInspectable NSUInteger inspectableShadowDirection;
@property (nonatomic) IBInspectable NSUInteger disableShadow;

@property (nonatomic) FloatingBoxShadowDirection shadowDirection;


@end
