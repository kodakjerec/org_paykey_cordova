//
//  PKOptionsDelegate.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 26/07/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#ifndef PKOptionsDelegate_h
#define PKOptionsDelegate_h

#import "PKJuncturePresenter.h"

@protocol PKOptionsDelegate <NSObject>

- (void)optionDisplay:(id)optionDisplay didUpdateToPresenter:(PKJuncturePresenter *)update;

@end


#endif /* PKOptionsDelegate_h */
