//
//  PKJuncturePresenter.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 24/07/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PKJuncturePresenter : NSObject

@property (readonly) NSString *name;
@property (readonly) NSArray *branches;
@property (readonly) NSString *selected;

+ (instancetype)presenterWithName:(NSString *)name branches:(NSArray *)branches selected:(NSString *)selected;
- (instancetype)copyWithSelected:(NSString *)selected;

@end
