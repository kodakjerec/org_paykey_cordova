//
//  PKOptionsViewController.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 26/07/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKOptionsDelegate.h"

@class PKOptionsViewController;

@protocol PKOptionsViewControllerDelegate <PKOptionsDelegate>

- (NSArray *)options;

@end

@interface PKOptionsViewController : UIViewController

@property (nonatomic, weak) id<PKOptionsViewControllerDelegate> delegate;

- (void)reloadData;

@end
