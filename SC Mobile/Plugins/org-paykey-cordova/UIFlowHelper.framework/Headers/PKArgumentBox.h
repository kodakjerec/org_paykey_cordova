//
//  PKArgumentBox.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 29/06/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PKArgumentBox : NSObject

- (void)encodeIntoSignature:(char **)signature;
- (void)assignToInvocation:(NSInvocation *)invocation atIndex:(NSInteger)index;

@end

@interface PKObjectArgumentBox : PKArgumentBox

@property (readonly) id retainedValue;

@end

@protocol Boxable <NSObject>

- (PKArgumentBox *)box;

@end

@interface NSNumber(PrimitiveBoxing)

- (PKArgumentBox *)integerBox;

@end

@interface PKNilBox : PKArgumentBox <Boxable> @end
@interface NSObject(Boxable) <Boxable> @end


