//
//  PKStubCollection.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 24/07/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKJuncturePresenter.h"

#define Method(name) (NSStringFromSelector(@selector(name)))
#define Stub(name, ...) @{Method(name): @[__VA_ARGS__]}

@interface PKCallbackFlow : NSObject

- (NSArray *)presenters;
- (NSDictionary *)stubs;

- (void)setStub:(NSDictionary *)stub forBranch:(NSString *)branch atJuncture:(NSString *)juncture;
- (void)selectBranch:(NSString *)branch atJuncture:(NSString *)juncture;

@end
