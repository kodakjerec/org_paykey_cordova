//
//  PKCallbackDispatcher.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 29/06/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKArgumentBox.h"

@interface PKInvalidDispatchException : NSException @end
@interface PKArgumentMismatchException : NSException @end
@interface PKNoCallbackException : NSException @end
@interface PKCallbackMethodSignatureTemplate : NSObject @end

@interface PKCallbackDispatcher : NSObject

@property NSDictionary<NSString *, NSArray<PKArgumentBox *> *>*stubs;
@property PKCallbackMethodSignatureTemplate *signatureTemplate;

@end
