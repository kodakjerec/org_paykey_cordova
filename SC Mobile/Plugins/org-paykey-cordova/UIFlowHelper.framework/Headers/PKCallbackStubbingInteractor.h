//
//  PKCallbackStubbingInteractor.h
//  PayKeyUI
//
//  Created by Jonathan Beyrak-Lev on 27/07/2017.
//  Copyright © 2017 PayKey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKOptionsDelegate.h"
#import "PKOptionsViewController.h"
#import "PKCallbackFlow.h"

@interface PKCallbackStubbingInteractor : NSObject <PKOptionsDelegate, PKOptionsViewControllerDelegate>

@property (readonly) PKCallbackFlow *flow;
@property (weak, nonatomic) PKOptionsViewController *viewController;

- (void)createContactsJuncture;
- (void)createOptions;
- (void)saveSelection;

@end
