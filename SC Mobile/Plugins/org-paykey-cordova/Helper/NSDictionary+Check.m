//
//  NSDictionary+Check.m
//  javaTest
//
//  Created by Carslos Chen on 2019/7/25.
//  Copyright © 2019 Carslos Chen. All rights reserved.
//

#import "NSDictionary+Check.h"

@implementation NSDictionary (Check)

#pragma mark - safeAccess
/**
 *  判断字典对于给定Key是否有内容
 *
 *  @param key 给定的Key
 *
 *  @return YES 存在 NO 不存在
 */
- (BOOL)hasKey:(NSString *)key {
    return [self objectForKey:key] != nil;
}

/**
 *  获取字符串
 *
 *  @param key 给定的key
 *
 *  @return 获得的字符串
 */
- (NSString*)stringForKey:(id)key {
    id value = [self objectForKey:key];
    if (value == nil || value == [NSNull null])
    {
        return @"";
    }
    if ([value isKindOfClass:[NSString class]]) {
        return (NSString*)value;
    }
    if ([value isKindOfClass:[NSNumber class]]) {
        return [value stringValue];
    }
    
    return nil;
}

/**
 *  获取number
 *
 *  @param key 给定的key
 *
 *  @return 获得的number
 */
- (NSNumber*)numberForKey:(id)key {
    id value = [self objectForKey:key];
    if ([value isKindOfClass:[NSNumber class]]) {
        return (NSNumber*)value;
    }
    if ([value isKindOfClass:[NSString class]]) {
        NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
        [f setNumberStyle:NSNumberFormatterDecimalStyle];
        return [f numberFromString:(NSString*)value];
    }
    return nil;
}

/**
 *  获取array
 *
 *  @param key 给定的key
 *
 *  @return 获得的array
 */
- (NSArray*)arrayForKey:(id)key {
    id value = [self objectForKey:key];
    if (value == nil || value == [NSNull null])
    {
        return nil;
    }
    if ([value isKindOfClass:[NSArray class]])
    {
        return value;
    }
    return nil;
}

/**
 *  获取dictionary
 *
 *  @param key 给定的key
 *
 *  @return 获得的dictionary
 */
- (NSDictionary*)dictionaryForKey:(id)key {
    id value = [self objectForKey:key];
    if (value == nil || value == [NSNull null])
    {
        return nil;
    }
    if ([value isKindOfClass:[NSDictionary class]])
    {
        return value;
    }
    return nil;
}

/**
 *  获取bool
 *
 *  @param key 给定的key
 *
 *  @return 获得的bool
 */
- (BOOL)boolForKey:(id)key {
    id value = [self objectForKey:key];
    
    if (value == nil || value == [NSNull null])
    {
        return NO;
    }
    if ([value isKindOfClass:[NSNumber class]])
    {
        return [value boolValue];
    }
    if ([value isKindOfClass:[NSString class]])
    {
        return [value boolValue];
    }
    return NO;
}

/**
 *  获取initeger
 *
 *  @param key 给定的key
 *
 *  @return 获得的integer
 */
- (NSInteger)integerForKey:(id)key {
    id value = [self objectForKey:key];
    if (value == nil || value == [NSNull null])
    {
        return 0;
    }
    if ([value isKindOfClass:[NSString class]] || [value isKindOfClass:[NSNumber class]])
    {
        return [value integerValue];
    }
    return 0;
}

/**
 *  获取unsignedInteger
 *
 *  @param key 给定的key
 *
 *  @return 获得的unsignedInteger
 */
- (NSUInteger)unsignedIntegerForKey:(id)key {
    id value = [self objectForKey:key];
    if (value == nil || value == [NSNull null])
    {
        return 0;
    }
    if ([value isKindOfClass:[NSString class]] || [value isKindOfClass:[NSNumber class]])
    {
        return [value unsignedIntegerValue];
    }
    return 0;
}

@end
