//
//  Response.h
//  javaTest
//
//  Created by Carslos Chen on 2018/9/3.
//  Copyright © 2018年 Carslos Chen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Response : NSObject
@property (strong, nonatomic) NSString *signature;
@property (strong, nonatomic) NSString *code;
@property (strong, nonatomic) NSString *message;
@property (strong, nonatomic) NSString *jwt;
@property (strong, nonatomic) NSString *timeout;
@property (strong, nonatomic) NSDictionary *header;
@property (strong, nonatomic) NSDictionary *body;

- (instancetype)init;
+ (Response *)initWithDictionary:(NSDictionary *)dic;
@end
