//
//  Request.h
//  javaTest
//
//  Created by Carslos Chen on 2018/9/3.
//  Copyright © 2018年 Carslos Chen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ConcatKeyHelper.h"
#import "GMEllipticCurveCrypto.h"
#import "GMEllipticCurveCrypto+hash.h"
#import "IAGAesGcm.h"
#import "IAGCipheredData.h"
#import "NSData+IAGHexString.h"
#import "AFNetworking.h"
#import "MIHRSAPublicKey.h"
#import "MIHKeyPair.h"
#import "MIHPrivateKey.h"
#import "MIHRSAPrivateKey.h"
#import "NSData+MIHConversion.h"
#import "NSDictionary+Check.h"

@interface Request : NSObject
//AesGcm 加密 String
+ (NSString *)aesGcm:(NSString *)enString AesKey:(NSString *)aeskey;
// then AesGcm 解密
+ (NSString *)aesDenCode:(NSString *)denString Aeskey:(NSString *)aeskey;
+ (NSData*)hexToBytes:(NSString *)resource;
+ (NSString*)byteToHex:(NSData *)resource;
//Rsa 使用 privateKey 簽章
+ (NSString *)RsaSignature:(NSString *)pemString Message:(NSString *)message;
//計算出AES key
+ (NSString *)getAesKey:(NSData *)serverpublicKey GMElliptic:(GMEllipticCurveCrypto *)cryptob;
//getSignatureDic
+ (NSDictionary *)getSignatureDic:(NSArray *)bodyKeys BodyValue:(NSArray *)bodyValue JwtToken:(NSString *)jwtToken AesKey:(NSString *)akey GMElliptic:(GMEllipticCurveCrypto *)cryptob;
//組簽章的String
+ (NSString *)getJsonString:(NSDictionary *)requestDic BodyArray:(NSArray *)bodyDicArray;
//加密後的Body
+ (NSDictionary *)enCodeBody:(NSMutableDictionary *)dic AesKey:(NSString *)aeskey;
//組requestDic
+ (NSMutableDictionary *)requestDic:(NSString *)signature JwtToken:(NSString *)jwt;
//組devicInfo
+ (NSMutableDictionary *)devicInfo;
//組header
+ (NSDictionary *)header:(NSString *)jwt;
//dic 转json 如果用系统自带的会出现空格。
+ (NSString *)returnJSONStringWithDictionary:(NSDictionary *)dictionary KeyArray:(NSArray *)keys;
//呼叫API Post
+ (void)apiFetcherWithCompletion:(NSDictionary *)orderInfo Api:(NSString *)api Completion:(void(^)(NSDictionary *dict, NSError *error))completion;
//存檔-Pliststring
+ (void)writePlist:(NSString*)mydesc key:(NSString*)mykey;
//取得getPliststring
+ (NSString*)getPliststring:(NSString*)keyword;
//setJwt
+ (void)setJwt:(NSDictionary *)dict;
//getAESKEY
+ (NSString *)getAeskeyString;
+ (NSString *)getRsapublicKey;
+ (NSString *)getRsaprivateKey;

+ (Request *)getInstance;
+ (GMEllipticCurveCrypto *)cryptob;
@end
