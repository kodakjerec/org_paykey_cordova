package tw.com.scb.test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.ECPublicKey;
import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import com.scb.mobile.config.Application;
import com.scb.mobile.rest.security.to.DeviceInfo;
import com.scb.mobile.rest.security.to.GenJwtTokenReq;
import com.scb.mobile.rest.security.to.RestRequest;
import com.scb.mobile.rest.security.to.RestRequestHeader;
import com.scb.mobile.rest.security.to.RestResponse;
import com.scb.mobile.rest.security.to.test.Login_2_Request;
import com.scb.mobile.rest.security.to.test.TestReq;
import com.scb.mobile.service.JwtService;
import com.scb.mobile.utils.AESHelper;
import com.scb.mobile.utils.ECCHelper;
import com.scb.mobile.utils.HexHelper;
import com.scb.mobile.utils.JsonHelper;
import com.scb.mobile.utils.KeyStoreHelper;
import com.scb.mobile.utils.SecurityHelper;

@ActiveProfiles(value = { "jdbc", "TOMCATE" })
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { Application.class })
@WebAppConfiguration
public class ApiTest {

	@Autowired
	KeyStoreHelper keyStoreHelper;

	@Autowired
	ECCHelper eccHelper;

	@Autowired
	AESHelper aesHelper;

	@Autowired
	HexHelper hexHelper;


	// @Test
	public void sampleTest() {
		String data = "Data to encrypt";
		String dataHex = hexHelper.byte2Hex(data.getBytes());
		System.out.println("Message to encrypt:" + data);
		System.out.println("Message to encrypt in hex:" + dataHex);

		PublicKey serverPublicKey = keyStoreHelper.getServerEccPublicKey();

		System.out.println("AP server public key hash in hex:" + hexHelper.byte2Hex(eccHelper.loadPublicKey(serverPublicKey)));

		KeyPair clientKeyPair = eccHelper.generateEphemeralKey();
		PublicKey clientPublicKey = clientKeyPair.getPublic();
		PrivateKey clientPrivateKey = clientKeyPair.getPrivate();

		System.out.println("Client public key in hash in hex:" + hexHelper.byte2Hex(eccHelper.loadPublicKey(clientPublicKey)));
		System.out.println("Client private key in hash in hex:" + hexHelper.byte2Hex(eccHelper.loadPrivateKey(clientPrivateKey)));

		String clientEccPublicKey = "";
		String clientEccPrivateKey = "";
		String clientEccAesKey = "";
		String clientEccAesIv = "000000000000000000000000";
		clientEccPublicKey = hexHelper.byte2Hex(eccHelper.loadPublicKey(clientPublicKey));
		clientEccPrivateKey = hexHelper.byte2Hex(eccHelper.loadPrivateKey(clientPrivateKey));
		byte[] clientSharedSecret = eccHelper.getSharedSecret(clientPrivateKey, serverPublicKey);
		byte[] clientAESKey = eccHelper.concatKDF(clientSharedSecret, clientPublicKey);
		clientEccAesKey = hexHelper.byte2Hex(clientAESKey);

		byte[] signb = eccHelper.signature(clientEccPrivateKey, data);
		String signature = hexHelper.byte2Hex(signb);

		System.out.println("Shared Secret in hex:" + hexHelper.byte2Hex(clientSharedSecret));
		System.out.println("AES key in hex:" + hexHelper.byte2Hex(clientAESKey));
		System.out.println("Encrypted data in hex:" + aesHelper.encrypt(clientEccAesKey, clientEccAesIv, data));
		System.out.println("Encrypted data signature in hex:" + signature);
	}

}
