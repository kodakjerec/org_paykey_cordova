package com.webcomm.util;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.KeyAgreement;
import javax.crypto.SecretKey;

import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.interfaces.ECPrivateKey;
import org.bouncycastle.jce.interfaces.ECPublicKey;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.bouncycastle.jce.spec.ECPrivateKeySpec;
import org.bouncycastle.jce.spec.ECPublicKeySpec;
import org.bouncycastle.math.ec.ECPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.webcomm.exception.RestRuntimeException;

@Component
public class ECCHelper {

	private final String GENERATOR_SPEC = "secp256r1";
	private final String ECDH = "ECDH";
	private final String SHA256WITHECDSA = "SHA256withECDSA";
	private final String PROVIDER = "BC";

	@Autowired
	HexHelper hexHelper;

	@Autowired
	ConcatKeyHelper concatKeyHeler;

	public KeyPair generateEphemeralKey() {
		try {
			KeyPairGenerator generator = KeyPairGenerator.getInstance(ECDH, PROVIDER);
			generator.initialize(new ECGenParameterSpec(GENERATOR_SPEC), new SecureRandom());
			return generator.generateKeyPair();
		} catch (Exception e) {
			throw new RestRuntimeException(e);
		}
	}

	/**
	 * 計算共用的share AES Key
	 * 
	 * @param privateKeyStr
	 * @param publicKeyStr
	 * @return
	 */
	public String getShareEccAesKey(PrivateKey privateKey, String publicKeyStr) {
		ECPublicKey publicKey = loadPublicKeyFromByte(hexHelper.hex2Byte(publicKeyStr));
		byte[] sharedSecretByte = getSharedSecret(privateKey, publicKey);
		byte[] kdf = concatKDF(sharedSecretByte, publicKey);
		String keyStr = hexHelper.byte2Hex(kdf);
		return keyStr;
	}

	public byte[] getSharedSecret(PrivateKey privateKey, PublicKey publicKey) {
		try {
			KeyAgreement agreement = KeyAgreement.getInstance(ECDH, PROVIDER);
			agreement.init(privateKey);
			agreement.doPhase(publicKey, true);

			SecretKey secretKey = agreement.generateSecret(ECDH);
			byte[] bytes = secretKey.getEncoded();

			return bytes;

		} catch (Exception e) {
			throw new RestRuntimeException(e);
		}
	}

	public byte[] concatKDF(byte[] sharedSecret, PublicKey key) {
		try {
			byte[] keyByte = concatKeyHeler.concatKDF("SHA-256", sharedSecret, 256, ((char) 0x0D + "id-aes256-GCM").getBytes("ASCII"), "SCB".getBytes("ASCII"), loadPublicKey(key), null, null);
			return keyByte;
		} catch (Exception e) {
			throw new RestRuntimeException(e);
		}
	}

	public byte[] loadPublicKey(PublicKey pubKey) {
		ECPoint eCPoint = ((ECPublicKey) pubKey).getQ();
		return eCPoint.getEncoded(false);
	}

	public byte[] loadPrivateKey(PrivateKey privKey) {
		ECPrivateKey ecPrivateKey = (ECPrivateKey) privKey;
		return ecPrivateKey.getD().toByteArray();
	}

	public ECPublicKey convertIbm2BcPublicKey(byte[] encoded) {
//		ECParameterSpec parameterSpec = ECNamedCurveTable.getParameterSpec(GENERATOR_SPEC);
//		ECPublicKeySpec pubKey = new ECPublicKeySpec(parameterSpec.getCurve().createPoint(x, y), parameterSpec);
		X509EncodedKeySpec spec = new X509EncodedKeySpec(encoded);
		try {
			KeyFactory factory = KeyFactory.getInstance(ECDH, PROVIDER);
			return (ECPublicKey) factory.generatePublic(spec);
		} catch (Exception e) {
			throw new RestRuntimeException(e);
		}
	}

	public ECPublicKey loadPublicKeyFromByte(byte[] keyBytes) {
		ECParameterSpec parameterSpec = ECNamedCurveTable.getParameterSpec(GENERATOR_SPEC);
		ECPublicKeySpec pubKey = new ECPublicKeySpec(parameterSpec.getCurve().decodePoint(keyBytes), parameterSpec);

		try {
			KeyFactory factory = KeyFactory.getInstance(ECDH, PROVIDER);
			return (ECPublicKey) factory.generatePublic(pubKey);
		} catch (Exception e) {
			throw new RestRuntimeException(e);
		}
	}

	public ECPrivateKey convertIbm2BcPrivate(byte[] encoded) {
		PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(encoded);
		try {
			KeyFactory factory = KeyFactory.getInstance(ECDH, PROVIDER);
			return (ECPrivateKey) factory.generatePrivate(spec);
		} catch (Exception e) {
			throw new RestRuntimeException(e);
		}
	}

	public ECPrivateKey loadPrivateKeyFromByte(byte[] keyBytes) {
		BigInteger D = new BigInteger(keyBytes);
		ECParameterSpec parameterSpec = ECNamedCurveTable.getParameterSpec(GENERATOR_SPEC);
		ECPrivateKeySpec privKey = new ECPrivateKeySpec(D, parameterSpec);

		try {
			KeyFactory factory = KeyFactory.getInstance(ECDH, PROVIDER);
			return (ECPrivateKey) factory.generatePrivate(privKey);
		} catch (Exception e) {
			throw new RestRuntimeException(e);
		}
	}

	public PublicKey loadPublicKeyFormString(String key) {
		return loadPublicKeyFromByte(hexHelper.hex2Byte(key));
	}

	public PrivateKey loadPrivateKeyFormString(String key) {
		return loadPrivateKeyFromByte(hexHelper.hex2Byte(key));
	}

	/**
	 * 簽章
	 * 
	 * @param data
	 * @param privateKey
	 * @return
	 */
	public byte[] signature(String privateKey, String data) {
		return signature(loadPrivateKeyFormString(privateKey), data);
	}

	/**
	 * 簽章
	 * 
	 * @param data
	 * @param privateKey
	 * @return
	 */
	public byte[] signature(PrivateKey privateKey, String data) {
		byte[] sign;
		try {
			Signature signature = Signature.getInstance(SHA256WITHECDSA, PROVIDER);
			signature.initSign(privateKey);
			signature.update(data.getBytes(StandardCharsets.UTF_8));
			sign = signature.sign();
		} catch (Exception e) {
			throw new RestRuntimeException(e);
		}
		return sign;
	}

	/**
	 * 驗證簽章
	 * 
	 * @param publicKey
	 * @param signatureBytes
	 * @param dataBytes
	 * @return
	 */
	public boolean verifySignature(PublicKey publicKey, byte[] signatureBytes, String data) {
		boolean result;
		try {
			Signature signature = Signature.getInstance(SHA256WITHECDSA, PROVIDER);
			signature.initVerify(publicKey);
			signature.update(data.getBytes(StandardCharsets.UTF_8));
			result = signature.verify(signatureBytes);
		} catch (Exception e) {
			throw new RestRuntimeException(e);
		}
		return result;
	}

	/**
	 * 驗證簽章
	 * 
	 * @param key
	 * @param signatureBytes
	 * @param dataBytes
	 * @return
	 */
	public boolean verifySignature(String key, String signature, String data) {
		return verifySignature(loadPublicKeyFormString(key), hexHelper.hex2Byte(signature), data);
	}

}
