package com.webcomm.util;

import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.webcomm.exception.RestRuntimeException;

/**
 * 
 * AES_GCM 工具
 * 
 * @author jarvis
 *
 */
@Component
public class AESHelper {
	
	public static final String AES = "AES";
	public static final String ALGORITHM = "AES/GCM/NoPadding";
	public static final int IV_SIZE = 12;
	public static final int KEY_SIZE = 256;
	public static final String PROVIDER = "BC";

	@Autowired
	HexHelper hexHelper;

	public String generateEphemeralKey() {
		return hexHelper.byte2Hex(generateEphemeralKey(KEY_SIZE).getEncoded());
	}

	public SecretKey generateEphemeralKey(int keySize) {
		try {
			KeyGenerator generator = KeyGenerator.getInstance(AES);
			generator.init(keySize, new SecureRandom());
			return generator.generateKey();
		} catch (NoSuchAlgorithmException e) {
			throw new RestRuntimeException(e);
		}
	}
	
	public String generateRandomIV() {
		return hexHelper.byte2Hex(generateRandomIV(IV_SIZE));
	}

	public byte[] generateRandomIV(int ivSize) {
		final byte[] randomIV = new byte[ivSize];
		SecureRandom random = new SecureRandom();
		random.nextBytes(randomIV);
		return randomIV;
	}

	public SecretKey generateEphemeralKeyFromKey(String key) {
		return generateEphemeralKeyFromKey(hexHelper.hex2Byte(key));
	}

	public SecretKey generateEphemeralKeyFromKey(byte[] key) {
		SecretKeySpec mSecretKeySpec = new SecretKeySpec(key, AES);
		return mSecretKeySpec;
	}

	public String encrypt(String key, String iv, String input) {
		byte[] encryptByte = encrypt(hexHelper.hex2Byte(key), hexHelper.hex2Byte(iv), input);
		return hexHelper.byte2Hex(encryptByte);
	}

	public byte[] encrypt(byte[] key, byte[] iv, String plainText) {
		byte[] encryptText;
		try {
			SecretKey secretKey = new SecretKeySpec(key, AES);
			AlgorithmParameterSpec specIv = new GCMParameterSpec(128, iv);
			Cipher cipher = Cipher.getInstance(ALGORITHM, PROVIDER);
			cipher.init(Cipher.ENCRYPT_MODE, secretKey, specIv);
			encryptText = cipher.doFinal(plainText.getBytes(StandardCharsets.UTF_8));
		} catch (Exception e) {
			throw new RestRuntimeException(e);
		}
		return encryptText;
	}

	public String decrypt(String key, String iv, String input) {
		return decrypt(hexHelper.hex2Byte(key), hexHelper.hex2Byte(iv), hexHelper.hex2Byte(input));
	}

	public String decrypt(byte[] key, byte[] iv, byte[] input) {
		String decryptText;
		try {
			SecretKey secretKey = new SecretKeySpec(key, AES);
			AlgorithmParameterSpec specIv = new GCMParameterSpec(128, iv);
			Cipher cipher = Cipher.getInstance(ALGORITHM, PROVIDER);
			cipher.init(Cipher.DECRYPT_MODE, secretKey, specIv);
			decryptText = new String(cipher.doFinal(input), StandardCharsets.UTF_8);
		} catch (Exception e) {
			throw new RestRuntimeException(e);
		}
		return decryptText;
	}

}
