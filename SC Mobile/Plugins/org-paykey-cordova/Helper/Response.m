//
//  Response.m
//  javaTest
//
//  Created by Carslos Chen on 2018/9/3.
//  Copyright © 2018年 Carslos Chen. All rights reserved.
//

#import "Response.h"

@implementation Response

- (instancetype)init {
    self = [super init];
    if (self) {
        self.signature = @"";
        self.code = @"";
        self.message = @"";
        self.jwt = @"";
        self.timeout = @"";
        self.header = nil;
        self.body = nil;
    }
    return self;
}

+ (Response *)initWithDictionary:(NSDictionary *)dic {
    Response *tmp_Response = [[Response alloc] init];
    
    if ([[dic objectForKey:@"signature"] isKindOfClass:[NSString class]]) {
        tmp_Response.signature = [dic objectForKey:@"signature"];
    }
    
    if ([[dic objectForKey:@"header"] isKindOfClass:[NSString class]]) {
        tmp_Response.header = [dic objectForKey:@"header"];
    }
    
    if ([[dic objectForKey:@"body"] isKindOfClass:[NSString class]]) {
        tmp_Response.body = [dic objectForKey:@"body"];
    }
    
    if ([[tmp_Response.header objectForKey:@"code"] isKindOfClass:[NSString class]]) {
        tmp_Response.code = [tmp_Response.header objectForKey:@"code"];
    }
    
    if ([[tmp_Response.header objectForKey:@"message"] isKindOfClass:[NSString class]]) {
        tmp_Response.message = [tmp_Response.header objectForKey:@"message"];
    }
    
    if ([[tmp_Response.header objectForKey:@"jwt"] isKindOfClass:[NSString class]]) {
        tmp_Response.jwt = [tmp_Response.header objectForKey:@"jwt"];
    }
    
    if ([[tmp_Response.header objectForKey:@"timeout"] isKindOfClass:[NSString class]]) {
        tmp_Response.timeout = [tmp_Response.header objectForKey:@"timeout"];
    }
    
    return tmp_Response;
}
@end
