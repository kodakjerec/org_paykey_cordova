//
//  Request.m
//  javaTest
//
//  Created by Carslos Chen on 2018/9/3.
//  Copyright © 2018年 Carslos Chen. All rights reserved.
//

#import "Request.h"

// 新竹渣打(中台)api
//#define API_Server_HOSTNAME @"https://10.229.202.92:9444/mobilebank/"
// 台北mock api
#define API_Server_HOSTNAME @"https://thinkpower.azurewebsites.net/"
#define AES_GCM 0

static Request * sharedInstance;
static GMEllipticCurveCrypto *cryptob;
static NSString * aeskey;
static NSString * rsaprivateKey;
static NSString * rsapublicKey;
@implementation Request

-(instancetype)init {
    self = [super init];
    if (self) {
        [self loadSingleton];
    }
    return self;
}

-(void) loadSingleton {
    
}

+(Request *) getInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[Request alloc] init];
        cryptob = [GMEllipticCurveCrypto generateKeyPairForCurve:GMEllipticCurveSecp256r1];
        rsaprivateKey = @"MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAMQKGp7zSUktNOQkPdfcvJ3sWP7O46ENmSO4s0+iDdhHbR7klyt2oj0gXM1sAJj8ZBWFudu8GpiDKqXwN88IZemfT5c1LEQshTD1WHfZC6EY2vf9MGl5yGtV5WkU8vDJpg0STUJrCAcJ5Cp/m5qqJqgEM5Op6jHzwtzWV3syx+CBAgMBAAECgYEApSzqPzE3d3uqi+tpXB71oY5JcfB55PIjLPDrzFX7mlacP6JVKN7dVemVp9OvMTe/UE8LSXRVaFlkLsqXC07FJjhuwFXHPdnUf5sanLLdnzt3Mc8vMgUamGJl+er0wdzxM1kPTh0Tmq+DSlu5TlopAHd5IqF3DYiORIen3xIwp0ECQQDj6GFaXWzWAu5oUq6j1msTRV3mRZnx8Amxt1ssYM0+JLf6QYmpkGFqiQOhHkMgVUwRFqJC8A9EVR1eqabcBXbpAkEA3DQfLVr94vsIWL6+VrFcPJW9Xk28CNY6Xnvkin815o2Q0JUHIIIod1eVKCiYDUzZAYAsW0gefJ49sJ4YiRJN2QJAKuxeQX2s/NWKfz1rRNIiUnvTBoZ/SvCxcrYcxsvoe9bAi7KCMdxObJknhNXFQLav39wKbV73ESCSqnx7P58L2QJABmhR2+0A5EDvvj1WpokkqPKmfv7+ELfDHQq33LvU4q+N3jPn8C85ZDedNHzx57kru1pyb/mKQZANNX10M1DgCQJBAMKn0lExQH2GrkjeWgGVpPZkp0YC+ztNjaUMJmY5g0INUlDgqTWFNftxe8ROvt7JtUvlgtKCXdXQrKaEnpebeUQ=";
        
        rsapublicKey = @"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDEChqe80lJLTTkJD3X3Lyd7Fj+zuOhDZkjuLNPog3YR20e5JcrdqI9IFzNbACY/GQVhbnbvBqYgyql8DfPCGXpn0+XNSxELIUw9Vh32QuhGNr3/TBpechrVeVpFPLwyaYNEk1CawgHCeQqf5uaqiaoBDOTqeox88Lc1ld7MsfggQIDAQAB";
        aeskey = @"";
    });
    return sharedInstance;
}

+ (GMEllipticCurveCrypto *)cryptob{
    return cryptob;
}

+ (NSString *)getAeskeyString{
    return [Request getPliststring:@"Aeskey"];;
}

+ (NSString *)getRsapublicKey{
    return  rsapublicKey;
}

+ (NSString *)getRsaprivateKey{
    return  rsaprivateKey;
}

//Rsa 使用 privateKey 簽章
+ (NSString *)RsaSignature:(NSString *)pemString Message:(NSString *)message{
    
    //    NSString * publicKeyString = @"MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAJma7iWHjLCpeiHHhkwf/IGVtJvrHVW/"
    //    "F+te83hB85YiCuAOqvaG7Isy4dYbwBZKkuIq9x+FVZ+6SrsaRQuaFSkCAwEAAQ==";
    //    NSData * pub = [NSData MIH_dataByBase64DecodingString:publicKeyString];
    //    MIHRSAPublicKey * publicKey = [[MIHRSAPublicKey alloc] initWithData:pub];
    
    if (![pemString hasPrefix:@"-----BEGIN RSA PRIVATE KEY-----"]) {
        pemString = [@"-----BEGIN RSA PRIVATE KEY-----\n" stringByAppendingString:pemString];
        pemString = [pemString stringByAppendingString:@"\n-----END RSA PRIVATE KEY-----"];
    }
    NSData *pem = [pemString dataUsingEncoding:NSUTF8StringEncoding];
    MIHRSAPrivateKey * privateKey = [[MIHRSAPrivateKey alloc] initWithData:pem];
    NSData * messageData = [message dataUsingEncoding:NSUTF8StringEncoding];
    NSError *signingError = nil;
    NSData *signatureData = [privateKey signWithSHA256:messageData error:&signingError];
    
    /*驗證
     //BOOL isVerified = [publicKey verifySignatureWithSHA256:signatureData message:messageData];
     //NSLog(@"%d",isVerified);
     */
    
    return [Request byteToHex:signatureData];
}

//計算出AES key
+ (NSString *)getAesKey:(NSData *)serverpublicKey GMElliptic:(GMEllipticCurveCrypto *)cryptob{
    //使用client private key和server public key透過KeyAgreement來計算出shared secret(crypto.publicKey 要換 server public key)
    NSData *aliceSharedSecret = [cryptob sharedSecretForPublicKey:serverpublicKey];
    //未壓縮的clientpublicKey
    NSData * decompressClientpublicKey = [cryptob decompressPublicKey:cryptob.publicKey];
    NSString *base64String = [aliceSharedSecret base64EncodedStringWithOptions:0];
    NSString *base64StringB = [decompressClientpublicKey base64EncodedStringWithOptions:0];
    //跑java kdf
    KdfjavaConcatKeyHelper * test = [[KdfjavaConcatKeyHelper alloc]init];
    //計算出的AES key in hex
    //NSLog(@"計算出的AES key in hex:%@",[test concatKDFWithNSString:base64String withNSString:base64StringB]);
    return [test concatKDFWithNSString:base64String withNSString:base64StringB];
}

//呼叫API Post
+ (void)apiFetcherWithCompletion:(NSDictionary *)orderInfo Api:(NSString *)api Completion:(void(^)(NSDictionary *dict, NSError *error))completion
{
    NSString * urlString = [NSString stringWithFormat:@"%@%@",API_Server_HOSTNAME,api];
    
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy defaultPolicy];
    securityPolicy.validatesDomainName = NO;
    securityPolicy.allowInvalidCertificates = YES;
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.securityPolicy = securityPolicy;
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:urlString parameters:orderInfo error:nil];
    [request setTimeoutInterval:30];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSURLSessionDataTask *task = [manager dataTaskWithRequest:request uploadProgress:nil downloadProgress:nil completionHandler:^(NSURLResponse * _Nonnull response, id _Nullable responseObject, NSError * _Nullable error) {
        
        if (!error) {
            NSLog(@"Call Api:%@ \n Request:%@ \n Response = %@",api,orderInfo,[NSString stringWithUTF8String:[responseObject bytes]]);
            NSDictionary *jsonObject=[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
            
            [Request setJwt:jsonObject];
            completion(jsonObject, nil);
        } else {
            NSLog(@"Call Api:%@ \n Request:%@ \n Response error = %@",api,orderInfo,error);
            completion(nil, error); //Http Or Server Error
        }
    }];
    [task resume];
}

+ (void)setJwt:(NSDictionary *)dict{
    if (dict[@"header"][@"jwt"]) {
        NSString * jwt = dict[@"header"][@"jwt"];
        [Request writePlist:jwt key:@"jwtToken"];
    }
}

//dic 转json 如果用系统自带的会出现空格。
+ (NSString *)returnJSONStringWithDictionary:(NSDictionary *)dictionary KeyArray:(NSArray *)keys{
    
    NSString *jsonStr = @"{";
    
    for (NSString * key in keys) {
        jsonStr = [NSString stringWithFormat:@"%@\"%@\":\"%@\",",jsonStr,key,[dictionary objectForKey:key]];
    }
    jsonStr = [NSString stringWithFormat:@"%@%@",[jsonStr substringWithRange:NSMakeRange(0, jsonStr.length-1)],@"}"];
    
    if (!keys) {
        return @"{}";
    }
    return jsonStr;
}

//getSignatureDic
+ (NSDictionary *)getSignatureDic:(NSArray *)bodyKeys BodyValue:(NSArray *)bodyValue JwtToken:(NSString *)jwtToken AesKey:(NSString *)akey GMElliptic:(GMEllipticCurveCrypto *)cryptob{
    NSMutableDictionary *body = [[NSMutableDictionary alloc] init];
    for (int i = 0; i < bodyKeys.count; i++) {
        [body setValue:bodyValue[i] forKey:bodyKeys[i]];
    }
    //先取到沒有signature的 jsonString
    NSMutableDictionary * requestDic = [Request requestDic:nil JwtToken:jwtToken];
    if (body == nil || [body allKeys].count == 0) {
        [requestDic setValue:@{} forKey:@"body"];
    }else{
        [requestDic setValue:body forKey:@"body"];
    }
    
    //組簽章的String 因為有順序問題不可以直接dic轉jsonString
    NSString * jsonString = [Request getJsonString:requestDic BodyArray:bodyKeys];
    NSData *signatureString =[jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    //明文資料透過SHA256WithECDSA簽章 in hex:
    NSData *signature = [cryptob hashSHA256AndSignDataEncoded:signatureString];
    //取到簽名後
    NSString * Sigstring = [Request byteToHex:signature];
    NSMutableDictionary * requestDicNew = [Request requestDic:Sigstring JwtToken:jwtToken];
    [requestDicNew setValue:[Request enCodeBody:body AesKey:akey] forKey:@"body"];
    return requestDicNew;
}

//組簽章的String
+ (NSString *)getJsonString:(NSDictionary *)requestDic BodyArray:(NSArray *)bodyDicArray{
    
    NSString * jwt = requestDic[@"header"][@"jwt"];
    NSDictionary * deviceInfo = requestDic[@"header"][@"deviceInfo"];
    NSArray * deviceArray = @[@"osVersion",@"os",@"deviceModel",@"version",@"udid"];
    NSDictionary * bodyDic = requestDic[@"body"];
    
    NSString * genJsonString = [NSString stringWithFormat:@"{\"header\":{\"jwt\":\"%@\",\"deviceInfo\":%@,\"locale\":\"%@\"},\"body\":%@}",jwt,[Request returnJSONStringWithDictionary:deviceInfo KeyArray:deviceArray],requestDic[@"header"][@"locale"],[Request returnJSONStringWithDictionary:bodyDic KeyArray:bodyDicArray]];
    
    return genJsonString;
}

//加密後的Body
+ (NSDictionary *)enCodeBody:(NSMutableDictionary *)dic AesKey:(NSString *)aeskey{
    
    NSArray * keys = [dic allKeys];
    for (NSString * key in keys) {
        if ([key isEqualToString:@"action"] || [key isEqualToString:@"validationType"] || [key isEqualToString:@"id"] || [key isEqualToString:@"txType"] || [key isEqualToString:@"functionId"] || [key isEqualToString:@"fundId"] || [key isEqualToString:@"pageTitle"] || [key isEqualToString:@"pageUrl"] || [key isEqualToString:@"amountType"] || [key isEqualToString:@"coverType"] || [key isEqualToString:@"fixAmt"] || [key isEqualToString:@"greetings"] || [key isEqualToString:@"maxAmt"] || [key isEqualToString:@"minAmt"] || [key isEqualToString:@"totalNum"] || [key isEqualToString:@"uploadCover"] || [key isEqualToString:@"inAcctType"] || [key isEqualToString:@"pageIndex"] || [key isEqualToString:@"type"] || [key isEqualToString:@"clientEccPublicKey"] || [key isEqualToString:@"signCode"] || [key isEqualToString:@"defaultCoverList"]) {
            [dic setValue:[dic objectForKey:key] forKey:key];
        }else{
            if (AES_GCM) {
                [dic setValue:[Request aesGcm:[dic objectForKey:key] AesKey:aeskey] forKey:key];
            }else{
                [dic setValue:[dic objectForKey:key] forKey:key];
            }
            
        }
    }
    return dic;
}

//組requestDic
+ (NSMutableDictionary *)requestDic:(NSString *)signature JwtToken:(NSString *)jwt{
    NSMutableDictionary *requestDic = [[NSMutableDictionary alloc] init];
    if (signature.length != 0) {
        [requestDic setValue:signature forKey:@"signature"];
    }
    [requestDic setValue:[Request header:jwt] forKey:@"header"];
    
    return requestDic;
}

//組header
+ (NSDictionary *)header:(NSString *)jwt{
    NSMutableDictionary *header = [[NSMutableDictionary alloc] init];
    if (jwt.length == 0) {
        jwt = @"";
    }
    [header setValue:jwt forKey:@"jwt"];
    [header setValue:[Request devicInfo] forKey:@"deviceInfo"];
    [header setValue:@"zh_TW" forKey:@"locale"];
    return header;
}

//組devicInfo
+ (NSMutableDictionary *)devicInfo{
    
    NSMutableDictionary *deviceInfo = [[NSMutableDictionary alloc] init];
    [deviceInfo setValue:@"7.1.2" forKey:@"osVersion"];
    [deviceInfo setValue:@"Android" forKey:@"os"];
    [deviceInfo setValue:@"Redmi 5 Plus" forKey:@"deviceModel"];
    [deviceInfo setValue:@"com.mitake.android.scb;9.5.1" forKey:@"version"];
    [deviceInfo setValue:@"3573145f0804" forKey:@"udid"];
    
    return deviceInfo;
}

// then AesGcm 解密
+ (NSString *)aesDenCode:(NSString *)denString Aeskey:(NSString *)aeskey{
    
    NSData *key = [NSData iag_dataWithHexString:aeskey];
    NSData *iv = [NSData iag_dataWithHexString:@"000000000000000000000000"];
    NSData *aad = [NSData iag_dataWithHexString:[Request hexFromStr:@"SCBSIT"]];
    
    NSData *strData = [NSData iag_dataWithHexString:denString];
    NSMutableData *data = [NSMutableData data];
    [data appendData:strData];
    // delete
    [data replaceBytesInRange:NSMakeRange(0, data.length-16) withBytes:NULL length:0];
    NSData * authenticationTag = [data copy];
    // delete
    [data replaceBytesInRange:NSMakeRange(0, data.length) withBytes:NULL length:0];
    [data appendData:strData];
    // delete
    [data replaceBytesInRange:NSMakeRange(data.length-16, 16) withBytes:NULL length:0];
    NSData * cipheredData = [data copy];

    // when
    IAGCipheredData *result = [[IAGCipheredData alloc] initWithCipheredData:cipheredData
                                                          authenticationTag:authenticationTag];
     // when
     NSData *plainData = [IAGAesGcm plainDataByAuthenticatedDecryptingCipheredData:result
     withAdditionalAuthenticatedData:aad
     initializationVector:iv
     key:key
     error:nil];
     NSString * convertedStr =[[NSString alloc] initWithData:plainData encoding:NSUTF8StringEncoding];
     //NSLog(@"Converted String 解密 = %@",convertedStr);
     return convertedStr;
}



//AesGcm 加密 String
+ (NSString *)aesGcm:(NSString *)enString AesKey:(NSString *)aeskey{
    
    if (!AES_GCM) {
        return enString;
    }

    NSData *key = [NSData iag_dataWithHexString:aeskey];
    NSData *iv = [NSData iag_dataWithHexString:@"000000000000000000000000"];
    NSData *aad = [NSData iag_dataWithHexString:[Request hexFromStr:@"SCBSIT"]];
    NSData *expectedPlainData = [enString dataUsingEncoding:NSUTF8StringEncoding];
    
    // The returned ciphered data is a simple class with 2 properties: the actual encrypted data and the authentication tag.
    // The authentication tag can have multiple sizes and it is up to you to set one, in this case the size is 128 bits
    // (16 bytes)
    //加密
    IAGCipheredData *cipheredData = [IAGAesGcm cipheredDataByAuthenticatedEncryptingPlainData:expectedPlainData
                                                              withAdditionalAuthenticatedData:aad
                                                                      authenticationTagLength:IAGAuthenticationTagLength128
                                                                         initializationVector:iv
                                                                                          key:key
                                                                                        error:nil];
    
    NSString *trimmedString = [cipheredData description];
    
    trimmedString=[trimmedString stringByReplacingOccurrencesOfString:@"Auth tag" withString:@""];
    trimmedString=[trimmedString stringByReplacingOccurrencesOfString:@"Ciphertext" withString:@""];
    trimmedString=[trimmedString stringByReplacingOccurrencesOfString:@" " withString:@""];
    trimmedString=[trimmedString stringByReplacingOccurrencesOfString:@"<" withString:@""];
    trimmedString=[trimmedString stringByReplacingOccurrencesOfString:@">" withString:@""];
    trimmedString=[trimmedString stringByReplacingOccurrencesOfString:@"." withString:@""];
    trimmedString=[trimmedString stringByReplacingOccurrencesOfString:@":" withString:@""];
    
    //全部大寫
    trimmedString = [trimmedString uppercaseStringWithLocale:[NSLocale currentLocale]];
    //NSLog(@"全部大寫: %@", trimmedString);
    return trimmedString;
}

//string to hexString
+(NSString*)hexFromStr:(NSString*)str
{
    NSData* nsData = [str dataUsingEncoding:NSUTF8StringEncoding];
    const char* data = [nsData bytes];
    NSUInteger len = nsData.length;
    NSMutableString* hex = [NSMutableString string];
    for(int i = 0; i < len; ++i)[hex appendFormat:@"%02X", data[i]];
    return hex;
}

+ (NSData*)hexToBytes:(NSString *)resource {
    NSString* str =[NSString stringWithString:resource];
    NSMutableData* data = [NSMutableData data];
    for (int idx=0; idx+2<=str.length; idx+=2) {
        NSRange range = NSMakeRange(idx, 2);
        NSString* hexStr = [str substringWithRange:range];
        NSScanner* scanner = [NSScanner scannerWithString:hexStr];
        unsigned int value;
        [scanner scanHexInt:&value];
        [data appendBytes:&value length:1];
    }
    return data;
}

+ (NSString*)byteToHex:(NSData *)resource {
    NSMutableString* buffer = [NSMutableString stringWithCapacity:([resource length]*2)];
    const unsigned char* data = [resource bytes];
    for (int idx=0; idx<[resource length]; ++idx) {
        [buffer appendFormat:@"%02lX", (unsigned long)data[idx]];
    }
    return [buffer copy];
}

//存檔-Pliststring
+ (void)writePlist:(NSString*)mydesc key:(NSString*)mykey
{
    [[NSUserDefaults standardUserDefaults] setObject:mydesc forKey:mykey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

//取得getPliststring
+ (NSString*)getPliststring:(NSString*)keyword{
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]stringForKey:keyword];
    
    if (savedValue) {
        return savedValue;
    }else{
        return nil;
    }
}

@end
