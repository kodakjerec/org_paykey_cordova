//
//  NSDictionary+Check.h
//  javaTest
//
//  Created by Carslos Chen on 2019/7/25.
//  Copyright © 2019 Carslos Chen. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDictionary (Check)
#pragma mark - 安全操作
- (BOOL)hasKey:(NSString *)key;
- (NSString*)stringForKey:(id)key;
- (NSNumber*)numberForKey:(id)key;
- (NSArray*)arrayForKey:(id)key;
- (NSDictionary*)dictionaryForKey:(id)key;
- (NSInteger)integerForKey:(id)key;
- (NSUInteger)unsignedIntegerForKey:(id)key;
- (BOOL)boolForKey:(id)key;
@end

NS_ASSUME_NONNULL_END
