#import <Cordova/CDVPlugin.h>

@interface CDVPassbook : CDVPlugin

- (void)avaliable:(CDVInvokedUrlCommand *)command;
- (void)getPasses:(CDVInvokedUrlCommand* )command;
- (void)isCardAdded:(CDVInvokedUrlCommand *)command;
- (void)addPass:(CDVInvokedUrlCommand *)command;
- (void)getEncryptDataApi:(CDVInvokedUrlCommand *)command;
- (void)callback:(CDVInvokedUrlCommand *)command;
- (void)language:(CDVInvokedUrlCommand *)command;

@end
