#import "CDVPassbook.h"
#import <Cordova/CDV.h>
#import <JavaScriptCore/JavaScriptCore.h>
#import <PassKit/PassKit.h>
#import <WatchConnectivity/WatchConnectivity.h>

typedef void (^AddPassResultBlock)(PKPass* pass, BOOL added);

@interface CDVPassbook() <PKAddPassesViewControllerDelegate, PKAddPaymentPassViewControllerDelegate, WCSessionDelegate>

//@property (nonatomic, retain) PKPass *lastPass;
@property (nonatomic, copy) AddPassResultBlock lastAddPassCallback;
@property (nonatomic, strong) NSArray<PKPass *>* passes;

- (BOOL)ensureAvaliability:(CDVInvokedUrlCommand *)command;

- (void)sendPassesResult:(NSArray<PKPass *>*)passes command:(CDVInvokedUrlCommand*)command;

- (void)sendError:(NSError *)error command:(CDVInvokedUrlCommand*)command;

- (void)tryAddPass:(NSString *)ccNo
          custName:(NSString *)custName
             token:(NSString *)token
      cardTypeName:(NSString *)cardTypeName
       pageCheckId:(NSString *)pageCheckId
primaryAccountIdentifier:(NSString *)primaryAccountIdentifier
           success:(AddPassResultBlock)successBlock;

- (UIViewController *) getTopMostViewController;

@end

@implementation CDVPassbook {
    CDVInvokedUrlCommand* lastCommand;
    NSString* pageCheckId;
    NSString* callbackString;
    NSMutableArray* languageJson;  // 系統預設中英字串
    NSString* callbackId_getEncryptDataApi;
}

@synthesize lastAddPassCallback;

+ (BOOL)avaliable {
    BOOL avaliable = [PKAddPaymentPassViewController class];
    if ([PKAddPaymentPassViewController respondsToSelector:@selector(canAddPaymentPass)]) {
        // iPad無法支援 票券，需額外判斷，否則會一律回覆NO，造成信用卡綜覽的信用卡列表無法顯示
        // TODO:check iOS9 iPhone/iPad
        if (![[UIDevice currentDevice].model isEqualToString:@"iPad"]) {
            avaliable = avaliable && [PKAddPassesViewController performSelector:@selector(canAddPasses)];
        }
    }
    return avaliable;
}

- (void)avaliable:(CDVInvokedUrlCommand *)command {
    CDVPluginResult *commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:[CDVPassbook avaliable]];
    [self.commandDelegate sendPluginResult:commandResult callbackId:command.callbackId];
}

- (BOOL)ensureAvaliability:(CDVInvokedUrlCommand *)command {
    if(![CDVPassbook avaliable]) {
        CDVPluginResult *commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_INVALID_ACTION messageAsString:[self getLanguageText:@"passbookAvaliableMsg"]];
        [self.commandDelegate sendPluginResult:commandResult callbackId:command.callbackId];
        return NO;
    }
    return YES;
}

// 原本呼叫的JavaScript
- (void)getEncryptDataApi:(CDVInvokedUrlCommand *)command {
    callbackId_getEncryptDataApi = command.callbackId;
}

// JavaScript完成後的回應
- (void)callback:(CDVInvokedUrlCommand *)command {
    NSString* tmp = [command argumentAtIndex:0][@"resultString"];
    if (tmp.length > 0) {
        callbackString = tmp;
        NSLog(@"encryptData callback: %@", callbackString);
    }
}

- (UIViewController*)getTopMostViewController {
    UIViewController *presentingViewController = [[UIApplication sharedApplication] keyWindow].rootViewController;
    while (presentingViewController.presentedViewController != nil) {
        presentingViewController = presentingViewController.presentedViewController;
    }
    return presentingViewController;
}

- (void)sendPassesResult:(NSArray<PKPass *>*)passes command:(CDVInvokedUrlCommand *)command {
    NSMutableArray* passesArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < passes.count; i++) {
        NSDictionary* tempData = @{@"pass":@{
                                           @"passTypeIdentifier": passes[i].passTypeIdentifier,
                                           @"serialNumber": passes[i].serialNumber
                                           }
                                   };
        [passesArray addObject:tempData];
    }
    CDVPluginResult *commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsArray:passesArray];
    [self.commandDelegate sendPluginResult:commandResult callbackId:command.callbackId];
}

- (void)sendError:(NSError*)error command:(CDVInvokedUrlCommand *)command{
    CDVPluginResult *commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:error.localizedDescription];
    [self.commandDelegate sendPluginResult:commandResult callbackId:command.callbackId];
}

- (void)getPasses:(CDVInvokedUrlCommand *)command {
    if (![self ensureAvaliability:command]) {
        return;
    }
    // get passes list
    PKPassLibrary* lib = [[PKPassLibrary alloc] init];
    self.passes = [lib passesOfType:PKPassTypePayment];
    
    [self sendPassesResult:self.passes command:command];
}

- (void)isCardAdded:(CDVInvokedUrlCommand *)command {
    if (![self ensureAvaliability:command]) {
        return;
    }
    NSString* ccNo = [command argumentAtIndex:0][@"creditCardNo"]; //前端傳入為完整卡號
    if (!ccNo) {
        CDVPluginResult *commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                           messageAsString:[self getLanguageText:@"passbookCcnoAlertMsg"]];
        [self.commandDelegate sendPluginResult:commandResult callbackId:command.callbackId];
        return;
    } else {
        if (ccNo.length > 4) {
            ccNo = [ccNo substringFromIndex:ccNo.length - 4];
        }
    }
    // 檢查此device的卡
    // DeviceType: iPhone // DPAN: DPAN_suffix // Addable: Y / N
    NSString* iPhone_resultStr = [NSString stringWithFormat:@"{\"DeviceType\":\"iPhone\",\"DPAN\":\"\",\"Addable\":\"N\",\"FPAN_ID\":\"\"}"];
    PKPassLibrary* lib = [[PKPassLibrary alloc] init];
    NSArray<PKPaymentPass *>* passesTemp = (NSArray<PKPaymentPass *>*)[lib passesOfType:PKPassTypePayment];
    if (passesTemp.count > 0) {
        for (PKPaymentPass* pass in passesTemp) {
            NSString* DPAN_suffix = pass.paymentPass.deviceAccountNumberSuffix;
            NSString* FPAN_ID = pass.paymentPass.primaryAccountIdentifier;
            NSString* FPAN_suffix = pass.primaryAccountNumberSuffix;
            if ([ccNo isEqualToString:FPAN_suffix]) {   // 只能比對末4碼或5碼
                iPhone_resultStr = [NSString stringWithFormat:@"{\"DeviceType\":\"iPhone\",\"DPAN\":\"%@\",\"Addable\":\"N\",\"FPAN_ID\":\"%@\"}", DPAN_suffix, FPAN_ID];
                break;
            } else {
                iPhone_resultStr = [NSString stringWithFormat:@"{\"DeviceType\":\"iPhone\",\"DPAN\":\"\",\"Addable\":\"Y\",\"FPAN_ID\":\"\"}"];
            }
        }
    } else {
        iPhone_resultStr = [NSString stringWithFormat:@"{\"DeviceType\":\"iPhone\",\"DPAN\":\"\",\"Addable\":\"Y\",\"FPAN_ID\":\"\"}"];
    }
    
    // 檢查遠端device的卡(Apple Watch)
    // DeviceType: Apple Watch // DPAN: DPAN_suffix // Addable: Y / N
    NSString* Watch_resultStr = [NSString stringWithFormat:@"{\"DeviceType\":\"Apple Watch\",\"DPAN\":\"\",\"Addable\":\"0\",\"FPAN_ID\":\"\"}"];
    passesTemp = (NSArray<PKPaymentPass *>*)[lib remotePaymentPasses];
    if (passesTemp.count > 0) {
        for (PKPaymentPass* pass in passesTemp) {
            NSString* DPAN_suffix = pass.paymentPass.deviceAccountNumberSuffix;
            NSString* FPAN_ID = pass.paymentPass.primaryAccountIdentifier;
            NSString* FPAN_suffix = pass.primaryAccountNumberSuffix;
            if ([ccNo isEqualToString:FPAN_suffix]) {   // 只能比對末4碼或5碼
                Watch_resultStr = [NSString stringWithFormat:@"{\"DeviceType\":\"Apple Watch\",\"DPAN\":\"%@\",\"Addable\":\"N\",\"FPAN_ID\":\"%@\"}", DPAN_suffix, FPAN_ID];
                break;
            } else {
                Watch_resultStr = [NSString stringWithFormat:@"{\"DeviceType\":\"Apple Watch\",\"DPAN\":\"\",\"Addable\":\"Y\",\"FPAN_ID\":\"\"}"];
            }
        }
    } else {
        if ([WCSession isSupported]) {
            WCSession* session = [WCSession defaultSession];
            session.delegate = self;
            [session activateSession];
            if (session.isPaired) {  // 有綁定Apple Watch ==> 可以加卡，顯示加卡按鈕
                Watch_resultStr = [NSString stringWithFormat:@"{\"DeviceType\":\"Apple Watch\",\"DPAN\":\"\",\"Addable\":\"Y\",\"FPAN_ID\":\"\"}"];
            } else {    // 沒綁定Apple Watch ==> 讓cordova判斷，不顯示加卡按鈕
                Watch_resultStr = [NSString stringWithFormat:@"{\"DeviceType\":\"Apple Watch\",\"DPAN\":\"\",\"Addable\":\"0\",\"FPAN_ID\":\"\"}"];
            }
        } else {    // 不支援Apple Watch ==> 讓cordova判斷，不顯示加卡按鈕
            Watch_resultStr = [NSString stringWithFormat:@"{\"DeviceType\":\"Apple Watch\",\"DPAN\":\"\",\"Addable\":\"0\",\"FPAN_ID\":\"\"}"];
        }
    }
    
    NSString* final_result = [NSString stringWithFormat:@"{\"Items\":[%@, %@]}", iPhone_resultStr, Watch_resultStr];
    
    CDVPluginResult *commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:final_result];
    [self.commandDelegate sendPluginResult:commandResult callbackId:command.callbackId];
}

- (void)addPass:(CDVInvokedUrlCommand *)command {
    
    if (![self ensureAvaliability:command]) {
        return;
    }
    //  exec(successCallback, errorCallback, "passbook", "addPass", [{creditCardNo: creditCardNo, custEngName: custEngName, token: token, pageCheckId: pageCheckId}]);
    NSString* ccNo = [command argumentAtIndex:0][@"creditCardNo"];
    if (!ccNo) {
        CDVPluginResult *commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:[self getLanguageText:@"passbookCcnoAlertMsg"]];
        [self.commandDelegate sendPluginResult:commandResult callbackId:command.callbackId];
        return;
    }
    
    NSString* custName = [command argumentAtIndex:0][@"custEngName"];
    if (!custName) {
        CDVPluginResult *commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:[self getLanguageText:@"passbookCustNameAlertMsg"]];
        [self.commandDelegate sendPluginResult:commandResult callbackId:command.callbackId];
        return;
    }
    
    NSString* token = @"";  // [command argumentAtIndex:2];
    //    if (!token) {
    //        CDVPluginResult *commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"The necessary information(token) was not provided"];
    //        [self.commandDelegate sendPluginResult:commandResult callbackId:command.callbackId];
    //        return;
    //    }
    
    NSString* cardTypeName = [command argumentAtIndex:0][@"cardTypeName"];
    //    if (!cardTypeName) {
    //        CDVPluginResult *commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"The necessary information(cardTypeName) was not provided"];
    //        [self.commandDelegate sendPluginResult:commandResult callbackId:command.callbackId];
    //        return;
    //    }
    
    pageCheckId = [command argumentAtIndex:0][@"pageCheckId"];
    if (!pageCheckId) {
        CDVPluginResult *commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                           messageAsString:[self getLanguageText:@"passbookPageCheckIdAlertMsg"]];
        [self.commandDelegate sendPluginResult:commandResult callbackId:command.callbackId];
        return;
    }
    
    NSString* primaryAccountIdentifier = [command argumentAtIndex:0][@"FPAN_ID"];
    
    lastCommand = command;
    
    [self tryAddPass:ccNo
            custName:custName
               token:token
        cardTypeName:cardTypeName
         pageCheckId:pageCheckId
primaryAccountIdentifier:primaryAccountIdentifier
             success:^(PKPass* pass, BOOL added) {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSMutableArray<PKPass *>* passes = [[NSMutableArray alloc] init];
                     [passes addObject:pass];
                     [self sendPassesResult:passes command:command];
                 });
             }];
}

- (void)tryAddPass:(NSString *)ccNo
          custName:(NSString *)custName
             token:(NSString *)token
      cardTypeName:(NSString *)cardTypeName
       pageCheckId:(NSString *)pageCheckId
primaryAccountIdentifier:(NSString *)primaryAccountId
           success:(AddPassResultBlock)successBlock {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.lastAddPassCallback = successBlock;
        
        // provided ECC & RSA encryption
        PKAddPaymentPassRequestConfiguration* configuration = [[PKAddPaymentPassRequestConfiguration alloc] initWithEncryptionScheme:PKEncryptionSchemeECC_V2];
        configuration.cardholderName =  custName;
        
        // configuration.primaryAccountSuffix = ccNo;  //末四或五位卡號，必須是真的卡號，隨機的卡號會造成 PKAddPaymentPassViewController 無法跳出來
        //正式用的
        NSArray* ccNoSplit = [ccNo componentsSeparatedByString:@"-"];
        configuration.primaryAccountSuffix = ccNoSplit[ccNoSplit.count - 1];
        configuration.localizedDescription = cardTypeName;
        //這一段看是否需要顯示正確卡面？如要就要將註解拿掉
        //        NSString* panFirstChar = [primaryAccountId substringToIndex:1];
        //        NSString* paymentNetwork = nil;
        //
        //        if ([panFirstChar isEqualToString:@"4"]) {
        //            paymentNetwork = PKPaymentNetworkVisa;
        //        } else if ([panFirstChar isEqualToString:@"5"]) {
        //            paymentNetwork = PKPaymentNetworkMasterCard;
        //        }
        //        configuration.paymentNetwork = paymentNetwork;
        
        if (primaryAccountId != nil && primaryAccountId.length > 0) {
            configuration.primaryAccountIdentifier = primaryAccountId;
        } else {
            configuration.primaryAccountIdentifier = @"";
        }
        
        PKAddPaymentPassViewController* addPaymentPassVC = [[PKAddPaymentPassViewController alloc] initWithRequestConfiguration:configuration delegate:self];
        addPaymentPassVC.delegate = self;
        [self.viewController presentViewController:addPaymentPassVC animated:YES completion:nil];
    });
}

- (void)addPaymentPassViewController:(PKAddPaymentPassViewController *)controller
 generateRequestWithCertificateChain:(NSArray<NSData *> *)certificates
                               nonce:(NSData *)nonce
                      nonceSignature:(NSData *)nonceSignature
                   completionHandler:(void (^)(PKAddPaymentPassRequest * _Nonnull))handler {
    
    /**
     nonce: The apple server generates a random number that can only be used once. Encrypted data must be included in addPaymentRequest
     nonceSignature: Device related nonce signature. Also must include encrypted data in addPaymentRequest
     */
    NSLog(@"delegate processing...");
    NSLog(@"pass nonce and nonceSignature to Issue server, to get activation data, payment pass data, and public key");
    
    NSString* strNonce = [self hexStringFromData:nonce];
    NSString* strNonceSignature = [self hexStringFromData:nonceSignature];
    NSMutableArray<NSString*>* arrCertificates = [[NSMutableArray alloc] init];
    for (int i = 0; i < certificates.count; i++) {
        //NSString* certTemp = [[NSString alloc] initWithData:certificates[i] encoding:NSUTF8StringEncoding];
        NSString* certTemp = [self hexStringFromData:[certificates objectAtIndex:i]];
        [arrCertificates addObject:certTemp];
    }
    
    NSDictionary* object = @{@"nonce":strNonce,@"nonceSignature":strNonceSignature,@"arrCertificates":(NSArray*)arrCertificates,@"pageCheckId":pageCheckId};
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                  messageAsDictionary:object];
    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId_getEncryptDataApi];
    
    // 改為背景執行緒
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        BOOL jsHasReturn = NO;
        int notReturnTimer = 0;
        while(!jsHasReturn){
            if(callbackString.length>0 || notReturnTimer>=20){
                jsHasReturn = YES;
            }
            notReturnTimer++;
            sleep(1);
        }
        
        NSLog(@"callbackString：%@", callbackString);
        if (callbackString.length > 0) {
            // header: code, message
            NSArray* stringArray = [callbackString componentsSeparatedByString: @"|"];
            
            callbackString = @"";
            // get body
            if ([stringArray[0] isEqualToString:@"0000"]) {
                /**
                 activationData: OTP(one-time pad), should be verified by the issuer and/or payment network. it's content, contact your payment network.
                 encryptedPassData: An encrypted JSON file containing sensitive information about the card that will be added to Apple Pay. This JSON file must contain the following:
                 primaryAccountNumber: The card's full master account number. There can only be numbers. Only the last 4 or 5 digits of the account are required.
                 expiration: The expiration date of the card. For example "11/20".
                 name: The name of the card holder.
                 nonce: Hexadecimal string with a nonce value. Take the argument nonce of this delegate method.
                 nonceSignature: The hexadecimal string for the nonceSignature value. Take the input argument nonceSignature of this delegate method.
                 
                 ["primaryAccountNumber":"","expiration":"","name":"","nonce":"","nonceSignature":""]
                 
                 Important
                 Do not encrypt the card data on the device. You should encrypt the data on your server, and then pass the encrypted data to the device.
                 ephemeralPublicKey: Short life cycle public key. Used when using ECC encryption algorithm. Base64 encoding is required.
                 
                 **This request needs to be passed to the handler within 20 seconds of creation, otherwise the request will fail and the system will display the error message to the client.**
                 */
                PKAddPaymentPassRequest* addRequest = [[PKAddPaymentPassRequest alloc] init];
                
                // 使用hex方法1
                addRequest.activationData = [stringArray[1] dataUsingEncoding:NSUTF8StringEncoding];
                addRequest.encryptedPassData = [self dataFromHexString:stringArray[2]];
                addRequest.ephemeralPublicKey = [self dataFromHexString:stringArray[3]];
                
                if (addRequest == nil) {
                    CDVPluginResult *commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:[self getLanguageText:@"passbookPKAddPaymentPassRequestAlertMsg"]];
                    [self.commandDelegate sendPluginResult:commandResult callbackId:lastCommand.callbackId];
                    return;
                }
                if (handler) {
                    handler(addRequest);
                }
            } else {
                CDVPluginResult* commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:[self getLanguageText:@"passbookAppleEncryptDateAlertMsg"]];
                [self.commandDelegate sendPluginResult:commandResult callbackId:lastCommand.callbackId];
                return;
            }
        } else {
            CDVPluginResult* commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:[self getLanguageText:@"passbookAppleEncryptDateAlertMsg"]];
            [self.commandDelegate sendPluginResult:commandResult callbackId:lastCommand.callbackId];
            return;
        }
    });
}

- (NSString *)hexStringFromData:(NSData *)data {
    const unsigned char *dataBytes = [data bytes];
    NSMutableString *ret = [NSMutableString stringWithCapacity:[data length] * 2];
    for (int i=0; i<[data length]; ++i)
        [ret appendFormat:@"%02lx", (unsigned long)dataBytes[i]];
    return ret;
    
}

// hex方法1
- (NSData *)dataWithHexString:(NSString *)hexString {
    NSString *newStr = [hexString stringByReplacingOccurrencesOfString:@" " withString:@""];//去掉空格
    NSString *replaceString = [newStr substringWithRange:NSMakeRange(1, newStr.length-2)];//去掉<>符号
    const char *hexChar = [replaceString UTF8String];//转换为 char 字符串
    Byte *bt = malloc(sizeof(Byte)*(replaceString.length/2));// 开辟空间 用来存放  转换后的byte
    char tmpChar[3] = {'\0','\0','\0'};
    int btIndex = 0;
    for (int i=0; i<replaceString.length; i += 2) {
        tmpChar[0] = hexChar[i];
        tmpChar[1] = hexChar[i+1];
        bt[btIndex] = strtoul(tmpChar, NULL, 16);// 将 hexstring 转换为 byte 的c方法   16 为16进制
        
        btIndex ++;
    }
    NSData *data = [NSData dataWithBytes:bt length:btIndex]; //创建 nsdata 对象
    
    free(bt);//释放空间
    
    return data;
}

// hex方法2
- (NSData *)dataFromHexString:(NSString *)string {
    string = [string lowercaseString];
    NSMutableData *data= [NSMutableData new];
    unsigned char whole_byte;
    char byte_chars[3] = {'\0','\0','\0'};
    int i = 0;
    int length = string.length;
    while (i < length-1) {
        char c = [string characterAtIndex:i++];
        if (c < '0' || (c > '9' && c < 'a') || c > 'f')
            continue;
        byte_chars[0] = c;
        byte_chars[1] = [string characterAtIndex:i++];
        whole_byte = strtol(byte_chars, NULL, 16);
        [data appendBytes:&whole_byte length:1];
    }
    return data;
}

- (NSData *)decodeBase64:(NSString *)str {
    NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:str options:0];
    return decodedData;
}

- (void)addPaymentPassViewController:(nonnull PKAddPaymentPassViewController *)controller
          didFinishAddingPaymentPass:(nullable PKPaymentPass *)pass
                               error:(nullable NSError *)error {
    NSString* errorMessage = @"";
    
    if (pass) {
        if (self.lastAddPassCallback && pass) {
            BOOL passAdded =[[[PKPassLibrary alloc] init] containsPass:pass];
            self.lastAddPassCallback(pass, passAdded);
            self.lastAddPassCallback = nil;
        }
        [controller dismissViewControllerAnimated:YES
                                       completion:^{
                                           CDVPluginResult *commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"OK"];
                                           [self.commandDelegate sendPluginResult:commandResult callbackId:lastCommand.callbackId];
                                           //                                           [self.viewController dismissViewControllerAnimated:YES completion:nil];
                                       }];
    } else {
        if (error.code == PKAddPaymentPassErrorUnsupported) {
            errorMessage = [self getLanguageText:@"passbookWalletNotSupport"];
        } else if (error.code == PKAddPaymentPassErrorUserCancelled) {
            errorMessage = [self getLanguageText:@"passbookUserCancel"];
        } else if (error.code == PKAddPaymentPassErrorSystemCancelled) {
            errorMessage = [self getLanguageText:@"passbookSystemCancel"];
        } else {
            errorMessage = [self getLanguageText:@"passbookSystemErrorOccu"];
        }
        [controller dismissViewControllerAnimated:YES
                                       completion:^{
                                           CDVPluginResult *commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:errorMessage];
                                           [self.commandDelegate sendPluginResult:commandResult callbackId:lastCommand.callbackId];
                                           //                                           [self.viewController dismissViewControllerAnimated:YES completion:nil];
                                       }];
    }
}

// 設定語言
- (void)language:(CDVInvokedUrlCommand *)command {
    languageJson = [command.arguments objectAtIndex:0];
}
// 取得對應的文字
- (NSString*)getLanguageText:(NSString*)key {
    if([languageJson valueForKey:key]){
        return [languageJson valueForKey:key];
    } else {
        return key;
    }
}

#pragma mark WCSessionDelegate

- (void)session:(WCSession *)session activationDidCompleteWithState:(WCSessionActivationState)activationState error:(nullable NSError *)error __IOS_AVAILABLE(9.3) __WATCHOS_AVAILABLE(2.2){
    
}

- (void)sessionDidBecomeInactive:(WCSession *)session __IOS_AVAILABLE(9.3) __WATCHOS_UNAVAILABLE{
    
}

- (void)sessionDidDeactivate:(WCSession *)session __IOS_AVAILABLE(9.3) __WATCHOS_UNAVAILABLE{
    
}

@end
