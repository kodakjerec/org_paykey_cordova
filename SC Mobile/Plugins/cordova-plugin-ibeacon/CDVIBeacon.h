#import <Cordova/CDVPlugin.h>
#import "AppDelegate.h"

@interface CDVIBeacon : CDVPlugin <CLLocationManagerDelegate, UIApplicationDelegate>

- (void)avaliable:(CDVInvokedUrlCommand *)command;
- (void)changeSwitch:(CDVInvokedUrlCommand *)command;
- (void)keepReservationFlag:(CDVInvokedUrlCommand *)command;
- (void)getIBeaconSignal:(CDVInvokedUrlCommand *)command;
- (void)resetIBeaconSignal:(CDVInvokedUrlCommand *)command;
- (void)setIBeaconService:(CDVInvokedUrlCommand*)command;
- (void)pushLocalNotificaion:(CDVInvokedUrlCommand*)command;
- (void)hasCallPullData:(CDVInvokedUrlCommand*)command;
- (void)findIbeacon:(NSString *)uuid andMajor:(NSString *)major andMinor:(NSString *)minor;
- (void)getIbeaconSignalCallback;
- (void)language:(CDVInvokedUrlCommand *)command;

- (void)initIBeacon:(NSString *)uuid;
- (void)stopRegin;
- (void)startRegion;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLBeaconRegion *beaconRegion;
@property (strong, nonatomic) NSString* regionUUId;

@end
