//
//  Appdelegate+ibeacon.m
//  SC Mobile
//
//  Created by 簡克達 on 2018/10/12.
//

#import "AppDelegate+ibeacon.h"
#import "CDVIBeacon.h"

@implementation AppDelegate (ibeacon)

+ (void)load
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        Class class = [self class];
        
        SEL originalInitSelector = @selector(init);
        SEL swizzledInitSelector = @selector(ibeaconSwizzledInit);
        
        Method originalInit = class_getInstanceMethod(class, originalInitSelector);
        Method swizzledInit = class_getInstanceMethod(class, swizzledInitSelector);
        
        BOOL didAddInitMethod =
        class_addMethod(class,
                        originalInitSelector,
                        method_getImplementation(swizzledInit),
                        method_getTypeEncoding(swizzledInit));
        
        if (didAddInitMethod) {
            class_replaceMethod(class,
                                swizzledInitSelector,
                                method_getImplementation(originalInit),
                                method_getTypeEncoding(originalInit));
        } else {
            method_exchangeImplementations(originalInit, swizzledInit);
        }
        
    });
}

- (AppDelegate *)ibeaconSwizzledInit
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(ibeacon_whenAppIsKilled:)
                                                 name:UIApplicationDidFinishLaunchingNotification
                                               object:nil];
    
    return [self ibeaconSwizzledInit];
}

- (void)ibeacon_whenAppIsKilled:(NSNotification*)notification{

    NSLog(@"ibeacon didFinishLaunchingWithOptions");
    if(notification){
        NSDictionary* launchOptions = [notification userInfo];
        NSMutableArray* localNotification = [launchOptions objectForKey: @"UIApplicationLaunchOptionsLocalNotificationKey"];
        NSDictionary* userInfo = [localNotification valueForKey:@"userInfo"];
        // 按下通知的時候, app is killed
        NSString *value = [userInfo objectForKey:@"Type"];
        if ([value isEqualToString:@"iBeacon"])
        {
            CDVIBeacon* beacon = [[CDVIBeacon alloc] init];
            [beacon getIbeaconSignalCallback];
        }
    }
}

@end
