#import "CDVIBeacon.h"
#import <Cordova/CDV.h>
#import <CoreLocation/CoreLocation.h>

@interface CDVIBeacon()

@property NSUUID* uuid;
@property NSNumber* major;
@property NSNumber* minor;

@end

@implementation CDVIBeacon {
    NSMutableArray* languageJson;  // 系統預設中英字串
    NSString *callbackId_setIBeaconService;
    NSString *callbackId_getIBeaconSignal;
    int intvalSecond;    // 設定iBeacon間隔秒數
    int secondCounter;  // beacon掃描秒數
    int secondBeginNotReceiveSignal; // 從第幾秒開始, 沒有接收到指定beacon訊號
}

- (BOOL)avaliable {
    BOOL avaliable = NO;
    if ([CLLocationManager locationServicesEnabled]
        && [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied
        && [CLLocationManager isRangingAvailable]) {
        avaliable = YES;
    }
    return avaliable;
}

- (void)avaliable:(CDVInvokedUrlCommand *)command {
    CDVPluginResult* commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:[self avaliable]];
    [self.commandDelegate sendPluginResult:commandResult callbackId:command.callbackId];
}

- (void)setIBeaconService:(CDVInvokedUrlCommand*)command {
    callbackId_setIBeaconService = command.callbackId;

    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSString* iBeaconStatus = [userDefaults stringForKey:@"startIBeacon"];
    if (iBeaconStatus) {
        if ([iBeaconStatus isEqualToString:@"Y"]) {
            NSString* iBeaconUUID = [userDefaults stringForKey:@"requestUuid"];
            if (iBeaconUUID) {
                [self initIBeacon:iBeaconUUID];
                [self startRegion];
            }
        }
    }
}

- (void)getIBeaconSignal:(CDVInvokedUrlCommand *)command {
    callbackId_getIBeaconSignal = command.callbackId;

    // 嘗試呼叫, 是否曾經有在app killed 時候按下通知
    [self getIbeaconSignalCallback];
}

- (void)pushLocalNotificaion:(CDVInvokedUrlCommand*)command {
    NSString* uuid = [command argumentAtIndex:0][@"uuid"];
    NSString* major = [command argumentAtIndex:0][@"major"];
    NSString* minor = [command argumentAtIndex:0][@"minor"];
    NSString* custType = [command argumentAtIndex:0][@"custType"];
    NSString* message = [self getLanguageText:@"iBeaconNotification1"];
    if ([custType isEqualToString:@"1"]) {
        message = [self getLanguageText:@"iBeaconNotification2"];
    }

    // 確定有送出通知, 才儲存iBeaconData
    NSString* jsonStr = [NSString stringWithFormat:@"{\"iBeaconNotify\":\"Y\",\"uuid\":\"%@\",\"major\":\"%@\",\"minor\":\"%@\"}", uuid, major, minor];
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:jsonStr forKey:@"iBeaconData"];

    [self showLocationNotificationWithMessage:message andUUId:uuid andMajor:major andMinor:minor];
}

- (void)showLocationNotificationWithMessage:(NSString *)message andUUId:(NSString *)uuid andMajor:(NSString *)major andMinor:(NSString *)minor {
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        UILocalNotification *notification = [UILocalNotification new];
        notification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
        notification.alertBody = message;
        notification.userInfo = @{ @"Type": @"iBeacon", @"uuid": [NSString stringWithFormat:@"%@", uuid], @"major": [NSString stringWithFormat:@"%@", major], @"minor": [NSString stringWithFormat:@"%@", minor] };
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    });
}

- (void)changeSwitch:(CDVInvokedUrlCommand *)command {
    NSString* switchStatus = [command argumentAtIndex:0][@"switchStatus"];
    if (!switchStatus) {
        CDVPluginResult* commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                           messageAsString:[self getLanguageText:@"iBeaconSwitchAlertMsg"]];
        [self.commandDelegate sendPluginResult:commandResult callbackId:command.callbackId];
        return;
    }
    NSString* uuid = [command argumentAtIndex:0][@"uuid"];
    if (!uuid) {
        CDVPluginResult* commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:[self getLanguageText:@"iBeaconUuidAlertMsg"]];
        [self.commandDelegate sendPluginResult:commandResult callbackId:command.callbackId];
        return;
    }
    // set default value
    [self initIBeacon:uuid];
    
    if ([switchStatus isEqualToString:@"Y"]) {
        [self startRegion];
    } else {
        NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:@"N" forKey:@"keepReservation"];
        [userDefaults setObject:@"" forKey:@"iBeaconData"];
        [userDefaults setObject:@"N" forKey:@"hasCallPullData"];
        [userDefaults setObject:@"N" forKey:@"keepReservation"];
        [self stopRegin];
    }
    CDVPluginResult* commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:true];
    [self.commandDelegate sendPluginResult:commandResult callbackId:command.callbackId];
}

- (void)keepReservationFlag:(CDVInvokedUrlCommand *)command {
    NSString* flag = [command argumentAtIndex:0][@"flag"];
    if (!flag) {
        CDVPluginResult* commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:[self getLanguageText:@"iBeaconFlagAlertMsg"]];
        [self.commandDelegate sendPluginResult:commandResult callbackId:command.callbackId];
        return;
    }
    
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    if ([flag isEqualToString:@"Y"]) {  // Keep
        [userDefaults setObject:@"Y" forKey:@"keepReservation"];
    } else {
        [userDefaults setObject:@"N" forKey:@"keepReservation"];
    }
    
    CDVPluginResult* commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"OK"];
    [self.commandDelegate sendPluginResult:commandResult callbackId:command.callbackId];
}

// 告知ibeacon, 前端已經發出推播, 不用再發送訊息
- (void)hasCallPullData:(CDVInvokedUrlCommand*)command {
    NSLog(@"call pullData 回調 hasCallPullData");
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    // has called pullData
    [userDefaults setObject:@"Y" forKey:@"hasCallPullData"];
    [userDefaults synchronize];
    
    CDVPluginResult* commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"OK"];
    [self.commandDelegate sendPluginResult:commandResult callbackId:command.callbackId];
}

// 找到ibeacon, 通知前端
- (void)findIbeacon:(NSString *)uuid andMajor:(NSString *)major andMinor:(NSString *)minor {
    NSLog(@"Native iOS findIBeacon");
    NSString* jsonStr = [NSString stringWithFormat:@"{\"IbeaconNotify\":\"Y\",\"uuid\":\"%@\",\"major\":\"%@\",\"minor\":\"%@\"}", uuid, major, minor];
    
    CDVPluginResult* commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:jsonStr];
    [commandResult setKeepCallbackAsBool:YES];
    [self.commandDelegate sendPluginResult:commandResult callbackId:callbackId_setIBeaconService];
}

// 按下通知後, 換頁
- (void)getIbeaconSignalCallback {
    NSLog(@"Native iOS getIbeaconSignalCallback");
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSString* ibeaconData = [userDefaults valueForKey:@"iBeaconData"];

    if ([ibeaconData isEqualToString:@""])
        return;
    
    CDVPluginResult* commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:ibeaconData];
    [commandResult setKeepCallbackAsBool:YES];
    [self.commandDelegate sendPluginResult:commandResult callbackId:callbackId_getIBeaconSignal];
}

// RESET
- (void)resetIBeaconSignal:(CDVInvokedUrlCommand *)command {
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:@"" forKey:@"iBeaconData"];
    [userDefaults synchronize];
    CDVPluginResult* commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"OK"];
    [self.commandDelegate sendPluginResult:commandResult callbackId:command.callbackId];
}

// 設定語言
- (void)language:(CDVInvokedUrlCommand *)command {
    languageJson = [command.arguments objectAtIndex:0];
}
// 取得對應的文字
- (NSString*)getLanguageText:(NSString*)key {
    if([languageJson valueForKey:key]){
        return [languageJson valueForKey:key];
    } else {
        return key;
    }
}

// #region ibeacon ON/OFF
- (void)initIBeacon:(NSString *)uuid {
    intvalSecond = 3;   // 幾秒搜尋一次
    secondCounter = 1;  // 分母
    secondBeginNotReceiveSignal = 0;    // 幾秒沒找到iBeacon訊號
    
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:uuid forKey:@"requestUuid"];
    
    self.locationManager = [CLLocationManager new];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    
    [self.locationManager requestAlwaysAuthorization];
    if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [self.locationManager requestAlwaysAuthorization];
    }
    // Prepare BeaconRegions
    self.regionUUId = uuid; // @"23542266-18D1-4FE4-B4A1-23F8195B9D39"; // just for test //uuid
    NSUUID* beaconUUID = [[NSUUID alloc] initWithUUIDString:self.regionUUId];
    self.beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:beaconUUID identifier:@"iBeacon"];
    self.beaconRegion.notifyOnEntry = YES;
    self.beaconRegion.notifyOnExit = YES;
    self.beaconRegion.notifyEntryStateOnDisplay = YES;
}

- (void)stopRegin {
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:@"N" forKey:@"startIBeacon"];
    [userDefaults setObject:@"" forKey:@"iBeaconData"];
    [self.locationManager stopMonitoringForRegion:self.beaconRegion];
    [self.locationManager stopRangingBeaconsInRegion:self.beaconRegion];
}

- (void)startRegion {
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:@"Y" forKey:@"startIBeacon"];
    [self.locationManager startMonitoringForRegion:self.beaconRegion];
    [self.locationManager startRangingBeaconsInRegion:self.beaconRegion];
}
// #endregion ibeacon ON/OFF

// #region ibeacon 運作

- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region {
    [self.locationManager requestStateForRegion:region];
    
}

// app權限="限app使用", 會無法觸發. 另外靠自訂規則假裝有離開範圍
// app權限="限app使用", 呼叫了 stopRangingBeaconsInRegion 會導致 beacon 永遠不會醒來.
- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region {
    if (state == CLRegionStateInside) {
        NSLog(@"ibeacon Inside");
        [self.locationManager startRangingBeaconsInRegion:(CLBeaconRegion*)region];
    } else {
        NSLog(@"ibeacon Outside");
        [self exitRegion];
        [self.locationManager stopRangingBeaconsInRegion:(CLBeaconRegion*)region];
    }
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region{
    NSLog(@"ibeacon didEnterRegion");
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region{
    NSLog(@"ibeacon didExitRegion");
}
// 離開範圍, 要清除已接收資料的flag
- (void) exitRegion{
    NSLog(@"beacon exitRegion");
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:@"N" forKey:@"keepReservation"];
    [userDefaults setObject:@"N" forKey:@"hasCallPullData"];
}

- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region {
    if ((secondCounter % intvalSecond) == 0 && (secondCounter > 0)) {
        NSLog(@"掃描第%d次", secondCounter);
        NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
        if (beacons.count > 0) {
            // 收到訊號了, reset timer
            secondBeginNotReceiveSignal = 0;
            
            if ([region isEqual:self.beaconRegion]) {
                NSString* hasCallPullData = [userDefaults stringForKey:@"hasCallPullData"];
                NSLog(@"hasCallPullData: %@", hasCallPullData);
                if (![hasCallPullData isEqualToString:@"Y"]) {
                    
                    // 只有指定的uuid才會發送推播
                    // initIBeacon已經指定uuid
                    CLBeacon* aBeacon = (CLBeacon *)beacons.firstObject; //[beacons objectAtIndex:0];
                    NSString* uuid = [aBeacon.proximityUUID UUIDString];
                    NSString* major = [NSString stringWithFormat:@"%@", aBeacon.major];
                    NSString* minor = [NSString stringWithFormat:@"%@", aBeacon.minor];
                    
                    if (!(aBeacon.proximity == CLProximityUnknown)) {
                        NSLog(@"! CLProximityUnknown");
                        NSString* keepReservation = [userDefaults stringForKey:@"keepReservation"];
                        if (![keepReservation isEqualToString:@"Y"]) { // 使用者選擇不進分行辦理，所以不再發出local notification，直到離開這家分行的範圍([userDefaults setObject:@"N" forKey:@"keepReservation"])
                            NSLog(@"before call pullData");
                            [self findIbeacon:uuid andMajor:major andMinor:minor];
                            NSLog(@"after call pullData");
                        }
                    } else {
                        [userDefaults setObject:@"N" forKey:@"keepReservation"];
                    }
                } else {
                    [userDefaults setObject:@"N" forKey:@"keepReservation"];
                }
            }
        } else {
            // 沒收到指定beacon訊號, 超過60秒自動視為離開範圍
            if (secondBeginNotReceiveSignal ==  0) {
                secondBeginNotReceiveSignal = secondCounter;
            } else {
                int passingTimer = secondCounter-secondBeginNotReceiveSignal;
                NSLog(@"secondBeginNotReceiveSignal 經過時間: %d", passingTimer);
                // 遵循 apple定義: 30秒沒收到訊號, 視為離開範圍
                // 會重複觸發, 於是再增設60秒以後不再觸發
                if( passingTimer >= 30 && passingTimer<=60){
                    [self exitRegion];
                }
            }
        }
    }
    secondCounter++;
}
// #endregion ibeacon 運作

@end
