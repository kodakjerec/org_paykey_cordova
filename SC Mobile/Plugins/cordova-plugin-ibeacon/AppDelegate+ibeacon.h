//
//  AppDelegate+ibeacon.h
//  SC Mobile
//
//  Created by 簡克達 on 2018/10/9.
//

#import "AppDelegate.h"
#import <objc/runtime.h>
#import <UserNotifications/UserNotifications.h>

@interface AppDelegate (ibeacon) <UNUserNotificationCenterDelegate>

@end
