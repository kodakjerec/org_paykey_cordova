/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 */

//
//  AppDelegate.m
//  SC Mobile
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright ___ORGANIZATIONNAME___ ___YEAR___. All rights reserved.
//

#import "AppDelegate.h"
#import <Cordova/CDVPlugin.h>
#import "MainViewController.h"
#import <JavaScriptCore/JavaScriptCore.h>
#import "SAMKeychain.h"
#import "Request.h"
#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
@import KeyboardFramework;

@interface AppDelegate ()

@property (nonatomic, strong) PKSettingsNavigationController* settingsNavigationController;

@end

@implementation AppDelegate

- (void)applicationDidEnterBackground:(UIApplication *)application {
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.window.bounds];
    imageView.tag = 101;
    imageView.backgroundColor = [UIColor blackColor];
    [UIApplication.sharedApplication.keyWindow.subviews.lastObject addSubview:imageView];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    UIImageView *imageView = (UIImageView *)[UIApplication.sharedApplication.keyWindow.subviews.lastObject viewWithTag:101];
    [imageView removeFromSuperview];
}


- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions {
    self.viewController = [[MainViewController alloc] init];

    if ([WCSession isSupported]) {
        WCSession* session = [WCSession defaultSession];
        session.delegate = self;
        [session activateSession];
    }
    
    return [super application:application didFinishLaunchingWithOptions:launchOptions];
}

#pragma mark WCSessionDelegate

- (void)session:(WCSession *)session activationDidCompleteWithState:(WCSessionActivationState)activationState error:(nullable NSError *)error __IOS_AVAILABLE(9.3) __WATCHOS_AVAILABLE(2.2){
}

- (void)sessionDidBecomeInactive:(WCSession *)session __IOS_AVAILABLE(9.3) __WATCHOS_UNAVAILABLE{
    
}

- (void)sessionDidDeactivate:(WCSession *)session __IOS_AVAILABLE(9.3) __WATCHOS_UNAVAILABLE{
    
}

// 讓SC Mobile 和 WebView 全部都是直立式
- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{
    return UIInterfaceOrientationMaskPortrait|UIInterfaceOrientationMaskPortraitUpsideDown;
}

@end

@implementation NSURLRequest(DataController)
+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host
{
    return YES;
}

-(BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    return [self handleUrl:url];
}

-(BOOL)handleUrl:(NSURL*) url {
    if (url.host == nil) {
        return true;
    }
    
//    if ([url.host containsString:@"settings"]) {
//        self.settingsNavigationController = [PKSettingsNavigationController new];
//        self.settingsNavigationController.rightItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissSettingsView)];
//        self.settingsNavigationController.shouldShowItemForFirstScreen = true;
//        
//        if (self.window) {
//            UIViewController* topViewController = [self topViewController];
//            if (topViewController) {
//                [topViewController presentViewController:_settingsNavigationController animated:true completion:nil];
//            }
//        }
//    }
    return true;
}


-(void)dismissSettingsView {
//    if (self.window) {
//        UIViewController* topViewController = [self topViewController];
//        if (topViewController) {
//            [topViewController dismissViewControllerAnimated:YES completion:nil];
//        }
//    }
}


- (UIViewController *)topViewController{
    return [self topViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController *)topViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil) {
        return rootViewController;
    }
    
    if ([rootViewController.presentedViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        return [self topViewController:lastViewController];
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    return [self topViewController:presentedViewController];
}

@end
